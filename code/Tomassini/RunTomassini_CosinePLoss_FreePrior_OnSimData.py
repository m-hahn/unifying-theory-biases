import getObservations
import glob
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sys
import torch
from cosineEstimator6 import CosineEstimator
from getObservations import retrieveObservations
from loadTomassini import *
from matplotlib import rc
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import makeGridIndicesCircular
from util import product
from util import savePlot

__file__ = __file__.split("/")[-1]
rc('font', **{'family':'FreeSans'})

OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
FOLD_HERE = int(sys.argv[2])
REG_WEIGHT = float(sys.argv[3])
GRID = int(sys.argv[4])
SHOW_PLOT = (len(sys.argv) < 6) or (sys.argv[5] == "SHOW_PLOT")
DEVICE = 'cuda'
FIT = f"SimulateTomassini_FreePrior.py_8_180.txt"

FILE = f"logs/SIMULATED_REPLICATE/fits/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"
if os.path.exists(FILE):
   quit()
##############################################
# Helper Functions dependent on the device

##############################################
with open(f"logs/SIMULATED_REPLICATE/{FIT}", "r") as inFile:
  data = [z.split(" ") for z in inFile.read().strip().split("=======\n")[1].split("\n")         ]

Duration__, StimulusSD__, sample__, responses__ = zip(*data)
StimulusSD__ = MakeLongTensor([int(q) for q in StimulusSD__])
Duration__ = MakeLongTensor([int(q) for q in Duration__])
assert (Duration__ == Duration).all()
print(StimulusSD__)
print(StimulusSD)
assert (StimulusSD__ == StimulusSD).all()
sample = MakeFloatTensor([float(q) for q in sample__])
responses = MakeFloatTensor([float(q) for q in responses__])
# Store observations
observations_x = sample
observations_y = responses

# Assign folds
N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

Fold = 0*Subject
for i in range(min(Subject), max(Subject)+1):
    trials = [j for j in range(Subject.size()[0]) if Subject[j] == i]
    randomGenerator.shuffle(trials)
    foldSize = int(len(trials)/N_FOLDS)
    for k in range(N_FOLDS):
        Fold[trials[k*foldSize:(k+1)*foldSize]] = k

##############################################
# Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 360

INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
grid, grid_indices_here = makeGridIndicesCircular(GRID, MIN_GRID, MAX_GRID)

# Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

#############################################################
# Part: Initialize the model
init_parameters = {}
init_parameters["sigma2_stimulus"] = MakeFloatTensor([0,0]).view(2)
init_parameters["log_motor_var"] = MakeFloatTensor([8]).view(1)
init_parameters["sigma_logit"] = MakeFloatTensor(3*[-3]).view(3)
init_parameters["mixture_logit"] = MakeFloatTensor([-1]).view(1)
init_parameters["prior"] = MakeZeros(GRID)
init_parameters["volume"] = MakeZeros(GRID)
for _, y in init_parameters.items():
    y.requires_grad = True

##############################################
# Initialize optimizer.
# The learning rate is a user-specified parameter.
learning_rate = 0.05
optim = torch.optim.SGD([y for _, y in init_parameters.items()], lr=learning_rate)

##############################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 2*math.pi

# Part: Specify `similariy` or `difference` functions.
## These are negative squared distances (for interval spaces) or
## trigonometric functions (for circular spaces), with
## some extra factors for numerical purposes.
## Exponentiating a `similarity` function and normalizing
## is equivalent to the Gaussian / von Mises density.
## The purpose of specifying these as `closeness` or `distance`,
## rather than simply calling squared or trigonometric
## functions is to  flexibly reuse the same model code for
## both interval and circular spaces.
def SQUARED_STIMULUS_DIFFERENCE(x):
    return torch.sin(math.pi*x/180)
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(math.pi*x/180)
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(x)
def SQUARED_SENSORY_DIFFERENCE(x):
    return torch.sin(x)

#############################################################
# Part: Configure the appropriate estimator for minimizing the loss function
assert P >= 2
CosineEstimator.set_parameters(GRID=GRID, OPTIMIZER_VERBOSE=OPTIMIZER_VERBOSE, P=P, SQUARED_SENSORY_DIFFERENCE=SQUARED_SENSORY_DIFFERENCE, SQUARED_SENSORY_SIMILARITY=SQUARED_SENSORY_SIMILARITY)

#############################################################
# Part: Run the model. This function implements the model itself:
## calculating the likelihood of a given dataset under that model
## and---if the computePredictions argument is set to True--- computes
## the bias and variability of the estimate.
def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, StimulusSD=None, computePredictions=False, subject=None, sigma_stimulus=None, sigma2_stimulus=None, Duration_=None, folds=None, lossReduce='mean'):

 # Part: Obtain the motor variance by exponentiating the appropriate model parameter
 motor_variance = torch.exp(- parameters["log_motor_var"])
 # Part: Obtain the sensory noise variance. We parameterize it as a fraction of the squared volume of the size of the sensory space
 sigma2 = (SENSORY_SPACE_VOLUME * SENSORY_SPACE_VOLUME)*torch.sigmoid(sigma_logit)
 # Part: Obtain the transfer function as the cumulative sum of the discretized resource allocation (referred to as `volume` element due to the geometric interpretation by Wei&Stocker 2015)
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 if True:
  folds = MakeLongTensor(folds)
  if subject is not None:
    MASK = torch.logical_and(torch.logical_and(torch.logical_and(Subject==subject, StimulusSD == sigma_stimulus), Duration == Duration_), (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]
  else:
    MASK = torch.logical_and(torch.logical_and(StimulusSD == sigma_stimulus, Duration == Duration_), (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]

  if sigma2_stimulus > 0:
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))
    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)
  else:
    assert False

  # Part: Compute sensory likelihoods. Across both interval and
  ## circular stimulus spaces, this amounts to exponentiaring a
  ## `similarity`
  sensory_likelihoods = torch.softmax(((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2)) + volumeElement.unsqueeze(1).log(), dim=0)

  # Part: If stimulus noise is nonzero, convolve the likelihood with the
  ## stimulus noise.
  if sigma2_stimulus == 0:
    ## On this dataset, this is nonzero, so the
    ## code block will not be used.
    assert False
    likelihoods = sensory_likelihoods
  else:
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  ## Compute posterior using Bayes' rule. As described in the paper, the posterior is computed
  ## in the discretized stimulus space.
  posterior = prior.unsqueeze(1) * likelihoods.t()
  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  ## Compute the estimator for each m in the discretized sensory space.
  bayesianEstimate = CosineEstimator.apply(grid_indices_here, posterior)

  ## Compute the motor likelihood

  ## `error' refers to the stimulus similarity between the estimator assigned to each m and
  ## the observations found in the dataset.
  ## The Gaussian or von Mises motor likelihood is obtained by exponentiating and normalizing
  error = (SQUARED_STIMULUS_SIMILARITY(360/GRID*bayesianEstimate.unsqueeze(0) - responses.unsqueeze(1)))
  ## The log normalizing constants, for each m in the discretized sensory space
  log_normalizing_constant = torch.logsumexp((SQUARED_STIMULUS_SIMILARITY(grid))/motor_variance, dim=0) + math.log(2 * math.pi / GRID)
  ## The log motor likelihoods, for each pair of sensory encoding m and observed human response
  log_motor_likelihoods = (error/motor_variance) - log_normalizing_constant
  ## Obtaining the motor likelihood by exponentiating.
  motor_likelihoods = torch.exp(log_motor_likelihoods)
  ## Obtain the guessing rate, parameterized via the (inverse) logit transform as described in SI Appendix
  uniform_part = torch.sigmoid(parameters["mixture_logit"])
  ## The full likelihood then consists of a mixture of the motor likelihood calculated before, and the uniform
  ## distribution on the full space.
  motor_likelihoods = (1-uniform_part) * motor_likelihoods + (uniform_part / (2*math.pi) + 0*motor_likelihoods)

  # Now the loss is obtained by marginalizing out m from the motor likelihood
  if lossReduce == 'mean':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().mean()
  elif lossReduce == 'sum':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().sum()
  else:
    assert False

  if computePredictions:
     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS
     bayesianEstimate_avg_byStimulus = computeCircularMeanWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(bayesianEstimate_byStimulus, likelihoods)

     bayesianEstimate_avg_byStimulus = torch.where((bayesianEstimate_avg_byStimulus-grid).abs()<180, bayesianEstimate_avg_byStimulus, torch.where(bayesianEstimate_avg_byStimulus > 180, bayesianEstimate_avg_byStimulus-360, bayesianEstimate_avg_byStimulus+360))
     assert float(((bayesianEstimate_avg_byStimulus-grid).abs()).max()) < 180, float(((bayesianEstimate_avg_byStimulus-grid).abs()).max())
     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = computeCircularMeanWeighted(posteriorMaxima.unsqueeze(1), likelihoods)
     encodingBias = computeCircularMeanWeighted(grid.unsqueeze(1), likelihoods)
     attraction = (posteriorMaxima-encodingBias)
     attraction1 = attraction
     attraction2 = attraction+360
     attraction3 = attraction-360
     attraction = torch.where(attraction1.abs() < 180, attraction1, torch.where(attraction2.abs() < 180, attraction2, attraction3))
  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction

getObservations.setData(x_set=x_set, observations_y=observations_y, xValues=xValues, Duration=Duration, grid=grid, x=x, StimulusSD=StimulusSD, GRID=GRID, Subject=Subject, SQUARED_STIMULUS_SIMILARITY=SQUARED_STIMULUS_SIMILARITY)

def model(grid):
  lossesBy500 = []
  crossLossesBy500 = []
  noImprovement = 0
  global optim, learning_rate
  for iteration in range(10000000):
   parameters = init_parameters

   ## In each iteration, recompute
   ## - the resource allocation (called `volume' due to a geometric interpretation)
   ## - the prior

   volume = SENSORY_SPACE_VOLUME * torch.softmax(parameters["volume"], dim=0)
   prior = torch.softmax(parameters["prior"], dim=0)

   loss = 0
   if iteration % 500 == 0:
     x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

   bayesianEstimate_28_by_duration = MakeZeros(GRID)
   bayesianEstimate_4_by_duration = MakeZeros(GRID)
   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   for DURATION in range(3):
    for SUBJECT in [1]:
     loss_2_28, bayesianEstimate_2_28, bayesianEstimate_sd_byStimulus_2_28, attraction_2_28 = computeBias(xValues, init_parameters["sigma_logit"][DURATION], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, StimulusSD=StimulusSD, computePredictions=(iteration%500 == 0), subject=None, sigma_stimulus=28, sigma2_stimulus=torch.exp(-init_parameters["sigma2_stimulus"][1]), Duration_=DURATION, folds=trainFolds, lossReduce='sum')
     loss += loss_2_28

     loss_2_4, bayesianEstimate_2_4, bayesianEstimate_sd_byStimulus_2_4, attraction_2_4 = computeBias(xValues, init_parameters["sigma_logit"][DURATION], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, StimulusSD=StimulusSD, computePredictions=(iteration%500 == 0), subject=None, sigma_stimulus=4, sigma2_stimulus=torch.exp(-init_parameters["sigma2_stimulus"][0]), Duration_=DURATION, folds=trainFolds, lossReduce='sum')
     loss += loss_2_4

     if iteration % 500 == 0:
       bayesianEstimate_28_by_duration = bayesianEstimate_28_by_duration + bayesianEstimate_2_28.detach()/3
       bayesianEstimate_4_by_duration = bayesianEstimate_4_by_duration + bayesianEstimate_2_4.detach()/3

       y_set, sd_set = retrieveObservations(x, 28, None, DURATION)

       y_set, sd_set = retrieveObservations(x, 4, None, DURATION)
   if iteration % 500 == 0:

     y_set_4, sd_set_4 = retrieveObservations(x, 4, None, None)
     y_set_28, sd_set_28 = retrieveObservations(x, 28, None, None)

   if iteration % 500 == 0:

     crossValidLoss = 0
     for DURATION in range(3):
      for SUBJECT in [1]:
       loss_2_28, bayesianEstimate_2_28, bayesianEstimate_sd_byStimulus_2_28, attraction_2_28 = computeBias(xValues, init_parameters["sigma_logit"][DURATION], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, StimulusSD=StimulusSD, computePredictions=(iteration%500 == 0), subject=None, sigma_stimulus=28, sigma2_stimulus=torch.exp(-init_parameters["sigma2_stimulus"][1]), Duration_=DURATION, folds=testFolds, lossReduce='sum')
       crossValidLoss += loss_2_28

       loss_2_4, bayesianEstimate_2_4, bayesianEstimate_sd_byStimulus_2_4, attraction_2_4 = computeBias(xValues, init_parameters["sigma_logit"][DURATION], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, StimulusSD=StimulusSD, computePredictions=(iteration%500 == 0), subject=None, sigma_stimulus=4, sigma2_stimulus=torch.exp(-init_parameters["sigma2_stimulus"][0]), Duration_=DURATION, folds=testFolds, lossReduce='sum')
       crossValidLoss += loss_2_4

   loss = loss / (6 * observations_y.size()[0])

   regularizer1 = ((init_parameters["volume"][1:] - init_parameters["volume"][:-1]).pow(2).sum() + (init_parameters["volume"][0] - init_parameters["volume"][-1]).pow(2))/GRID
   regularizer2 = ((init_parameters["prior"][1:] - init_parameters["prior"][:-1]).pow(2).sum() + (init_parameters["prior"][0] - init_parameters["prior"][-1]).pow(2))/GRID
   regularizer_total = regularizer1 + regularizer2

   loss = loss + REG_WEIGHT * regularizer_total

   # Calculate gradients
   optim.zero_grad()
   loss.backward()
   optim.step()
   if iteration % 10 == 0:
     print(iteration, loss, init_parameters["sigma_logit"], init_parameters["mixture_logit"], init_parameters["log_motor_var"], torch.exp(-init_parameters["sigma2_stimulus"]))

   if iteration % 500 == 0 and iteration > 0:
       lossesBy500.append(float(loss))
       crossLossesBy500.append(float(crossValidLoss))
       if len(lossesBy500) > 0 and float(loss) <= min(lossesBy500):
        with open(f"losses/{__file__.replace('_VIZ', '')}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt.txt", "w") as outFile:
            print(float(crossValidLoss), file=outFile)
        with open(f"logs/CROSSVALID/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt", "w") as outFile:
           print(float(loss), "CrossValid", float(crossValidLoss), "CrossValidLossesBy500", " ".join([str(q) for q in crossLossesBy500]), file=outFile)
           print(iteration, "LossesBy500_"+str(noImprovement), " ".join([str(q) for q in lossesBy500]), file=outFile)
           for z, y in init_parameters.items():
               print(z, "\t", y.detach().cpu().numpy().tolist(), file=outFile)
       if len(lossesBy500) > 1 and float(loss) > lossesBy500[-2]:
         learning_rate *= 0.8
         optim = torch.optim.SGD([y for _, y in init_parameters.items()], lr=learning_rate)
       if len(lossesBy500) > 1 and float(loss) > min(lossesBy500[:-1]):
         noImprovement += 1
       else:
         noImprovement = 0
       if noImprovement >= 5:
           print("Stopping")
           break

############################3

model(grid)
