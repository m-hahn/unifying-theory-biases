import computations
import getObservations
import glob
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sys
import torch
from computations import computeResources
from computations import computeResourcesWithStimulusNoise
from cosineEstimator import CosineEstimator
from getObservations import retrieveAndSmoothObservations
from getObservations import retrieveAndSmoothObservationsAcrossSubjects
from loadModel import loadModel
from loadTomassini import *
from matplotlib import rc
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import makeGridIndicesCircular
from util import product
from util import savePlot
from util import sech
from util import sign

rc('font', **{'family':'FreeSans'})

OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
assert P > 0
FOLD_HERE = int(sys.argv[2])
REG_WEIGHT = float(sys.argv[3])
GRID = int(sys.argv[4])
assert GRID == 180

SHOW_PLOT = (len(sys.argv) < 6) or (sys.argv[5] == "SHOW_PLOT")

FILE = f"logs/CROSSVALID/{__file__.replace('_VIZ_OnlyHuman_MainPaper', '')}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"
if not os.path.exists(FILE):
    assert False, FILE

##############################################
# Helper Functions dependent on the device

##############################################

N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

Fold = 0*Subject
for i in range(min(Subject), max(Subject)+1):
    trials = [j for j in range(Subject.size()[0]) if Subject[j] == i]
    randomGenerator.shuffle(trials)
    foldSize = int(len(trials)/N_FOLDS)
    for k in range(N_FOLDS):
        Fold[trials[k*foldSize:(k+1)*foldSize]] = k

print(Fold)

assert (observations_x == target).all()
assert (observations_y == response).all()

##############################################
# Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 360

CIRCULAR = True
INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
grid, grid_indices_here = makeGridIndicesCircular(GRID, MIN_GRID, MAX_GRID)

# Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

#############################################################
# Part: Initialize the model
init_parameters = {}

init_parameters["sigma2_stimulus"] = MakeFloatTensor([0,0]).view(2)
init_parameters["log_motor_var"] = MakeFloatTensor([8]).view(1)
init_parameters["sigma_logit"] = MakeFloatTensor(3*[-3]).view(3)
init_parameters["mixture_logit"] = MakeFloatTensor([-1]).view(1)
init_parameters["prior"] = MakeFloatTensor([1]).view(1)
init_parameters["volume"] = MakeZeros(GRID)
loadModel(FILE, init_parameters)
assert "volume" in init_parameters
for _, y in init_parameters.items():
    y.requires_grad = True

##############################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 2*math.pi

def SQUARED_STIMULUS_DIFFERENCE(x):
    return torch.sin(math.pi*x/180)
def SQUARED_STIMULUS_SIMILARITY(x):
    return torch.cos(math.pi*x/180)
def SQUARED_SENSORY_SIMILARITY(x):
    return torch.cos(x)
def SQUARED_SENSORY_DIFFERENCE(x):
    return torch.sin(x)

class MAPEstimator(torch.autograd.Function):
    """
    We can implement our own custom autograd Functions by subclassing
    torch.autograd.Function and implementing the forward and backward passes
    which operate on Tensors.
    """

    SCALE=100
    KERNEL_WIDTH = 0.05
    averageNumberOfNewtonSteps = 2
    W = 100

    # For this MAP estimator implementation, there are checks in ExampleGardelle_Inference_Discretized_Likelihood_Nonparam_MotorNoise1_Lp_Scale_NewStrict_NaturalPrior_ZeroExponent4_CROSSVALI_DebugD6_Smoothed2_UnitTest_GD_Check_Unnormalized_Hess_FineGrid_Fix2.py
    @staticmethod
    def forward(ctx, grid_indices_here, posterior):
        """
        In the forward pass we receive a Tensor containing the input and return
        a Tensor containing the output. ctx is a context object that can be used
        to stash information for backward computation. You can cache arbitrary
        objects for use in the backward pass using the ctx.save_for_backward method.
        """
        global averageNumberOfNewtonSteps
        SCALE = MAPEstimator.SCALE
        KERNEL_WIDTH = MAPEstimator.KERNEL_WIDTH
        averageNumberOfNewtonSteps = MAPEstimator.averageNumberOfNewtonSteps
        W = MAPEstimator.W
        # Step 1: To avoid over-/underflow, rescale the indices
        grid_indices_here = grid_indices_here/SCALE
        n_inputs, n_batch = posterior.size()

        # Step 1: Identify the argmax on the full discretized grid
        resultDiscrete = posterior.argmax(dim=0)
        resultDiscrete = torch.stack([grid_indices_here[resultDiscrete[i],i] for i in range(GRID)], dim =0)
        resultDiscrete1 = resultDiscrete
        resultDiscreteFromRaw = resultDiscrete

        Q = 20
        FINE_GRID_SCALE = 2
        for J in range(1):
           environment = (torch.arange(start=-Q, end=Q+1)/(FINE_GRID_SCALE*SCALE)).view(-1, 1) + resultDiscrete.view(1, -1)
           K = environment.size()[0]
           kernelFunction = torch.nn.functional.softmax(-(environment.view(K, 1, GRID) - grid_indices_here.view(1,GRID,GRID)).pow(2) / (2*(KERNEL_WIDTH**2)), dim=1)
           smoothedPosterior = (kernelFunction * posterior.view(1,GRID,GRID)).sum(dim=1)

           extraStep = -torch.exp(-W*(environment-MIN_GRID/SCALE)) - torch.exp(W*(environment-MAX_GRID/SCALE))

           smoothedPosterior = smoothedPosterior + .1 * extraStep

           smoothedPosteriorInEnvironment = smoothedPosterior.detach()

           argmaxInFineGrid = smoothedPosterior.argmax(dim=0)
           resultFromIntermediateGrid = ((argmaxInFineGrid-K/2)/(FINE_GRID_SCALE*SCALE) + resultDiscrete)
           if False:
            onBoundary = torch.logical_and(torch.logical_or(argmaxInFineGrid == 0, argmaxInFineGrid== K-1), torch.logical_and(resultFromIntermediateGrid > grid_indices_here.min(dim=0)[0], resultFromIntermediateGrid < grid_indices_here.max(dim=0)[0]))
            if onBoundary.float().sum() > 0:
               print("Warning: Some points are on the boundary of the trust region in iteration ",J, "Retrying with a larger region" if J == 0 else "Accepting nonetheless", "# of points:", onBoundary.float().sum())
               if True:
                for B in range(GRID):
                 if onBoundary[B]:
                   print(B,resultDiscrete[B], environment[0,B], environment[K-1,B], argmaxInFineGrid[B], resultFromIntermediateGrid[B], GRID/SCALE)
                   figure, axis = plt.subplots(1, 2)

                   kernelFunction = torch.nn.functional.softmax(-(grid_indices_here.view(GRID, 1, GRID) - grid_indices_here.view(1,GRID,GRID)).pow(2) / (2*(KERNEL_WIDTH**2)), dim=1)
                   smoothedPosteriorOverAll = (kernelFunction * posterior.view(1,GRID,GRID)).sum(dim=1)

                   axis[1].scatter(SCALE*grid_indices_here[:,B].detach().cpu(), posterior[:,B].detach().cpu(), color="gray")
                   axis[1].scatter(SCALE*grid_indices_here[:,B].detach().cpu(), smoothedPosteriorOverAll[:,B].detach().cpu(), color="yellow")
                   axis[1].scatter(SCALE*environment[:,B].detach().cpu(), smoothedPosteriorInEnvironment[:,B].detach().cpu())
                   Z = max(float(smoothedPosteriorInEnvironment[:,0].max()), float(smoothedPosteriorInEnvironment[:,B].max()))
                   Y = min(float(smoothedPosteriorInEnvironment[:,0].min()), float(smoothedPosteriorInEnvironment[:,B].min()))

                   axis[1].plot([SCALE*resultDiscrete1[B].detach().cpu(), SCALE*resultDiscrete1[B].detach().cpu()], [Y,Z], color="orange")
                   axis[1].plot([SCALE*resultDiscrete[B].detach().cpu(), SCALE*resultDiscrete[B].detach().cpu()], [Y,Z], color="red")
                   axis[1].plot([SCALE*resultFromIntermediateGrid[B].detach().cpu(), SCALE*resultFromIntermediateGrid[B].detach().cpu()], [Y,Z], color="purple")

                   savePlot(f"figures/DEBUG_{__file__}.pdf")
                   plt.close()
                   quit()

                quit()
               FINE_GRID_SCALE = .5
            else:
               break

        PLOT = (random.random() < .1) and False
        if PLOT:
           # Plot the smoothed posterior
           figure, axis = plt.subplots(1, 2)
           axis[0].scatter(SCALE*environment[:,0], smoothedPosterior[:,0])
           axis[1].scatter(SCALE*environment[:,80], smoothedPosterior[:,80])
           Z = max(float(smoothedPosterior[:,0].max()), float(smoothedPosterior[:,80].max()))
           Y = min(float(smoothedPosterior[:,0].min()), float(smoothedPosterior[:,80].min()))

        if PLOT:
           axis[0].plot([SCALE*resultDiscrete[0], SCALE*resultDiscrete[0]], [Y,Z], color="orange")
           axis[0].plot([SCALE*resultFromIntermediateGrid[0], SCALE*resultFromIntermediateGrid[0]], [Y,Z], color="green")
           axis[1].plot([SCALE*resultDiscrete[80], SCALE*resultDiscrete[80]], [Y,Z], color="orange")
           axis[1].plot([SCALE*resultFromIntermediateGrid[80], SCALE*resultFromIntermediateGrid[80]], [Y,Z], color="green")

        # Now use the result from the intermediate grid
        resultDiscrete = resultFromIntermediateGrid
        resultDiscrete = resultDiscrete.clamp(min=MIN_GRID/SCALE, max=MAX_GRID/SCALE)

        optimizationSequence = []
        if True:
          for i in range(40):
             # First compute P'(theta)
             innerDerivatives = -(resultDiscrete.view(1, GRID) - grid_indices_here) / (KERNEL_WIDTH**2)
             kernelFunction = torch.nn.functional.softmax(-(resultDiscrete.view(1, GRID) - grid_indices_here).pow(2) / (2*(KERNEL_WIDTH**2)), dim=0)
             if OPTIMIZER_VERBOSE:
               smoothedPosterior = (kernelFunction * posterior).sum(dim=0)
               extraStep = -torch.exp(-W*(resultDiscrete-MIN_GRID/SCALE)) - torch.exp(W*(resultDiscrete-MAX_GRID/SCALE))
               smoothedPosterior = smoothedPosterior + .1 * extraStep

             kernelFunctionDerivativeI = innerDerivatives * kernelFunction
             # This sum is needed repeatedly
             kernelFunctionDerivativeISum = kernelFunctionDerivativeI.sum(dim=0, keepdim=True)

             kernelDerivative = kernelFunctionDerivativeI - kernelFunction * kernelFunctionDerivativeISum
             derivativeSmoothedPosterior = (kernelDerivative * posterior).sum(dim=0)

             # First derivative has been verified against Pytorch autograd 

             if True:
               smoothedPosterior = (kernelFunction * posterior).sum(dim=0)
             # Now compute P''(theta)
             kernelFunctionSecondDerivativeI = -1/(KERNEL_WIDTH**2) * kernelFunction + innerDerivatives.pow(2) * kernelFunction
             part1 = kernelFunctionSecondDerivativeI - kernelFunctionDerivativeI * kernelFunctionDerivativeISum
             kernelSecondDerivative = part1 - kernelDerivative * kernelFunctionDerivativeISum - kernelFunction * part1.sum(dim=0, keepdim=True)
             secondDerivativeSmoothedPosterior = (kernelSecondDerivative * posterior).sum(dim=0)

             # Second derivative has been verified against finite differences of the first derivatives
             # These calculations are all bottlenecks. Could perhaps reduce some of the inefficiency.

             hessian = secondDerivativeSmoothedPosterior
             if OPTIMIZER_VERBOSE and i % 10 == 0 or False:
                print(i, smoothedPosterior.mean(), derivativeSmoothedPosterior.abs().mean(), derivativeSmoothedPosterior.abs().max(), "\t", resultDiscrete[10:15]*SCALE)

             # Optimization step
             # For those batch elements where P'' < 0, we do a Newton step.
             # For the others, we do a GD step, with stepsize based 1/P'', cutting off excessively large resulting stepsizes
             # For a well-behaved problem (concave within the trust region and attains its maximum on it), only Newton steps should be required. GD steps are intended as a fallback when this is not satisfied.
             MASK = hessian>=-0.005
             updateHessian =  - derivativeSmoothedPosterior / hessian.clamp(max=-0.001)

             updateGD = derivativeSmoothedPosterior / hessian.abs().clamp(min=0.01)
             # Prevent excessive jumps, and decay the allowed learning rate.
             MAXIMAL_UPDATE_SIZE = 0.1 / (1+i/5)
             update = torch.where(MASK, updateGD, updateHessian).clamp(min=-MAXIMAL_UPDATE_SIZE, max=MAXIMAL_UPDATE_SIZE)
             if random.random() < 0.0002:
               print("Maximal update", update.abs().max(), update.abs().median(), (update.abs() >= 0.1).float().sum(), "Doing Non-Newton", MASK.float().sum(), hessian.abs().median())

             resultDiscrete = resultDiscrete + update
             optimizationSequence.append((resultDiscrete, smoothedPosterior, derivativeSmoothedPosterior, secondDerivativeSmoothedPosterior))
             if PLOT:
                axis[0].plot([SCALE*resultDiscrete[0], SCALE*resultDiscrete[0]], [Y,Z], color="red")
                axis[1].plot([SCALE*resultDiscrete[80], SCALE*resultDiscrete[80]], [Y,Z], color="red")

             if False:

               MaskOutsideOfRegion = torch.logical_or(resultDiscrete < MIN_GRID/SCALE, resultDiscrete > MAX_GRID/SCALE)
               derivativeSmoothedPosterior = torch.where(MaskOutsideOfRegion, 0*derivativeSmoothedPosterior, derivativeSmoothedPosterior)
               resultDiscrete = resultDiscrete.clamp(min=MIN_GRID/SCALE, max=MAX_GRID/SCALE)

             if False:
             # Check whether solution has left trust region
               lowerThanTrustRegion = (environment[0] > resultDiscrete)
               higherThanTrustRegion = (environment[-1] < resultDiscrete)
               if lowerThanTrustRegion.float().sum() + higherThanTrustRegion.float().sum() > 0:
                   print("Warning: some batches have left the trust region.", lowerThanTrustRegion.float().sum(), higherThanTrustRegion.float().sum())

             if float(derivativeSmoothedPosterior.abs().max()) < 1e-6:
                 break
        else:
            assert False
        averageNumberOfNewtonSteps = 0.98 * averageNumberOfNewtonSteps + (1-0.98) * i
        if random.random() < 0.0003:
           print("Number of Newton iterations", i, "average", averageNumberOfNewtonSteps)
        if i > 20:
            print("Warning: Finding MAP estimator took", i, "iterations. Maximal gradient", float(derivativeSmoothedPosterior.abs().max()))

        if float(derivativeSmoothedPosterior.abs().max()) > 1e-4:
            print("Warning: Finding MAP estimator took", i, "iterations. Maximal gradient", float(derivativeSmoothedPosterior.abs().max()))
            worst = derivativeSmoothedPosterior.abs().argmax()

            print(sorted(derivativeSmoothedPosterior.detach().cpu().numpy().tolist()))
            print(derivativeSmoothedPosterior)
            print("PROBLEM", worst, derivativeSmoothedPosterior[worst], float(derivativeSmoothedPosterior.abs().max()), derivativeSmoothedPosterior[GRID-1])
            plt.close()
            figure, axis = plt.subplots(1, 2, figsize=(15,15))
            axis[0].scatter(SCALE*grid_indices_here[:,0].detach().cpu(), posterior[:,0].detach().cpu(), color="gray")

            kernelFunction = torch.nn.functional.softmax(-(grid_indices_here.view(GRID, 1, GRID) - grid_indices_here.view(1,GRID,GRID)).pow(2) / (2*(KERNEL_WIDTH**2)), dim=1)
            smoothedPosteriorOverAll = (kernelFunction * posterior.view(1,GRID,GRID)).sum(dim=1)

            KernelTimesMinusOnePlusKernel = kernelFunction * (kernelFunction-1)
            kernelFunctionDerivative = 2 * (grid_indices_here.view(GRID, 1, GRID) - grid_indices_here.view(1,GRID,GRID)) / (2*(KERNEL_WIDTH**2)) * KernelTimesMinusOnePlusKernel
            kernelFunctionSecondDerivative = 1 / (KERNEL_WIDTH**2) * KernelTimesMinusOnePlusKernel + ((grid_indices_here.view(GRID, 1, GRID) - grid_indices_here.view(1,GRID,GRID)) / (KERNEL_WIDTH**2)) * (kernelFunctionDerivative * (kernelFunction-1) + kernelFunction*kernelFunctionDerivative)
            hessianOverAll = (kernelFunctionSecondDerivative * posterior).sum(dim=1)
            derivativeSmoothedPosteriorOverAll = (kernelFunctionDerivative * posterior).sum(dim=1)

            axis[0].scatter(SCALE*grid_indices_here[:,0].detach().cpu(), smoothedPosteriorOverAll[:,0].detach().cpu(), color="yellow")
            axis[0].scatter(SCALE*grid_indices_here[:,0].detach().cpu(), derivativeSmoothedPosteriorOverAll[:,0].detach().cpu(), color="purple")

            axis[0].scatter(SCALE*environment[:,0].detach().cpu(), smoothedPosteriorInEnvironment[:,0].detach().cpu())
            axis[1].scatter(SCALE*grid_indices_here[:,worst].detach().cpu(), posterior[:,worst].detach().cpu(), color="gray")
            axis[1].scatter(SCALE*grid_indices_here[:,worst].detach().cpu(), smoothedPosteriorOverAll[:,worst].detach().cpu(), color="yellow")
            axis[1].scatter(SCALE*grid_indices_here[:,worst].detach().cpu(), derivativeSmoothedPosteriorOverAll[:,worst].detach().cpu(), color="purple")
            axis[1].scatter(SCALE*environment[:,worst].detach().cpu(), smoothedPosteriorInEnvironment[:,worst].detach().cpu())
            Z = max(float(smoothedPosteriorInEnvironment[:,0].max()), float(smoothedPosteriorInEnvironment[:,worst].max()))
            Y = min(float(smoothedPosteriorInEnvironment[:,0].min()), float(smoothedPosteriorInEnvironment[:,worst].min()))
            for x, y, z, u in optimizationSequence:
               print(x[0], x[worst], "P", y[worst], "dP", z[worst], "d2P", u[worst], derivativeSmoothedPosterior[worst], resultDiscrete[worst])
               axis[0].plot([SCALE*x[0].detach().cpu(), SCALE*x[0].detach().cpu()], [Y,Z], color="yellow")
               axis[1].plot([SCALE*x[worst].detach().cpu(), SCALE*x[worst].detach().cpu()], [Y,Z], color="yellow")
            axis[0].plot([SCALE*resultDiscrete1[0].detach().cpu(), SCALE*resultDiscrete1[0].detach().cpu()], [Y,Z], color="orange")
            axis[1].plot([SCALE*resultDiscrete1[worst].detach().cpu(), SCALE*resultDiscrete1[worst].detach().cpu()], [Y,Z], color="orange")
            axis[0].plot([SCALE*resultDiscrete[0].detach().cpu(), SCALE*resultDiscrete[0].detach().cpu()], [Y,Z], color="red")
            axis[1].plot([SCALE*resultDiscrete[worst].detach().cpu(), SCALE*resultDiscrete[worst].detach().cpu()], [Y,Z], color="red")
            savePlot(f"figures/DEBUG_{__file__}.pdf")
            plt.close()
            assert False

        if PLOT:
           plt.show()

        result = resultDiscrete

        ctx.save_for_backward(grid_indices_here, posterior, result)
        return result.detach()*SCALE

    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor containing the gradient of the loss
        with respect to the output, and we need to compute the gradient of the loss
        with respect to the input.
        """
        grid_indices_here, posterior, result = ctx.saved_tensors

        #################
        # g'_j and g''_j are computed as in during the Newton iterations of the forward pass
        innerDerivatives = -(result.view(1, GRID) - grid_indices_here) / (KERNEL_WIDTH**2)
        kernelFunction = torch.nn.functional.softmax(-(result.view(1, GRID) - grid_indices_here).pow(2) / (2*(KERNEL_WIDTH**2)), dim=0)
        kernelFunctionDerivativeI = innerDerivatives * kernelFunction
        # This sum is needed repeatedly
        kernelFunctionDerivativeISum = kernelFunctionDerivativeI.sum(dim=0, keepdim=True)
        kernelDerivative = kernelFunctionDerivativeI - kernelFunction * kernelFunctionDerivativeISum
        derivativeSmoothedPosterior = (kernelDerivative * posterior).sum(dim=0)

        kernelFunctionSecondDerivativeI = -1/(KERNEL_WIDTH**2) * kernelFunction + innerDerivatives.pow(2) * kernelFunction
        part1 = kernelFunctionSecondDerivativeI - kernelFunctionDerivativeI * kernelFunctionDerivativeISum
        kernelSecondDerivative = part1 - kernelDerivative * kernelFunctionDerivativeISum - kernelFunction * part1.sum(dim=0, keepdim=True)
        secondDerivativeSmoothedPosterior = (kernelSecondDerivative * posterior).sum(dim=0)

        hessian = secondDerivativeSmoothedPosterior
        #################

        # Now using implicit differentiation
        gradient_implicit = - kernelDerivative / hessian
        gradient = grad_output.unsqueeze(0) * gradient_implicit

        return None, gradient*SCALE

CosineEstimator.set_parameters(GRID=GRID, OPTIMIZER_VERBOSE=OPTIMIZER_VERBOSE, P=P, SQUARED_SENSORY_DIFFERENCE=SQUARED_SENSORY_DIFFERENCE, SQUARED_SENSORY_SIMILARITY=SQUARED_SENSORY_SIMILARITY)

# Run the model
def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, StimulusSD=None, computePredictions=False, subject=None, sigma_stimulus=None, sigma2_stimulus=None, Duration_=None, folds=None, lossReduce='mean'):

 motor_variance = torch.exp(- parameters["log_motor_var"])
 sigma2 = 2*math.pi*torch.sigmoid(sigma_logit)
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 loss = 0
 if True:

  folds = MakeLongTensor(folds)
  if subject is not None:
    MASK = torch.logical_and(torch.logical_and(torch.logical_and(Subject==subject, StimulusSD == sigma_stimulus), Duration == Duration_), (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]
  else:
    MASK = torch.logical_and(torch.logical_and(StimulusSD == sigma_stimulus, Duration == Duration_), (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]

  if sigma2_stimulus > 0:
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))

    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  sensory_likelihoods = torch.nn.functional.softmax(((torch.cos(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(2*sigma2)) / math.sqrt(2*math.pi*sigma2)  + volumeElement.unsqueeze(1).log(), dim=0)

  if sigma2_stimulus == 0:
    assert False
    likelihoods = sensory_likelihoods
  else:
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  posterior = prior.unsqueeze(1) * likelihoods.t()

  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  bayesianEstimate = CosineEstimator.apply(grid_indices_here, posterior)
  if False:
     assert P == 2

     bayesianEstimate2 = ((grid_indices_here * posterior).sum(dim=0))
     assert (bayesianEstimate-bayesianEstimate2).abs().max() < 0.001
     GRAD1 = torch.autograd.grad(bayesianEstimate.sum(), [posterior], retain_graph=True)[0]
     print(GRAD1)
     print("This is the implicit function gradient", GRAD1.pow(2).mean())
     GRAD1 = GRAD1 - GRAD1.mean(dim=0, keepdim=True)
     GRAD2 = torch.autograd.grad(bayesianEstimate2.sum(), [posterior], retain_graph=True)[0]
     print(GRAD2)
     print("This is the naive gradient", GRAD2.pow(2).mean())
     GRAD2 = GRAD2 - GRAD2.mean(dim=0, keepdim=True)
     print("This is the resulting gradient", GRAD2.pow(2).mean())

     assert (GRAD1-GRAD2).abs().max() < 0.0001
     print("...")

  error = (SQUARED_STIMULUS_SIMILARITY(360/GRID*bayesianEstimate.unsqueeze(0) - responses.unsqueeze(1)))

  log_normalizing_constant = torch.logsumexp((SQUARED_STIMULUS_SIMILARITY(grid))/motor_variance, dim=0) + math.log(2 * math.pi / GRID)

  normalizing_constant = torch.exp((SQUARED_STIMULUS_SIMILARITY(grid))/motor_variance).sum() * 360 / GRID * 0.0175

  log_motor_likelihoods = (error/motor_variance) - log_normalizing_constant

  motor_likelihoods = torch.exp(log_motor_likelihoods)

  uniform_part = torch.sigmoid(parameters["mixture_logit"])

  motor_likelihoods = (1-uniform_part) * motor_likelihoods + (uniform_part / (2*math.pi) + 0*motor_likelihoods)

  # Now the loss is obtained by marginalizing out m from the motor likelihood
  if lossReduce == 'mean':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().mean()
  elif lossReduce == 'sum':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().sum()
  else:
    assert False

  if computePredictions:

     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS
     bayesianEstimate_avg_byStimulus = computeCircularMeanWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(bayesianEstimate_byStimulus, likelihoods)

     bayesianEstimate_avg_byStimulus = torch.where((bayesianEstimate_avg_byStimulus-grid).abs()<180, bayesianEstimate_avg_byStimulus, torch.where(bayesianEstimate_avg_byStimulus > 180, bayesianEstimate_avg_byStimulus-360, bayesianEstimate_avg_byStimulus+360))

     assert float(((bayesianEstimate_avg_byStimulus-grid).abs()).max()) < 180, float(((bayesianEstimate_avg_byStimulus-grid).abs()).max())
     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = computeCircularMeanWeighted(posteriorMaxima.unsqueeze(1), likelihoods)

     encodingBias = computeCircularMeanWeighted(grid.unsqueeze(1), likelihoods)
     attraction = (posteriorMaxima-encodingBias)
     attraction1 = attraction
     attraction2 = attraction+360
     attraction3 = attraction-360
     attraction = torch.where(attraction1.abs() < 180, attraction1, torch.where(attraction2.abs() < 180, attraction2, attraction3))
     encodingBias = encodingBias-grid
     encodingBias1 = encodingBias
     encodingBias2 = encodingBias+360
     encodingBias3 = encodingBias-360
     encodingBias = torch.where(encodingBias1.abs() < 180, encodingBias1, torch.where(encodingBias2.abs() < 180, encodingBias2, encodingBias3))
  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction, encodingBias

getObservations.setData(x_set=x_set, observations_y=observations_y, xValues=xValues, Duration=Duration, grid=grid, x=x, StimulusSD=StimulusSD, GRID=GRID, Subject=Subject, SQUARED_STIMULUS_SIMILARITY=SQUARED_STIMULUS_SIMILARITY)

computations.setData(STIMULUS_SPACE_VOLUME=STIMULUS_SPACE_VOLUME, GRID=GRID)

def model(grid):

  for iteration in range(1):
   parameters = init_parameters

   volume = SENSORY_SPACE_VOLUME * torch.softmax(parameters["volume"], dim=0)
   prior = 2-(torch.sin(grid*math.pi/180).abs())
   prior = prior/prior.sum()

   loss = 0
   if iteration % 500 == 0:
     assigned = ["ENC", None, "PRI", None, "ATT", None, "REP", None, "HUM"]
     for w, k in enumerate(assigned):
         globals()[k] = w
     PAD = [w for w, q in enumerate(assigned) if q != "HUM"]
     gridspec = dict(width_ratios=[1 if x is not None else .4 for x in assigned])
     figure, axis = plt.subplots(1, len(assigned), figsize=(10, 1.3*0.8*1.8), gridspec_kw=gridspec)
     plt.tight_layout()
     figure.subplots_adjust(wspace=0.1, hspace=0.0)

     kappa = 1/((SENSORY_SPACE_VOLUME * SENSORY_SPACE_VOLUME)*torch.sigmoid(parameters["sigma_logit"][0]))
     for stimulus_noise, color in zip([0,1],["green","red"]):
      kappa_stimulus = 1/(torch.exp(-init_parameters["sigma2_stimulus"][stimulus_noise]) * math.pow(180 / math.pi,2))
      axis[ENC].plot(grid, 2*computeResourcesWithStimulusNoise(volume.detach(), kappa, kappa_stimulus).detach(), color=color)
     print(volume.detach() / (2*math.pi*torch.sigmoid(init_parameters["sigma_logit"][0]) ).sqrt().detach())

     axis[PRI].plot(grid, prior.detach(), color="gray")
     axis[PRI].set_ylim(0, 1.1*float(prior.max()))
     axis[ENC].set_ylim(0,0.2)
     x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

   bayesianEstimate_28_by_duration = MakeZeros(GRID)
   bayesianEstimate_4_by_duration = MakeZeros(GRID)
   bayesianEstimate_28_by_duration_UNIFORM = MakeZeros(GRID)
   bayesianEstimate_4_by_duration_UNIFORM = MakeZeros(GRID)

   prior_Uniform = MakeZeros(GRID) + 1/GRID

   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   for DURATION in range(3):
    for SUBJECT in [1]:
     loss_2_28, bayesianEstimate_2_28, bayesianEstimate_sd_byStimulus_2_28, attraction_2_28, _ = computeBias(xValues, init_parameters["sigma_logit"][DURATION], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, StimulusSD=StimulusSD, computePredictions=(iteration%500 == 0), subject=None, sigma_stimulus=28, sigma2_stimulus=torch.exp(-init_parameters["sigma2_stimulus"][1]), Duration_=DURATION, folds=testFolds, lossReduce='sum')
     loss += loss_2_28

     loss_2_4, bayesianEstimate_2_4, bayesianEstimate_sd_byStimulus_2_4, attraction_2_4, _ = computeBias(xValues, init_parameters["sigma_logit"][DURATION], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, StimulusSD=StimulusSD, computePredictions=(iteration%500 == 0), subject=None, sigma_stimulus=4, sigma2_stimulus=torch.exp(-init_parameters["sigma2_stimulus"][0]), Duration_=DURATION, folds=testFolds, lossReduce='sum')
     loss += loss_2_4

     if iteration % 500 == 0:
       bayesianEstimate_28_by_duration = bayesianEstimate_28_by_duration + bayesianEstimate_2_28.detach()/3
       bayesianEstimate_4_by_duration = bayesianEstimate_4_by_duration + bayesianEstimate_2_4.detach()/3

   if iteration % 500 == 0:
     x_set, y_set_28, sd_set_28 = retrieveAndSmoothObservationsAcrossSubjects(28)
     x_set, y_set_4, sd_set_4 = retrieveAndSmoothObservationsAcrossSubjects(4)

     axis[HUM].plot([grid[q] for q in x_set], y_set_28/2, color="red")

     axis[HUM].plot([grid[q] for q in x_set], y_set_4/2, color="green")

     axis[REP].plot(grid, (bayesianEstimate_28_by_duration-grid-attraction_2_28).detach()/2, color="red")
     axis[ATT].plot(grid, (attraction_2_28).detach()/2, color="red")

     axis[REP].plot(grid, (bayesianEstimate_4_by_duration-grid-attraction_2_4).detach()/2, color="green")
     axis[ATT].plot(grid, (attraction_2_4).detach()/2, color="green")

     for w in PAD:
       axis[w].set_visible(False)

     axis[PRI].set_yticks([0])
     for w in [REP]:
         axis[w].tick_params(labelleft=False, labelbottom=False)

     for w in range(len(assigned)):
       axis[w].tick_params(labelbottom=False)
       axis[w].spines['top'].set_visible(False)
       axis[w].spines['right'].set_visible(False)

     for Y in [ATT,REP,HUM]:
             axis[Y].set_yticks(ticks=[-4,0,4], labels=["-4", "0", "4"])
             axis[Y].set_ylim(-5,5)

     if True:
       axis[ENC].set_title("Resources")
       axis[PRI].set_title("Prior")
       axis[REP].set_title("Repulsion")
       axis[ATT].set_title("Attraction")
     if True:
       axis[HUM].set_title("Bias (data)", fontsize=20)

   if iteration % 500 == 0:

     for w in range(len(assigned)):
         axis[w].set_xticks(ticks=[0,180,360], labels=["0", "90", "180"])
         axis[w].tick_params(axis='x', labelsize=17, width=0.4)
         axis[w].tick_params(axis='y', labelsize=17, width=0.4)
         if w != ENC:
             axis[w].tick_params(labelbottom=False)
     figure.subplots_adjust(bottom=0.23,top=0.77)
     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.pdf", transparent=True)

   if iteration % 500 == 0:
     from matplotlib import cm
     from matplotlib.colors import ListedColormap, LinearSegmentedColormap
     viridis = cm.get_cmap('viridis', 8)

     figure, axis = plt.subplots(1, 1, figsize=(1.5,1.5))
     axis.violinplot((attraction_2_28).detach().abs(), positions=[0], showextrema=False)
     axis.violinplot((bayesianEstimate_28_by_duration-grid-attraction_2_28).detach().abs(), positions=[1], showextrema=False)
     plt.xticks([])
     plt.yticks([])
     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_Magnitudes.pdf")
     if SHOW_PLOT:

        plt.show()
     plt.close()

     figure, axis = plt.subplots(1, 1, figsize=(1.8,1.8))
     plt.tight_layout()
     ratio = (prior / volume.pow((P+2)/2)).detach()
     kernel = torch.softmax(-(((grid.unsqueeze(0) - grid.unsqueeze(1))/MAX_GRID)).pow(2)/0.001, dim=0)
     ratio = torch.matmul(ratio.unsqueeze(0), kernel).view(-1)
     prior_smoothed = torch.matmul(prior.unsqueeze(0), kernel).view(-1)
     volume_smoothed = torch.matmul(volume.unsqueeze(0), kernel).view(-1)
     axis.plot([MIN_GRID, MAX_GRID/2], [8,8], color="gray", linestyle='dotted')
     axis.plot([MIN_GRID, MAX_GRID/2], [0,0], color="gray", linestyle='dotted')
     axis.plot([MIN_GRID, MAX_GRID/2], [-8,-8], color="gray", linestyle='dotted')
     axis.plot([MIN_GRID, MAX_GRID/2], [-16,-16], color="gray", linestyle='dotted')
     plt.yticks([])
     axis.set_xticks(ticks=[0,90,180], labels=["0°", "90°", "180°"])
     axis.set_yticks(ticks=[8, 0, -8, -16], labels=["A", "B", "C", "D"])

     for w in range(2,GRID):
        y1 = float(ratio[w-1]-ratio[w-2])
        y2 = float(ratio[w]-ratio[w-1])

        repulsive_part = float((bayesianEstimate_2_4-grid-attraction_2_4)[w].cpu().detach())
        attractive_part = float((attraction_2_4)[w].cpu().detach())
        repulsivePredominance = math.pow(repulsive_part, 2) / (math.pow(repulsive_part,2) + math.pow(attractive_part,2))

        hasAttraction = False
        repulsion = False
        if sign(float(prior_smoothed[w]-prior_smoothed[w-1])) == sign(y2):
            hasAttraction=True
        if sign(float(volume_smoothed[w]-volume_smoothed[w-1])) == -sign(y2):
            repulsion=True
        if hasAttraction and repulsion:
            c = "gray"
        elif hasAttraction:
            c = "green"
        elif repulsion:
            c = "blue"
        else:
            c = "black"

        axis.plot([float(grid[w-1])/2, float(grid[w])/2], [0+sign(y2),0+sign(y2)], color=["green", "red", "gray"][{-1 : 0, 0 : 2, 1 : 1}[sign(y2)]])

        y2 = repulsive_part
        axis.plot([float(grid[w-1])/2, float(grid[w])/2], [-8+sign(y2),-8+sign(y2)], color=["green", "red", "gray"][{-1 : 0, 0 : 2, 1 : 1}[sign(y2)]])

        y2 = attractive_part
        axis.plot([float(grid[w-1])/2, float(grid[w])/2], [-16+sign(y2),-16+sign(y2)], color=["green", "red", "gray"][{-1 : 0, 0 : 2, 1 : 1}[sign(y2)]])

     for w in range(2, len(x_set)):
        y2 = y_set_4[w]
        axis.plot([float(grid[x_set[w-1]])/2, float(grid[x_set[w]])/2], [8+sign(y2),8+sign(y2)], color=["green", "red", "gray"][{-1 : 0, 0 : 2, 1 : 1}[sign(y2)]])

     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_PPRatio.pdf")
     if SHOW_PLOT:

        plt.show()
     plt.close()

############################3

lowestError = 100000

xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))

yValues = []
for y in observations_y:
   yValues.append(int( torch.argmin((grid - y).abs())))

xValues = MakeLongTensor(xValues)
yValues = MakeLongTensor(yValues)

model(grid)
