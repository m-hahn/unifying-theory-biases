import matplotlib.pyplot as plt
import math
import torch
from util import computeCircularMean, computeCenteredMean, computeCircularSD, MakeFloatTensor, computeCircularMeanWeighted, computeCircularSDWeighted, computeCircularConfidenceInterval, bringCircularBiasCloseToZero
def setData(**kwargs):
    global SQUARED_STIMULUS_SIMILARITY
    SQUARED_STIMULUS_SIMILARITY = kwargs["SQUARED_STIMULUS_SIMILARITY"]
    global GRID
    GRID = kwargs["GRID"]
    global x
    x = kwargs["x"]
    global Subject
    Subject = kwargs["Subject"]
    global x_set
    x_set = kwargs["x_set"]
    global observations_y
    observations_y=kwargs["observations_y"]
    global xValues
    xValues=kwargs["xValues"]
    global Duration
    Duration=kwargs["Duration"]
    global grid
    grid=kwargs["grid"]
    global StimulusSD
    StimulusSD=kwargs["StimulusSD"]

def retrieveObservations(x, StimulusSD_, Subject_, Duration_, meanMethod = "circular"):
     y_set = []
     sd_set = []
     for x in x_set:
        if Subject_ is not None:
           if Duration_ is not None:
              y_here = observations_y[torch.logical_and(torch.logical_and(torch.logical_and(xValues == x, StimulusSD == StimulusSD_), Subject==Subject_), Duration==Duration_)]
           else:
              y_here = observations_y[torch.logical_and(torch.logical_and(xValues == x, StimulusSD == StimulusSD_), Subject==Subject_)]
        else:
           if Duration_ is not None:
              y_here = observations_y[torch.logical_and(torch.logical_and(xValues == x, StimulusSD == StimulusSD_), Duration==Duration_)]
           else:
              y_here = observations_y[torch.logical_and(xValues == x, StimulusSD == StimulusSD_)]
        Mean1 = computeCircularMean(y_here)
        Mean2 = computeCenteredMean(y_here, grid[x])
        if abs(Mean1-grid[x]) > 180 and Mean1 > 180:
            Mean1 = Mean1-360
        elif abs(Mean1-grid[x]) > 180 and Mean1 < 180:
            Mean1 = Mean1+360
        if abs(Mean1-Mean2) > 180:
            print(y_here)
            print(Mean1, Mean2, grid[x])
            assert False
        if meanMethod == "circular":
            Mean = Mean1
        elif meanMethod == "centered":
            Mean = Mean2
        else:
            assert False

        bias = Mean - grid[x]
        if abs(bias) > 180:
            bias = bias+360
        y_set.append(bias)
        sd_set.append(computeCircularSD(y_here))
     return y_set, sd_set
def retrieveAndSmoothObservations(x, StimulusSD_, Subject_=None, Duration_=None, meanMethod = "circular"):
     y_set = []
     sd_set = []
     for x in x_set:
        if Subject_ is not None:
           if Duration_ is not None:
              y_here = observations_y[torch.logical_and(torch.logical_and(torch.logical_and(xValues == x, StimulusSD == StimulusSD_), Subject==Subject_), Duration=Duration_)]
           else:
              y_here = observations_y[torch.logical_and(torch.logical_and(xValues == x, StimulusSD == StimulusSD_), Subject==Subject_)]
        else:
           if Duration_ is not None:
              y_here = observations_y[torch.logical_and(torch.logical_and(xValues == x, StimulusSD == StimulusSD_), Duration==Duration_)]
           else:
              y_here = observations_y[torch.logical_and(xValues == x, StimulusSD == StimulusSD_)]

        Mean1 = computeCircularMean(y_here)
        Mean2 = computeCenteredMean(y_here, grid[x])
        if abs(Mean1-grid[x]) > 180 and Mean1 > 180:
            Mean1 = Mean1-360
        elif abs(Mean1-grid[x]) > 180 and Mean1 < 180:
            Mean1 = Mean1+360

        if abs(Mean1-Mean2) > 180:
            print(y_here)
            print(Mean1, Mean2, grid[x])
            assert False
        if meanMethod == "circular":
            Mean = Mean1
        elif meanMethod == "centered":
            Mean = Mean2
        else:
            assert False

        bias = Mean - grid[x]
        if abs(bias) > 180:
            bias = bias+360
        y_set.append(bias)
        sd_set.append(computeCircularSD(y_here))

     x_set_tensor = MakeFloatTensor(x_set) * 360 / GRID
     y_set_tensor = MakeFloatTensor(y_set)
     sd_set_tensor = MakeFloatTensor(sd_set)
     kappa = 100
     KERNEL = torch.nn.functional.softmax(kappa * SQUARED_STIMULUS_SIMILARITY(x_set_tensor.view(-1,1) - x_set_tensor.view(1,-1)), dim=0)
     y_set = (y_set_tensor.view(-1, 1) * KERNEL).sum(dim=0)
     sd_set = (sd_set_tensor.view(-1, 1) * KERNEL).sum(dim=0)

     return x_set, y_set, sd_set, None


def retrieveAndSmoothObservationsAcrossSubjects(noise):
     y_set_28_overall = []
     sd_set_28_overall = []
     for s in [1,2,3,4,5]:
        x_set, y_set_28, sd_set_28, _ = retrieveAndSmoothObservations(x, noise, Subject_=s)
        y_set_28_overall.append(y_set_28)
        sd_set_28_overall.append(sd_set_28)
     y_set_28 = torch.stack(y_set_28_overall, dim=0).mean(dim=0)
     sd_set_28 = torch.stack(sd_set_28_overall, dim=0).mean(dim=0)
     return x_set, y_set_28, sd_set_28, None

#
#def retrieveAndSmoothObservationsWithErrBySubjects(x, StimulusSD_):
#     y_set = []
#     y_set_var = []
#     for x in x_set:
#       y_set_28_overall = []
#       sd_set_28_overall = []
#       for s in [1,2,3,4,5]:
#          y_here = observations_y[torch.logical_and(xValues == x, torch.logical_and(StimulusSD == StimulusSD_, Subject==s))]
#          print(y_here)
#          Mean1 = computeCircularMean(y_here)
#          y_set_28_overall.append(Mean1)
#       y_set_28 = MakeFloatTensor(y_set_28_overall)
#       y_set_var.append(computeCircularSD(y_set_28))
#       y_set.append(computeCircularMean(y_set_28))
#     y_set = MakeFloatTensor(y_set)
#     y_set_var = MakeFloatTensor(y_set_var) / math.sqrt(5)
#     y_set = bringCircularBiasCloseToZero(y_set - grid[x_set])
##     y_set_var = bringCircularBiasCloseToZero(y_set_var - grid[x_set])
#
#
#     print(y_set.size(), y_set_var.size(), y_set, y_set_var)
#     return x_set, y_set, None, y_set_var
#


#def retrieveAndSmoothObservationsWithErr(x, StimulusSD_, Subject_=None, Duration_=None, meanMethod = "circular"):
#   y_set_boot = []
#   sd_set_boot = []
#   for _ in range(100):
#     boot = torch.randint(low=0, high=observations_y.size()[0]-1, size=observations_y.size())
#     assert (observations_y.size()[0]) == 4320 # this is the number of trials
#     observations_y__ = observations_y[boot]
#     xValues__ = xValues[boot]
#     Duration__ = Duration[boot]
#     StimulusSD__ = StimulusSD[boot]
#     y_sets = []
#     sd_sets = []
#     y_set = []
#     sd_set = []
#     trialCount = []
#     for x in x_set:
#        y_here = observations_y__[torch.logical_and(xValues__ == x, StimulusSD__ == StimulusSD_)]
#        trialCount.append(y_here.size()[0]) # this is the number of trials
#        Mean1 = computeCircularMean(y_here)
#        y_set.append(Mean1)
#     print("min and max trial counts across stimulus values", min(trialCount), max(trialCount), len(trialCount), trialCount)
#     x_set_tensor = MakeFloatTensor(x_set) * 360 / GRID
#     y_set_tensor = MakeFloatTensor(y_set)
#     sd_set_tensor = MakeFloatTensor(sd_set)
#     y_set_boot.append(y_set_tensor)
#   print(set(list(xValues.numpy().tolist())), x_set)
#
#   y_set = torch.stack(y_set_boot, dim=0)
#
#
#   if True:
#     y_set_var = computeCircularConfidenceInterval(y_set) #.pow(2).mean(dim=0) - y_set.mean(dim=0).pow(2)).sqrt()
#   else:
#     y_set_var = computeCircularSDWeighted(y_set)
#   print(y_set)
#   print(y_set.size())
#   y_set = computeCircularMeanWeighted(y_set)
#   print(y_set.size())
##   figure, axis = plt.subplots(1, 2, figsize=(8,8))
##   for i in y_set_boot:
##     axis[0].plot(grid[x_set], i, alpha=0.1)
##   axis[1].plot(grid[x_set], y_set)
##   plt.show()
#
#
#   y_set = bringCircularBiasCloseToZero(y_set - grid[x_set])
#   y_set_var[:,0] = bringCircularBiasCloseToZero(y_set_var[:,0] - grid[x_set])
#   y_set_var[:,1] = bringCircularBiasCloseToZero(y_set_var[:,1] - grid[x_set])
#
#
#
##   _, y_set, _, _ = retrieveAndSmoothObservations(x,StimulusSD_)
#
#   return x_set, y_set, None, y_set_var
#
# 





def retrieveAndSmoothObservationsWithErrBootPerObs(x, StimulusSD_):
   # StimulusSD_ is the stimulus noise level
   y_set_boot = []
   y_set_CI_boot = []
   trialCount = []
   for x in x_set:
     y_set = []
     for _ in range(100):
        y_here = observations_y[torch.logical_and(xValues == x, StimulusSD == StimulusSD_)]
        boot = torch.randint(low=0, high=y_here.size()[0]-1, size=y_here.size())
#        print(y_here.size()[0])
        Mean1 = computeCircularMean(y_here[boot])
        y_set.append(Mean1)
     trialCount.append(y_here.size()[0]) # this is the number of trials
     print("min and max trial counts across stimulus values", min(trialCount), max(trialCount), len(trialCount), trialCount)
     y_set = MakeFloatTensor(y_set).unsqueeze(1) # turn the bootstrapping samples into a tensor
     y_set_CI = computeCircularConfidenceInterval(y_set) # compute the confidence interval
     y_set = computeCircularMeanWeighted(y_set) # compute the mean
     y_set_boot.append(y_set) # store the mean
     y_set_CI_boot.append(y_set_CI) # store the confidence interval
   

#   x_set_tensor = MakeFloatTensor(x_set) * 360 / GRID
  
   # collect the bootstrapped means and confidence intervals across stimulus values
   y_set = torch.cat(y_set_boot, dim=0)
   y_set_CI = torch.cat(y_set_CI_boot, dim=0)

  # print(y_set.size(), y_set_CI.size())

   # express the quantities in terms of the bias, rather than raw responses themselves
   y_set = bringCircularBiasCloseToZero(y_set - grid[x_set])
   y_set_CI[:,0] = bringCircularBiasCloseToZero(y_set_CI[:,0] - grid[x_set])
   y_set_CI[:,1] = bringCircularBiasCloseToZero(y_set_CI[:,1] - grid[x_set])
 #  print(y_set)
#   print(y_set_CI)

#   _, y_set, _, _ = retrieveAndSmoothObservations(x,StimulusSD_)

   return x_set, y_set, None, y_set_CI

 



