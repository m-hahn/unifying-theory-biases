import torch
from util import MakeZeros

from util import MakeFloatTensor

from util import MakeLongTensor

from util import toFactor

with open("data/SOLOMON/rawObNu.dat", "r") as inFile:
    data = [x.split("\t") for x in inFile.read().strip().split("\n")]
    data = [[float(q) for q in w] for w in data]
    Target = 0
    StimulusSD = 1
    Report = 2

target = 2*MakeFloatTensor([x[Target]+90 for x in data])
response = 2*MakeFloatTensor([x[Report]+90 for x in data])
StimulusSD = 2*MakeFloatTensor([x[StimulusSD] for x in data])
Duration = MakeLongTensor([[0.1, 0.5, 1].index(x[3]) for x in data])
Subject = MakeLongTensor([x[4] for x in data])

observations_x = target
observations_y = response

