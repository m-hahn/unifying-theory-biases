python3 evaluateCrossValidationResults_Tomassini_180.py
python3 RunTomassini_Zero_NaturalPrior_Zero_VIZ.py 0 0 0.1 180 NO_PLOT
python3 RunTomassini_Zero_UniPrior_Zero_VIZ.py 0 0 0.1 180 NO_PLOT
python3 RunTomassini_Zero_UniEncoding_Zero_VIZ.py 0 0 0.1 180 NO_PLOT
python3 RunTomassini_Zero_FreePrior_Zero_VIZ.py 0 0 0.1 180 NO_PLOT
for p in 2 4 6 8 10
do
python3 RunTomassini_CosinePLoss_NaturalisticPrior_VIZ.py $p 0 0.1 180 NO_PLOT
python3 RunTomassini_CosinePLoss_UniPrior_VIZ.py $p 0 0.1 180 NO_PLOT
python3 RunTomassini_CosinePLoss_UniEncoding_VIZ.py $p 0 0.1 180 NO_PLOT
python3 RunTomassini_CosinePLoss_FreePrior_VIZ.py $p 0 0.1 180 NO_PLOT
done

python3 RunTomassini_CosinePLoss_NaturalisticPrior_VIZ_OnlyModel.py 8 0 0.1 180 NO_PLOT
python3 RunTomassini_CosinePLoss_NaturalisticPrior_VIZ_OnlyHuman_MainPaper.py 8 0 0.1 180 NO_PLOT
python3 RunTomassini_CosinePLoss_UniPrior_VIZ_OnlyModel.py 8 0 0.1 180 NO_PLOT

for p in 2 4 6 8 10
do
  
  python3 RunTomassini_CosinePLoss_NaturalisticPrior_VIZ.py $p 0 0.1 180
  python3 RunTomassini_CosinePLoss_UniPrior_VIZ.py $p 0 0.1 180
  python3 RunTomassini_CosinePLoss_UniEncoding_VIZ.py $p 0 0.1 180
  python3 RunTomassini_CosinePLoss_FreePrior_VIZ.py $p 0 0.1 180
  

done


python3 RunTomassini_Zero_FreePrior_Zero_VIZ.py 0 0 0.1 180
python3 RunTomassini_Zero_UniEncoding_Zero_VIZ.py 0 0 0.1 180
python3 RunTomassini_Zero_NaturalPrior_Zero_VIZ.py 0 0 0.1 180
python3 RunTomassini_Zero_UniPrior_Zero_VIZ.py 0 0 0.1 180

python3 RunFalseTomassini_CosinePLoss_FreePrior_VIZ.py 8 0 0.1 180

python3 RunTomassini_CosinePLoss_NaturalisticPrior_VIZ_OnlyHuman_MainPaper_ErrBar.py 8 0 0.1 180
python3 RunTomassini_CosinePLoss_NaturalisticPrior_VIZ_OnlyModel_MainPaper.py 8 0 0.1 180
python3 RunTomassini_CosinePLoss_UniPrior_VIZ_OnlyModel_MainPaper.py 8 0 0.1 180


python3 RunTomassini_CosinePLoss_FreePrior_OnSimData_VIZ.py 8 0 0.1 180
python3 RunTomassini_CosinePLoss_NaturalisticPrior_OnSimData_VIZ.py 8 0 0.1 180
python3 RunTomassini_CosinePLoss_UniPrior_OnSimData_VIZ.py 8 0 0.1 180

python3 evaluateCrossValidationResults_Tomassini_180_FreePrior_OnSimData.py
python3 evaluateCrossValidationResults_Tomassini_180_NaturalisticPrior_OnSimData.py
python3 evaluateCrossValidationResults_Tomassini_180_UniPrior_OnSimData.py

# For Figure 7 in main paper
python3 RunTomassini_CosinePLoss_NaturalisticPrior_VIZ.py 8 0 0.1 180


python3 evaluateCrossValidationResults_Tomassini_180_FreePrior_OnSimData.py ; python3 evaluateCrossValidationResults_Tomassini_180_NaturalisticPrior_OnSimData.py ; python3 evaluateCrossValidationResults_Tomassini_180_UniPrior_OnSimData.py




