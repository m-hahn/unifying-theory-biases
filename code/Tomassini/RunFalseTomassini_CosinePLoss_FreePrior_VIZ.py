import computations
import getObservations
import glob
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import scipy.special
import sys
import torch
from computations import *
from cosineEstimator import CosineEstimator
from getObservations import retrieveObservations
from loadModel import loadModel
from loadTomassini import *
from matplotlib import rc
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import makeGridIndicesCircular
from util import product
from util import savePlot
from util import sign
rc('font', **{'family':'FreeSans'})

OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
FOLD_HERE = int(sys.argv[2])
REG_WEIGHT = float(sys.argv[3])
GRID = int(sys.argv[4])
print(P)
FILE = f"logs/CROSSVALID/{__file__.replace('_VIZ', '')}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"
SHOW_PLOT = (len(sys.argv) < 6) or (sys.argv[5] == "SHOW_PLOT")
DEVICE = 'cpu'

if not os.path.exists(FILE):
    assert False, FILE

##############################################
# Helper Functions dependent on the device

##############################################

N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

Fold = 0*Subject
for i in range(min(Subject), max(Subject)+1):
    trials = [j for j in range(Subject.size()[0]) if Subject[j] == i]
    randomGenerator.shuffle(trials)
    foldSize = int(len(trials)/N_FOLDS)
    for k in range(N_FOLDS):
        Fold[trials[k*foldSize:(k+1)*foldSize]] = k

observations_x = target
observations_y = response

##############################################
# Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 360

CIRCULAR = True
INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
grid, grid_indices_here = makeGridIndicesCircular(GRID, MIN_GRID, MAX_GRID)

# Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

#############################################################
# Part: Initialize the model
init_parameters = {}
init_parameters["sigma2_stimulus"] = MakeFloatTensor(3*[0]).view(3)
init_parameters["log_motor_var"] = MakeFloatTensor([8]).view(1)
init_parameters["sigma_logit"] = MakeFloatTensor(2*[-3]).view(2)
init_parameters["mixture_logit"] = MakeFloatTensor([-1]).view(1)
init_parameters["prior"] = MakeZeros(GRID)
init_parameters["volume"] = MakeZeros(GRID)
loadModel(FILE, init_parameters)
assert "volume" in init_parameters
for _, y in init_parameters.items():
    y.requires_grad = True

##############################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 2*math.pi

# Part: Specify `similariy` or `difference` functions.
## These are negative squared distances (for interval spaces) or
## trigonometric functions (for circular spaces), with
## some extra factors for numerical purposes.
## Exponentiating a `similarity` function and normalizing
## is equivalent to the Gaussian / von Mises density.
## The purpose of specifying these as `closeness` or `distance`,
## rather than simply calling squared or trigonometric
## functions is to  flexibly reuse the same model code for
## both interval and circular spaces.
def SQUARED_STIMULUS_DIFFERENCE(x):
    return torch.sin(math.pi*x/180)
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(math.pi*x/180)
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(x)
def SQUARED_SENSORY_DIFFERENCE(x):
    return torch.sin(x)

##############################################
# Import/define the appropriate estimator for minimizing the loss function
assert P >= 2
CosineEstimator.set_parameters(GRID=GRID, OPTIMIZER_VERBOSE=OPTIMIZER_VERBOSE, P=P, SQUARED_SENSORY_DIFFERENCE=SQUARED_SENSORY_DIFFERENCE, SQUARED_SENSORY_SIMILARITY=SQUARED_SENSORY_SIMILARITY)

#############################################################
# Part: Run the model. This function implements the model itself:
## calculating the likelihood of a given dataset under that model
## and---if the computePredictions argument is set to True--- computes
## the bias and variability of the estimate.
def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, StimulusSD=None, computePredictions=False, subject=None, sigma_stimulus=None, sigma2_stimulus=None, Duration_=None, folds=None, lossReduce='mean'):

 # Part: Obtain the motor variance by exponentiating the appropriate model parameter
 motor_variance = torch.exp(- parameters["log_motor_var"])
 # Part: Obtain the sensory noise variance. We parameterize it as a fraction of the squared volume of the size of the sensory space
 sigma2 = (SENSORY_SPACE_VOLUME * SENSORY_SPACE_VOLUME)*torch.sigmoid(sigma_logit)
 # Part: Obtain the transfer function as the cumulative sum of the discretized resource allocation (referred to as `volume` element due to the geometric interpretation by Wei&Stocker 2015)
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 if True:
  folds = MakeLongTensor(folds)
  if subject is not None:
    MASK = torch.logical_and(torch.logical_and(torch.logical_and(Subject==subject, StimulusSD == sigma_stimulus), Duration == Duration_), (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]
  else:
    MASK = torch.logical_and(torch.logical_and(StimulusSD == sigma_stimulus, Duration == Duration_), (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]

  if sigma2_stimulus > 0:
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))
    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)
  else:
    assert False

  # Part: Compute sensory likelihoods. Across both interval and
  ## circular stimulus spaces, this amounts to exponentiaring a
  ## `similarity`
  sensory_likelihoods = torch.softmax(((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2)) + volumeElement.unsqueeze(1).log(), dim=0)

  if sigma2_stimulus == 0:
    assert False
    likelihoods = sensory_likelihoods
  else:
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  # Compute posterior
  posterior = prior.unsqueeze(1) * likelihoods.t()
  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  # Estimator
  bayesianEstimate = CosineEstimator.apply(grid_indices_here, posterior)

  # Caculate motor likelihood
  error = (SQUARED_STIMULUS_SIMILARITY(360/GRID*bayesianEstimate.unsqueeze(0) - responses.unsqueeze(1)))
  log_normalizing_constant = torch.logsumexp((SQUARED_STIMULUS_SIMILARITY(grid))/motor_variance, dim=0) + math.log(2 * math.pi / GRID)
  log_motor_likelihoods = (error/motor_variance) - log_normalizing_constant
  motor_likelihoods = torch.exp(log_motor_likelihoods)

  # Mixture of estimation and uniform response
  uniform_part = torch.sigmoid(parameters["mixture_logit"])

  ## The full likelihood then consists of a mixture of the motor likelihood calculated before, and the uniform
  ## distribution on the full space.
  motor_likelihoods = (1-uniform_part) * motor_likelihoods + (uniform_part / (2*math.pi) + 0*motor_likelihoods)

  # Now the loss is obtained by marginalizing out m from the motor likelihood
  if lossReduce == 'mean':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().mean()
  elif lossReduce == 'sum':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().sum()
  else:
    assert False

  if computePredictions:
     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS
     bayesianEstimate_avg_byStimulus = computeCircularMeanWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(bayesianEstimate_byStimulus, likelihoods)

     bayesianEstimate_avg_byStimulus = torch.where((bayesianEstimate_avg_byStimulus-grid).abs()<180, bayesianEstimate_avg_byStimulus, torch.where(bayesianEstimate_avg_byStimulus > 180, bayesianEstimate_avg_byStimulus-360, bayesianEstimate_avg_byStimulus+360))
     assert float(((bayesianEstimate_avg_byStimulus-grid).abs()).max()) < 180, float(((bayesianEstimate_avg_byStimulus-grid).abs()).max())
     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = computeCircularMeanWeighted(posteriorMaxima.unsqueeze(1), likelihoods)
     encodingBias = computeCircularMeanWeighted(grid.unsqueeze(1), likelihoods)
     attraction = (posteriorMaxima-encodingBias)
     attraction1 = attraction
     attraction2 = attraction+360
     attraction3 = attraction-360
     attraction = torch.where(attraction1.abs() < 180, attraction1, torch.where(attraction2.abs() < 180, attraction2, attraction3))
  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction

getObservations.setData(x_set=x_set, observations_y=observations_y, xValues=xValues, Duration=Duration, grid=grid, x=x, StimulusSD=StimulusSD, GRID=GRID, Subject=Subject, SQUARED_STIMULUS_SIMILARITY=SQUARED_STIMULUS_SIMILARITY)

computations.setData(STIMULUS_SPACE_VOLUME=STIMULUS_SPACE_VOLUME, GRID=GRID)

def model(grid):

  for iteration in range(1):
   parameters = init_parameters

   ## In each iteration, recompute
   ## - the resource allocation (called `volume' due to a geometric interpretation)
   ## - the prior

   volume = SENSORY_SPACE_VOLUME * torch.softmax(parameters["volume"], dim=0)
   prior = torch.softmax(parameters["prior"], dim=0)

   loss = 0
   if iteration % 500 == 0:
     assigned = ["PRI", None, "ENC", None, "ATT", "REP", "TOT", None, "HUM"]
     for w, k in enumerate(assigned):
         globals()[k] = w
     gridspec = dict(width_ratios=[1,0.2,1,0.2,1,1,1,0,0])
     figure, axis = plt.subplots(1, len(assigned), figsize=(10, 1.3*0.8*1.8), gridspec_kw=gridspec)
     plt.tight_layout()
     figure.subplots_adjust(wspace=0.1, hspace=0.0)

     assert PRI == 0
     assert ENC == 2
     assert ATT == 4
     assert REP == 5
     assert TOT == 6
     assert HUM == 8
     PAD = [1,3,7,8]
     resources_joint = [[],[]]
     for DURATION in range(3):
      for stimulus_noise, color in zip([0,1],["green","red"]):
       kappa_stimulus = 1/(torch.exp(-init_parameters["sigma2_stimulus"][DURATION]) * math.pow(180 / math.pi,2))
       kappa = 1/(4*math.pi*torch.sigmoid(init_parameters["sigma_logit"][stimulus_noise]))
       resources_joint[stimulus_noise].append(2*computeResourcesWithStimulusNoise(volume.detach(), kappa, kappa_stimulus).detach())
     for stimulus_noise, color in zip([0,1],["green","red"]):
       axis[ENC].plot(grid, torch.stack(resources_joint[stimulus_noise], dim=0).mean(dim=0).detach(), color=color)
     axis[PRI].plot(grid, prior.detach(), color="gray")
     axis[PRI].set_ylim(0, 1.1*float(prior.max()))
     axis[ENC].set_ylim(0,0.25)
     x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

   bayesianEstimate_28_by_duration = MakeZeros(GRID)
   bayesianEstimate_4_by_duration = MakeZeros(GRID)
   bayesianEstimate_28_by_duration_UNIFORM = MakeZeros(GRID)
   bayesianEstimate_4_by_duration_UNIFORM = MakeZeros(GRID)

   prior_Uniform = MakeZeros(GRID) + 1/GRID

   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   for DURATION in range(3):
    for SUBJECT in [1]:
     loss_2_28, bayesianEstimate_2_28, bayesianEstimate_sd_byStimulus_2_28, attraction_2_28 = computeBias(xValues, init_parameters["sigma_logit"][1], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, StimulusSD=StimulusSD, computePredictions=(iteration%500 == 0), subject=None, sigma_stimulus=28, sigma2_stimulus=torch.exp(-init_parameters["sigma2_stimulus"][DURATION]), Duration_=DURATION, folds=testFolds, lossReduce='sum')
     loss += loss_2_28

     loss_2_4, bayesianEstimate_2_4, bayesianEstimate_sd_byStimulus_2_4, attraction_2_4 = computeBias(xValues, init_parameters["sigma_logit"][0], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, StimulusSD=StimulusSD, computePredictions=(iteration%500 == 0), subject=None, sigma_stimulus=4, sigma2_stimulus=torch.exp(-init_parameters["sigma2_stimulus"][DURATION]), Duration_=DURATION, folds=testFolds, lossReduce='sum')
     loss += loss_2_4

     if iteration % 500 == 0:
       bayesianEstimate_28_by_duration = bayesianEstimate_28_by_duration + bayesianEstimate_2_28.detach()/3
       bayesianEstimate_4_by_duration = bayesianEstimate_4_by_duration + bayesianEstimate_2_4.detach()/3

   if iteration % 500 == 0:
     y_set_28_overall = []
     sd_set_28_overall = []
     y_set_4_overall = []
     sd_set_4_overall = []

     x_set_tensor = MakeFloatTensor(x_set) * 360 / GRID
     for s in [1,2,3,4,5]:
        y_set_28, sd_set_28 = retrieveObservations(x, 28, s, None)

        print(sd_set_28)
        y_set_4, sd_set_4 = retrieveObservations(x, 4, s, None)
        y_set_4_tensor = MakeFloatTensor(y_set_4)
        sd_set_4_tensor = MakeFloatTensor(sd_set_4)
        kappa = 200
        KERNEL = torch.nn.functional.softmax(kappa * SQUARED_STIMULUS_SIMILARITY(x_set_tensor.view(-1,1) - x_set_tensor.view(1,-1)), dim=0)
        y_set_4 = (y_set_4_tensor.view(-1, 1) * KERNEL).sum(dim=0)
        sd_set_4 = (sd_set_4_tensor.view(-1, 1) * KERNEL).sum(dim=0)

        y_set_28_tensor = MakeFloatTensor(y_set_28)
        sd_set_28_tensor = MakeFloatTensor(sd_set_28)
        kappa = 200
        KERNEL = torch.nn.functional.softmax(kappa * SQUARED_STIMULUS_SIMILARITY(x_set_tensor.view(-1,1) - x_set_tensor.view(1,-1)), dim=0)
        y_set_28 = (y_set_28_tensor.view(-1, 1) * KERNEL).sum(dim=0)
        sd_set_28 = (sd_set_28_tensor.view(-1, 1) * KERNEL).sum(dim=0)

        y_set_28_overall.append(y_set_28)
        sd_set_28_overall.append(sd_set_28)
        y_set_4_overall.append(y_set_4)
        sd_set_4_overall.append(sd_set_4)

     y_set_28 = torch.stack(y_set_28_overall, dim=0).mean(dim=0)
     sd_set_28 = torch.stack(sd_set_28_overall, dim=0).mean(dim=0)
     y_set_4 = torch.stack(y_set_4_overall, dim=0).mean(dim=0)
     sd_set_4 = torch.stack(sd_set_4_overall, dim=0).mean(dim=0)

     axis[HUM].plot([grid[q] for q in x_set], y_set_28/2, color="red")
     axis[HUM].plot([grid[q] for q in x_set], y_set_4/2, color="green")

     axis[REP].plot(grid, (bayesianEstimate_28_by_duration-grid-attraction_2_28).detach()/2, color="red")
     axis[ATT].plot(grid, (attraction_2_28).detach()/2, color="red")
     axis[TOT].plot(grid, (bayesianEstimate_28_by_duration-grid).detach()/2, color="red")

     axis[REP].plot(grid, (bayesianEstimate_4_by_duration-grid-attraction_2_4).detach()/2, color="green")
     axis[ATT].plot(grid, (attraction_2_4).detach()/2, color="green")
     axis[TOT].plot(grid, (bayesianEstimate_4_by_duration-grid).detach()/2, color="green")

     for w in PAD:
       axis[w].set_visible(False)

     axis[PRI].set_yticks([0])
     for w in [REP, TOT]:
         axis[w].tick_params(labelleft=False)

     for w in range(len(assigned)):
       if w not in [ENC, PRI, ATT]:
          axis[w].tick_params(labelbottom=False)
       axis[w].spines['top'].set_visible(False)
       axis[w].spines['right'].set_visible(False)

     for Y in [ATT,REP,TOT,HUM]:
             axis[Y].set_yticks(ticks=[-4,0,4], labels=["-4", "0", "4"])
             axis[Y].set_ylim(-5,5)
     if True:
       axis[ENC].set_title("Resources", fontsize=20)
       axis[PRI].set_title("Prior", fontsize=20)
       axis[REP].set_title("Repulsion", fontsize=20)
       axis[ATT].set_title("Attraction", fontsize=20)
     if True:
       axis[TOT].set_title("Total Bias", fontsize=20)
       axis[HUM].set_title("Data", fontsize=20)

   if iteration % 500 == 0:
     for w in range(len(assigned)):
         axis[w].set_xticks(ticks=[0,180,360], labels=["0", "90", "180"])
         axis[w].tick_params(axis='x', labelsize=17, width=0.4)
         axis[w].tick_params(axis='y', labelsize=17, width=0.4)
     figure.subplots_adjust(bottom=0.23,top=0.77)
     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.pdf", transparent=True)

############################3

# Project the stimuli onto the discrete grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

model(grid)
