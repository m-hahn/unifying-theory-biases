import math

def setData(**kwargs):
    global STIMULUS_SPACE_VOLUME
    STIMULUS_SPACE_VOLUME = kwargs["STIMULUS_SPACE_VOLUME"]
    global GRID
    GRID = kwargs["GRID"]




def computeResources(volume, kappa):
    return volume * math.sqrt(kappa) * GRID / STIMULUS_SPACE_VOLUME
def computeResourcesWithStimulusNoise(volume, kappa, kappa_stimulus):
    return 1/(1/computeResources(volume, kappa).pow(2) + 1/kappa_stimulus).sqrt()
def computeResourcesWithStimulusNoise_TakesVariance(volume, kappa, variance_stimulus):
    return 1/(1/computeResources(volume, kappa).pow(2) + variance_stimulus).sqrt()

