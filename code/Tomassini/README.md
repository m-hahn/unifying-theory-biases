# Tomassini

* [Collect quantitative results](evaluateCrossValidationResults_Tomassini_180.py), [NLL by exponent (full)](figures/evaluateCrossValidationResults_Tomassini_180.py_RelativeLF.pdf), [NLL by exponent (simple)](figures/evaluateCrossValidationResults_Tomassini_180.py_simple.pdf), [Results](output/evaluateCrossValidationResults_Tomassini_180.py.txt)

# Model specification:

 `sigma2_stimulus` (2)
 `log_motor_var` (1) 
 `sigma_logit` (5)
 `chance_prob_logit` (1)
 `mixture_logit` (1)
 `volume` (180)

* Parameterization of prior

# Model fitting

* MAP estimator:
* [Naturalistic prior](RunTomassini_Zero_NaturalPrior_Zero.py), 
[visualization script](RunTomassini_Zero_NaturalPrior_Zero_VIZ.py)
[resulting plot](figures/RunTomassini_Zero_NaturalPrior_Zero_VIZ.py_0_0_0.1_180.pdf)






* [Uniform prior](RunTomassini_Zero_UniPrior_Zero.py), 
[visualization script](RunTomassini_Zero_UniPrior_Zero_VIZ.py)

[resulting plot](figures/RunTomassini_Zero_UniPrior_Zero_VIZ.py_0_0_0.1_180.pdf)

* [Uniform encoding](RunTomassini_Zero_UniEncoding_Zero.py), 
[visualization script](RunTomassini_Zero_UniEncoding_Zero_VIZ.py)
[resulting plot](figures/RunTomassini_Zero_UniEncoding_Zero_VIZ.py_0_0_0.1_180.pdf)

* [Free prior](RunTomassini_Zero_FreePrior_Zero.py), 
[visualization script](RunTomassini_Zero_FreePrior_Zero_VIZ.py)
[resulting plot](figures/RunTomassini_Zero_FreePrior_Zero_VIZ.py_0_0_0.1_180.pdf)

* Positive exponents (cosine-based implementation):
* [Naturalistic prior](RunTomassini_CosinePLoss_NaturalisticPrior.py),
[visualization 1 (for SI)](RunTomassini_CosinePLoss_NaturalisticPrior_VIZ.py),
[visualization 2](RunTomassini_CosinePLoss_NaturalisticPrior_VIZ_OnlyModel.py),
[visualization 3](RunTomassini_CosinePLoss_NaturalisticPrior_VIZ_OnlyModel_MainPaper.py),
resulting plots:
[resulting plot](figures/RunTomassini_CosinePLoss_NaturalisticPrior_VIZ.py_2_0_0.1_180.pdf)
[resulting plot](figures/RunTomassini_CosinePLoss_NaturalisticPrior_VIZ.py_4_0_0.1_180.pdf)
[resulting plot](figures/RunTomassini_CosinePLoss_NaturalisticPrior_VIZ.py_6_0_0.1_180.pdf)
[resulting plot](figures/RunTomassini_CosinePLoss_NaturalisticPrior_VIZ.py_8_0_0.1_180.pdf)
[resulting plot](figures/RunTomassini_CosinePLoss_NaturalisticPrior_VIZ.py_10_0_0.1_180.pdf)

* [Uniform prior](RunTomassini_CosinePLoss_UniPrior.py), 
[visualization 1](RunTomassini_CosinePLoss_UniPrior_VIZ.py),
[visualization 2](RunTomassini_CosinePLoss_UniPrior_VIZ_OnlyModel.py),
[visualization 3](RunTomassini_CosinePLoss_UniPrior_VIZ_OnlyModel_MainPaper.py),
resulting plots:
[resulting plot](figures/RunTomassini_CosinePLoss_UniPrior_VIZ.py_2_0_0.1_180.pdf)
[resulting plot](figures/RunTomassini_CosinePLoss_UniPrior_VIZ.py_4_0_0.1_180.pdf)
[resulting plot](figures/RunTomassini_CosinePLoss_UniPrior_VIZ.py_6_0_0.1_180.pdf)
[resulting plot](figures/RunTomassini_CosinePLoss_UniPrior_VIZ.py_8_0_0.1_180.pdf)

* [Uniform encoding](RunTomassini_CosinePLoss_UniEncoding.py), 
[visualization 1](RunTomassini_CosinePLoss_UniEncoding_VIZ.py),
resulting plots:
[p=2](figures/RunTomassini_CosinePLoss_UniEncoding_VIZ.py_2_0_0.1_180.pdf)
[p=4](figures/RunTomassini_CosinePLoss_UniEncoding_VIZ.py_4_0_0.1_180.pdf)
[p=6](figures/RunTomassini_CosinePLoss_UniEncoding_VIZ.py_6_0_0.1_180.pdf)
[p=8](figures/RunTomassini_CosinePLoss_UniEncoding_VIZ.py_8_0_0.1_180.pdf)
[p=10](figures/RunTomassini_CosinePLoss_UniEncoding_VIZ.py_10_0_0.1_180.pdf)


* [Free prior](RunTomassini_CosinePLoss_FreePrior.py), 
[visualization 1](RunTomassini_CosinePLoss_FreePrior_VIZ.py),
[visualization 3](RunTomassini_CosinePLoss_FreePrior_VIZ_OnlyModel_MainPaper.py),
resulting plots:
[p=2](figures/RunTomassini_CosinePLoss_FreePrior_VIZ.py_2_0_0.1_180.pdf)
[p=4](figures/RunTomassini_CosinePLoss_FreePrior_VIZ.py_4_0_0.1_180.pdf)
[p=6](figures/RunTomassini_CosinePLoss_FreePrior_VIZ.py_6_0_0.1_180.pdf)
[p=8](figures/RunTomassini_CosinePLoss_FreePrior_VIZ.py_8_0_0.1_180.pdf)
[p=10](figures/RunTomassini_CosinePLoss_FreePrior_VIZ.py_10_0_0.1_180.pdf)


## Raw data (human)
* [visualization](RunTomassini_CosinePLoss_NaturalisticPrior_VIZ_OnlyHuman_MainPaper_ErrBar.py), [resulting plot](figures/RunTomassini_CosinePLoss_NaturalisticPrior_VIZ_OnlyHuman_MainPaper_ErrBar.py_8_0_0.1_180.pdf)

## Simulated Data
* Uniform Prior: [simulate](SimulateTomassini_UniformPrior.py), [fit]((RunTomassini_CosinePLoss_UniPrior_OnSimData.py), [visualization](RunTomassini_CosinePLoss_UniPrior_OnSimData_VIZ.py), [resulting plot](figures/RunTomassini_CosinePLoss_UniPrior_OnSimData_VIZ.py_8_0_0.1_180.pdf)
* Natural Prior: [simulate](SimulateTomassini_NaturalPrior.py), [fit](RunTomassini_CosinePLoss_NaturalisticPrior_OnSimData.py), [visualization](RunTomassini_CosinePLoss_NaturalisticPrior_OnSimData_VIZ.py), [resulting plot](figures/RunTomassini_CosinePLoss_NaturalisticPrior_OnSimData_VIZ.py_8_0_0.1_180.pdf)

[fitted model](RunTomassini_CosinePLoss_NaturalisticPrior_OnSimData.py_8_0_0.1_180_527206.txt)
[fitted model](RunTomassini_CosinePLoss_UniPrior_OnSimData.py_8_0_0.1_180_451911.txt)




## Helpers
* [loading a model](loadModel.py)
* [cleaning](selectUnnecessary.py)
* [loading dataset](loadTomassini.py)
* [helper]( process.py)
* [helper]( computations.py)
* [cleaning]( organizeImports.py)
* [cleaning]( selectNotInREADME.py)
* [helper]( createACREXP.py)
* [helper]( util.py)
* [helper]( getObservations.py)
* [collect and plot NLL]( evaluateCrossValidationResults2.py)
* [fit model (batch mode)]( runCrossValidAllTomassini180_LowReg2.py)


## Counterfactual (falsifiability) simulation
* exachanging sensory noise and stimulus noise: [fitting](RunFalseTomassini_CosinePLoss_FreePrior.py), [visualization](RunFalseTomassini_CosinePLoss_FreePrior_VIZ.py)

## For Main Paper
* [P/P Ratio](figures/RunTomassini_CosinePLoss_NaturalisticPrior_VIZ.py_8_0_0.1_180_PPRatio.pdf)
* [Attraction and Repulsion](figures/RunTomassini_CosinePLoss_NaturalisticPrior_VIZ.py_8_0_0.1_180_Magnitudes.pdf)
* model: [resulting plot](figures/RunTomassini_CosinePLoss_FreePrior_VIZ_OnlyModel_MainPaper.py_8_0_0.1_180.pdf)
* human: [resulting plot](figures/RunTomassini_CosinePLoss_NaturalisticPrior_VIZ_OnlyHuman_MainPaper_ErrBar.py_8_0_0.1_180.pdf)

## Simulated Data

* [script for running across exponents](runCrossValidAllTomassini180_LowReg2_OnlyOneFold.py)

RunTomassini_CosinePLoss_UniEncoding_OnSimData.py
RunTomassini_Zero_NaturalPrior_OnSimData.py
RunTomassini_Zero_UniPrior_OnSimData.py
SimulateTomassini_FreePrior.py
RunSynthetic_Tomassini_CosinePLoss_NaturalisticPrior_OnSimData.py
RunTomassini_CosinePLoss_Zero_FreePrior_OnSimData.py
runCrossValidAllTomassini180_LowReg2_OnlyOneFold.py
RunSynthetic_Tomassini_CosinePLoss_FreePrior_OnSimData.py
RunSynthetic_Tomassini_CosinePLoss_FreePrior_OnSimData_VIZ.py
RunSynthetic_Tomassini_CosinePLoss_UniEncoding_OnSimData.py
RunSynthetic_Tomassini_CosinePLoss_UniPrior_OnSimData.py
RunTomassini_CosinePLoss_FreePrior_OnSimData.py



# Properties of Dataset
* [count number of trials](RecordNumberOfTrials.py)





python3 evaluateCrossValidationResults_Tomassini_180_FreePrior_OnSimData.py ; python3 evaluateCrossValidationResults_Tomassini_180_NaturalisticPrior_OnSimData.py ; python3 evaluateCrossValidationResults_Tomassini_180_UniPrior_OnSimData.py

