# Estimators

There are four types of Lp estimator implementations.

At p>0:
* CosineEstimator, [version1](cosineEstimator.py), two versions used [in simulations](../Simulation/README.md) guarding against certain numerical instabilities relevant there (such as exponents close to 0): [version 2](cosineEstimator2.py), [version 3](cosineEstimator3.py).
* [LPEstimator](lpEstimator.py)

Gradient computations are as described in Section S2.1.2.

At p=0:
* MAPCircularEstimator: [base version](mapCircularEstimator.py), and multiple aliases differing only in optimization-related numerical parameters
* MAPIntervalEstimator: [base version](mapIntervalEstimator.py), and multiple aliases differing only in optimization-related numerical parameters
When the settings in the base version did not guarantee numerical stability in some setting (e.g., NaN results or slow convergence of Newton's method), we changed numerical hyperparameters (such as decay of step size and stopping criterion for Newton iterations), leading to a set of aliases of the scripts. We note that no systematic exploration of numerical hyperparameters was attempted; while computationally costly, such an exploration is likely to further improve efficiency of the fitting method.

The MAP estimators, and their gradients, are implemented as described in Section S2.1.2. The difference between Circular and Interval lies in the fact that only the latter includes a (soft approximation to) the boundary of the stimulus space in order to maintain correct gradients even close to the boundary, as described in Section S2.1.2.

