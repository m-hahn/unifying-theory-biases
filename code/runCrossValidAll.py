import os
import random
import sys
import subprocess
script = sys.argv[1]

Ps = [0,2,3,4,5,6,7,8, 10, 12] #, 12, 14]
folds = list(range(10))



for _ in range(100):
  P = random.choice(Ps)
  if (P == 0) != ("Zero" in script):
      continue
  if P % 2 == 1:
    continue
  fold = random.choice(folds)
  if "osine" in script and P == 3: #(P > 8 or P == 3):
    continue
  print(f"logs/CROSSVALID/{script}_{P}_{fold}_{10.0}_{181}.txt")
  if os.path.exists(f"logs/CROSSVALID/{script}_{P}_{fold}_{10.0}_{181}.txt"):
     continue
  subprocess.call([str(q) for q in ["/u/nlp/anaconda/main/anaconda3/envs/py37-mhahn/bin/python", script, P, fold, 10.0, 181]])
