import os
import random
import subprocess
import sys

script = sys.argv[1]

fits = [x for x in os.listdir("logs/SIMULATED_REPLICATE") if x.startswith("SimulateSynthetic_Parameterized")]
random.shuffle(fits)
for f in fits:
# if "FOUR" not in f and "FIT" not in f and "SQRT" not in f and "STEEP" in f and "SQUARE" not in f and (".py_2_" in f or ".py_8_" in f) and ("UNIMODAL" not in f): # and "SHIFTED" in f:
 # if "OtherNoise" not in f and "Subset" not in f:
 #  continue
  print(f)
  subprocess.call([str(q) for q in ["/proj/mhahn.shadow/SOFTWARE/miniconda3/envs/py39-first/bin/python3.9", "runCrossValidAllGardelle180_OnlyOneFold.py", script, f]])
#  subprocess.call([str(q) for q in ["/proj/mhahn.shadow/SOFTWARE/miniconda3/envs/py39-first/bin/python3.9", "runCrossValidAllGardelle180_OnlyOneFold.py", "RunSynthetic_FreePrior_Zero_OnSim.py", f]])

