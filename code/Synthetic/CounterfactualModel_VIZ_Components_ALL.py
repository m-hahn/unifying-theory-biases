import os
import random
import subprocess
import sys

fits = [x for x in os.listdir("logs/SIMULATED_REPLICATE") if x.startswith("SimulateSynthetic_Parameterized")]
random.shuffle(fits)
for f in fits:
 if "UNIFORM_UNIFORM" in f or ("FOUR" not in f and "SQRT" not in f and "STEEP" in f and "SQUARE" not in f): # and "SHIFT" in f:
  print(f)
  if ".py_2_" in f:
    loss = "2"
  elif ".py_8_" in f:
    continue
  else:
    assert False
#  subprocess.call([str(q) for q in ["python3", "RunSynthetic_FreePrior_CosineLoss_OnSim_VIZ_OnlyModel.py", loss, "0", "10.0", "180", f]])
  subprocess.call([str(q) for q in ["python3", "CounterfactualModel_VIZ_Components.py", "2", "0", "10.0", "180", f]])

