# Gardelle



python3 createACREXP.py RunGardelle_FreePrior_CosineLoss_VIZ.py
python3 createACREXP.py RunGardelle_FreePrior_CenteredLoss_VIZ.py
python3 createACREXP.py RunGardelle_NaturalPrior_CosineLoss_VIZ.py
python3 createACREXP.py RunGardelle_UniformEncoding_CosineLoss_VIZ.py
python3 createACREXP.py RunGardelle_UniformPrior_CosineLoss_VIZ.py



python3 RunGardelle_FreePrior_CosineLoss_VIZ_ACREXP.py 8 0 10.0 180 NO_PLOT
python3 RunGardelle_FreePrior_CenteredLoss_VIZ_ACREXP.py 8 0 10.0 180 NO_PLOT
python3 RunGardelle_NaturalPrior_CosineLoss_VIZ_ACREXP.py 8 0 10.0 180 NO_PLOT
python3 RunGardelle_UniformEncoding_CosineLoss_VIZ_ACREXP.py 8 0 10.0 180 NO_PLOT
python3 RunGardelle_UniformPrior_CosineLoss_VIZ_ACREXP.py 8 0 10.0 180 NO_PLOT



python3 RunGardelle_NaturalPrior_CenteredLoss_VIZ_OnlyModel.py 8 0 10.0 180 NO_PLOT
python3 RunGardelle_UniformPrior_CenteredLoss_VIZ_OnlyModel.py 8 0 10.0 180 NO_PLOT
python3 RunGardelle_FreePrior_Zero_VIZ.py 0 0 10.0 180 NO_PLOT
python3 RunGardelle_FreePrior_Zero_VIZ.py 0 0 10.0 180 NO_PLOT
python3 RunGardelle_UniformPrior_Zero_VIZ.py 0 0 10.0 180 NO_PLOT
python3 RunGardelle_NaturalPrior_Zero_VIZ.py 0 0 10.0 180 NO_PLOT
python3 RunGardelle_UniformEncoding_Zero_VIZ.py 0 0 10.0 180 NO_PLOT
python3 RunGardelle_NaturalPrior_CosineLoss_VIZ_OnlyModel.py 8 0 20.0 1800 NO_PLOT
python3 RunGardelle_UniformPrior_CosineLoss_VIZ_OnlyModel.py 8 0 20.0 1800 NO_PLOT
for p in 2 4 6 8 10
do
python3 RunGardelle_NaturalPrior_CenteredLoss_VIZ2_OnlyVar.py $p 0 10.0 180 NO_PLOT
python3 RunGardelle_FreePrior_CosineLoss_VIZ.py $p 0 10.0 180 NO_PLOT
python3 RunGardelle_FreePrior_CenteredLoss_VIZ.py $p 0 10.0 180 NO_PLOT
python3 RunGardelle_NaturalPrior_CosineLoss_VIZ2_OnlyVar.py $p 0 2.0 1800 NO_PLOT
python3 RunGardelle_UniformPrior_CosineLoss_VIZ.py $p 0 10.0 180 NO_PLOT
python3 RunGardelle_NaturalPrior_CosineLoss_VIZ.py $p 0 10.0 180 NO_PLOT
python3 RunGardelle_UniformEncoding_CosineLoss_VIZ.py $p 0 10.0 180 NO_PLOT
python3 RunGardelle_NaturalPrior_CosineLoss_VIZ2_OnlyVar.py $p 0 10.0 180 NO_PLOT
done

python3 RunGardelle_UniformPrior_CosineLoss_VIZ_OnlyHuman_ErrBar.py 8 0 10.0 180
python3 RunGardelle_NaturalPrior_CenteredLoss_Viz_Human_CircStat_OnlyVar_ErrBar.py



python3 RunFalseGardelle_FreePrior_CosineLoss_AddConstant_Veri_VIZ.py 8 0 10.0 180
python3 RunFalseGardelle_FreePrior_CosineLoss_AddConstant_VIZ.py 8 0 10.0 180
python3 RunFalseGardelle_FreePrior_CosineLoss_Shift_Veri_VIZ.py 8 0 10.0 180
python3 RunFalseGardelle_FreePrior_CosineLoss_Shift_VIZ.py 8 0 10.0 180
python3 RunFalseGardelle_FreePrior_CosineLoss_NoiseType_VIZ.py 8 0 10.0 180





python3 RunGardelle_FreePrior_CosineLoss_OnSim_VIZ.py 8 0 10.0 180
python3 RunGardelle_NaturalPrior_CosineLoss_OnSim_VIZ.py 8 0 10.0 180
python3 RunGardelle_UniformPrior_CosineLoss_OnSim_VIZ.py 8 0 10.0 180


python3 RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py 8 0 10.0 180



python3 RunGardelle_NaturalPrior_CosineLoss_VIZ_OnlyModel.py 8 0 20.0 1800
python3 RunGardelle_UniformPrior_CosineLoss_VIZ_OnlyModel.py 8 0 20.0 1800
python3 RunGardelle_UniformPrior_CosineLoss_VIZ_OnlyHuman_ErrBar.py 8 0 10.0 180

python3 evaluateCrossValidationResults_Gardelle_180_FreePrior_OnSim.py
python3 evaluateCrossValidationResults_Gardelle_180_NaturalPrior_OnSim.py
python3 evaluateCrossValidationResults_Gardelle_180_UniformPrior_OnSim.py


# for Figure 7 in main paper, using this
python3 RunGardelle_NaturalPrior_CosineLoss_VIZ.py 8 0 10.0 180
