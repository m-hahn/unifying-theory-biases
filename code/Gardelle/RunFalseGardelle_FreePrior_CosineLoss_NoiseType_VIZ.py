import computations
import getObservations
import glob
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sys
import torch
from computations import computeResourcesWithStimulusNoise
from loadGardelle import *
from loadModel import loadModel
from matplotlib import rc
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import makeGridIndicesCircular
from util import product
from util import savePlot
from util import toFactor

__file__ = __file__.split("/")[-1]
rc('font', **{'family':'FreeSans'})

OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
assert P > 0
FOLD_HERE = int(sys.argv[2])
REG_WEIGHT = float(sys.argv[3])
GRID = int(sys.argv[4])
SHOW_PLOT = (len(sys.argv) < 6) or (sys.argv[5] == "SHOW_PLOT")
DEVICE = 'cpu'

FILE = f"logs/CROSSVALID/{__file__.replace('_VIZ', '')}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"

if not os.path.exists(FILE):
   print("Missing model")
   assert False, FILE

# Helper Functions dependent on the device

##############################################

# Store observations
assert (observations_x == sample).all()
assert (observations_y == responses).all()

#############################################################
# Part: Partition data into folds. As described in the paper,
# this is done within each subject.
N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

Fold = 0*Subject
for i in range(int(min(Subject)), int(max(Subject))+1):
    trials = [j for j in range(Subject.size()[0]) if Subject[j] == i]
    randomGenerator.shuffle(trials)
    foldSize = int(len(trials)/N_FOLDS)
    for k in range(N_FOLDS):
        Fold[trials[k*foldSize:(k+1)*foldSize]] = k

##############################################
# Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 360

CIRCULAR = True
INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
grid, grid_indices_here = makeGridIndicesCircular(GRID, MIN_GRID, MAX_GRID)
assert grid_indices_here.max() >= GRID, grid_indices_here.max()

# Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

##############################################
# Initialize the model
# Part: Initialize the model
init_parameters = {}
init_parameters["sigma2_stimulus"] = MakeFloatTensor(6*[0]).view(6)
init_parameters["log_motor_var"] = MakeFloatTensor([0]).view(1)
init_parameters["sigma_logit"] = MakeFloatTensor(6*[-3]).view(6)
init_parameters["mixture_logit"] = MakeFloatTensor([-1]).view(1)
init_parameters["prior"] = MakeZeros(GRID)
init_parameters["volume"] = MakeZeros(GRID)
loadModel(FILE, init_parameters)
assert "volume" in init_parameters
for _, y in init_parameters.items():
    y.requires_grad = True

##############################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 2*math.pi

# Part: Specify `similariy` or `difference` functions.
## These are negative squared distances (for interval spaces) or
## trigonometric functions (for circular spaces), with
## some extra factors for numerical purposes.
## Exponentiating a `similarity` function and normalizing
## is equivalent to the Gaussian / von Mises density.
## The purpose of specifying these as `closeness` or `distance`,
## rather than simply calling squared or trigonometric
## functions is to  flexibly reuse the same model code for
## both interval and circular spaces.
def SQUARED_STIMULUS_DIFFERENCE(x):
    return torch.sin(math.pi*x/180)
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(math.pi*x/180)
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(x)
def SQUARED_SENSORY_DIFFERENCE(x):
    return torch.sin(x)

##############################################
# Import the appropriate estimator for minimizing the loss function
SCALE = 50

class CosineEstimator(torch.autograd.Function):
    """
    We can implement our own custom autograd Functions by subclassing
    torch.autograd.Function and implementing the forward and backward passes
    which operate on Tensors.
    """

    @staticmethod
    def forward(ctx, grid_indices_here, posterior):
        """
        In the forward pass we receive a Tensor containing the input and return
        a Tensor containing the output. ctx is a context object that can be used
        to stash information for backward computation. You can cache arbitrary
        objects for use in the backward pass using the ctx.save_for_backward method.
        """
        grid_indices_here = grid_indices_here * 2 * math.pi/GRID
        n_inputs, n_batch = posterior.size()
        initialized = (grid_indices_here.data * posterior).detach().sum(dim=0).data.clone()
        result = initialized.clone()

        momentum = MakeZeros(GRID)

        # Preinitialize using ordinary Lp loss
        if False:
         for itera in range(20):
          if False:
            loss = (((result.unsqueeze(0)/SCALE - grid_indices_here/SCALE).abs().pow(P)) * posterior.detach()).sum(dim=0).sum() / 1
          loss_gradient = ((P * torch.sign(result.unsqueeze(0)/SCALE - grid_indices_here/SCALE) * (result.unsqueeze(0)/SCALE - grid_indices_here/SCALE).abs().pow(P-1)) * posterior.detach()).sum(dim=0) / 1
          loss_gradient2 = ((P * (P-1) * (result.unsqueeze(0)/SCALE - grid_indices_here/SCALE).abs().pow(P-2)) * posterior.detach()).sum(dim=0) / 1
          result -= loss_gradient / loss_gradient2
          if float(loss_gradient.abs().max()) < 1e-5:
              break

        for itera in range(30):

          if OPTIMIZER_VERBOSE:
            loss = (((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2)) * posterior.detach()).sum(dim=0)
          loss_gradient = (P/2 * (SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-1)) * posterior.detach()).sum(dim=0)

          loss_gradient2 = (P/2 * (SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-1)) * posterior.detach()).sum(dim=0) / 1
          if P == 2:
             pass
          else:
             loss_gradient2 = loss_gradient2 + (P/2 * (P/2-1) * (SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)).pow(2) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-2)) * posterior.detach()).sum(dim=0) / 1

          MASK = (loss_gradient2 > 0.001)
          updateGD = - loss_gradient /loss_gradient2.abs().clamp(min=0.0001)
          updateNewton = - loss_gradient/loss_gradient2

          update = torch.where(MASK, updateNewton, updateGD)
          result = result + update
          if OPTIMIZER_VERBOSE:
             print(itera, float(loss.mean()), "max absolute gradient after GD steps", loss_gradient.abs().max(), sum(MASK.float()), "Newton steps", "max update", update.abs().max())
          if float(loss_gradient.abs().max()) < 1e-6:
              break
        if random.random() < 0.01:
            print("Newton Iterations", itera)
        if float(loss_gradient.abs().max()) >= 1e-4:
            print("WARNING", float(loss_gradient.abs().max()), "after ", itera, " Newton steps")
            assert False
        ctx.save_for_backward(grid_indices_here, posterior, result)
        return result * GRID / (2*math.pi)

    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor containing the gradient of the loss
        with respect to the output, and we need to compute the gradient of the loss
        with respect to the input.
        """
        grid_indices_here, posterior, result = ctx.saved_tensors
        initialized = (grid_indices_here.data * posterior).detach().sum(dim=0).data

        n_inputs, n_batch = posterior.size()

        F = (P/2 * (SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-1)) * posterior.detach()).sum(dim=0)

        assert F.abs().mean() < 0.01, (F, F.abs().mean(), F.abs().max()/n_batch, n_batch, ((SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)) * posterior.detach()).sum(dim=0) / n_batch)

        dF_posterior = P/2 * (SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-1))
        dF_result = 0
        dF_result = dF_result + (P/2 * (SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-1)) * posterior.detach()).sum(dim=0)
        if P == 2:
           pass
        else:
           dF_result = dF_result + (P/2 * (P/2-1) * (SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)).pow(2) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-2)) * posterior.detach()).sum(dim=0)
        if OPTIMIZER_VERBOSE:
           if torch.isnan(result).any():
                print(result.size(), dF_result.size())
                assert False, result
           if torch.isnan(posterior).any():
                print(result.size(), dF_result.size())
                assert False, posterior
           if torch.isnan(dF_result).any():
                print(result[torch.isnan(dF_result)])
                print((SQUARED_STIMULUS_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)).pow(2))
                print(((1+1e-10-SQUARED_STIMULUS_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-2)))
                print(result.size(), dF_result.size(), posterior.size())
                assert False

        gradient = - ((1 / dF_result).unsqueeze(0) * dF_posterior).detach().data
        if OPTIMIZER_VERBOSE:
           if dF_result.abs().min() < .0001:
                assert False, dF_result.abs().min()
           if torch.isnan(dF_result).any():
                assert False, dF_result
        if OPTIMIZER_VERBOSE:
           if torch.isnan(dF_posterior).any():
                assert False, dF_posterior

        if OPTIMIZER_VERBOSE:
           if torch.isnan(grad_output).any():
                assert False, grad_output
        gradient = grad_output.unsqueeze(0) * gradient
        if OPTIMIZER_VERBOSE:
           if torch.isnan(gradient).any():
                assert False, gradient

        return None, gradient * GRID / (2*math.pi)

# Run the model
def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, computePredictions=False, subject=None, sigma_stimulus=None, sigma2_stimulus=None, duration_=None, folds=None, lossReduce='mean'):

 # Part: Obtain the motor variance by exponentiating the appropriate model parameter
 motor_variance = torch.exp(- parameters["log_motor_var"])
 sigma2 = 4*torch.sigmoid(sigma_logit)
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 if True:
  folds = MakeLongTensor(folds)
  if subject is not None:
    assert False, "subject and duration"
    stimulus = stimulus_[((Subject==subject))]
    responses = responses_[((Subject==subject))]
  else:
    MASK = torch.logical_and(duration==duration_, (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]
  assert stimulus.view(-1).size()[0] > 0

  if sigma2_stimulus > 0:
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))
    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)
  else:
    assert False

  # Part: Compute sensory likelihoods. Across both interval and
  ## circular stimulus spaces, this amounts to exponentiaring a
  ## `similarity`
  sensory_likelihoods = torch.softmax(((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2))  + volumeElement.unsqueeze(1).log(), dim=0)

  if sigma2_stimulus == 0:
    assert False
    likelihoods = sensory_likelihoods
  else:
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  # Compute posterior
  posterior = prior.unsqueeze(1) * likelihoods.t()
  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  # Estimator
  bayesianEstimate = CosineEstimator.apply(grid_indices_here, posterior)

  # Caculate motor likelihood
  error = (SQUARED_STIMULUS_SIMILARITY(360/GRID*bayesianEstimate.unsqueeze(0) - responses.unsqueeze(1)))
  log_normalizing_constant = torch.logsumexp((SQUARED_STIMULUS_SIMILARITY(grid))/motor_variance, dim=0) + math.log(2 * math.pi / GRID)
  log_motor_likelihoods = (error/motor_variance) - log_normalizing_constant
  motor_likelihoods = torch.exp(log_motor_likelihoods)
  ## Obtain the guessing rate, parameterized via the (inverse) logit transform as described in SI Appendix
  # Mixture of estimation and uniform response
  uniform_part = torch.sigmoid(parameters["mixture_logit"])
  ## The full likelihood then consists of a mixture of the motor likelihood calculated before, and the uniform
  ## distribution on the full space.
  motor_likelihoods = (1-uniform_part) * motor_likelihoods + (uniform_part / (2*math.pi) + 0*motor_likelihoods)

  # Now the loss is obtained by marginalizing out m from the motor likelihood
  if lossReduce == 'mean':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().mean()
  elif lossReduce == 'sum':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().sum()
  else:
    assert False

  ## If computePredictions==True, compute the bias and variability of the estimate
  if computePredictions:
     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS
     bayesianEstimate_avg_byStimulus = computeCircularMeanWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = (bayesianEstimate_sd_byStimulus.pow(2) + motor_variance * math.pow(180/math.pi,2)).sqrt()

     bayesianEstimate_avg_byStimulus = torch.where((bayesianEstimate_avg_byStimulus-grid).abs()<180, bayesianEstimate_avg_byStimulus, torch.where(bayesianEstimate_avg_byStimulus > 180, bayesianEstimate_avg_byStimulus-360, bayesianEstimate_avg_byStimulus+360))
     assert float(((bayesianEstimate_avg_byStimulus-grid).abs()).max()) < 180, float(((bayesianEstimate_avg_byStimulus-grid).abs()).max())
     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = computeCircularMeanWeighted(posteriorMaxima.unsqueeze(1), likelihoods)
     encodingBias = computeCircularMeanWeighted(grid.unsqueeze(1), likelihoods)
     attraction = (posteriorMaxima-encodingBias)
     attraction1 = attraction
     attraction2 = attraction+360
     attraction3 = attraction-360
     attraction = torch.where(attraction1.abs() < 180, attraction1, torch.where(attraction2.abs() < 180, attraction2, attraction3))
  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction

## Pass data to auxiliary script used for retrieving smoothed fits from the dataset
getObservations.setData(x_set=x_set, observations_y=observations_y, xValues=xValues, duration=duration, grid=grid)
def retrieveObservations(x, Subject_, Duration_, meanMethod = "circular"):
     assert Subject_ is None
     y_set = []
     sd_set = []
     for x in x_set:
        y_here = observations_y[(torch.logical_and((xValues == x), duration==Duration_))]

        Mean1 = computeCircularMean(y_here)
        Mean2 = computeCenteredMean(y_here, grid[x])
        if abs(Mean1-grid[x]) > 180 and Mean1 > 180:
            Mean1 = Mean1-360
        elif abs(Mean1-grid[x]) > 180 and Mean1 < 180:
            Mean1 = Mean1+360
        if Duration_ > 1 and abs(Mean1-Mean2) > 180:
            print(Duration_)
            print(y_here)
            print("Warning: circular and centered means are very different", Mean1, Mean2, grid[x], y_here)
        if meanMethod == "circular":
            Mean = Mean1
        elif meanMethod == "centered":
            Mean = Mean2
        else:
            assert False

        bias = Mean - grid[x]
        if abs(bias) > 180:
            bias = bias+360
        y_set.append(bias)
        sd_set.append(computeCircularSD(y_here))
     return y_set, sd_set

computations.setData(GRID=GRID, STIMULUS_SPACE_VOLUME=STIMULUS_SPACE_VOLUME)

def model(grid):

  lowestLoss = 10000
  for iteration in range(1):
   parameters = init_parameters
   ## In each iteration, recompute
   ## - the resource allocation (called `volume' due to a geometric interpretation)
   ## - the prior

   # Define prior and resource allocation
   volume = 2 * math.pi * torch.nn.functional.softmax(parameters["volume"], dim=0)
   prior = torch.nn.functional.softmax(parameters["prior"], dim=0)

   loss = 0

   if iteration % 100 == 0:
     assigned = ["PRI", None, "ENC", None, "ATT", "REP", "TOT", None, "VAR"]
     for w, k in enumerate(assigned):
         globals()[k] = w
     gridspec = dict(width_ratios=[1,0.2,1,0.2,1,1,1,0,0])
     figure, axis = plt.subplots(1, len(assigned), figsize=(10, 1.3*0.8*1.8), gridspec_kw=gridspec)
     plt.tight_layout()
     figure.subplots_adjust(wspace=0.1, hspace=0.0)

     assert PRI == 0
     assert ENC == 2
     assert ATT == 4
     assert REP == 5
     assert TOT == 6
     assert VAR == 8
     PAD = [1,3,7,8]
     axis[PRI].plot(grid, prior.detach(), color="gray")
     axis[PRI].set_ylim(bottom=0)
     axis[ENC].set_ylim(0, 0.5)
     axis[ENC].set_yticks(ticks=[0,0.2,0.4], labels=["0", "0.2", "0.4"])
     x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   ## Iterate over the conditions and possibly subjects, if parameters are fitted separately.
   ## In this dataset, all parameters are fitted across subjects.
   for DURATION in range(2,6):
    for SUBJECT in [1]:

     loss_model, bayesianEstimate_model, bayesianEstimate_sd_byStimulus_model, attraction_model = computeBias(xValues, init_parameters["sigma_logit"][0], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, computePredictions=(iteration%100 == 0), subject=None, sigma_stimulus=0, sigma2_stimulus=torch.exp(-init_parameters["sigma2_stimulus"][DURATION]), duration_=DURATION, folds=trainFolds, lossReduce='sum')
     loss += loss_model

     if iteration % 100 == 0:
       axis[ENC].plot(grid, 2*computeResourcesWithStimulusNoise(volume.detach(), inverse_variance=1/float((2 * torch.sigmoid(init_parameters["sigma_logit"][0]))), inverse_variance_stimulus=math.pow(2*math.pi/STIMULUS_SPACE_VOLUME, 2) * torch.exp(init_parameters["sigma2_stimulus"][DURATION])).detach())
       y_set, sd_set = retrieveObservations(x, None, DURATION, meanMethod="circular")
       axis[TOT].plot(grid, (bayesianEstimate_model-grid).detach()/2)

     axis[ATT].plot(grid, (attraction_model).detach()/2)
     axis[REP].plot(grid, (bayesianEstimate_model-attraction_model-grid).detach()/2)
     axis[VAR].plot(grid, bayesianEstimate_sd_byStimulus_model.detach()/2)
     for w in PAD:
       axis[w].set_visible(False)

     axis[PRI].set_yticks(ticks=[0], labels=[0])
     axis[REP].tick_params(labelleft=False)
     axis[TOT].tick_params(labelleft=False)

     for w in range(9):
       axis[w].spines['top'].set_visible(False)
       axis[w].spines['right'].set_visible(False)

     axis[ATT].set_ylim(-25, 25)
     axis[REP].set_ylim(-25, 25)
     axis[TOT].set_ylim(-25, 25)

     if True:
       axis[PRI].set_title("Prior", fontsize=19)
       axis[ENC].set_title("Resources", fontsize=19)
       axis[ATT].set_title("Attraction", fontsize=19)
       axis[REP].set_title("Repulsion", fontsize=19)
       axis[TOT].set_title("Total Bias", fontsize=19)
       axis[VAR].set_title("Variability", fontsize=19)

   for w in [ATT, REP, TOT]:
      axis[w].set_yticks(ticks=[-20,0,20], labels=["-20","0","20"])
   for w in range(9):
       axis[w].set_xticks(ticks=[0,180,360], labels=["0", "90", "180"])

   if iteration % 100 == 0:
     for w in range(len(assigned)):
         if w not in [ENC, PRI, ATT]:
           axis[w].tick_params(labelbottom=False)
         axis[w].tick_params(axis='x', labelsize=17, width=0.4)
         axis[w].tick_params(axis='y', labelsize=17, width=0.4)
     figure.subplots_adjust(bottom=0.23,top=0.77)
     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.pdf")
     if SHOW_PLOT:
       plt.show()
     plt.close()

############################3

# Project the stimuli onto the discrete grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

model(grid)
