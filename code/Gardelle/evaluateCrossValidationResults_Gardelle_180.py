import math
import matplotlib.pyplot as plt
from evaluateCrossValidationResults2 import crossValidResults
from matplotlib import rc
from util import savePlot
rc('font', **{'family':'FreeSans'})

def mean(x):
    return sum(x)/len(x)

def round_(x):
    if str(x).lower() == "nan":
        return "--"
    else:
        return round(x)

def deltaDiff(x,y):
    if len(x) < 10 or len(y) < 10:
        return "--"
    return round(mean([x[i]-y[i] for i in range(len(x))]),1)

def deltaSD(x,y):
    if len(x) < 10 or len(y) < 10:
        return "--"
    mu = mean([x[i]-y[i] for i in range(len(x))])
    muSquared = mean([math.pow(x[i]-y[i],2) for i in range(len(x))])
    return round(math.sqrt(muSquared - math.pow(mu, 2)) / math.sqrt(10),1)

curves = {}
def plot(color, style, loss, result):
    if (color,style) not in curves:
       curves[(color, style)] = []
    if result[2] != result[2]:
        return
    curves[(color, style)].append((loss, result[2], result[3]))

curvesRelative = {}
def plotRelative(color, style, loss, result, resultRef):
    if (color,style) not in curvesRelative:
       curvesRelative[(color, style)] = []
    if result[2] != result[2]:
        return
    sd = deltaSD(result[4],resultRef[4])
    if sd == '--':
        return
    curvesRelative[(color, style)].append((loss, result[2]-resultRef[2], sd))

curvesRelativeLF = {}
def plotEffectOfLossFunction(color, style, loss, result, reference):
    if result is None:
        return
    if (color,style) not in curvesRelativeLF:
       curvesRelativeLF[(color, style)] = []
    meanRelative = result[2] - reference[2]
    sd = deltaSD(result[4], reference[4])
    if sd == '--':
        return
    curvesRelativeLF[(color, style)].append((loss, meanRelative, sd))

with open(f"output/{__file__}.tex", "w") as outFile:
 for loss in range(0,15):
    if loss == 1:
        continue
    if loss == 0:

       full = crossValidResults(f"RunGardelle_NaturalPrior_Zero.py_{loss}_*_10.0_180.txt", STRICT=True)
       full_freeprior = crossValidResults(f"RunGardelle_FreePrior_Zero.py_{loss}_*_10.0_180.txt", STRICT=True)
       full_uniprior = crossValidResults(f"RunGardelle_UniformPrior_Zero.py_{loss}_*_10.0_180.txt", STRICT=True)
       full_uniencoding = crossValidResults(f"RunGardelle_UniformEncoding_Zero.py_{loss}_*_10.0_180.txt", STRICT=True)
    else:
       full = crossValidResults(f"RunGardelle_NaturalPrior_CenteredLoss.py_{loss}_*_10.0_180.txt", STRICT=True)
       full_freeprior = crossValidResults(f"RunGardelle_FreePrior_CenteredLoss.py_{loss}_*_10.0_180.txt", STRICT=True)
       full_uniprior = crossValidResults(f"RunGardelle_UniformPrior_CenteredLoss.py_{loss}_*_10.0_180.txt", STRICT=True)
       full_uniencoding = crossValidResults(f"RunGardelle_UniformEncoding_CenteredLoss.py_{loss}_*_10.0_180.txt", STRICT=True)

    if loss == 0:
       cosine = full
       cosine_freeprior = full_freeprior
       cosine_uniprior = full_uniprior
       cosine_uniencoding = full_uniencoding
    else:
       cosine = crossValidResults(f"RunGardelle_NaturalPrior_CosineLoss.py_{loss}_*_10.0_180.txt", STRICT=True)
       cosine_freeprior = crossValidResults(f"RunGardelle_FreePrior_CosineLoss.py_{loss}_*_10.0_180.txt", STRICT=True)
       cosine_uniprior = crossValidResults(f"RunGardelle_UniformPrior_CosineLoss.py_{loss}_*_10.0_180.txt", STRICT=True)
       cosine_uniencoding = crossValidResults(f"RunGardelle_UniformEncoding_CosineLoss.py_{loss}_*_10.0_180.txt", STRICT=True)

    COLOR_FREE = "green"
    COLOR_UNIFORM_PRIOR = "red"
    COLOR_UNIFORM_ENCODING = "blue"
    COLOR_HARD1 = "purple"
    COLOR_HARD2 = "orange"

    plot(COLOR_HARD1, "solid", loss, full)
    plot(COLOR_HARD1, "dotted", loss, cosine if loss > 0 else full)

    plot(COLOR_FREE, "solid", loss, full_freeprior)
    plot(COLOR_FREE, "dotted", loss, cosine_freeprior if loss > 0 else full_freeprior)

    plot(COLOR_UNIFORM_PRIOR, "solid", loss, full_uniprior)
    plot(COLOR_UNIFORM_PRIOR, "dotted", loss, cosine_uniprior if loss > 0 else full_uniprior)

    plot(COLOR_UNIFORM_ENCODING, "solid", loss, full_uniencoding)
    plot(COLOR_UNIFORM_ENCODING, "dotted", loss, cosine_uniencoding if loss > 0 else full_uniencoding)

    plotEffectOfLossFunction(COLOR_HARD1, "solid", loss, full, crossValidResults(f"RunGardelle_FreePrior_CosineLoss.py_8_*_10.0_180.txt"))
    plotEffectOfLossFunction(COLOR_FREE, "solid", loss, full_freeprior, crossValidResults(f"RunGardelle_FreePrior_CosineLoss.py_8_*_10.0_180.txt"))
    plotEffectOfLossFunction(COLOR_UNIFORM_PRIOR, "solid", loss, full_uniprior, crossValidResults(f"RunGardelle_FreePrior_CosineLoss.py_8_*_10.0_180.txt"))
    plotEffectOfLossFunction(COLOR_UNIFORM_ENCODING, "solid", loss, full_uniencoding, crossValidResults(f"RunGardelle_FreePrior_CosineLoss.py_8_*_10.0_180.txt"))

    plotEffectOfLossFunction(COLOR_HARD1, "dotted", loss, cosine, crossValidResults(f"RunGardelle_FreePrior_CosineLoss.py_8_*_10.0_180.txt"))
    plotEffectOfLossFunction(COLOR_FREE, "dotted", loss, cosine_freeprior, crossValidResults(f"RunGardelle_FreePrior_CosineLoss.py_8_*_10.0_180.txt"))
    plotEffectOfLossFunction(COLOR_UNIFORM_PRIOR, "dotted", loss, cosine_uniprior, crossValidResults(f"RunGardelle_FreePrior_CosineLoss.py_8_*_10.0_180.txt"))
    plotEffectOfLossFunction(COLOR_UNIFORM_ENCODING, "dotted", loss, cosine_uniencoding, crossValidResults(f"RunGardelle_FreePrior_CosineLoss.py_8_*_10.0_180.txt"))

    plotRelative(COLOR_HARD1, "solid", loss, full, full_freeprior)
    plotRelative(COLOR_HARD1, "dotted", loss, cosine if loss > 0 else full, cosine_freeprior if loss > 0 else full_freeprior)
    plotRelative(COLOR_FREE, "solid", loss, full_freeprior, full_freeprior)
    plotRelative(COLOR_FREE, "dotted", loss, cosine_freeprior if loss > 0 else full_freeprior, cosine_freeprior if loss > 0 else full_freeprior)
    plotRelative(COLOR_UNIFORM_PRIOR, "solid", loss, full_uniprior, full_freeprior)
    plotRelative(COLOR_UNIFORM_PRIOR, "dotted", loss, cosine_uniprior if loss > 0 else full_uniprior, cosine_freeprior if loss > 0 else full_freeprior)
    plotRelative(COLOR_UNIFORM_ENCODING, "solid", loss, full_uniencoding, full_freeprior)
    plotRelative(COLOR_UNIFORM_ENCODING, "dotted", loss, cosine_uniencoding if loss > 0 else full_uniencoding, cosine_freeprior if loss > 0 else full_freeprior)

minY = 100000000000000
maxY = -100000000000000
figure, axis = plt.subplots(1,1, figsize=(0.9*2,0.9*2))
figure.subplots_adjust(left=0.25, bottom=0.25)
for key, values in curvesRelativeLF.items():
    color, style = key
    if color != COLOR_FREE or style != "dotted":
        continue
    if len(values) == 0:
        continue
    x, y, errors = zip(*values)
    print(x, y, errors)
    color = "gray"
    axis.plot(x, y, color=color, linestyle='solid', linewidth=0.5)

    minY = min(minY, min(y))
    maxY = max(maxY, max(y))
    (_, caps, _) = axis.errorbar(x, y, yerr=[z for z in errors], color=color, fmt='none', linewidth=0.5, capsize=2)
    for cap in caps:
       cap.set_markeredgewidth(0.5)
## done plotting
print(minY, maxY)
axis.set_xlim(-1,11)
axis.set_ylim(minY-20, maxY+20)
axis.spines['top'].set_visible(False)
axis.spines['right'].set_visible(False)
axis.set_yticks(ticks=[0,50,100,150], labels=[0, "", "100", ""])
axis.set_xticks(ticks=[0,5,10])
axis.tick_params(labelsize=14, width=0.4)

savePlot(f"figures/{__file__}_simple.pdf")
plt.show()

#######################

minY = 100000000000000
maxY = -100000000000000
figure, axis = plt.subplots(1,2, figsize=(6,3))
plt.tight_layout()
for key, values in curves.items():
    color, style = key
    if len(values) == 0:
        continue
    x, y, errors = zip(*values)
    print(x, y, errors)
    i = ["solid", "dotted"].index(style)
    axis[i].plot(x, y, color=color, linestyle=style)
    axis[i].scatter(x, y, color=color)
    minY = min(minY, min(y))
    maxY = max(maxY, max(y))
    axis[i].errorbar(x, y, yerr=errors, color=color)

print(minY, maxY)
axis[0].set_ylim(minY-10, maxY+10)
axis[1].set_ylim(minY-10, maxY+10)
axis[0].set_xlim(-1, 13)
axis[1].set_xlim(-1, 13)
axis[0].plot([0,16], [minY, minY], linestyle='dotted')
axis[1].plot([0,16], [minY, minY], linestyle='dotted')
savePlot(f"figures/{__file__}.pdf")
plt.show()

figure, axis = plt.subplots(1, 2, figsize=(6,3))
plt.tight_layout()
counter = 0
for key, values in curvesRelative.items():
    counter += 1
    color, style = key
    if len(values) == 0:
        continue
    x, y, errors = zip(*values)
    x = [z + 0.2*(counter-2) for z in x]
    print(x, y, errors)
    i = ["solid", "dotted"].index(style)
    axis[i].plot(x, y, color=color, linestyle=style)
    axis[i].scatter(x, y, color=color)
    axis[i].errorbar(x, y, yerr=errors, color=color)

for i in range(2):
 axis[i].plot([0,16], [0,0])
 axis[i].set_xlabel("Exponent")
 axis[i].set_ylabel("Delta NLL")
axis[0].set_xlim(-1, 15)
axis[1].set_xlim(-1, 15)
savePlot(f"figures/{__file__}_Relative.pdf")
plt.show()

minY = 100000000000000
maxY = -100000000000000
figure, axis = plt.subplots(1, 2, figsize=(6,3), layout='constrained')

with open(f"output/{__file__}.txt", "w") as outFile:
 for key, values in curvesRelativeLF.items():
    color, style = key
    if len(values) == 0:
        continue
    x, y, errors = zip(*values)
    print(x, y, errors)
    i = ["solid", "dotted"].index(style)
    minY = min(minY, min(y))
    maxY = max(maxY, max(y))
    axis[i].plot(x, y, color=color, linestyle=style)
    axis[i].scatter(x, y, color=color, s=10)
    axis[i].errorbar(x, y, yerr=errors, color=color)
    print(color, style, [round(q) for q in y], file=outFile)

for i in range(2):
 axis[i].plot([0,16], [0,0], color="gray", linestyle="dotted")
 axis[i].set_xlabel("Exponent")
axis[0].set_ylabel("Δ NLL")
axis[0].set_ylim(minY-10, maxY+10)
axis[1].set_ylim(minY-10, maxY+10)
axis[0].set_xlim(-1, 15)
axis[1].set_xlim(-1, 15)
axis[0].set_title("Centered Loss")
axis[1].set_title("Cosine Loss")
axis[0].set_xticks(ticks=[0,2,4,6,8,10,12])
axis[1].set_xticks(ticks=[0,2,4,6,8,10,12])
savePlot(f"figures/{__file__}_RelativeLF.pdf")
plt.show()
