# Gardelle et al (2011)

* [Collect quantitative results](evaluateCrossValidationResults_Gardelle_180.py), [NLL by exponent (full)](evaluateCrossValidationResults_Gardelle_180.py_RelativeLF.pdf), [NLL by exponent (simple)](evaluateCrossValidationResults_Gardelle_180.py_simple.pdf)

Model specification:
* `sigma2_stimulus` (1): logarithmic parameterization of stimulus noise variance (this is zero throughout these analyses, except in one counterfactual falsifiability study)
* `log_motor_var` (1): logarithmic parameterization of motor nosie variance
* `sigma_logit` (6): logit parameterization of , for the six levels of duration in the dataset. The lowest level (0ms, i.e. not shown at all) is excluded, hence, the first entry is unused.
* `mixture_logit` (1): logit parameterization of guessing rate
* `prior` (180): logit parameterization of prior
* `volume` (180): logit parameterization of resource allocation


## Model Fitting

For fitting all models, see `fitAll.sh`.

## MAP Estimator

* [Naturalistic prior](RunGardelle_NaturalPrior_Zero.py), [visualization script](RunGardelle_NaturalPrior_Zero_VIZ.py), [resulting plot](figures/RunGardelle_NaturalPrior_Zero_VIZ.py_0_0_10.0_180.pdf)
* [Free prior](RunGardelle_FreePrior_Zero.py), [visualization script](RunGardelle_FreePrior_Zero_VIZ.py), [resulting plot](figures/RunGardelle_FreePrior_Zero_VIZ.py_0_0_10.0_180.pdf)
* [Uniform prior](RunGardelle_UniformPrior_Zero.py), [visualization script](RunGardelle_UniformPrior_Zero_VIZ.py), [resulting plot](figures/RunGardelle_UniformPrior_Zero_VIZ.py_0_0_10.0_180.pdf)
* [Uniform encoding](RunGardelle_UniformEncoding_Zero.py), [visualization script](RunGardelle_UniformEncoding_Zero_VIZ.py), [resulting plot](figures/RunGardelle_UniformEncoding_Zero_VIZ.py_0_0_10.0_180.pdf)




## Positive exponents

### Centered implementation

* [Naturalistic prior](RunGardelle_NaturalPrior_CenteredLoss.py), [visualization script](RunGardelle_NaturalPrior_CenteredLoss_VIZ_OnlyModel.py), [var](RunGardelle_NaturalPrior_CenteredLoss_VIZ2_OnlyVar.py), resulting plot: [2](figures/RunGardelle_NaturalPrior_CenteredLoss_VIZ2_OnlyVar.py_2_0_10.0_180.pdf), [4](figures/RunGardelle_NaturalPrior_CenteredLoss_VIZ2_OnlyVar.py_4_0_10.0_180.pdf), [6](figures/RunGardelle_NaturalPrior_CenteredLoss_VIZ2_OnlyVar.py_6_0_10.0_180.pdf), [8](figures/RunGardelle_NaturalPrior_CenteredLoss_VIZ2_OnlyVar.py_8_0_10.0_180.pdf)
* [Free prior](RunGardelle_FreePrior_CenteredLoss.py), [visualization script](RunGardelle_FreePrior_CenteredLoss_VIZ.py), [batch viz](RunGardelle_FreePrior_CenteredLoss_VIZ_ACREXP.py), [resulting plot](figures/RunGardelle_FreePrior_CenteredLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf)
* [Uniform prior](RunGardelle_UniformPrior_CenteredLoss.py), [visualization script](RunGardelle_UniformPrior_CenteredLoss_VIZ_OnlyModel.py), [resulting plot](figures/RunGardelle_UniformPrior_CenteredLoss_VIZ_OnlyModel.py_8_0_10.0_180.pdf)
* [Uniform encoding](RunGardelle_UniformEncoding_CenteredLoss.py), (no viz script)




### Cosine-based implementation

* [Naturalistic prior](RunGardelle_NaturalPrior_CosineLoss.py), [visualization script for SI figures](RunGardelle_NaturalPrior_CosineLoss_VIZ.py), [visualization script for Main text, Figure 3](RunGardelle_NaturalPrior_CosineLoss_VIZ_OnlyModel.py), [batch viz](RunGardelle_NaturalPrior_CosineLoss_VIZ_ACREXP.py), [var](RunGardelle_NaturalPrior_CosineLoss_VIZ2_OnlyVar.py), resulting plot: [2](figures/RunGardelle_NaturalPrior_CosineLoss_VIZ2_OnlyVar.py_2_0_10.0_180.pdf), [4](figures/RunGardelle_NaturalPrior_CosineLoss_VIZ2_OnlyVar.py_4_0_10.0_180.pdf), [6](figures/RunGardelle_NaturalPrior_CosineLoss_VIZ2_OnlyVar.py_6_0_10.0_180.pdf), [8](figures/RunGardelle_NaturalPrior_CosineLoss_VIZ2_OnlyVar.py_8_0_10.0_180.pdf); resulting plot: [2](figures/RunGardelle_NaturalPrior_CosineLoss_VIZ2_OnlyVar_MainPaper.py_2_0_10.0_180.pdf), [4](figures/RunGardelle_NaturalPrior_CosineLoss_VIZ2_OnlyVar_MainPaper.py_4_0_10.0_180.pdf), [6](figures/RunGardelle_NaturalPrior_CosineLoss_VIZ2_OnlyVar_MainPaper.py_6_0_10.0_180.pdf), [8](figures/RunGardelle_NaturalPrior_CosineLoss_VIZ2_OnlyVar_MainPaper.py_8_0_10.0_180.pdf),
[resulting plot](figures/RunGardelle_NaturalPrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf),
[resulting plot](figures/RunGardelle_NaturalPrior_CosineLoss_VIZ_OnlyModel.py_8_0_10.0_180.pdf),
[resulting plot](figures/RunGardelle_NaturalPrior_CosineLoss_VIZ.py_8_0_10.0_180_Magnitudes.pdf),
[resulting plot](figures/RunGardelle_NaturalPrior_CosineLoss_VIZ.py_8_0_10.0_180_PPRatio.pdf)


* [Free prior](RunGardelle_FreePrior_CosineLoss.py), [visualization script](RunGardelle_FreePrior_CosineLoss_VIZ.py), [visualization script](RunGardelle_FreePrior_CosineLoss_VIZ_OnlyModel.py), [viz](RunGardelle_FreePrior_CosineLoss_VIZ_ACREXP.py), [resulting plot](figures/RunGardelle_FreePrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf), [resulting plot](figures/RunGardelle_FreePrior_CosineLoss_VIZ_OnlyModel.py_8_0_10.0_180.pdf)


* [Uniform prior](RunGardelle_UniformPrior_CosineLoss.py), [visua;ization script](RunGardelle_UniformPrior_CosineLoss_VIZ.py), [visualization script](RunGardelle_UniformPrior_CosineLoss_VIZ_OnlyModel.py), [batch viz](RunGardelle_UniformPrior_CosineLoss_VIZ_ACREXP.py), [resulting plot](figures/RunGardelle_UniformPrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf), [resulting plot](figures/RunGardelle_UniformPrior_CosineLoss_VIZ_OnlyModel.py_8_0_10.0_180.pdf)

* [Uniform encoding](RunGardelle_UniformEncoding_CosineLoss.py), [batch viz](RunGardelle_UniformEncoding_CosineLoss_VIZ_ACREXP.py), [viz](RunGardelle_UniformEncoding_CosineLoss_VIZ.py), [resulting plot](figures/RunGardelle_UniformEncoding_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf)




## for original (human) data
* [bias](RunGardelle_UniformPrior_CosineLoss_VIZ_OnlyHuman_ErrBar.py), [resulting plot](figures/RunGardelle_UniformPrior_CosineLoss_VIZ_OnlyHuman_ErrBar.py_8_0_10.0_180.pdf)
* [variability](RunGardelle_NaturalPrior_CenteredLoss_Viz_Human_CircStat_OnlyVar_ErrBar.py), [resulting plot](figures/RunGardelle_NaturalPrior_CenteredLoss_Viz_Human_CircStat_OnlyVar_ErrBar.py.pdf)

## Fitting and visualizing in batch mode:

`python3 runCrossValidAllGardelle180.py RunGardelle_NaturalPrior_CosineLoss.py`


`python3 createACREXP.py' automatically derives a script working across exponents from a visualization script.



## robustness to random initialization
* [fitting](RunGardelle_FreePrior_CosineLoss_InitControl3.py), [viz](RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py)




# On simulated data

* Naturalistic prior: [simulating](SimulateGardelle_NaturalPrior.py), [simulated datase](logs/SIMULATED_REPLICATE/SimulateGardelle_NaturalPrior.py_8_180_838710.txt), [fitting](RunGardelle_NaturalPrior_CosineLoss_OnSim.py), [fitted model](RunGardelle_NaturalPrior_CosineLoss_OnSim.py_8_0_10.0_180_838710.txt), [viz](RunGardelle_NaturalPrior_CosineLoss_OnSim_VIZ.py), [resulting plot](figures/RunGardelle_NaturalPrior_CosineLoss_OnSim_VIZ.py_8_0_10.0_180_838710.pdf)
* Uniform prior: [simulating](SimulateGardelle_UniformPrior.py), [simulated dataset](logs/SIMULATED_REPLICATE/SimulateGardelle_UniformPrior.py_8_180_934795.txt), [fitting](RunGardelle_UniformPrior_CosineLoss_OnSim.py), [fitted model](RunGardelle_UniformPrior_CosineLoss_OnSim.py_8_0_10.0_180_934795.txt), [viz](RunGardelle_UniformPrior_CosineLoss_OnSim_VIZ.py), [resulting plot](figures/RunGardelle_UniformPrior_CosineLoss_OnSim_VIZ.py_8_0_10.0_180_934795.pdf)

* Free prior: RunGardelle_FreePrior_CosineLoss_OnSim.py

* For MAP estimator:
RunGardelle_FreePrior_Zero_OnSim.py 
RunGardelle_NaturalPrior_Zero_OnSim.py 
RunGardelle_UniformPrior_Zero_OnSim.py 




## Falsifiability Analysis
* [fitting](RunFalseGardelle_FreePrior_CosineLoss_AddConstant.py), [viz](RunFalseGardelle_FreePrior_CosineLoss_AddConstant_VIZ.py), [viz](RunFalseGardelle_FreePrior_CosineLoss_AddConstant_VIZ.py), [resulting plot](figures/RunFalseGardelle_FreePrior_CosineLoss_AddConstant_VIZ.py_8_0_10.0_180.pdf), [counterfactual](RunFalseGardelle_FreePrior_CosineLoss_AddConstant_Veri_VIZ.py), [resulting plot](figures/RunFalseGardelle_FreePrior_CosineLoss_AddConstant_Veri_VIZ.py_8_0_10.0_20.pdf)


[model](logs/CROSSVALID/RunFalseGardelle_FreePrior_CosineLoss_AddConstant.py_8_0_10.0_180.txt)


* [fitting](RunFalseGardelle_FreePrior_CosineLoss_Shift.py), [viz](RunFalseGardelle_FreePrior_CosineLoss_Shift_VIZ.py), [resulting plot](figures/RunFalseGardelle_FreePrior_CosineLoss_Shift_VIZ.py_8_0_10.0_180.pdf), [counterfactual](RunFalseGardelle_FreePrior_CosineLoss_Shift_Veri_VIZ.py), [resulting plot](figures/RunFalseGardelle_FreePrior_CosineLoss_Shift_Veri_VIZ.py_8_0_10.0_20.pdf)


* Exchanging stimulus noise and sensory noise: [fitting](RunFalseGardelle_FreePrior_CosineLoss_NoiseType.py), [viz](RunFalseGardelle_FreePrior_CosineLoss_NoiseType_VIZ.py), [resulting plot](figures/RunFalseGardelle_FreePrior_CosineLoss_NoiseType_VIZ.py_8_0_10.0_180.pdf)



## Evaluate NLL
* Analysis: [relative](figures/evaluateCrossValidationResults_Gardelle_180.py_RelativeLF.pdf), [simple](figures/evaluateCrossValidationResults_Gardelle_180.py_simple.pdf)
* Supplementary analysis: [script](evaluateCrossValidationResults_Gardelle_180_Extra.py), [resulting plot](figures/evaluateCrossValidationResults_Gardelle_180_Extra.py_RelativeLF.pdf)






## Controlling for Initialization
* [resulting plot](figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_ALL_0_10.0_180.pdf)

## Variance

* [resulting plot](figures/RunGardelle_NaturalPrior_CenteredLoss_VIZ_OnlyModel.py_8_0_10.0_180.pdf)



## Grid granularity
Fits at N=1800 instead of N=180:
* naturalistic prior: [resulting plot](figures/RunGardelle_NaturalPrior_CosineLoss_VIZ_OnlyModel.py_8_0_20.0_1800.pdf)
* uniform prior: [resulting plot](figures/RunGardelle_UniformPrior_CosineLoss_VIZ_OnlyModel.py_8_0_20.0_1800.pdf)
* [fitted model](RunGardelle_NaturalPrior_CosineLoss.py_8_0_20.0_1800.txt.txt)
* [fitted model](RunGardelle_UniformPrior_CosineLoss.py_8_0_20.0_1800.txt.txt)

# Recoverability Simulation

