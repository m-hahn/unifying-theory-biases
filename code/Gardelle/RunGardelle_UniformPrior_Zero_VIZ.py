import computations
import getObservations
import glob
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sys
import torch
from computations import computeResources
from getObservations import retrieveObservations
from loadGardelle import *
from loadModel import loadModel
from matplotlib import rc
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import makeGridIndicesCircular
from util import product
from util import savePlot
from util import toFactor

rc('font', **{'family':'FreeSans'})

OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
assert P == 0

FOLD_HERE = int(sys.argv[2])
REG_WEIGHT = float(sys.argv[3])
GRID = int(sys.argv[4])
SHOW_PLOT = (len(sys.argv) < 6) or (sys.argv[5] == "SHOW_PLOT")
DEVICE = 'cpu'

FILE = f"logs/CROSSVALID/{__file__.replace('_VIZ', '')}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"

# Helper Functions dependent on the device

##############################################

# Store observations
assert (observations_x == sample).all()
assert (observations_y == responses).all()

#############################################################
# Part: Partition data into folds. As described in the paper,
# this is done within each subject.
N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

Fold = 0*Subject
for i in range(int(min(Subject)), int(max(Subject))+1):
    trials = [j for j in range(Subject.size()[0]) if Subject[j] == i]
    randomGenerator.shuffle(trials)
    foldSize = int(len(trials)/N_FOLDS)
    for k in range(N_FOLDS):
        Fold[trials[k*foldSize:(k+1)*foldSize]] = k

##############################################
# Part: Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 360

CIRCULAR = True
INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
grid, grid_indices_here = makeGridIndicesCircular(GRID, MIN_GRID, MAX_GRID)
assert grid_indices_here.max() >= GRID, grid_indices_here.max()

# Part: Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

##############################################
# Initialize the model
# Part: Initialize the model
init_parameters = {}
init_parameters["sigma2_stimulus"] = MakeFloatTensor([0]).view(1)
init_parameters["log_motor_var"] = MakeFloatTensor([0]).view(1)
init_parameters["sigma_logit"] = MakeFloatTensor(6*[-3]).view(6)
init_parameters["mixture_logit"] = MakeFloatTensor([-1]).view(1)
init_parameters["prior"] = MakeZeros(GRID)
init_parameters["volume"] = MakeZeros(GRID)
loadModel(FILE, init_parameters)
assert "volume" in init_parameters
for _, y in init_parameters.items():
    y.requires_grad = True

# Initialize optimizer.
# The learning rate is a user-specified parameter.
learning_rate=.1
optim = torch.optim.SGD([y for _, y in init_parameters.items()], lr=learning_rate)

##############################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 2*math.pi

# Part: Specify `similariy` or `difference` functions.
## These are negative squared distances (for interval spaces) or
## trigonometric functions (for circular spaces), with
## some extra factors for numerical purposes.
## Exponentiating a `similarity` function and normalizing
## is equivalent to the Gaussian / von Mises density.
## The purpose of specifying these as `closeness` or `distance`,
## rather than simply calling squared or trigonometric
## functions is to  flexibly reuse the same model code for
## both interval and circular spaces.
def SQUARED_STIMULUS_DIFFERENCE(x):
    return torch.sin(math.pi*x/180)
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(math.pi*x/180)
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(x)
def SQUARED_SENSORY_DIFFERENCE(x):
    return torch.sin(x)

##############################################
# Import the appropriate estimator for minimizing the loss function
SCALE = 50

KERNEL_WIDTH = 0.05

averageNumberOfNewtonSteps = 2

class MAPCircularEstimator(torch.autograd.Function):
    """
    We can implement our own custom autograd Functions by subclassing
    torch.autograd.Function and implementing the forward and backward passes
    which operate on Tensors.
    """

    # For this MAP estimator implementation, there are checks in ExampleGardelle_Inference_Discretized_Likelihood_Nonparam_MotorNoise1_Lp_Scale_NewStrict_NaturalPrior_ZeroExponent4_CROSSVALI_DebugD6_Smoothed2_UnitTest_GD_Check_Unnormalized_Hess_FineGrid_Fix2.py
    @staticmethod
    def forward(ctx, grid_indices_here, posterior):
        """
        In the forward pass we receive a Tensor containing the input and return
        a Tensor containing the output. ctx is a context object that can be used
        to stash information for backward computation. You can cache arbitrary
        objects for use in the backward pass using the ctx.save_for_backward method.
        """
        global averageNumberOfNewtonSteps
        # Step 1: To avoid over-/underflow, rescale the indices
        grid_indices_here = grid_indices_here/SCALE
        n_inputs, n_batch = posterior.size()

        # Step 1: Identify the argmax on the full discretized grid
        kernelFunctionForAll = torch.nn.functional.softmax(-(grid_indices_here.view(GRID, 1, GRID) - grid_indices_here.view(1,GRID,GRID)).pow(2) / (2*(KERNEL_WIDTH**2)), dim=1)
        smoothedPosteriorForAll = (kernelFunctionForAll * posterior.view(1,GRID,GRID)).sum(dim=1)
        resultDiscrete = smoothedPosteriorForAll.argmax(dim=0)
        resultDiscrete1 = resultDiscrete
        resultDiscreteFromRaw = resultDiscrete

        resultDiscrete = torch.stack([grid_indices_here[resultDiscrete[i],i] for i in range(GRID)], dim =0)

        # Step 2: Improve estimator by optimizing one finer grid centered around the coarse maximum.
        # A small trust region leads to a finer grid and a better starting point of Newton's.
        # But sometimes the optimum will be found on the boundary of the trust region. In this case, we increase the size of the trust region by a factor of four and try again. NOTE: This behavior only makes sense when the optimum is not at the boundary of the overall stimulus space.
        # One issue that can make the small trust region very important is when there are abrupt spikes in the discretized poosterior.
        Q = 20
        FINE_GRID_SCALE = 2
        for J in range(1):
           environment = (torch.arange(start=-Q, end=Q+1)/(FINE_GRID_SCALE*SCALE)).view(-1, 1) + resultDiscrete.view(1, -1)
           K = environment.size()[0]
           kernelFunction = torch.nn.functional.softmax(-(environment.view(K, 1, GRID) - grid_indices_here.view(1,GRID,GRID)).pow(2) / (2*(KERNEL_WIDTH**2)), dim=1)
           smoothedPosterior = (kernelFunction * posterior.view(1,GRID,GRID)).sum(dim=1)
           smoothedPosteriorInEnvironment = smoothedPosterior.detach()

           argmaxInFineGrid = smoothedPosterior.argmax(dim=0)
           onBoundary = torch.logical_or(argmaxInFineGrid == 0, argmaxInFineGrid== K-1)
           if onBoundary.float().sum() > 0:
               print("Warning: Some points are on the boundary of the trust region in iteration ",J, "Retrying with a larger region" if J == 0 else "Accepting nonetheless", "# of points:", onBoundary.float().sum())
               FINE_GRID_SCALE = .5
           else:
               break
        resultFromIntermediateGrid = ((argmaxInFineGrid-K/2)/(FINE_GRID_SCALE*SCALE) + resultDiscrete)

        PLOT = (random.random() < .1) and False
        if PLOT:
           # Plot the smoothed posterior
           figure, axis = plt.subplots(1, 2)
           axis[0].scatter(SCALE*environment[:,0], smoothedPosterior[:,0])
           axis[1].scatter(SCALE*environment[:,80], smoothedPosterior[:,80])
           Z = max(float(smoothedPosterior[:,0].max()), float(smoothedPosterior[:,80].max()))
           Y = min(float(smoothedPosterior[:,0].min()), float(smoothedPosterior[:,80].min()))

        if PLOT:
           axis[0].plot([SCALE*resultDiscrete[0], SCALE*resultDiscrete[0]], [Y,Z], color="orange")
           axis[0].plot([SCALE*resultFromIntermediateGrid[0], SCALE*resultFromIntermediateGrid[0]], [Y,Z], color="green")
           axis[1].plot([SCALE*resultDiscrete[80], SCALE*resultDiscrete[80]], [Y,Z], color="orange")
           axis[1].plot([SCALE*resultFromIntermediateGrid[80], SCALE*resultFromIntermediateGrid[80]], [Y,Z], color="green")

        # Now use the result from the intermediate grid
        resultDiscrete = resultFromIntermediateGrid
        resultDiscrete = resultDiscrete.clamp(min=MIN_GRID/SCALE, max=MAX_GRID/SCALE)

        optimizationSequence = []
        if True:
          for i in range(40):
             # First compute P'(theta)
             innerDerivatives = -(resultDiscrete.view(1, GRID) - grid_indices_here) / (KERNEL_WIDTH**2)
             kernelFunction = torch.nn.functional.softmax(-(resultDiscrete.view(1, GRID) - grid_indices_here).pow(2) / (2*(KERNEL_WIDTH**2)), dim=0)
             if OPTIMIZER_VERBOSE:
               smoothedPosterior = (kernelFunction * posterior).sum(dim=0)

             kernelFunctionDerivativeI = innerDerivatives * kernelFunction
             # This sum is needed repeatedly
             kernelFunctionDerivativeISum = kernelFunctionDerivativeI.sum(dim=0, keepdim=True)

             kernelDerivative = kernelFunctionDerivativeI - kernelFunction * kernelFunctionDerivativeISum
             derivativeSmoothedPosterior = (kernelDerivative * posterior).sum(dim=0)

             # First derivative has been verified against Pytorch autograd 

             if True:
               smoothedPosterior = (kernelFunction * posterior).sum(dim=0)
             # Now compute P''(theta)
             kernelFunctionSecondDerivativeI = -1/(KERNEL_WIDTH**2) * kernelFunction + innerDerivatives.pow(2) * kernelFunction
             part1 = kernelFunctionSecondDerivativeI - kernelFunctionDerivativeI * kernelFunctionDerivativeISum
             kernelSecondDerivative = part1 - kernelDerivative * kernelFunctionDerivativeISum - kernelFunction * part1.sum(dim=0, keepdim=True)
             secondDerivativeSmoothedPosterior = (kernelSecondDerivative * posterior).sum(dim=0)
             # Second derivative has been verified against finite differences of the first derivatives
             # These calculations are all bottlenecks. Could perhaps reduce some of the inefficiency.

             hessian = secondDerivativeSmoothedPosterior
             if OPTIMIZER_VERBOSE and i % 10 == 0 or False:
                print(i, smoothedPosterior.mean(), derivativeSmoothedPosterior.abs().mean(), derivativeSmoothedPosterior.abs().max(), "\t", resultDiscrete[10:15]*SCALE)

             # Optimization step
             # For those batch elements where P'' < 0, we do a Newton step.
             # For the others, we do a GD step, with stepsize based 1/P'', cutting off excessively large resulting stepsizes
             # For a well-behaved problem (concave within the trust region and attains its maximum on it), only Newton steps should be required. GD steps are intended as a fallback when this is not satisfied.
             MASK = hessian>=-0.001
             updateHessian =  - derivativeSmoothedPosterior / hessian.clamp(max=-0.001)
             updateGD = .2 * derivativeSmoothedPosterior
             updateGD = derivativeSmoothedPosterior / hessian.abs().clamp(min=0.01)
             # Prevent excessive jumps, and decay the allowed learning rate.
             MAXIMAL_UPDATE_SIZE = 0.1 / (1+i/5)
             update = torch.where(MASK, updateGD, updateHessian).clamp(min=-MAXIMAL_UPDATE_SIZE, max=MAXIMAL_UPDATE_SIZE)
             if random.random() < 0.002:
               print("Maximal update", update.abs().max(), update.abs().median(), (update.abs() >= 0.1).float().sum(), "Doing Non-Newton", MASK.float().sum(), hessian.abs().median())
             resultDiscrete = resultDiscrete + update
             optimizationSequence.append((resultDiscrete, smoothedPosterior, derivativeSmoothedPosterior, secondDerivativeSmoothedPosterior))
             if PLOT:
                axis[0].plot([SCALE*resultDiscrete[0], SCALE*resultDiscrete[0]], [Y,Z], color="red")
                axis[1].plot([SCALE*resultDiscrete[80], SCALE*resultDiscrete[80]], [Y,Z], color="red")
             if float(derivativeSmoothedPosterior.abs().max()) < 1e-5:
                 break

             # Check whether solution has left trust region
             lowerThanTrustRegion = (environment[0] > resultDiscrete)
             higherThanTrustRegion = (environment[-1] < resultDiscrete)
             if lowerThanTrustRegion.float().sum() + higherThanTrustRegion.float().sum() > 0:
                 print("Warning: some batches have left the trust region.", lowerThanTrustRegion.float().sum(), higherThanTrustRegion.float().sum())
        else:
            assert False
        averageNumberOfNewtonSteps = 0.98 * averageNumberOfNewtonSteps + (1-0.98) * i
        if random.random() < 0.003:
           print("Number of Newton iterations", i, "average", averageNumberOfNewtonSteps)
        if i > 20:
            print("Warning: Finding MAP estimator took", i, "iterations. Maximal gradient", float(derivativeSmoothedPosterior.abs().max()))

        if float(derivativeSmoothedPosterior.abs().max()) > 1e-4:
            print("Warning: Finding MAP estimator took", i, "iterations. Maximal gradient", float(derivativeSmoothedPosterior.abs().max()))
            worst = derivativeSmoothedPosterior.abs().argmax()

            print(sorted(derivativeSmoothedPosterior.detach().cpu().numpy().tolist()))
            print(derivativeSmoothedPosterior)
            print("PROBLEM", worst, derivativeSmoothedPosterior[worst], float(derivativeSmoothedPosterior.abs().max()), derivativeSmoothedPosterior[GRID-1])
            plt.close()
            figure, axis = plt.subplots(1, 2, figsize=(15,15))
            axis[0].scatter(SCALE*grid_indices_here[:,0].detach().cpu(), posterior[:,0].detach().cpu(), color="gray")

            kernelFunction = torch.nn.functional.softmax(-(grid_indices_here.view(GRID, 1, GRID) - grid_indices_here.view(1,GRID,GRID)).pow(2) / (2*(KERNEL_WIDTH**2)), dim=1)
            smoothedPosteriorOverAll = (kernelFunction * posterior.view(1,GRID,GRID)).sum(dim=1)

            KernelTimesMinusOnePlusKernel = kernelFunction * (kernelFunction-1)
            kernelFunctionDerivative = 2 * (grid_indices_here.view(GRID, 1, GRID) - grid_indices_here.view(1,GRID,GRID)) / (2*(KERNEL_WIDTH**2)) * KernelTimesMinusOnePlusKernel
            kernelFunctionSecondDerivative = 1 / (KERNEL_WIDTH**2) * KernelTimesMinusOnePlusKernel + ((grid_indices_here.view(GRID, 1, GRID) - grid_indices_here.view(1,GRID,GRID)) / (KERNEL_WIDTH**2)) * (kernelFunctionDerivative * (kernelFunction-1) + kernelFunction*kernelFunctionDerivative)
            hessianOverAll = (kernelFunctionSecondDerivative * posterior).sum(dim=1)
            derivativeSmoothedPosteriorOverAll = (kernelFunctionDerivative * posterior).sum(dim=1)

            axis[0].scatter(SCALE*grid_indices_here[:,0].detach().cpu(), smoothedPosteriorOverAll[:,0].detach().cpu(), color="yellow")
            axis[0].scatter(SCALE*grid_indices_here[:,0].detach().cpu(), derivativeSmoothedPosteriorOverAll[:,0].detach().cpu(), color="purple")

            axis[0].scatter(SCALE*environment[:,0].detach().cpu(), smoothedPosteriorInEnvironment[:,0].detach().cpu())
            axis[1].scatter(SCALE*grid_indices_here[:,worst].detach().cpu(), posterior[:,worst].detach().cpu(), color="gray")
            axis[1].scatter(SCALE*grid_indices_here[:,worst].detach().cpu(), smoothedPosteriorOverAll[:,worst].detach().cpu(), color="yellow")
            axis[1].scatter(SCALE*grid_indices_here[:,worst].detach().cpu(), derivativeSmoothedPosteriorOverAll[:,worst].detach().cpu(), color="purple")
            axis[1].scatter(SCALE*environment[:,worst].detach().cpu(), smoothedPosteriorInEnvironment[:,worst].detach().cpu())
            Z = max(float(smoothedPosteriorInEnvironment[:,0].max()), float(smoothedPosteriorInEnvironment[:,worst].max()))
            Y = min(float(smoothedPosteriorInEnvironment[:,0].min()), float(smoothedPosteriorInEnvironment[:,worst].min()))
            for x, y, z, u in optimizationSequence:
               print(x[0], x[worst], "P", y[worst], "dP", z[worst], "d2P", u[worst], derivativeSmoothedPosterior[worst], resultDiscrete[worst])
               axis[0].plot([SCALE*x[0].detach().cpu(), SCALE*x[0].detach().cpu()], [Y,Z], color="yellow")
               axis[1].plot([SCALE*x[worst].detach().cpu(), SCALE*x[worst].detach().cpu()], [Y,Z], color="yellow")
            axis[0].plot([SCALE*resultDiscrete1[0].detach().cpu(), SCALE*resultDiscrete1[0].detach().cpu()], [Y,Z], color="orange")
            axis[1].plot([SCALE*resultDiscrete1[worst].detach().cpu(), SCALE*resultDiscrete1[worst].detach().cpu()], [Y,Z], color="orange")
            axis[0].plot([SCALE*resultDiscrete[0].detach().cpu(), SCALE*resultDiscrete[0].detach().cpu()], [Y,Z], color="red")
            axis[1].plot([SCALE*resultDiscrete[worst].detach().cpu(), SCALE*resultDiscrete[worst].detach().cpu()], [Y,Z], color="red")
            savePlot(f"figures/DEBUG_{__file__}.pdf")
            plt.close()
            assert False

        if PLOT:
           plt.show()
           plt.close()

        result = resultDiscrete

        ctx.save_for_backward(grid_indices_here, posterior, result)
        return result.detach()*SCALE

    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor containing the gradient of the loss
        with respect to the output, and we need to compute the gradient of the loss
        with respect to the input.
        """
        grid_indices_here, posterior, result = ctx.saved_tensors

        #################
        # g'_j and g''_j are computed as in during the Newton iterations of the forward pass
        innerDerivatives = -(result.view(1, GRID) - grid_indices_here) / (KERNEL_WIDTH**2)
        kernelFunction = torch.nn.functional.softmax(-(result.view(1, GRID) - grid_indices_here).pow(2) / (2*(KERNEL_WIDTH**2)), dim=0)
        kernelFunctionDerivativeI = innerDerivatives * kernelFunction
        # This sum is needed repeatedly
        kernelFunctionDerivativeISum = kernelFunctionDerivativeI.sum(dim=0, keepdim=True)
        kernelDerivative = kernelFunctionDerivativeI - kernelFunction * kernelFunctionDerivativeISum
        derivativeSmoothedPosterior = (kernelDerivative * posterior).sum(dim=0)

        kernelFunctionSecondDerivativeI = -1/(KERNEL_WIDTH**2) * kernelFunction + innerDerivatives.pow(2) * kernelFunction
        part1 = kernelFunctionSecondDerivativeI - kernelFunctionDerivativeI * kernelFunctionDerivativeISum
        kernelSecondDerivative = part1 - kernelDerivative * kernelFunctionDerivativeISum - kernelFunction * part1.sum(dim=0, keepdim=True)
        secondDerivativeSmoothedPosterior = (kernelSecondDerivative * posterior).sum(dim=0)
        hessian = secondDerivativeSmoothedPosterior
        #################

        # Now using implicit differentiation
        gradient_implicit = - kernelDerivative / hessian
        gradient = grad_output.unsqueeze(0) * gradient_implicit

        return None, gradient*SCALE

# Run the model
def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, computePredictions=False, subject=None, sigma_stimulus=None, sigma2_stimulus=None, duration_=None, folds=None, lossReduce='mean'):

 # Part: Obtain the motor variance by exponentiating the appropriate model parameter
 motor_variance = torch.exp(- parameters["log_motor_var"])
 # Part: Obtain the sensory noise variance.
 sigma2 = 4*torch.sigmoid(sigma_logit)
 # Part: Obtain the transfer function as the cumulative sum of the discretized resource allocation (referred to as `volume` element due to the geometric interpretation by Wei&Stocker 2015)
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 if True:
  # Part: Select data for the relevant fold
  folds = MakeLongTensor(folds)
  if subject is not None:
    assert False
  else:
    MASK = torch.logical_and(duration==duration_, (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]
  assert stimulus.view(-1).size()[0] > 0

  # Part: Apply stimulus noise, if nonzero.
  if sigma2_stimulus > 0:
    ## On this dataset, this is zero, so the
    ## code block will not be used.
    assert False
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))
    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  # Part: Compute sensory likelihoods. Across both interval and
  ## circular stimulus spaces, this amounts to exponentiaring a
  ## `similarity`
  sensory_likelihoods = torch.softmax(((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2))  + volumeElement.unsqueeze(1).log(), dim=0)

  # Part: If stimulus noise is nonzero, convolve the likelihood with the
  ## stimulus noise.
  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    ## On this dataset, this is zero, so the
    ## code block will not be used.
    assert False
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  ## Compute posterior using Bayes' rule. As described in the paper, the posterior is computed
  ## in the discretized stimulus space.
  posterior = prior.unsqueeze(1) * likelihoods.t()
  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  # Estimator
  bayesianEstimate = MAPCircularEstimator.apply(grid_indices_here, posterior)

  ## Compute the motor likelihood
  ## `error' refers to the stimulus similarity between the estimator assigned to each m and
  ## the observations found in the dataset.
  ## The Gaussian or von Mises motor likelihood is obtained by exponentiating and normalizing
  error = (SQUARED_STIMULUS_SIMILARITY(360/GRID*bayesianEstimate.unsqueeze(0) - responses.unsqueeze(1)))
  ## The log normalizing constants, for each m in the discretized sensory space
  log_normalizing_constant = torch.logsumexp((SQUARED_STIMULUS_SIMILARITY(grid))/motor_variance, dim=0) + math.log(2 * math.pi / GRID)
  ## The log motor likelihoods, for each pair of sensory encoding m and observed human response
  log_motor_likelihoods = (error/motor_variance) - log_normalizing_constant
  ## Obtaining the motor likelihood by exponentiating.
  motor_likelihoods = torch.exp(log_motor_likelihoods)
  ## Obtain the guessing rate, parameterized via the (inverse) logit transform as described in SI Appendix
  # Mixture of estimation and uniform response
  uniform_part = torch.sigmoid(parameters["mixture_logit"])
  ## The full likelihood then consists of a mixture of the motor likelihood calculated before, and the uniform
  ## distribution on the full space.
  motor_likelihoods = (1-uniform_part) * motor_likelihoods + (uniform_part / (2*math.pi) + 0*motor_likelihoods)

  # Now the loss is obtained by marginalizing out m from the motor likelihood
  if lossReduce == 'mean':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().mean()
  elif lossReduce == 'sum':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().sum()
  else:
    assert False

  ## If computePredictions==True, compute the bias and variability of the estimate
  if computePredictions:
     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS
     bayesianEstimate_avg_byStimulus = computeCircularMeanWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = (bayesianEstimate_sd_byStimulus.pow(2) + motor_variance * math.pow(180/math.pi,2)).sqrt()

     bayesianEstimate_avg_byStimulus = torch.where((bayesianEstimate_avg_byStimulus-grid).abs()<180, bayesianEstimate_avg_byStimulus, torch.where(bayesianEstimate_avg_byStimulus > 180, bayesianEstimate_avg_byStimulus-360, bayesianEstimate_avg_byStimulus+360))

     assert float(((bayesianEstimate_avg_byStimulus-grid).abs()).max()) < 180, float(((bayesianEstimate_avg_byStimulus-grid).abs()).max())
     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = computeCircularMeanWeighted(posteriorMaxima.unsqueeze(1), likelihoods)
     encodingBias = computeCircularMeanWeighted(grid.unsqueeze(1), likelihoods)
     attraction = (posteriorMaxima-encodingBias)
     attraction1 = attraction
     attraction2 = attraction+360
     attraction3 = attraction-360
     attraction = torch.where(attraction1.abs() < 180, attraction1, torch.where(attraction2.abs() < 180, attraction2, attraction3))
  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction

## Pass data to auxiliary script used for retrieving smoothed fits from the dataset
getObservations.setData(x_set=x_set, observations_y=observations_y, xValues=xValues, duration=duration, grid=grid)

computations.setData(GRID=GRID, STIMULUS_SPACE_VOLUME=STIMULUS_SPACE_VOLUME)

def model(grid):

  lowestLoss = 10000
  for iteration in range(1):
   parameters = init_parameters
   ## In each iteration, recompute
   ## - the resource allocation (called `volume' due to a geometric interpretation)
   ## - the prior

   volume = SENSORY_SPACE_VOLUME * torch.softmax(parameters["volume"], dim=0)
   prior = torch.nn.functional.softmax(0*parameters["prior"], dim=0)

   loss = 0

   if iteration % 100 == 0:
     gridspec = dict(width_ratios=[1,0.2,1,0.2,1,1,1,0,0])
     figure, axis = plt.subplots(1, 9, figsize=(1.3*0.8*5/6*10,0.8*2), gridspec_kw=gridspec)
     plt.tight_layout()
     figure.subplots_adjust(wspace=0.1, hspace=0.0)

     PRI = 0
     ENC = 2
     ATT = 4
     REP = 5
     TOT = 6
     HUM = 8
     PAD = [1,3,7,8]
     axis[PRI].plot(grid, prior.detach(), color="gray")
     axis[PRI].set_ylim(bottom=0)
     axis[ENC].set_ylim(0, 0.5)
     axis[ENC].set_yticks(ticks=[0,0.2,0.4], labels=["0", "0.2", "0.4"])
     x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

   ## Separate train and test/heldout partitions of the data
   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   ## Iterate over the conditions and possibly subjects, if parameters are fitted separately.
   ## In this dataset, all parameters are fitted across subjects.
   for DURATION in range(2,6):
    for SUBJECT in [1]:
     ## Run the model at its current parameter values.

     loss_model, bayesianEstimate_model, bayesianEstimate_sd_byStimulus_model, attraction_model = computeBias(xValues, init_parameters["sigma_logit"][DURATION], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, computePredictions=(iteration%100 == 0), subject=None, sigma_stimulus=0, sigma2_stimulus=0, duration_=DURATION, folds=testFolds, lossReduce='sum')
     loss += loss_model

     if iteration % 100 == 0:
       axis[ENC].plot(grid, 2*computeResources(volume.detach(), inverse_variance=1/float((2 * torch.sigmoid(init_parameters["sigma_logit"][DURATION])))))
       y_set, sd_set = retrieveObservations(x, None, DURATION, meanMethod="circular")
       axis[TOT].plot(grid, (bayesianEstimate_model-grid).detach()/2)

     axis[ATT].plot(grid, (attraction_model).detach()/2)
     axis[REP].plot(grid, (bayesianEstimate_model-attraction_model-grid).detach()/2)

     for w in PAD:
       axis[w].set_visible(False)

     axis[PRI].set_yticks(ticks=[0], labels=[0])
     axis[REP].tick_params(labelleft=False)
     axis[TOT].tick_params(labelleft=False)

     for w in range(9):
       axis[w].spines['top'].set_visible(False)
       axis[w].spines['right'].set_visible(False)

     axis[ATT].set_ylim(-25, 25)
     axis[REP].set_ylim(-25, 25)
     axis[TOT].set_ylim(-25, 25)
     axis[HUM].set_ylim(-25, 25)
     if True:
       axis[PRI].set_title("Prior")
       axis[ENC].set_title("Resources")
       axis[ATT].set_title("Attraction")
       axis[REP].set_title("Repulsion")
       axis[TOT].set_title("Total")
       axis[HUM].set_title("Data")

   for w in [ATT, REP, TOT, HUM]:
      axis[w].set_yticks(ticks=[-20,0,20], labels=["-20°","0°","20°"])
   for w in range(9):
       axis[w].set_xticks(ticks=[0,180,360], labels=["0°", "90°", "180°"])

   if iteration % 100 == 0:
     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.pdf", transparent=True)

############################3

model(grid)
