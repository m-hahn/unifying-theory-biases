import math
import matplotlib.pyplot as plt
from evaluateCrossValidationResults2 import crossValidResults
from matplotlib import rc
from util import savePlot
rc('font', **{'family':'FreeSans'})

def mean(x):
    return sum(x)/len(x)

def round_(x):
    if str(x).lower() == "nan":
        return "--"
    else:
        return round(x)

def deltaDiff(x,y):
    if len(x) < 10 or len(y) < 10:
        return "--"
    return round(mean([x[i]-y[i] for i in range(len(x))]),1)

def deltaSD(x,y):
    if len(x) < 10 or len(y) < 10:
        return "--"
    mu = mean([x[i]-y[i] for i in range(len(x))])
    muSquared = mean([math.pow(x[i]-y[i],2) for i in range(len(x))])
    return round(math.sqrt(muSquared - math.pow(mu, 2)) / math.sqrt(10),1)

curves = {}
def plot(color, style, loss, result):
    if (color,style) not in curves:
       curves[(color, style)] = []
    if result[2] != result[2]:
        return
    curves[(color, style)].append((loss, result[2], result[3]))

curvesRelative = {}
def plotRelative(color, style, loss, result, resultRef):
    if (color,style) not in curvesRelative:
       curvesRelative[(color, style)] = []
    if result[2] != result[2]:
        return
    sd = deltaSD(result[4],resultRef[4])
    if sd == '--':
        return
    curvesRelative[(color, style)].append((loss, result[2]-resultRef[2], sd))

curvesRelativeLF = {}
def plotEffectOfLossFunction(color, style, loss, result, reference):
    if result is None:
        return
    if (color,style) not in curvesRelativeLF:
       curvesRelativeLF[(color, style)] = []
    meanRelative = result[2] - reference[2]
    sd = deltaSD(result[4], reference[4])
    if sd == '--':
        return
    curvesRelativeLF[(color, style)].append((loss, meanRelative, sd))

with open(f"output/{__file__}.tex", "w") as outFile:
 for loss in range(1,15):
    if loss == 1:
        continue
    if loss == 0:

       full = crossValidResults(f"RunGardelle_NaturalPrior_Zero.py_{loss}_*_10.0_180.txt", STRICT=True)
       full_freeprior = crossValidResults(f"RunGardelle_FreePrior_Zero.py_{loss}_*_10.0_180.txt", STRICT=True)
       full_uniprior = crossValidResults(f"RunGardelle_UniformPrior_Zero.py_{loss}_*_10.0_180.txt", STRICT=True)
       full_uniencoding = crossValidResults(f"RunGardelle_UniformEncoding_Zero.py_{loss}_*_10.0_180.txt", STRICT=True)
       full_powerlawprior = crossValidResults(f"ExampleGardelle_Inference_Discretized_Likelihood_Nonparam_MotorNoise1_Lp_Scale_NewStrict_PowerLawPrior_CROSSVALID_Zero_SignGD_Balance.py_{loss}_*_10.0_180.txt", STRICT=True)
       full_matchedprior = crossValidResults(f"ExampleGardelle_Inference_Discretized_Likelihood_Nonparam_MotorNoise1_Lp_Scale_NewStrict_MatchedPrior_CROSSVALID_Zero_SignGD_Balance.py_{loss}_*_10.0_180.txt", STRICT=True)
    else:
       full = crossValidResults(f"RunGardelle_NaturalPrior_CenteredLoss.py_{loss}_*_10.0_180.txt", STRICT=True)
       full_freeprior = crossValidResults(f"RunGardelle_FreePrior_CenteredLoss.py_{loss}_*_10.0_180.txt", STRICT=True)
       full_uniprior = crossValidResults(f"RunGardelle_UniformPrior_CenteredLoss.py_{loss}_*_10.0_180.txt", STRICT=True)
       full_uniencoding = crossValidResults(f"RunGardelle_UniformEncoding_CenteredLoss.py_{loss}_*_10.0_180.txt", STRICT=True)

    if loss == 0:
       cosine = full
       cosine_freeprior = full_freeprior
       cosine_uniprior = full_uniprior
       cosine_uniencoding = full_uniencoding
    else:
       cosine = crossValidResults(f"RunGardelle_NaturalPrior_CosineLoss.py_{loss}_*_10.0_180.txt", STRICT=True)
       cosine_freeprior = crossValidResults(f"RunGardelle_FreePrior_CosineLoss.py_{loss}_*_10.0_180.txt", STRICT=True)
       cosine_uniprior = crossValidResults(f"RunGardelle_UniformPrior_CosineLoss.py_{loss}_*_10.0_180.txt", STRICT=True)
       cosine_uniencoding = crossValidResults(f"RunGardelle_UniformEncoding_CosineLoss.py_{loss}_*_10.0_180.txt", STRICT=True)
       cosine_powerlawprior = crossValidResults(f"RunGardelle_PowerLawPrior_CosineLoss.py_{loss}_*_10.0_180.txt", STRICT=True)
       cosine_matchedprior = crossValidResults(f"RunGardelle_MatchedPrior_CosineLoss.py_{loss}_*_10.0_180.txt", STRICT=True)
       print("MATCHED", loss, cosine_matchedprior)
    COLOR_FREE = "green"
    COLOR_UNIFORM_PRIOR = "red"
    COLOR_UNIFORM_ENCODING = "blue"
    COLOR_HARD1 = "purple"
    COLOR_HARD2 = "orange"

    plot(COLOR_HARD1, "dotted", loss, cosine if loss > 0 else full)

    plot(COLOR_FREE, "dotted", loss, cosine_freeprior if loss > 0 else full_freeprior)
    plot(COLOR_UNIFORM_PRIOR, "dotted", loss, cosine_uniprior if loss > 0 else full_uniprior)
    plot(COLOR_UNIFORM_ENCODING, "dotted", loss, cosine_uniencoding if loss > 0 else full_uniencoding)
    plot("yellow", "dotted", loss, cosine_powerlawprior if loss > 0 else full_powerlawprior)
    plot("pink", "dotted", loss, cosine_matchedprior if loss > 0 else full_matchedprior)

    plotEffectOfLossFunction(COLOR_HARD1, "dotted", loss, cosine, crossValidResults(f"RunGardelle_FreePrior_CosineLoss.py_8_*_10.0_180.txt"))
    plotEffectOfLossFunction(COLOR_FREE, "dotted", loss, cosine_freeprior, crossValidResults(f"RunGardelle_FreePrior_CosineLoss.py_8_*_10.0_180.txt"))
    plotEffectOfLossFunction(COLOR_UNIFORM_PRIOR, "dotted", loss, cosine_uniprior, crossValidResults(f"RunGardelle_FreePrior_CosineLoss.py_8_*_10.0_180.txt"))
    plotEffectOfLossFunction(COLOR_UNIFORM_ENCODING, "dotted", loss, cosine_uniencoding, crossValidResults(f"RunGardelle_FreePrior_CosineLoss.py_8_*_10.0_180.txt"))
    plotEffectOfLossFunction("yellow", "dotted", loss, cosine_powerlawprior if loss > 0 else full_powerlawprior, crossValidResults(f"RunGardelle_FreePrior_CosineLoss.py_8_*_10.0_180.txt"))
    plotEffectOfLossFunction("pink", "dotted", loss, cosine_matchedprior if loss > 0 else full_matchedprior, crossValidResults(f"RunGardelle_FreePrior_CosineLoss.py_8_*_10.0_180.txt"))

    plotRelative(COLOR_HARD1, "solid", loss, full, full_freeprior)
    plotRelative(COLOR_HARD1, "dotted", loss, cosine if loss > 0 else full, cosine_freeprior if loss > 0 else full_freeprior)
    plotRelative(COLOR_FREE, "solid", loss, full_freeprior, full_freeprior)
    plotRelative(COLOR_FREE, "dotted", loss, cosine_freeprior if loss > 0 else full_freeprior, cosine_freeprior if loss > 0 else full_freeprior)
    plotRelative(COLOR_UNIFORM_PRIOR, "solid", loss, full_uniprior, full_freeprior)
    plotRelative(COLOR_UNIFORM_PRIOR, "dotted", loss, cosine_uniprior if loss > 0 else full_uniprior, cosine_freeprior if loss > 0 else full_freeprior)
    plotRelative(COLOR_UNIFORM_ENCODING, "solid", loss, full_uniencoding, full_freeprior)
    plotRelative(COLOR_UNIFORM_ENCODING, "dotted", loss, cosine_uniencoding if loss > 0 else full_uniencoding, cosine_freeprior if loss > 0 else full_freeprior)
    plotRelative("yellow", "dotted", loss, cosine_powerlawprior if loss > 0 else full_powerlawprior, cosine_freeprior if loss > 0 else full_freeprior)
    plotRelative("pink", "dotted", loss, cosine_matchedprior if loss > 0 else full_matchedprior, cosine_freeprior if loss > 0 else full_freeprior)

minY = 100000000000000
maxY = -100000000000000
figure, axis = plt.subplots(1,1, figsize=(2,2))
plt.tight_layout()
for key, values in curvesRelativeLF.items():
    color, style = key
    if color != COLOR_FREE or style != "dotted":
        continue
    if len(values) == 0:
        continue
    x, y, errors = zip(*values)
    print(x, y, errors)
    color = "blue"

    axis.plot(x, y, color=color, linestyle=style)
    axis.scatter(x, y, color=color)
    minY = min(minY, min(y))
    maxY = max(maxY, max(y))
    axis.errorbar(x, y, yerr=[z for z in errors], color=color)
print(minY, maxY)
axis.set_xlim(-1,11)
axis.set_ylim(minY-10, maxY+10)
savePlot(f"figures/{__file__}_simple.pdf")
plt.show()

minY = 100000000000000
maxY = -100000000000000
figure, axis = plt.subplots(1,1, figsize=(3,3))
plt.tight_layout()
for key, values in curves.items():
    print("CURVE", key, values)
    color, style = key
    if len(values) == 0:
        continue
    x, y, errors = zip(*values)
    print(x, y, errors)
    i = ["solid", "dotted"].index(style)
    axis.plot(x, y, color=color, linestyle=style)
    axis.scatter(x, y, color=color)
    minY = min(minY, min(y))
    maxY = max(maxY, max(y))
    axis.errorbar(x, y, yerr=errors, color=color)

print(minY, maxY)
axis.set_ylim(minY-10, maxY+10)
axis.set_ylim(minY-10, maxY+10)
axis.set_xlim(-1, 13)
axis.set_xlim(-1, 13)
axis.plot([0,16], [minY, minY], linestyle='dotted')
axis.plot([0,16], [minY, minY], linestyle='dotted')
savePlot(f"figures/{__file__}.pdf")
plt.show()

figure, axis = plt.subplots(1, 1, figsize=(3,3))
plt.tight_layout()
counter = 0
for key, values in curvesRelative.items():
    counter += 1
    color, style = key
    if len(values) == 0:
        continue
    x, y, errors = zip(*values)
    x = [z + 0.2*(counter-2) for z in x]
    print(x, y, errors)
    i = ["solid", "dotted"].index(style)
    axis.plot(x, y, color=color, linestyle=style)
    axis.scatter(x, y, color=color)
    axis.errorbar(x, y, yerr=errors, color=color)

for i in range(2):
 axis.plot([0,16], [0,0])
 axis.set_xlabel("Exponent")
 axis.set_ylabel("Δ NLL")
axis.set_xlim(-1, 15)
axis.set_xlim(-1, 15)
savePlot(f"figures/{__file__}_Relative.pdf")
plt.show()

minY = 100000000000000
maxY = -100000000000000
figure, axis = plt.subplots(1, 1, figsize=(3,3), layout='constrained')

for key, values in curvesRelativeLF.items():
    color, style = key
    if len(values) == 0:
        continue
    x, y, errors = zip(*values)
    print(x, y, errors)
    i = ["solid", "dotted"].index(style)
    minY = min(minY, min(y))
    maxY = max(maxY, max(y))
    axis.plot(x, y, color=color, linestyle=style)
    axis.scatter(x, y, color=color, s=10)
    axis.errorbar(x, y, yerr=errors, color=color)

for i in range(1):
 axis.plot([0,16], [0,0], color="gray", linestyle="dotted")
 axis.set_xlabel("Exponent")
axis.set_ylabel("Δ NLL")
axis.set_ylim(minY-10, maxY+10)
axis.set_ylim(minY-10, maxY+10)
axis.set_xlim(1, 13)
axis.set_xlim(1, 13)

savePlot(f"figures/{__file__}_RelativeLF.pdf")
plt.show()
