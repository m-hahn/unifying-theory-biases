import computations
import getObservations
import glob
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import torch
from computations import computeResources
from cosineEstimator import CosineEstimator
from getObservations import retrieveAndSmoothObservationsDirect
from loadGardelle import *
from matplotlib import rc
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import makeGridIndicesCircular
from util import product
from util import savePlot
from util import toFactor

rc('font', **{'family':'FreeSans'})

# Helper Functions dependent on the device

##############################################

# Store observations
assert (observations_x == sample).all()
assert (observations_y == responses).all()

MIN_GRID = 0
MAX_GRID = 360
GRID = 20

INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
grid, grid_indices_here = makeGridIndicesCircular(GRID, MIN_GRID, MAX_GRID)
assert grid_indices_here.max() >= GRID, grid_indices_here.max()

# Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

#############################################################
# Part: Initialize the model
init_parameters = {}
init_parameters["sigma2_stimulus"] = MakeFloatTensor([0]).view(1)
init_parameters["log_motor_var"] = MakeFloatTensor([0]).view(1)
init_parameters["sigma_logit"] = MakeFloatTensor(6*[-3]).view(6)
init_parameters["mixture_logit"] = MakeFloatTensor([-1]).view(1)
init_parameters["prior"] = MakeZeros(GRID)
init_parameters["volume"] = MakeZeros(GRID)

##############################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 2*math.pi

# Part: Specify `similariy` or `difference` functions.
## These are negative squared distances (for interval spaces) or
## trigonometric functions (for circular spaces), with
## some extra factors for numerical purposes.
## Exponentiating a `similarity` function and normalizing
## is equivalent to the Gaussian / von Mises density.
## The purpose of specifying these as `closeness` or `distance`,
## rather than simply calling squared or trigonometric
## functions is to  flexibly reuse the same model code for
## both interval and circular spaces.
def SQUARED_STIMULUS_DIFFERENCE(x):
    return torch.sin(math.pi*x/180)
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(math.pi*x/180)
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(x)
def SQUARED_SENSORY_DIFFERENCE(x):
    return torch.sin(x)

SCALE = 50

## Pass data to auxiliary script used for retrieving smoothed fits from the dataset
getObservations.setData(x_set=x_set, observations_y=observations_y, xValues=xValues, duration=duration, grid=grid, SQUARED_STIMULUS_SIMILARITY=SQUARED_STIMULUS_SIMILARITY, Subject=Subject)

computations.setData(GRID=GRID, STIMULUS_SPACE_VOLUME=STIMULUS_SPACE_VOLUME)

def model(grid):

  lowestLoss = 10000
  for iteration in range(1):
   parameters = init_parameters

   ## In each iteration, recompute
   ## - the resource allocation (called `volume' due to a geometric interpretation)
   ## - the prior

   volume = SENSORY_SPACE_VOLUME * torch.softmax(parameters["volume"], dim=0)
   prior = torch.softmax(0*parameters["prior"], dim=0)

   loss = 0

   if iteration % 100 == 0:
     figure, axis = plt.subplots(1, 1, figsize=(0.95*1.2*2,0.95*1.2*1.5))
     plt.tight_layout()

     x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

   for DURATION in range(2,6):
    for SUBJECT in [1]:

     if iteration % 100 == 0:

       x_set, y_set, sd_set, y_errbar, sd_errbar = retrieveAndSmoothObservationsDirect(x, DURATION)

       CI_x = [float(grid[x]) for x in x_set]
       CI_sd1 = (.5*(sd_errbar[:,0])).detach().numpy().tolist()
       CI_sd2 = (.5*(sd_errbar[:,1])).detach().numpy().tolist()
       poly = plt.Polygon(list(zip(CI_x + CI_x[::-1], CI_sd1 + CI_sd2[::-1])), facecolor=(.65, .65, .65), alpha=0.5)

       axis.add_patch(poly)
       axis.plot([float(grid[x]) for x in x_set], sd_set/2)

     axis.set_xticks(ticks=[0,180,360], labels=["0", "90", "180"], fontsize=17)
     axis.set_yticks(ticks=[0,20,40], labels=["0", "", "40"], fontsize=17)
     axis.set_ylim(0, 40)
     if True:
         axis.tick_params(labelleft=False)
         axis.tick_params(labelbottom=False)
     axis.spines['top'].set_visible(False)
     axis.spines['right'].set_visible(False)

   if iteration % 100 == 0:

     savePlot(f"figures/{__file__}.pdf", transparent=True)
     plt.show()
     plt.close()

############################3

model(grid)
