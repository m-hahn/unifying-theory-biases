import glob
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sys
import torch
from loadModel import loadModel
from matplotlib import cm
from matplotlib import rc
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import ToDevice
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import product
from util import savePlot
from util import sign

rc('font', **{'family':'FreeSans'})

def cudaZeros(*x):
    return torch.zeros(x)

OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
assert P > 0
FOLD_HERE = int(sys.argv[2])
REG_WEIGHT = float(sys.argv[3])
assert REG_WEIGHT == 1.0
GRID = int(sys.argv[4])
assert GRID % 180 == 0
FOURIER_BASIS_SIZE = int(sys.argv[5])
assert FOURIER_BASIS_SIZE in [30, 50, 80]
SHOW_PLOT = (len(sys.argv) < 7) or (sys.argv[6] == "SHOW_PLOT")
DEVICE = 'cpu'

FILE = f"logs/CROSSVALID/{__file__.replace('_VIZ_Priors','')}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_{FOURIER_BASIS_SIZE}.txt"

if os.path.exists(f"logs/CROSSVALID/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"):
   quit()

# Helper Functions dependent on the device

files = sorted(glob.glob("data/Gekas/Exp1/session1/*mat") + glob.glob("data/Gekas/Exp1/session2/*mat"))
files = files + sorted(glob.glob("data/Gekas/Exp2/session1/*mat") + glob.glob("data/Gekas/Exp2/session2/*mat"))

target = []
response = []

transformed_sample_total = []
transformed_response_total = []
original_sample_total = []
original_response_total = []
contrast_total = []
subjects_total = []
condition_total = []
transformed_centralOrientationPerSubject = {}
original_centralOrientationPerSubject = {}
for f in files:
 annots = loadmat(f)
 orientation = annots["central_orientation"][0][0]

 transformed_orientation = orientation % 90 + 90
 original_orientation = orientation

 transformed_transformation = transformed_orientation - orientation
 original_transformation = 0
 print(f, orientation)
 data = annots["data"][200:]
 sample = MakeFloatTensor(data[:,0])
 transformed_sample = (sample + transformed_transformation) % 360
 original_sample = sample
 contrast = MakeLongTensor(data[:,1])
 response = MakeFloatTensor(data[:,4])
 transformed_response = (response + transformed_transformation) % 360
 original_response = response
 color = MakeLongTensor(data[:,9])

 samplesRed = sample[color == 1]
 samplesGreen = sample[color == 2]
 if computeCircularSDWeighted(samplesGreen) < computeCircularSDWeighted(samplesRed):
     bimodalCondition = 2
     condition = color
 else:
     bimodalCondition = 1
     condition = torch.where(color==0, 0, 3-color)

 contrast = torch.where(contrast == 1, 1, torch.where(contrast == 4, 4, condition+1))

 print(f)
 subjectHere = int(f[f.index("sub")+3:f.rfind("ses")])-1
 if "Exp2" in f:
    subjectHere = subjectHere+18
 transformed_centralOrientationPerSubject[subjectHere] = transformed_orientation
 original_centralOrientationPerSubject[subjectHere] = original_orientation
 transformed_sample_total.append(transformed_sample)
 original_sample_total.append(original_sample)
 subjects_total.append(subjectHere + MakeZeros(sample.size()[0]))
 transformed_response_total.append(transformed_response)
 original_response_total.append(original_response)
 contrast_total.append(contrast)
 condition_total.append(condition)
 print(subjectHere)

sample = torch.cat(original_sample_total, dim=0)
transformed_sample = torch.cat(transformed_sample_total, dim=0)
responses = torch.cat(original_response_total, dim=0)
transformed_responses = torch.cat(transformed_response_total, dim=0)
contrast = torch.cat(contrast_total, dim=0)
Subject = torch.cat(subjects_total, dim=0)
condition = torch.cat(condition_total, dim=0)

mask = (condition == 2)
sample = sample[mask]
responses = responses[mask]
condition = condition[mask]
contrast = contrast[mask]
Subject = Subject[mask]

observations_x = sample
observations_y = responses

N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

Fold = 0*Subject
for i in range(int(min(Subject)), int(max(Subject))+1):
    trials = [j for j in range(Subject.size()[0]) if Subject[j] == i]
    randomGenerator.shuffle(trials)
    foldSize = int(len(trials)/N_FOLDS)
    for k in range(N_FOLDS):
        Fold[trials[k*foldSize:(k+1)*foldSize]] = k

MIN_GRID = 0
MAX_GRID = 360

INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
def point(p, reference):
    p1 = p
    p2 = p+GRID
    p3 = p-GRID
    ds = [abs(reference-x) for x in [p1,p2,p3]]
    m = min(ds)
    if m == ds[0]:
        return p1
    elif m == ds[1]:
        return p2
    else:
        return p3
grid_indices_here = MakeFloatTensor([[point(y,x) for x in range(GRID)] for y in range(GRID)])
assert grid_indices_here.max() >= GRID, grid_indices_here.max()

xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

subject = 1

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

N_SUBJECTS = int(max(Subject))+1
subjectsOrdering = sorted(range(N_SUBJECTS), key=lambda x:original_centralOrientationPerSubject[x])

init_parameters = {}
init_parameters["sigma2_stimulus"] = MakeFloatTensor([0]).view(1)
init_parameters["log_motor_var"] = MakeFloatTensor(N_SUBJECTS*[0])
init_parameters["sigma_logit"] = -3 + MakeZeros(N_SUBJECTS, 6)
init_parameters["chance_prob_logit"] = MakeFloatTensor([-0]).view(1)
init_parameters["mixture_logit"] = MakeFloatTensor(N_SUBJECTS*[-1])
init_parameters["prior"] = MakeZeros(GRID)
init_parameters["volume"] = MakeZeros(GRID)
init_parameters["priorBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["prior_logits_effect_weight"] = MakeZeros(1)
init_parameters["volumeBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["volume_logits_effect_weight"] = MakeZeros(1)

init_parameters["SIGMA_priorBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["SIGMA_volumeBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)

loadModel(FILE, init_parameters)
assert "volume" in init_parameters
for _, y in init_parameters.items():
    y.requires_grad = True

learning_rate=.1
optim = torch.optim.SGD([y for _, y in init_parameters.items()], lr=learning_rate)

shapes = {x : y.size() for x, y in init_parameters.items()}

def priors(x):
   return priors_for_ps[x]

def SQUARED_STIMULUS_DIFFERENCE(x):
    return torch.sin(math.pi*x/180)
def SQUARED_STIMULUS_SIMILARITY(x):
    return torch.cos(math.pi*x/180)
def SQUARED_SENSORY_SIMILARITY(x):
    return torch.cos(x)
def SQUARED_SENSORY_DIFFERENCE(x):
    return torch.sin(x)

assert P >= 2

SCALE = 50

class LPEstimator(torch.autograd.Function):
    """
    We can implement our own custom autograd Functions by subclassing
    torch.autograd.Function and implementing the forward and backward passes
    which operate on Tensors.
    """

    @staticmethod
    def forward(ctx, grid_indices_here, posterior):
        """
        In the forward pass we receive a Tensor containing the input and return
        a Tensor containing the output. ctx is a context object that can be used
        to stash information for backward computation. You can cache arbitrary
        objects for use in the backward pass using the ctx.save_for_backward method.
        """
        grid_indices_here = grid_indices_here/SCALE
        n_inputs, n_batch = posterior.size()
        initialized = (grid_indices_here.data * posterior).detach().sum(dim=0).data
        result = initialized.clone()

        for itera in range(20):
          if OPTIMIZER_VERBOSE:
             loss = (((result.unsqueeze(0) - grid_indices_here).abs().pow(P)) * posterior.detach()).sum(dim=0).sum() / n_batch
          loss_gradient = ((P * torch.sign(result.unsqueeze(0) - grid_indices_here) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-1)) * posterior.detach()).sum(dim=0) / n_batch
          loss_gradient2 = ((P * (P-1) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-2)) * posterior.detach()).sum(dim=0) / n_batch
          result -= loss_gradient / loss_gradient2
          if OPTIMIZER_VERBOSE:
             print(itera, "max absolute gradient after Newton steps", loss_gradient.abs().max())
          if float(loss_gradient.abs().max()) < 1e-5:
              break
        if float(loss_gradient.abs().max()) >= 1e-5:
            print("WARNING", float(loss_gradient.abs().max()), "after ", itera, " Newton steps")
        if P == 2:
            assert itera == 0
        ctx.save_for_backward(grid_indices_here, posterior, result)
        return SCALE*result

    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor containing the gradient of the loss
        with respect to the output, and we need to compute the gradient of the loss
        with respect to the input.
        """
        grid_indices_here, posterior, result = ctx.saved_tensors
        initialized = (grid_indices_here.data * posterior).detach().sum(dim=0).data

        F = ((P * torch.sign(result.unsqueeze(0) - grid_indices_here) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-1)) * posterior.detach()).sum(dim=0)
        assert F.abs().mean() < 0.001, F
        dF_posterior = ((P * torch.sign(result.unsqueeze(0) - grid_indices_here) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-1)))
        dF_result = ((P * (P-1) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-2)) * posterior.detach()).sum(dim=0)

        gradient = - ((1 / dF_result).unsqueeze(0) * dF_posterior).detach().data

        gradient = grad_output.unsqueeze(0) * gradient

        return None, gradient*SCALE

class CosineEstimator(torch.autograd.Function):
    """
    We can implement our own custom autograd Functions by subclassing
    torch.autograd.Function and implementing the forward and backward passes
    which operate on Tensors.
    """

    @staticmethod
    def forward(ctx, grid_indices_here, posterior):
        """
        In the forward pass we receive a Tensor containing the input and return
        a Tensor containing the output. ctx is a context object that can be used
        to stash information for backward computation. You can cache arbitrary
        objects for use in the backward pass using the ctx.save_for_backward method.
        """
        grid_indices_here = grid_indices_here * 2 * math.pi/GRID
        n_inputs, n_batch = posterior.size()
        initialized = (grid_indices_here.data * posterior).detach().sum(dim=0).data.clone()
        result = initialized.clone()

        momentum = MakeZeros(GRID)

        # Preinitialize using ordinary Lp loss
        if False:
         for itera in range(20):
          if False:
            loss = (((result.unsqueeze(0)/SCALE - grid_indices_here/SCALE).abs().pow(P)) * posterior.detach()).sum(dim=0).sum() / 1
          loss_gradient = ((P * torch.sign(result.unsqueeze(0)/SCALE - grid_indices_here/SCALE) * (result.unsqueeze(0)/SCALE - grid_indices_here/SCALE).abs().pow(P-1)) * posterior.detach()).sum(dim=0) / 1
          loss_gradient2 = ((P * (P-1) * (result.unsqueeze(0)/SCALE - grid_indices_here/SCALE).abs().pow(P-2)) * posterior.detach()).sum(dim=0) / 1
          result -= loss_gradient / loss_gradient2
          if float(loss_gradient.abs().max()) < 1e-5:
              break

        for itera in range(30):

          if OPTIMIZER_VERBOSE:
            loss = (((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2)) * posterior.detach()).sum(dim=0)
          loss_gradient = (P/2 * (SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-1)) * posterior.detach()).sum(dim=0)

          loss_gradient2 = (P/2 * (SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-1)) * posterior.detach()).sum(dim=0) / 1
          if P == 2:
             pass
          else:
             loss_gradient2 = loss_gradient2 + (P/2 * (P/2-1) * (SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)).pow(2) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-2)) * posterior.detach()).sum(dim=0) / 1

          MASK = (loss_gradient2 > 0.001)
          updateGD = - loss_gradient /loss_gradient2.abs().clamp(min=0.0001)
          updateNewton = - loss_gradient/loss_gradient2

          update = torch.where(MASK, updateNewton, updateGD)
          result = result + update
          if OPTIMIZER_VERBOSE:
             print(itera, float(loss.mean()), "max absolute gradient after GD steps", loss_gradient.abs().max(), sum(MASK.float()), "Newton steps", "max update", update.abs().max())
          if float(loss_gradient.abs().max()) < 1e-6:
              break
        if random.random() < 0.01:
            print("Newton Iterations", itera)
        if float(loss_gradient.abs().max()) >= 1e-4:
            print("WARNING", float(loss_gradient.abs().max()), "after ", itera, " Newton steps")
            assert False
        ctx.save_for_backward(grid_indices_here, posterior, result)
        return result * GRID / (2*math.pi)

    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor containing the gradient of the loss
        with respect to the output, and we need to compute the gradient of the loss
        with respect to the input.
        """
        grid_indices_here, posterior, result = ctx.saved_tensors
        initialized = (grid_indices_here.data * posterior).detach().sum(dim=0).data

        n_inputs, n_batch = posterior.size()

        F = (P/2 * (SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-1)) * posterior.detach()).sum(dim=0)

        assert F.abs().mean() < 0.01, (F, F.abs().mean(), F.abs().max()/n_batch, n_batch, ((SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)) * posterior.detach()).sum(dim=0) / n_batch)

        dF_posterior = P/2 * (SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-1))
        dF_result = 0
        dF_result = dF_result + (P/2 * (SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-1)) * posterior.detach()).sum(dim=0)
        if P == 2:
           pass
        else:
           dF_result = dF_result + (P/2 * (P/2-1) * (SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)).pow(2) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-2)) * posterior.detach()).sum(dim=0)
        if OPTIMIZER_VERBOSE:
           if torch.isnan(result).any():
                print(result.size(), dF_result.size())
                assert False, result
           if torch.isnan(posterior).any():
                print(result.size(), dF_result.size())
                assert False, posterior
           if torch.isnan(dF_result).any():
                print(result[torch.isnan(dF_result)])
                print((SQUARED_STIMULUS_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)).pow(2))
                print(((1+1e-10-SQUARED_STIMULUS_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-2)))
                print(result.size(), dF_result.size(), posterior.size())
                assert False

        gradient = - ((1 / dF_result).unsqueeze(0) * dF_posterior).detach().data
        if OPTIMIZER_VERBOSE:
           if dF_result.abs().min() < .0001:
                assert False, dF_result.abs().min()
           if torch.isnan(dF_result).any():
                assert False, dF_result
        if OPTIMIZER_VERBOSE:
           if torch.isnan(dF_posterior).any():
                assert False, dF_posterior

        if OPTIMIZER_VERBOSE:
           if torch.isnan(grad_output).any():
                assert False, grad_output
        gradient = grad_output.unsqueeze(0) * gradient
        if OPTIMIZER_VERBOSE:
           if torch.isnan(gradient).any():
                assert False, gradient

        return None, gradient * GRID / (2*math.pi)

def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, computePredictions=False, sigma_stimulus=None, sigma2_stimulus=None, contrast_=None, folds=None, lossReduce='mean', subject=None):
 motor_variance = torch.exp(- parameters["log_motor_var"][subject])
 sigma2 = 2*torch.sigmoid(sigma_logit)
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 loss = 0
 if True:
  folds = MakeLongTensor(folds)
  if subject is not None:
    MASK = torch.logical_and(contrast==contrast_, torch.logical_and((Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0), Subject==subject))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]
  else:
    assert False
    MASK = torch.logical_and(contrast==contrast_, (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]
  assert stimulus.view(-1).size()[0] > 0, (contrast_)

  if sigma2_stimulus > 0:
    assert False
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))
    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  sensory_likelihoods = torch.nn.functional.softmax(((torch.cos(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(2*sigma2)) / math.sqrt(2*math.pi*sigma2)  + volumeElement.unsqueeze(1).log(), dim=0)

  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    assert False
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  posterior = prior.unsqueeze(1) * likelihoods.t()
  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  bayesianEstimate = CosineEstimator.apply(grid_indices_here, posterior)

  error = (SQUARED_STIMULUS_SIMILARITY(360/GRID*bayesianEstimate.unsqueeze(0) - responses.unsqueeze(1)))
  log_normalizing_constant = torch.logsumexp((SQUARED_STIMULUS_SIMILARITY(grid))/motor_variance, dim=0) + math.log(2 * math.pi / GRID)

  normalizing_constant = torch.exp((SQUARED_STIMULUS_SIMILARITY(grid))/motor_variance).sum() * 360 / GRID * 0.0175
  log_motor_likelihoods = (error/motor_variance) - log_normalizing_constant

  motor_likelihoods = torch.exp(log_motor_likelihoods)

  uniform_part = torch.sigmoid(parameters["mixture_logit"][subject])

  motor_likelihoods = (1-uniform_part) * motor_likelihoods + (uniform_part / (2*math.pi) + 0*motor_likelihoods)

  # Now the loss is obtained by marginalizing out m from the motor likelihood
  if lossReduce == 'mean':
    loss += -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().mean()
  elif lossReduce == 'sum':
    loss += -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().sum()
  else:
    assert False

  if computePredictions:
     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS
     bayesianEstimate_avg_byStimulus = computeCircularMeanWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(bayesianEstimate_byStimulus, likelihoods)

     bayesianEstimate_avg_byStimulus = torch.where((bayesianEstimate_avg_byStimulus-grid).abs()<180, bayesianEstimate_avg_byStimulus, torch.where(bayesianEstimate_avg_byStimulus > 180, bayesianEstimate_avg_byStimulus-360, bayesianEstimate_avg_byStimulus+360))
     assert float(((bayesianEstimate_avg_byStimulus-grid).abs()).max()) < 180, float(((bayesianEstimate_avg_byStimulus-grid).abs()).max())

     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = computeCircularMeanWeighted(posteriorMaxima.unsqueeze(1), likelihoods)
     encodingBias = computeCircularMeanWeighted(grid.unsqueeze(1), likelihoods)
     attraction = (posteriorMaxima-encodingBias)
     attraction1 = attraction
     attraction2 = attraction+360
     attraction3 = attraction-360
     attraction = torch.where(attraction1.abs() < 180, attraction1, torch.where(attraction2.abs() < 180, attraction2, attraction3))
  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction

def retrieveObservations(x, Subject_, Contrast_, meanMethod = "circular"):
     assert Subject_ is None
     y_set = []
     sd_set = []
     for x in x_set:
        y_here = observations_y[(torch.logical_and((xValues == x), contrast==Contrast_))]

        Mean1 = computeCircularMean(y_here)
        Mean2 = computeCenteredMean(y_here, grid[x])
        if abs(Mean1-grid[x]) > 180 and Mean1 > 180:
            Mean1 = Mean1-360
        elif abs(Mean1-grid[x]) > 180 and Mean1 < 180:
            Mean1 = Mean1+360
        if Contrast_ > 1 and abs(Mean1-Mean2) > 180:
            print(Contrast_)
            print(y_here)
            print("Warning: circular and centered means are very different", Mean1, Mean2, grid[x], y_here)
        if meanMethod == "circular":
            Mean = Mean1
        elif meanMethod == "centered":
            Mean = Mean2
        else:
            assert False

        bias = Mean - grid[x]
        if abs(bias) > 180:
            bias = bias+360
        y_set.append(bias)
        sd_set.append(computeCircularSD(y_here))
     return y_set, sd_set

trigonometric_basis = torch.stack([SQUARED_STIMULUS_SIMILARITY(i*grid) for i in range(1, FOURIER_BASIS_SIZE+1)] + [SQUARED_STIMULUS_DIFFERENCE(i*grid) for i in range(1, FOURIER_BASIS_SIZE+1)], dim=0)
trigonometric_basis = trigonometric_basis * (GRID / (2*trigonometric_basis.pow(2).sum(dim=1, keepdim=True)))
fourierMultiplier = MakeFloatTensor(list(range(1,FOURIER_BASIS_SIZE+1)) + list(range(1,FOURIER_BASIS_SIZE+1)))

print(fourierMultiplier)
print(trigonometric_basis.pow(2).sum(dim=1))
if False:
  figure, axis = plt.subplots(2*FOURIER_BASIS_SIZE, 1, figsize=(50, 50))
  for i in range(trigonometric_basis.size()[0]):
     axis[i].plot(grid.cpu(), trigonometric_basis[i].cpu())
  savePlot(f"figures/{__file__}_BASIS_{GRID}_{FOURIER_BASIS_SIZE}.pdf")
  plt.close()

byPlot = {}

def plot(ax_, MASK, X, Y, alpha=1, color="gray", group=0, name="?", row=None):
    if group != 1:
      ax_.plot(X[MASK], Y[MASK], alpha=alpha, color=color)
    if (group,ax_,name,row) not in byPlot:
        byPlot[(group,ax_,name,row)] = []
    byPlot[(group,ax_,name,row)].append((X,Y,MASK))

viridis = cm.get_cmap('Dark2',10)
COLOR1 = viridis(1)
COLOR2 = viridis(3)
COLOR3 = viridis(2)

def model(grid):
  lossesBy500 = []
  crossLossesBy500 = []
  noImprovement = 0
  global optim, learning_rate
  for iteration in range(1):
   parameters = init_parameters

   loss = 0

   if iteration % 500 == 0:
     plt.close()
     figure, axis = plt.subplots(6, 6, figsize=(10, 5))
     plt.tight_layout()
     figure.subplots_adjust(wspace=0.1, hspace=0.2)

     PRI = 2
     ENC = 0
     ATT = 4
     REP = 5
     TOT = 6
     HUM = 7
     PAD = [1,3]

     for q in range(6):
       for w in range(6):
         axis[q,w].spines['right'].set_visible(False)
         axis[q,w].spines['top'].set_visible(False)

     x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   volumeBySubjectRandomAdjustment =MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE) * (init_parameters["SIGMA_volumeBySubject"]+.1)
   priorBySubjectRandomAdjustment = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE) * (init_parameters["SIGMA_priorBySubject"]+.1)

   volumeBySubjectFourierTimesWeightInclMean = volumeBySubjectRandomAdjustment + init_parameters["volumeBySubject"]
   priorBySubjectFourierTimesWeightInclMean = priorBySubjectRandomAdjustment + init_parameters["priorBySubject"]

   volumeBySubjects = torch.matmul(volumeBySubjectFourierTimesWeightInclMean / fourierMultiplier.unsqueeze(0), trigonometric_basis)
   priorBySubjects = torch.matmul(priorBySubjectFourierTimesWeightInclMean / fourierMultiplier.unsqueeze(0), trigonometric_basis)

   for CONTRAST in [1,2,3,4]:
    if not torch.any(contrast == CONTRAST):
        continue
    for SUBJECT in range(N_SUBJECTS):
     volume = 2 * math.pi * torch.nn.functional.softmax(parameters["volume"] + volumeBySubjects[SUBJECT])
     prior_overall = torch.nn.functional.softmax(parameters["prior"] + priorBySubjects[SUBJECT])
     shift_by = GRID-int((grid-original_centralOrientationPerSubject[SUBJECT]).abs().argmin())
     prior = torch.cat([prior_overall[shift_by:], prior_overall[:shift_by]], dim=0)

     loss_2_4, bayesianEstimate_2_4, bayesianEstimate_sd_byStimulus_2_4, attraction = computeBias(xValues, init_parameters["sigma_logit"][SUBJECT,CONTRAST], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, computePredictions=(iteration%500 == 0), sigma_stimulus=0, sigma2_stimulus=0, contrast_=CONTRAST, folds=trainFolds, lossReduce='sum', subject=SUBJECT)
     loss += loss_2_4

     if iteration % 500 == 0:
       I_SUBJECT = subjectsOrdering.index(SUBJECT)
       if abs(MakeFloatTensor([transformed_centralOrientationPerSubject[SUBJECT]]) - 135) < 15:
           GROUP=2

       elif abs(MakeFloatTensor([transformed_centralOrientationPerSubject[SUBJECT]]) - 135) < 30:
           GROUP=1

       elif abs(MakeFloatTensor([transformed_centralOrientationPerSubject[SUBJECT]]) - 135) < 46:
           GROUP=0
       else:
           assert False

       MASK = (SQUARED_STIMULUS_SIMILARITY(original_centralOrientationPerSubject[SUBJECT]-grid) > .5)

       if MASK[0]:
           SEGMENT1 = torch.logical_and(MASK, grid < 180)
           SEGMENT2 = torch.logical_and(MASK, grid > 180)
       else:
           SEGMENT1 = MASK
           SEGMENT2 = 0*MASK
       axis[I_SUBJECT//6, I_SUBJECT%6].plot(grid[SEGMENT1],prior.detach().cpu()[SEGMENT1], color=[COLOR1,COLOR2,COLOR3][GROUP])
       axis[I_SUBJECT//6, I_SUBJECT%6].plot(grid[SEGMENT2],prior.detach().cpu()[SEGMENT2], color=[COLOR1,COLOR2,COLOR3][GROUP])

       axis[I_SUBJECT//6, I_SUBJECT%6].set_ylim(0,1.1*float(prior.max()))

       axis[I_SUBJECT//6, I_SUBJECT%6].arrow(original_centralOrientationPerSubject[SUBJECT], float(prior[MASK].max()), 0, 0.8*float(prior[MASK].min()-prior[MASK].max()), width=0.2, head_length=0.001, head_width=10, color=[COLOR1,COLOR2,COLOR3][GROUP])

   for w in range(6):
            for v in range(6):
                axis[w,v].set_xticks(ticks=[0,90,180,270,360], labels=["0°", "90°", "180°", "270°", ""])
                if w < 5:
                   axis[w,v].tick_params(labelbottom=False)
                if v > 0:
                   axis[w,v].tick_params(labelleft=False)
                axis[w,v].set_yticks(ticks=[0])
                axis[w,v].set_xlim(0,360)

   print("Saving plot...")
   savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.pdf")

############################3

xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

model(grid)
