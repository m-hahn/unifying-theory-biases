import os
import random
import subprocess
import sys
from util import savePlot
script = sys.argv[1]

Ps = [0,2,4,6,8,10]
folds = list(range(10))

for _ in range(100):
  P = random.choice(Ps)
  if (P == 0) != ("Zero" in script):
      continue
  if P % 2 == 1:
    continue
  fourier = 50
  reg = 1.0
  fold = random.choice(folds)
  if "osine" in script and P == 3:
    continue
  print(f"logs/CROSSVALID/{script}_{P}_{fold}_{reg}_{180}_{fourier}.txt")
  if os.path.exists(f"logs/CROSSVALID/{script}_{P}_{fold}_{reg}_{180}_{fourier}.txt"):
     continue
  subprocess.call([str(q) for q in ["/proj/mhahn.shadow/SOFTWARE/miniconda3/envs/py39-first/bin/python3.9", script, P, fold, reg, 180, fourier]])
