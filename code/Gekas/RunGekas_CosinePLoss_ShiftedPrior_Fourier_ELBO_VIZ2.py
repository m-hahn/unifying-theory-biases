import computations
import glob
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sys
import torch
from cosineEstimator import CosineEstimator
from computations import computeResources
from loadModel import loadModel
from matplotlib import cm
from matplotlib import rc
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import ToDevice
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import makeGridIndicesCircular
from util import product
from util import savePlot
__file__ = __file__.split("/")[-1]

rc('font', **{'family':'FreeSans'})

#############################################################
# Part: Collect arguments
OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
assert P > 0
FOLD_HERE = int(sys.argv[2])
REG_WEIGHT = float(sys.argv[3])
assert REG_WEIGHT == 1.0
GRID = int(sys.argv[4])
assert GRID % 180 == 0

FOURIER_BASIS_SIZE = int(sys.argv[5])
assert FOURIER_BASIS_SIZE in [30, 50, 80]
SHOW_PLOT = (len(sys.argv) < 7) or (sys.argv[6] == "SHOW_PLOT")
DEVICE = 'cpu'

FILE = f"logs/CROSSVALID/{__file__.replace('_VIZ2','')}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_{FOURIER_BASIS_SIZE}.txt"

if os.path.exists(f"logs/CROSSVALID/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"):
   quit()

# Helper Functions dependent on the device

files = sorted(glob.glob("data/Gekas/Exp1/session1/*mat") + glob.glob("data/Gekas/Exp1/session2/*mat"))
files = files + sorted(glob.glob("data/Gekas/Exp2/session1/*mat") + glob.glob("data/Gekas/Exp2/session2/*mat"))

target = []
response = []

sample_total = []
response_total = []
contrast_total = []
subjects_total = []
condition_total = []
centralOrientationPerSubject = {}
for f in files:
 annots = loadmat(f)
 orientation = annots["central_orientation"][0][0]

 transformed_orientation = orientation % 90 + 90

 transformation = transformed_orientation - orientation
 orientation = transformed_orientation
 print(f, orientation)
 data = annots["data"][200:]
 sample = MakeFloatTensor(data[:,0])
 sample = (sample + transformation) % 360
 contrast = MakeLongTensor(data[:,1])
 response = MakeFloatTensor(data[:,4])
 response = (response + transformation) % 360
 color = MakeLongTensor(data[:,9])

 samplesRed = sample[color == 1]
 samplesGreen = sample[color == 2]
 if computeCircularSDWeighted(samplesGreen) < computeCircularSDWeighted(samplesRed):
     bimodalCondition = 2
     condition = color
 else:
     bimodalCondition = 1
     condition = torch.where(color==0, 0, 3-color)

 contrast = torch.where(contrast == 1, 1, torch.where(contrast == 4, 4, condition+1))

 print(f)
 subjectHere = int(f[f.index("sub")+3:f.rfind("ses")])-1
 if "Exp2" in f:
    subjectHere = subjectHere+18
 centralOrientationPerSubject[subjectHere] = orientation
 sample_total.append(sample)
 subjects_total.append(subjectHere + MakeZeros(sample.size()[0]))
 response_total.append(response)
 contrast_total.append(contrast)
 condition_total.append(condition)
 print(subjectHere)

sample = torch.cat(sample_total, dim=0)
responses = torch.cat(response_total, dim=0)
contrast = torch.cat(contrast_total, dim=0)
Subject = torch.cat(subjects_total, dim=0)
condition = torch.cat(condition_total, dim=0)

mask = (condition == 2)
sample = sample[mask]
responses = responses[mask]
condition = condition[mask]
contrast = contrast[mask]
Subject = Subject[mask]

# Store observations
observations_x = sample
observations_y = responses

#############################################################
# Part: Partition data into folds. As described in the paper,
# this is done within each subject.
N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

Subject_cpu = Subject.cpu()
Fold = 0*Subject_cpu
for i in range(int(min(Subject_cpu)), int(max(Subject_cpu))+1):
    print("Making folds for subject", i)
    trials = [j for j in range(Subject_cpu.size()[0]) if Subject_cpu[j] == i]
    randomGenerator.shuffle(trials)
    foldSize = int(len(trials)/N_FOLDS)
    for k in range(N_FOLDS):
        Fold[trials[k*foldSize:(k+1)*foldSize]] = k
Fold = ToDevice(Fold)

##############################################
# Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 360

INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
grid, grid_indices_here = makeGridIndicesCircular(GRID, MIN_GRID, MAX_GRID)
assert grid_indices_here.max() >= GRID, grid_indices_here.max()

# Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

N_SUBJECTS = int(max(Subject))+1
subjectsOrdering = sorted(range(N_SUBJECTS), key=lambda x:centralOrientationPerSubject[x])

#############################################################
# Part: Initialize the model
init_parameters = {}
init_parameters["sigma2_stimulus"] = MakeFloatTensor([0]).view(1)
init_parameters["log_motor_var"] = MakeFloatTensor(N_SUBJECTS*[0])
init_parameters["sigma_logit"] = -3 + MakeZeros(N_SUBJECTS, 6)
init_parameters["mixture_logit"] = MakeFloatTensor(N_SUBJECTS*[-1])
init_parameters["prior"] = MakeZeros(GRID)
init_parameters["volume"] = MakeZeros(GRID)
init_parameters["priorBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["prior_logits_effect_weight"] = MakeZeros(1)
init_parameters["volumeBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["volume_logits_effect_weight"] = MakeZeros(1)

init_parameters["SIGMA_priorBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["SIGMA_volumeBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)

loadModel(FILE, init_parameters)
assert "volume" in init_parameters

for _, y in init_parameters.items():
    y.requires_grad = True

##############################################
# Initialize optimizer.
# The learning rate is a user-specified parameter.
learning_rate=.1
optim = torch.optim.SGD([y for _, y in init_parameters.items()], lr=learning_rate)

##############################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 2*math.pi

def SQUARED_STIMULUS_DIFFERENCE(x):
    return torch.sin(math.pi*x/180)
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(math.pi*x/180)
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(x)
def SQUARED_SENSORY_DIFFERENCE(x):
    return torch.sin(x)

#############################################################
# Part: Configure the appropriate estimator for minimizing the loss function
assert P >= 2
CosineEstimator.set_parameters(GRID=GRID, OPTIMIZER_VERBOSE=OPTIMIZER_VERBOSE, P=P, SQUARED_SENSORY_DIFFERENCE=SQUARED_SENSORY_DIFFERENCE, SQUARED_SENSORY_SIMILARITY=SQUARED_SENSORY_SIMILARITY)

#############################################################
# Part: Run the model. This function implements the model itself:
## calculating the likelihood of a given dataset under that model
## and---if the computePredictions argument is set to True--- computes
## the bias and variability of the estimate.
def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, computePredictions=False, sigma_stimulus=None, sigma2_stimulus=None, contrast_=None, folds=None, lossReduce='mean', subject=None):

 # Part: Obtain the motor variance by exponentiating the appropriate model parameter
 motor_variance = torch.exp(- parameters["log_motor_var"][subject])
 # Part: Obtain the sensory noise variance. We parameterize it as a fraction of the squared volume of the size of the sensory space
 sigma2 = 2*torch.sigmoid(sigma_logit)
 # Part: Obtain the transfer function as the cumulative sum of the discretized resource allocation (referred to as `volume` element due to the geometric interpretation by Wei&Stocker 2015)
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 if True:
  folds = MakeLongTensor(folds)
  if subject is not None:
    MASK = torch.logical_and(contrast==contrast_, torch.logical_and((Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0), Subject==subject))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]
  else:
    MASK = torch.logical_and(contrast==contrast_, (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]
  assert stimulus.view(-1).size()[0] > 0, (contrast_)

  if sigma2_stimulus > 0:
    assert False
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))
    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  # Part: Compute sensory likelihoods. Across both interval and
  ## circular stimulus spaces, this amounts to exponentiaring a
  ## `similarity`
  sensory_likelihoods = torch.softmax(((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2)) + volumeElement.unsqueeze(1).log(), dim=0)

  # Part: If stimulus noise is nonzero, convolve the likelihood with the
  ## stimulus noise.
  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    assert False
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  ## Compute posterior using Bayes' rule. As described in the paper, the posterior is computed
  ## in the discretized stimulus space.
  posterior = prior.unsqueeze(1) * likelihoods.t()
  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  ## Compute the estimator for each m in the discretized sensory space.
  bayesianEstimate = CosineEstimator.apply(grid_indices_here, posterior)

  ## Compute the motor likelihood

  ## `error' refers to the stimulus similarity between the estimator assigned to each m and
  ## the observations found in the dataset.
  ## The Gaussian or von Mises motor likelihood is obtained by exponentiating and normalizing
  error = (SQUARED_STIMULUS_SIMILARITY(360/GRID*bayesianEstimate.unsqueeze(0) - responses.unsqueeze(1)))
  ## The log normalizing constants, for each m in the discretized sensory space
  log_normalizing_constant = torch.logsumexp((SQUARED_STIMULUS_SIMILARITY(grid))/motor_variance, dim=0) + math.log(2 * math.pi / GRID)
  ## The log motor likelihoods, for each pair of sensory encoding m and observed human response
  log_motor_likelihoods = (error/motor_variance) - log_normalizing_constant
  ## Obtaining the motor likelihood by exponentiating.
  motor_likelihoods = torch.exp(log_motor_likelihoods)
  ## Obtain the guessing rate, parameterized via the (inverse) logit transform as described in SI Appendix
  uniform_part = torch.sigmoid(parameters["mixture_logit"][subject])

  ## The full likelihood then consists of a mixture of the motor likelihood calculated before, and the uniform
  ## distribution on the full space.
  motor_likelihoods = (1-uniform_part) * motor_likelihoods + (uniform_part / (2*math.pi) + 0*motor_likelihoods)

  # Now the loss is obtained by marginalizing out m from the motor likelihood
  if lossReduce == 'mean':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().mean()
  elif lossReduce == 'sum':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().sum()
  else:
    assert False

  if computePredictions:
     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS
     bayesianEstimate_avg_byStimulus = computeCircularMeanWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(bayesianEstimate_byStimulus, likelihoods)

     bayesianEstimate_avg_byStimulus = torch.where((bayesianEstimate_avg_byStimulus-grid).abs()<180, bayesianEstimate_avg_byStimulus, torch.where(bayesianEstimate_avg_byStimulus > 180, bayesianEstimate_avg_byStimulus-360, bayesianEstimate_avg_byStimulus+360))
     assert float(((bayesianEstimate_avg_byStimulus-grid).abs()).max()) < 180, float(((bayesianEstimate_avg_byStimulus-grid).abs()).max())

     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = computeCircularMeanWeighted(posteriorMaxima.unsqueeze(1), likelihoods)
     encodingBias = computeCircularMeanWeighted(grid.unsqueeze(1), likelihoods)
     attraction = (posteriorMaxima-encodingBias)
     attraction1 = attraction
     attraction2 = attraction+360
     attraction3 = attraction-360
     attraction = torch.where(attraction1.abs() < 180, attraction1, torch.where(attraction2.abs() < 180, attraction2, attraction3))
  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction

def retrieveObservations(x, Subject_, Contrast_, meanMethod = "circular"):
     assert Subject_ is None
     y_set = []
     sd_set = []
     for x in x_set:
        y_here = observations_y[(torch.logical_and((xValues == x), contrast==Contrast_))]

        Mean1 = computeCircularMean(y_here)
        Mean2 = computeCenteredMean(y_here, grid[x])
        if abs(Mean1-grid[x]) > 180 and Mean1 > 180:
            Mean1 = Mean1-360
        elif abs(Mean1-grid[x]) > 180 and Mean1 < 180:
            Mean1 = Mean1+360
        if Contrast_ > 1 and abs(Mean1-Mean2) > 180:
            print(Contrast_)
            print(y_here)
            print("Warning: circular and centered means are very different", Mean1, Mean2, grid[x], y_here)
        if meanMethod == "circular":
            Mean = Mean1
        elif meanMethod == "centered":
            Mean = Mean2
        else:
            assert False

        bias = Mean - grid[x]
        if abs(bias) > 180:
            bias = bias+360
        y_set.append(bias)
        sd_set.append(computeCircularSD(y_here))
     return y_set, sd_set

trigonometric_basis = torch.stack([SQUARED_STIMULUS_SIMILARITY(i*grid) for i in range(1, FOURIER_BASIS_SIZE+1)] + [SQUARED_STIMULUS_DIFFERENCE(i*grid) for i in range(1, FOURIER_BASIS_SIZE+1)], dim=0)
trigonometric_basis = trigonometric_basis * (GRID / (2*trigonometric_basis.pow(2).sum(dim=1, keepdim=True)))
fourierMultiplier = MakeFloatTensor(list(range(1,FOURIER_BASIS_SIZE+1)) + list(range(1,FOURIER_BASIS_SIZE+1)))

print(fourierMultiplier)
print(trigonometric_basis.pow(2).sum(dim=1))
if False:
  figure, axis = plt.subplots(2*FOURIER_BASIS_SIZE, 1, figsize=(50, 50))
  for i in range(trigonometric_basis.size()[0]):
     axis[i].plot(grid.cpu(), trigonometric_basis[i].cpu())
  savePlot(f"figures/{__file__}_BASIS_{GRID}_{FOURIER_BASIS_SIZE}.pdf")
  plt.close()

byPlot = {}

def plot(ax_, MASK, X, Y, alpha=1, color="gray", group=0, name="?", row=None, size=None, subject=None):
    if group != 1:
      ax_.plot(X[MASK], Y[MASK], alpha=alpha, color='gray')
    if (group,ax_,name,row,color) not in byPlot:
        byPlot[(group,ax_,name,row,color)] = {}
    if subject not in byPlot[(group,ax_,name,row,color)]:
        byPlot[(group,ax_,name,row,color)][subject] = []
    byPlot[(group,ax_,name,row,color)][subject].append((X,Y,MASK))

viridis = cm.get_cmap('Dark2',10)
COLOR1 = viridis(1)
COLOR2 = viridis(3)
COLOR3 = viridis(2)
print(viridis(1))
print(viridis(2))
print(viridis(3))

def makeExperimentalPrior(centralDirectionForExperimentalPrior):

   prior1 = MakeZeros(GRID) + 1/GRID
   prior2 = (((grid + 80 - centralDirectionForExperimentalPrior)/16)) % 22.5

   print(prior2)

   priorBy16 = [0, 0.025, 0.058, 0.345, 0.058, 0.025, 0.058, 0.345, 0.058, 0.025, 0, 0]
   for i in range(GRID):
   #  print(i, prior2[i])
     if prior2[i]+1 >= len(priorBy16):
       prior2[i] = 0
     elif prior2[i] % 1 == 0:
       prior2[i] = priorBy16[prior2[i].long()]
     else:
       lower = int(prior2[i])
       upper = lower+1
       lower_v = priorBy16[lower]
       upper_v = priorBy16[upper]
       calculatedPrior = (1-(prior2[i]-lower)) * lower_v + ((prior2[i]-lower)) * upper_v
       print(prior2[i], lower, upper, (prior2[i]-lower), (1-(prior2[i]-lower)), lower_v, upper_v, calculatedPrior)
       prior2[i] = calculatedPrior

   prior2 = prior2 / prior2.sum()


   prior_experimental = 0.05 * prior1 + 0.95 * prior2
   return prior_experimental


computations.setData(GRID=GRID, STIMULUS_SPACE_VOLUME=STIMULUS_SPACE_VOLUME)

def model(grid):
  lossesBy500 = []
  crossLossesBy500 = []
  noImprovement = 0
  global optim, learning_rate
  for iteration in range(1):
   parameters = init_parameters

   loss = 0

   if iteration % 500 == 0:
     plt.close()
     assigned = ["PRI", None, "ENC", None, "ATT", "REP", "TOT"]
     for w, k in enumerate(assigned):
         globals()[k] = w
     PAD = [w for w, q in enumerate(assigned) if q is None]
     gridspec = dict(width_ratios=[1 if x is not None else .2 for x in assigned])
     figure, axis = plt.subplots(3, len(gridspec["width_ratios"]), figsize=(8, 4), gridspec_kw=gridspec, squeeze=False)
     plt.tight_layout()
     figure.subplots_adjust(wspace=0.1, hspace=0.2)

     for q in range(3):
      for w in PAD:
       axis[q,w].set_visible(False)
     for q in range(3):
       for w in range(len(assigned)):
         axis[q,w].spines['right'].set_visible(False)
         axis[q,w].spines['top'].set_visible(False)
     for q in range(2):
       for w in range(len(assigned)):
             axis[q,w].tick_params(labelbottom=False)
     for q in range(3):
         for w in [REP, TOT]:
             axis[q,w].tick_params(labelleft=False)

     x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   volumeBySubjectRandomAdjustment =MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE) * (init_parameters["SIGMA_volumeBySubject"]+.1)
   priorBySubjectRandomAdjustment = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE) * (init_parameters["SIGMA_priorBySubject"]+.1)

   volumeBySubjectFourierTimesWeightInclMean = volumeBySubjectRandomAdjustment + init_parameters["volumeBySubject"]
   priorBySubjectFourierTimesWeightInclMean = priorBySubjectRandomAdjustment + init_parameters["priorBySubject"]

   volumeBySubjects = torch.matmul(volumeBySubjectFourierTimesWeightInclMean / fourierMultiplier.unsqueeze(0), trigonometric_basis)
   priorBySubjects = torch.matmul(priorBySubjectFourierTimesWeightInclMean / fourierMultiplier.unsqueeze(0), trigonometric_basis)

   for I_SUBJECT in range(3):
     MASK = (SQUARED_STIMULUS_SIMILARITY([97.5, 117.5, 137.5][I_SUBJECT]-grid) > .5)

     axis[I_SUBJECT,PRI].plot(grid.cpu()[MASK], 0.3 * makeExperimentalPrior([90,112.5,135][I_SUBJECT])[MASK], color=[COLOR1,COLOR2,COLOR3][I_SUBJECT], alpha=1, linestyle="dashed")

#     axis[I_SUBJECT,PRI].plot(grid.cpu()[MASK], 0.03 * torch.maximum(0.025 + MakeZeros(GRID), torch.maximum((0.42 - 0.025 * (grid- [90,112.5,135][I_SUBJECT] - 32).abs()), (0.42 - 0.025 * (grid-[90,112.5,135][I_SUBJECT]+32).abs()))).cpu()[MASK], color=[COLOR1,COLOR2,COLOR3][I_SUBJECT], alpha=1, linestyle="dashed")

   for SUBJECT in range(N_SUBJECTS):
     attraction = 0.0
     bayesianEstimate_2_4 = 0.0
     resources = 0.0
 
     for CONTRAST in [1,2,3,4]:
      assert torch.any(contrast == CONTRAST) == (CONTRAST in [3,4])
      if not torch.any(contrast == CONTRAST):
          continue
      volume = 2 * math.pi * torch.nn.functional.softmax(parameters["volume"] + volumeBySubjects[SUBJECT])
      prior_overall = torch.nn.functional.softmax(parameters["prior"] + priorBySubjects[SUBJECT])
      shift_by = GRID-int((grid-centralOrientationPerSubject[SUBJECT]).abs().argmin())
      prior = torch.cat([prior_overall[shift_by:], prior_overall[:shift_by]], dim=0)
 
      loss_2_4, bayesianEstimate_2_4_, bayesianEstimate_sd_byStimulus_2_4, attraction_ = computeBias(xValues, init_parameters["sigma_logit"][SUBJECT,CONTRAST], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, computePredictions=(iteration%500 == 0), sigma_stimulus=0, sigma2_stimulus=0, contrast_=CONTRAST, folds=trainFolds, lossReduce='sum', subject=SUBJECT)
      loss += loss_2_4
      bayesianEstimate_2_4 = bayesianEstimate_2_4 + 0.5 * bayesianEstimate_2_4_
      attraction = attraction + 0.5 * attraction_
      resources = resources + 0.5 * computeResources(volume.detach().cpu(), inverse_variance = 1/float((2 * torch.sigmoid(init_parameters["sigma_logit"][SUBJECT,CONTRAST]))))
     if iteration % 500 == 0:
       I_SUBJECT = subjectsOrdering.index(SUBJECT)
       if abs(MakeFloatTensor([centralOrientationPerSubject[SUBJECT]]) - 135) < 15:
           I_SUBJECT=2
       elif abs(MakeFloatTensor([centralOrientationPerSubject[SUBJECT]]) - 135) < 30:
           I_SUBJECT=1
       elif abs(MakeFloatTensor([centralOrientationPerSubject[SUBJECT]]) - 135) < 46:
           I_SUBJECT=0
       else:
           assert False


       grid_transformed = grid - centralOrientationPerSubject[SUBJECT] + [90,112.5,135][I_SUBJECT]

       if centralOrientationPerSubject[SUBJECT] > 135:
           REVERSE=True
       else:
           REVERSE=False
       if REVERSE:
           grid_transformed = [180,225,270][I_SUBJECT]-grid_transformed
           attraction = -attraction
           bayesianEstimate_2_4 = -bayesianEstimate_2_4+2*grid
           grid_reflected = 270-grid
           centralOrientation_reflected = 270-centralOrientationPerSubject[SUBJECT]
       else:
           grid_reflected = grid
           centralOrientation_reflected = centralOrientationPerSubject[SUBJECT]

       MASK = (SQUARED_STIMULUS_SIMILARITY(centralOrientationPerSubject[SUBJECT]-grid) > .5)
       MASK = torch.logical_and(MASK, grid % 16 == 0)

       PLOT_ROW = I_SUBJECT


       plot(axis[PLOT_ROW,ENC], MASK, grid_reflected.cpu(), resources, alpha=.2, row=I_SUBJECT, color="black", size=6)



       axis[PLOT_ROW,ENC].scatter([centralOrientation_reflected], [0.02], s=5, color=[COLOR1,COLOR2,COLOR3][I_SUBJECT])

       OneMinusGuessingRate = float(1-torch.sigmoid(parameters["mixture_logit"][SUBJECT]))

       plot(axis[PLOT_ROW,PRI], MASK, grid_transformed.cpu(), prior.detach().cpu(), alpha=.2, color=[COLOR1,COLOR2,COLOR3][I_SUBJECT], row=I_SUBJECT, size=6)
       axis[PLOT_ROW,PRI].set_ylim(0, 0.013)
       axis[PLOT_ROW,ENC].set_yticks(ticks=[0,0.2], labels=["0","0.2"])
       axis[PLOT_ROW,PRI].set_yticks(ticks=[0])
       plot(axis[PLOT_ROW,ATT], MASK, grid_transformed.cpu(), OneMinusGuessingRate*(attraction).detach().cpu(), alpha=.2, color=[COLOR1,COLOR2,COLOR3][I_SUBJECT], row=I_SUBJECT, size=6, subject=SUBJECT)
       plot(axis[PLOT_ROW,REP], MASK, grid_transformed.cpu(), OneMinusGuessingRate*(bayesianEstimate_2_4-grid-attraction).detach().cpu(), alpha=.2, color="black", row=I_SUBJECT, subject=SUBJECT)

       plot(axis[PLOT_ROW,TOT], MASK, grid_transformed.cpu(), OneMinusGuessingRate*(bayesianEstimate_2_4-grid).detach().cpu(), alpha=.0, color=[COLOR1,COLOR2,COLOR3][I_SUBJECT], row=I_SUBJECT, size=6, subject=SUBJECT)


     for CONTRAST in [1,2,3,4]:
       assert torch.any(contrast == CONTRAST) == (CONTRAST in [3,4])
       if not torch.any(contrast == CONTRAST):
           continue
       y_here = observations_y[torch.logical_and(Subject == SUBJECT, contrast==CONTRAST)]
       x_here = observations_x[torch.logical_and(Subject == SUBJECT, contrast==CONTRAST)]
       kappa = 29
       kernel = torch.exp(kappa*SQUARED_STIMULUS_SIMILARITY(x_here.view(-1,1)-grid.view(1,-1))) / (2*math.pi*np.i0(kappa))
       kernel = kernel / kernel.sum(dim=0).max()

       bias = y_here - x_here
       if REVERSE:
           bias = -bias
       bias = torch.where(bias > 180, bias-360, torch.where(bias < -180, bias+360, bias))
       y_smoothed = computeCircularMeanWeighted(bias.unsqueeze(1), kernel)
       y_smoothed = torch.where(y_smoothed > 180, y_smoothed-360, torch.where(y_smoothed < -180, y_smoothed+360, y_smoothed))

       plot(axis[PLOT_ROW,TOT], MASK, grid_transformed.cpu(), y_smoothed.cpu(), alpha=.2, color="gray", group=1, name="human", row=I_SUBJECT, subject=SUBJECT)

       for w in range(len(assigned)):
         axis[PLOT_ROW,w].set_xticks(ticks=[0,90,180], labels=["0°", "90°", "180°"])

       for w in [ATT,REP,TOT]:
         axis[PLOT_ROW,w].set_ylim(-8, 8)
         axis[PLOT_ROW,w].set_yticks(ticks=[-5,0,5], labels=["-5°","0°","5°"])

       for w in range(len(assigned)):

         axis[PLOT_ROW,w].set_xlim(0,200)

       axis[PLOT_ROW,ENC].set_ylim(0,0.32)

   if iteration % 500 == 0:

     axis[0,ATT].arrow(65, -2, 0, -4, width=2, color="black", head_length=1)
     axis[0,ATT].arrow(115, 2, 0, 4, width=2, color="black", head_length=1)
     axis[1,ATT].arrow(22.5+65, -2, 0, -4, width=2, color="black", head_length=1)
     axis[1,ATT].arrow(22.5+115, 2, 0, 4, width=2, color="black", head_length=1)
     axis[2,ATT].arrow(45+65, -2, 0, -4, width=2, color="black", head_length=1)
     axis[2,ATT].arrow(45+115, 2, 0, 4, width=2, color="black", head_length=1)

     for group, ax_, name, row, color in byPlot:
      if len(byPlot[(group, ax_, name, row, color)]) > 1:
       byPlot[(group, ax_, name, row, color)][None] = []
       for subject in byPlot[(group, ax_, name, row, color)]:
           if subject is None:
               continue
           print(subject)

           Xs, Ys, MASKs = zip(*(byPlot[(group, ax_, name, row, color)][subject]))
           print(Xs[0])
#           print(Xs[1])
           print(len(Xs))
           print(subject)
           assert float((torch.stack(Xs, dim=0) - torch.stack(Xs, dim=0).mean(dim=0)).pow(2).sum()) < 0.1, float((torch.stack(Xs, dim=0) - torch.stack(Xs, dim=0).mean(dim=0)).pow(2).sum())
           assert float((torch.stack(MASKs, dim=0).float() - torch.stack(MASKs, dim=0).float().mean(dim=0)).pow(2).sum()) < 0.1, float((torch.stack(MASKs, dim=0).float() - torch.stack(MASKs, dim=0).float().mean(dim=0)).pow(2).sum())
           overallX = torch.stack(Xs, dim=0).mean(dim=0)
           overallY = torch.stack(Ys, dim=0).mean(dim=0)
           MASK = torch.stack(MASKs, dim=0).any(dim=0)
           byPlot[(group, ax_, name, row, color)][None].append((overallX, overallY, MASK))

     for group, ax_, name, row, color in byPlot:
           Xs, Ys, MASKs = zip(*byPlot[(group, ax_, name, row, color)][None])
           overallX = torch.stack(Xs, dim=0)
           overallY = torch.stack(Ys, dim=0)

           MASK = torch.stack(MASKs, dim=0).any(dim=0)
           minX = 360
           maxX = 0
           for x, m in zip(Xs, MASKs):
               x_ = x[m]
               minX = min(minX, float(x_.min()))
               maxX = max(maxX, float(x_.max()))
           kernel = ((grid.view(-1, 1, 1)-overallX.unsqueeze(0)).abs() < 2).float()

           kernel = kernel / kernel.sum(dim=2).sum(dim=1)
           kernel = torch.where(torch.isnan(kernel), MakeZeros(kernel.size()), kernel)
           if torch.isnan(kernel).any():
               quit()
           smoothed = (overallY.unsqueeze(0) * kernel).sum(dim=2).sum(dim=1)
           MASK = torch.logical_and(grid >= minX, grid<=maxX)

           if group == 1:
               kernel_per_subject = kernel / kernel.sum(dim=2, keepdim=True)
               smoothed_per_subject = (overallY.unsqueeze(0) * kernel_per_subject).sum(dim=2)

               sd = (smoothed_per_subject.pow(2).mean(dim=1) - smoothed_per_subject.mean(dim=1).pow(2)).sqrt() / math.sqrt(smoothed_per_subject.size()[1])
               CI_x = grid.detach()[MASK].numpy().tolist()
               CI_y1 = (smoothed_per_subject.mean(dim=1)-sd).detach()[MASK].numpy().tolist()
               CI_y2 = (smoothed_per_subject.mean(dim=1)+sd).detach()[MASK].numpy().tolist()
               poly = plt.Polygon(list(zip(CI_x + CI_x[::-1], CI_y1 + CI_y2[::-1])), facecolor=(.65, .65, .65))
               ax_.add_patch(poly)

               ax_.plot(grid.detach()[MASK], (smoothed_per_subject.mean(dim=1)).detach()[MASK], color="gray", linewidth=2, linestyle='dashed')
           else:
              ax_.plot(grid.detach()[MASK], smoothed.detach()[MASK], color=color, linewidth=3)

     axis[0,ENC].set_title("Resources")
     axis[0,PRI].set_title("Prior")
     axis[0,TOT].set_title("Total bias")

     axis[0,ATT].set_title("Attraction")
     axis[0,REP].set_title("Repulsion")

     print("Saving plot...")
     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.pdf")

############################3

model(grid)
