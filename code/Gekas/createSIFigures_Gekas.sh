# Gekas

python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_ACREXP.py 2 0 1.0 180 50
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_ELBO_VIZ_ACREXP.py 2 0 1.0 180 50
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_ELBO_VIZ_ACREXP.py 2 0 1.0 180 50


python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_MainPaper.py 4 0 1.0 180 50 NO_PLOT

python3 RunGekas_CosinePLoss_NoShiftedPrior_Fourier_ELBO_VIZ_Encoding_MainPaper.py 4 0 1.0 180 50 NO_PLOT
python3 RunGekas_CosinePLoss_NoShiftedPrior_Fourier_ELBO_VIZ_Priors.py 4 0 1.0 180 50 NO_PLOT

python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_Cardinal.py 4 0 1.0 180 50 NO_PLOT
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_MAP_ELBO_VIZ.py 0 0 1.0 180 50 NO_PLOT
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_MAP_ELBO_VIZ.py 0 0 1.0 180 50 NO_PLOT
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_MAP_ELBO_VIZ.py 0 0 1.0 180 50 NO_PLOT
#for p in 2 4 6 8 10
#do
#python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2.py $p 0 1.0 180 50 NO_PLOT
#python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_ELBO_VIZ.py $p 0 1.0 180 50 NO_PLOT
#python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_ELBO_VIZ.py $p 0 1.0 180 50 NO_PLOT
#done
python3 evaluateCrossValidationResults_Gekas_Fixed.py
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_Stylized.py 4 0 1.0 180 50 NO_PLOT
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_Stylized_ExactPrior.py 4 0 1.0 180 50 

