import computations
import glob
import json
import math
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sys
import torch
from cosineEstimator import CosineEstimator
from computations import computeResources
from loadModel import loadModel
from matplotlib import cm
from matplotlib import rc
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import ToDevice
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import makeGridIndicesCircular
from util import product
from util import savePlot
from util import sign

rc('font', **{'family':'FreeSans'})

#############################################################
# Part: Collect arguments
OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
assert P > 0
FOLD_HERE = int(sys.argv[2])
REG_WEIGHT = float(sys.argv[3])
assert REG_WEIGHT == 1.0
GRID = int(sys.argv[4])
assert GRID % 180 == 0

FOURIER_BASIS_SIZE = int(sys.argv[5])
assert FOURIER_BASIS_SIZE in [30, 50, 80]
SHOW_PLOT = (len(sys.argv) < 7) or (sys.argv[6] == "SHOW_PLOT")
DEVICE = 'cpu'

FILE = f"logs/CROSSVALID/{__file__.replace('_VIZ_Encoding_MainPaper','')}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_{FOURIER_BASIS_SIZE}.txt"

if os.path.exists(f"logs/CROSSVALID/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"):
   quit()

# Helper Functions dependent on the device

files = sorted(glob.glob("data/Gekas/Exp1/session1/*mat") + glob.glob("data/Gekas/Exp1/session2/*mat"))
files = files + sorted(glob.glob("data/Gekas/Exp2/session1/*mat") + glob.glob("data/Gekas/Exp2/session2/*mat"))

target = []
response = []

transformed_sample_total = []
transformed_response_total = []
original_sample_total = []
original_response_total = []
contrast_total = []
subjects_total = []
condition_total = []
transformed_centralOrientationPerSubject = {}
original_centralOrientationPerSubject = {}
for f in files:
 annots = loadmat(f)
 orientation = annots["central_orientation"][0][0]

 transformed_orientation = orientation % 90 + 90
 original_orientation = orientation

 transformed_transformation = transformed_orientation - orientation
 original_transformation = 0

 print(f, orientation)
 data = annots["data"][200:]

 sample = MakeFloatTensor(data[:,0])
 transformed_sample = (sample + transformed_transformation) % 360
 original_sample = sample

 contrast = MakeLongTensor(data[:,1])

 response = MakeFloatTensor(data[:,4])
 transformed_response = (response + transformed_transformation) % 360
 original_response = response

 color = MakeLongTensor(data[:,9])

 samplesRed = sample[color == 1]
 samplesGreen = sample[color == 2]

 if computeCircularSDWeighted(samplesGreen) < computeCircularSDWeighted(samplesRed):
     bimodalCondition = 2
     condition = color

 else:
     bimodalCondition = 1
     condition = torch.where(color==0, 0, 3-color)

 contrast = torch.where(contrast == 1, 1, torch.where(contrast == 4, 4, condition+1))

 print(f)
 subjectHere = int(f[f.index("sub")+3:f.rfind("ses")])-1
 if "Exp2" in f:
    subjectHere = subjectHere+18
 transformed_centralOrientationPerSubject[subjectHere] = transformed_orientation
 original_centralOrientationPerSubject[subjectHere] = original_orientation
 transformed_sample_total.append(transformed_sample)
 original_sample_total.append(original_sample)
 subjects_total.append(subjectHere + MakeZeros(sample.size()[0]))
 transformed_response_total.append(transformed_response)
 original_response_total.append(original_response)
 contrast_total.append(contrast)
 condition_total.append(condition)
 print(subjectHere)

sample = torch.cat(original_sample_total, dim=0)
transformed_sample = torch.cat(transformed_sample_total, dim=0)
responses = torch.cat(original_response_total, dim=0)
transformed_responses = torch.cat(transformed_response_total, dim=0)
contrast = torch.cat(contrast_total, dim=0)
Subject = torch.cat(subjects_total, dim=0)
condition = torch.cat(condition_total, dim=0)

mask = (condition == 2)
sample = sample[mask]
responses = responses[mask]
condition = condition[mask]
contrast = contrast[mask]
Subject = Subject[mask]

observations_x = sample
observations_y = responses

N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

Fold = 0*Subject
for i in range(int(min(Subject)), int(max(Subject))+1):
    trials = [j for j in range(Subject.size()[0]) if Subject[j] == i]
    randomGenerator.shuffle(trials)
    foldSize = int(len(trials)/N_FOLDS)
    for k in range(N_FOLDS):
        Fold[trials[k*foldSize:(k+1)*foldSize]] = k

##############################################
# Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 360

INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
grid, grid_indices_here = makeGridIndicesCircular(GRID, MIN_GRID, MAX_GRID)
assert grid_indices_here.max() >= GRID, grid_indices_here.max()

# Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))

xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

N_SUBJECTS = int(max(Subject))+1
subjectsOrdering = sorted(range(N_SUBJECTS), key=lambda x:original_centralOrientationPerSubject[x])

#############################################################
# Part: Initialize the model
init_parameters = {}
init_parameters["sigma2_stimulus"] = MakeFloatTensor([0]).view(1)
init_parameters["log_motor_var"] = MakeFloatTensor(N_SUBJECTS*[0])
init_parameters["sigma_logit"] = -3 + MakeZeros(N_SUBJECTS, 6)
init_parameters["mixture_logit"] = MakeFloatTensor(N_SUBJECTS*[-1])
init_parameters["prior"] = MakeZeros(GRID)
init_parameters["volume"] = MakeZeros(GRID)
init_parameters["priorBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["prior_logits_effect_weight"] = MakeZeros(1)
init_parameters["volumeBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["volume_logits_effect_weight"] = MakeZeros(1)

init_parameters["SIGMA_priorBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["SIGMA_volumeBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)

loadModel(FILE, init_parameters)
assert "volume" in init_parameters
for _, y in init_parameters.items():
    y.requires_grad = True

##############################################
# Initialize optimizer.
# The learning rate is a user-specified parameter.
learning_rate=.1
optim = torch.optim.SGD([y for _, y in init_parameters.items()], lr=learning_rate)

##############################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 2*math.pi

def SQUARED_STIMULUS_DIFFERENCE(x):
    return torch.sin(math.pi*x/180)
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(math.pi*x/180)
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(x)
def SQUARED_SENSORY_DIFFERENCE(x):
    return torch.sin(x)

#############################################################
# Part: Configure the appropriate estimator for minimizing the loss function
assert P >= 2
CosineEstimator.set_parameters(GRID=GRID, OPTIMIZER_VERBOSE=OPTIMIZER_VERBOSE, P=P, SQUARED_SENSORY_DIFFERENCE=SQUARED_SENSORY_DIFFERENCE, SQUARED_SENSORY_SIMILARITY=SQUARED_SENSORY_SIMILARITY)

SCALE = 50

#############################################################
# Part: Run the model. This function implements the model itself:
## calculating the likelihood of a given dataset under that model
## and---if the computePredictions argument is set to True--- computes
## the bias and variability of the estimate.
def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, computePredictions=False, sigma_stimulus=None, sigma2_stimulus=None, contrast_=None, folds=None, lossReduce='mean', subject=None):

 # Part: Obtain the motor variance by exponentiating the appropriate model parameter
 motor_variance = torch.exp(- parameters["log_motor_var"][subject])
 # Part: Obtain the sensory noise variance. We parameterize it as a fraction of the squared volume of the size of the sensory space
 sigma2 = 2*torch.sigmoid(sigma_logit)
 # Part: Obtain the transfer function as the cumulative sum of the discretized resource allocation (referred to as `volume` element due to the geometric interpretation by Wei&Stocker 2015)
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 if True:
  folds = MakeLongTensor(folds)
  if subject is not None:
    MASK = torch.logical_and(contrast==contrast_, torch.logical_and((Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0), Subject==subject))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]
  else:
    assert False
    MASK = torch.logical_and(contrast==contrast_, (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]
  assert stimulus.view(-1).size()[0] > 0, (contrast_)

  if sigma2_stimulus > 0:
    assert False
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))

    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  # Part: Compute sensory likelihoods. Across both interval and
  ## circular stimulus spaces, this amounts to exponentiaring a
  ## `similarity`
  sensory_likelihoods = torch.softmax(((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2)) + volumeElement.unsqueeze(1).log(), dim=0)

  # Part: If stimulus noise is nonzero, convolve the likelihood with the
  ## stimulus noise.
  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    assert False
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  ## Compute posterior using Bayes' rule. As described in the paper, the posterior is computed
  ## in the discretized stimulus space.
  posterior = prior.unsqueeze(1) * likelihoods.t()
  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  ## Compute the estimator for each m in the discretized sensory space.
  bayesianEstimate = CosineEstimator.apply(grid_indices_here, posterior)

  ## Compute the motor likelihood

  ## `error' refers to the stimulus similarity between the estimator assigned to each m and
  ## the observations found in the dataset.
  ## The Gaussian or von Mises motor likelihood is obtained by exponentiating and normalizing
  error = (SQUARED_STIMULUS_SIMILARITY(360/GRID*bayesianEstimate.unsqueeze(0) - responses.unsqueeze(1)))
  ## The log normalizing constants, for each m in the discretized sensory space
  log_normalizing_constant = torch.logsumexp((SQUARED_STIMULUS_SIMILARITY(grid))/motor_variance, dim=0) + math.log(2 * math.pi / GRID)
  ## The log motor likelihoods, for each pair of sensory encoding m and observed human response
  log_motor_likelihoods = (error/motor_variance) - log_normalizing_constant
  ## Obtaining the motor likelihood by exponentiating.
  motor_likelihoods = torch.exp(log_motor_likelihoods)
  ## Obtain the guessing rate, parameterized via the (inverse) logit transform as described in SI Appendix
  uniform_part = torch.sigmoid(parameters["mixture_logit"][subject])

  ## The full likelihood then consists of a mixture of the motor likelihood calculated before, and the uniform
  ## distribution on the full space.
  motor_likelihoods = (1-uniform_part) * motor_likelihoods + (uniform_part / (2*math.pi) + 0*motor_likelihoods)

  # Now the loss is obtained by marginalizing out m from the motor likelihood
  if lossReduce == 'mean':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().mean()
  elif lossReduce == 'sum':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().sum()
  else:
    assert False

  if computePredictions:
     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS
     bayesianEstimate_avg_byStimulus = computeCircularMeanWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(bayesianEstimate_byStimulus, likelihoods)

     bayesianEstimate_avg_byStimulus = torch.where((bayesianEstimate_avg_byStimulus-grid).abs()<180, bayesianEstimate_avg_byStimulus, torch.where(bayesianEstimate_avg_byStimulus > 180, bayesianEstimate_avg_byStimulus-360, bayesianEstimate_avg_byStimulus+360))
     assert float(((bayesianEstimate_avg_byStimulus-grid).abs()).max()) < 180, float(((bayesianEstimate_avg_byStimulus-grid).abs()).max())

     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = computeCircularMeanWeighted(posteriorMaxima.unsqueeze(1), likelihoods)
     encodingBias = computeCircularMeanWeighted(grid.unsqueeze(1), likelihoods)
     attraction = (posteriorMaxima-encodingBias)
     attraction1 = attraction
     attraction2 = attraction+360
     attraction3 = attraction-360
     attraction = torch.where(attraction1.abs() < 180, attraction1, torch.where(attraction2.abs() < 180, attraction2, attraction3))
  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction

def retrieveObservations(x, Subject_, Contrast_, meanMethod = "circular"):
     assert Subject_ is None
     y_set = []
     sd_set = []
     for x in x_set:
        y_here = observations_y[(torch.logical_and((xValues == x), contrast==Contrast_))]

        Mean1 = computeCircularMean(y_here)
        Mean2 = computeCenteredMean(y_here, grid[x])
        if abs(Mean1-grid[x]) > 180 and Mean1 > 180:
            Mean1 = Mean1-360
        elif abs(Mean1-grid[x]) > 180 and Mean1 < 180:
            Mean1 = Mean1+360
        if Contrast_ > 1 and abs(Mean1-Mean2) > 180:
            print(Contrast_)
            print(y_here)
            print("Warning: circular and centered means are very different", Mean1, Mean2, grid[x], y_here)
        if meanMethod == "circular":
            Mean = Mean1
        elif meanMethod == "centered":
            Mean = Mean2
        else:
            assert False

        bias = Mean - grid[x]
        if abs(bias) > 180:
            bias = bias+360
        y_set.append(bias)
        sd_set.append(computeCircularSD(y_here))
     return y_set, sd_set

trigonometric_basis = torch.stack([SQUARED_STIMULUS_SIMILARITY(i*grid) for i in range(1, FOURIER_BASIS_SIZE+1)] + [SQUARED_STIMULUS_DIFFERENCE(i*grid) for i in range(1, FOURIER_BASIS_SIZE+1)], dim=0)
trigonometric_basis = trigonometric_basis * (GRID / (2*trigonometric_basis.pow(2).sum(dim=1, keepdim=True)))
fourierMultiplier = MakeFloatTensor(list(range(1,FOURIER_BASIS_SIZE+1)) + list(range(1,FOURIER_BASIS_SIZE+1)))

print(fourierMultiplier)
print(trigonometric_basis.pow(2).sum(dim=1))
if False:
  figure, axis = plt.subplots(2*FOURIER_BASIS_SIZE, 1, figsize=(50, 50))
  for i in range(trigonometric_basis.size()[0]):
     axis[i].plot(grid.cpu(), trigonometric_basis[i].cpu())
  savePlot(f"figures/{__file__}_BASIS_{GRID}_{FOURIER_BASIS_SIZE}.pdf")
  plt.close()

byPlot = {}

def plot(ax_, MASK, X, Y, alpha=1, color="gray", group=0, name="?", row=None):
    if group != 1:
      ax_.plot(X[MASK], Y[MASK], alpha=alpha, color='gray')
    if (group,ax_,name,row,color) not in byPlot:
        byPlot[(group,ax_,name,row,color)] = []
    byPlot[(group,ax_,name,row,color)].append((X,Y,MASK))

viridis = cm.get_cmap('Dark2',10)
COLOR1 = viridis(1)
COLOR2 = viridis(3)
COLOR3 = viridis(2)
computations.setData(GRID=GRID, STIMULUS_SPACE_VOLUME=STIMULUS_SPACE_VOLUME)


def model(grid):
  lossesBy500 = []
  crossLossesBy500 = []
  noImprovement = 0
  global optim, learning_rate
  for iteration in range(1):
   parameters = init_parameters

   loss = 0

   if iteration % 500 == 0:
     plt.close()
     figure, axis = plt.subplots(1, 1, figsize=(1.2*1.2*4,1.2*1.3), squeeze=False)
     plt.tight_layout()
     figure.subplots_adjust(wspace=0.1, hspace=0.2)
     ENC = 0
     for q in range(1):
       for w in range(1):
         axis[q,w].spines['right'].set_visible(False)
         axis[q,w].spines['top'].set_visible(False)

     x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   volumeBySubjectRandomAdjustment =MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE) * (init_parameters["SIGMA_volumeBySubject"]+.1)
   priorBySubjectRandomAdjustment = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE) * (init_parameters["SIGMA_priorBySubject"]+.1)

   volumeBySubjectFourierTimesWeightInclMean = volumeBySubjectRandomAdjustment + init_parameters["volumeBySubject"]
   priorBySubjectFourierTimesWeightInclMean = priorBySubjectRandomAdjustment + init_parameters["priorBySubject"]

   volumeBySubjects = torch.matmul(volumeBySubjectFourierTimesWeightInclMean / fourierMultiplier.unsqueeze(0), trigonometric_basis)
   priorBySubjects = torch.matmul(priorBySubjectFourierTimesWeightInclMean / fourierMultiplier.unsqueeze(0), trigonometric_basis)

   plottedLabels = []

   if True:

    for SUBJECT in range(N_SUBJECTS):
     volume = 2 * math.pi * torch.nn.functional.softmax(parameters["volume"] + volumeBySubjects[SUBJECT])
     prior_overall = torch.nn.functional.softmax(parameters["prior"] + priorBySubjects[SUBJECT])
     shift_by = GRID-int((grid-original_centralOrientationPerSubject[SUBJECT]).abs().argmin())
     prior = torch.cat([prior_overall[shift_by:], prior_overall[:shift_by]], dim=0)

     if iteration % 500 == 0:
       I_SUBJECT = subjectsOrdering.index(SUBJECT)
       if abs(MakeFloatTensor([transformed_centralOrientationPerSubject[SUBJECT]]) - 135) < 15:
           I_SUBJECT=2

       elif abs(MakeFloatTensor([transformed_centralOrientationPerSubject[SUBJECT]]) - 135) < 30:
           I_SUBJECT=1

       elif abs(MakeFloatTensor([transformed_centralOrientationPerSubject[SUBJECT]]) - 135) < 46:
           I_SUBJECT=0
       else:
           assert False
       plottedLabels.append({ 'x' : original_centralOrientationPerSubject[SUBJECT], 'y' : 0.08, 's' : "BCD"[I_SUBJECT], 'color' : [COLOR1,COLOR2,COLOR3][I_SUBJECT], 'size' : 5, 'horizontalalignment' : 'center'})

       MASK = (SQUARED_STIMULUS_SIMILARITY(original_centralOrientationPerSubject[SUBJECT]-grid) > .5)
       resourcesOverall = 0
       for CONTRAST_ in [3,4]:
         resourcesOverall = resourcesOverall + 0.5 * computeResources(volume.detach().cpu(), inverse_variance = 1/float((2 * torch.sigmoid(init_parameters["sigma_logit"][SUBJECT,CONTRAST_]))))
       plot(axis[0,ENC], (MASK > -1), grid.cpu(), resourcesOverall, alpha=.2, row=0, color="dimgray")

   originalX = MakeFloatTensor([x["x"] for x in plottedLabels])
   noisedX = MakeZeros(len(plottedLabels))
   noisedX.requires_grad = True
   optim = torch.optim.SGD([noisedX], lr=0.1)

   for i in range(100):
       positions = originalX + noisedX
       loss = 100*torch.exp(-(positions.unsqueeze(0) - positions.unsqueeze(1)).pow(2)/200).sum() + 0.4 * noisedX.pow(2).sum()
       print(i, noisedX)
       optim.zero_grad()
       loss.backward()
       optim.step()

   for i in range(len(plottedLabels)):

       l = plottedLabels[i]

       if False:
          axis[0,0].annotate(l["s"], xy=(l["x"], 0.0), xytext=(l["x"] + float(noisedX[i]) - 5, 0.08), arrowprops=dict(arrowstyle="->", linewidth=.1, color=l["color"]), color=l["color"], size=7)
       axis[0,0].scatter([l["x"]], [0.0], color=l["color"], s=10)

   for w in range(1):
            for v in range(1):
                axis[w,v].set_xticks(ticks=[0,90,180,270,360], labels=["0", "90", "180", "270", "360"], size=12)
                axis[w,v].set_yticks(ticks=[0, 0.1, 0.2, 0.3], labels=["0", "", "0.2", ""], size=12)
                axis[w,v].set_xlim(0,360)
                axis[w,v].set_ylim(-0.01,0.4)

   for group, ax_, name, row, color in byPlot:
           Xs, Ys, MASKs = zip(*byPlot[(group, ax_, name, row, color)])
           overallX = torch.stack(Xs, dim=0)
           overallY = torch.stack(Ys, dim=0)

           MASK = torch.stack(MASKs, dim=0).all(dim=0)
           minX = 360
           maxX = 0
           for x, m in zip(Xs, MASKs):
               x_ = x[m]
               minX = min(minX, float(x_.min()))
               maxX = max(maxX, float(x_.max()))
           kernel = ((grid.view(-1, 1, 1)-overallX.unsqueeze(0)).abs() < 2).float()

           kernel = kernel / kernel.sum(dim=2).sum(dim=1)
           kernel = torch.where(torch.isnan(kernel), MakeZeros(kernel.size()), kernel)
           if torch.isnan(kernel).any():
               quit()
           smoothed = (overallY.unsqueeze(0) * kernel).sum(dim=2).sum(dim=1)

           if group == 1 and MASK.float().sum() > 0:
               kernel_per_subject = kernel / kernel.sum(dim=2, keepdim=True)
               smoothed_per_subject = (overallY.unsqueeze(0) * kernel_per_subject).sum(dim=2)

               sd = (smoothed_per_subject.pow(2).mean(dim=1) - smoothed_per_subject.mean(dim=1).pow(2)).sqrt() / math.sqrt(36)
               CI_x = grid.detach()[MASK].numpy().tolist()
               CI_y1 = (smoothed_per_subject.mean(dim=1)-sd).detach()[MASK].numpy().tolist()
               CI_y2 = (smoothed_per_subject.mean(dim=1)+sd).detach()[MASK].numpy().tolist()
               poly = plt.Polygon(list(zip(CI_x + CI_x[::-1], CI_y1 + CI_y2[::-1])), facecolor=(.65, .65, .65))
               ax_.add_patch(poly)

               ax_.plot(grid.detach()[MASK], (smoothed_per_subject.mean(dim=1)).detach()[MASK], color="gray", linewidth=2, linestyle='dashed')

           else:
              ax_.plot(grid.detach()[MASK], smoothed.detach()[MASK], color=color, linewidth=3)

   print("Saving plot...")
   savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.pdf", transparent=True)

############################3

model(grid)
