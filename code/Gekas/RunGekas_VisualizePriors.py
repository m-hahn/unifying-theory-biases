import glob
import json
import math
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sys
import torch
from loadModel import loadModel
from matplotlib import cm
from matplotlib import rc
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import ToDevice
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import makeGridIndicesCircular
from util import product
from util import savePlot
from util import sign

rc('font', **{'family':'FreeSans'})

OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
assert P > 0
FOLD_HERE = int(sys.argv[2])
REG_WEIGHT = float(sys.argv[3])
assert REG_WEIGHT == 1.0
GRID = int(sys.argv[4])
assert GRID % 180 == 0

FOURIER_BASIS_SIZE = int(sys.argv[5])
assert FOURIER_BASIS_SIZE in [30, 50, 80]
SHOW_PLOT = (len(sys.argv) < 7) or (sys.argv[6] == "SHOW_PLOT")
DEVICE = 'cpu'

FILE = f"logs/CROSSVALID/{__file__.replace('_VIZ_MainPaper','')}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_{FOURIER_BASIS_SIZE}.txt"

if os.path.exists(f"logs/CROSSVALID/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"):
   quit()

# Helper Functions dependent on the device

files = sorted(glob.glob("data/Gekas/Exp1/session1/*mat") + glob.glob("data/Gekas/Exp1/session2/*mat"))
files = files + sorted(glob.glob("data/Gekas/Exp2/session1/*mat") + glob.glob("data/Gekas/Exp2/session2/*mat"))

target = []
response = []

sample_total = []
response_total = []
contrast_total = []
subjects_total = []
condition_total = []
centralOrientationPerSubject = {}
for f in files:
 annots = loadmat(f)
 orientation = annots["central_orientation"][0][0]

 ## for encoding
# transformed_orientation = orientation % 90 + 90
# transformation = transformed_orientation - orientation

 ## for prior
 transformed_orientation = orientation #% 90 + 90
 transformation =  - orientation + 180

 orientation = transformed_orientation
 print(f, orientation)
 data = annots["data"][200:]
 sample = MakeFloatTensor(data[:,0])
 sample = (sample + transformation) % 360
 contrast = MakeLongTensor(data[:,1])
 response = MakeFloatTensor(data[:,4])
 response = (response + transformation) % 360
 color = MakeLongTensor(data[:,9])

 samplesRed = sample[color == 1]
 samplesGreen = sample[color == 2]
 if computeCircularSDWeighted(samplesGreen) < computeCircularSDWeighted(samplesRed):
     bimodalCondition = 2
     condition = color
 else:
     bimodalCondition = 1
     condition = torch.where(color==0, 0, 3-color)

 contrast = torch.where(contrast == 1, 1, torch.where(contrast == 4, 4, condition+1))

 print(f)
 subjectHere = int(f[f.index("sub")+3:f.rfind("ses")])-1
 if "Exp2" in f:
    subjectHere = subjectHere+18
 centralOrientationPerSubject[subjectHere] = orientation
 sample_total.append(sample)
 subjects_total.append(subjectHere + MakeZeros(sample.size()[0]))
 response_total.append(response)
 contrast_total.append(contrast)
 condition_total.append(condition)
 print(subjectHere)

sample = torch.cat(sample_total, dim=0)
responses = torch.cat(response_total, dim=0)
contrast = torch.cat(contrast_total, dim=0)
Subject = torch.cat(subjects_total, dim=0)
condition = torch.cat(condition_total, dim=0)

mask = (condition == 2)
sample = sample[mask]
responses = responses[mask]
condition = condition[mask]
contrast = contrast[mask]
Subject = Subject[mask]


# Extract the experimental prior from the dataset

print(sample.tolist())
a_ = [100, 116, 132, 148, 164, 180, 196, 212, 228, 244, 260]
others = (sample[torch.stack([sample != q for q in a_], dim=1).all(dim=1)].tolist())
for q in a_:
  print("prior", q, (sample==q).float().mean().item() / (1-len(others) / sample.size()[0]))
print("prior", "others", len(others) / sample.size()[0])



# Store observations
observations_x = sample
observations_y = responses

# Assign folds
N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

Subject_cpu = Subject.cpu()
#Fold = 0*Subject_cpu
#for i in range(int(min(Subject_cpu)), int(max(Subject_cpu))+1):
#    print("Making folds for subject", i)
#    trials = [j for j in range(Subject_cpu.size()[0]) if Subject_cpu[j] == i]
#    randomGenerator.shuffle(trials)
#    foldSize = int(len(trials)/N_FOLDS)
#    for k in range(N_FOLDS):
#        Fold[trials[k*foldSize:(k+1)*foldSize]] = k
#Fold = ToDevice(Fold)

# Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 360

CIRCULAR = True
INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
grid, grid_indices_here = makeGridIndicesCircular(GRID, MIN_GRID, MAX_GRID)
assert grid_indices_here.max() >= GRID, grid_indices_here.max()




#The exact experimental prior consists of $\approx$ 5\% uniform distribution and $\approx$ 95\% a discrete distribution supported at discrete steps in intervals of 16 degrees  around the central direction, with densities of 0\% at $\pm$ 80 degrees, $\approx 2.5\%$ at $\pm$ 64 degrees, $\approx 5.8\%$ at $\pm$ 48 degrees, $\approx 34.5\%$ at $\pm$ 32 degrees, $\approx 5.8\%$ at $\pm$ 16 degrees, and $\approx 2.5\%$ at $\pm$ 0 degrees.

def makeExperimentalPrior(centralDirectionForExperimentalPrior):
   
   prior1 = MakeZeros(GRID) + 1/GRID
   centralDirectionForExperimentalPrior = 161
   prior2 = (((grid + 80 - centralDirectionForExperimentalPrior)/16)) % 22.5
   
   print(prior2)
   
   priorBy16 = [0, 0.025, 0.058, 0.345, 0.058, 0.025, 0.058, 0.345, 0.058, 0.025, 0, 0]
   for i in range(GRID):
   #  print(i, prior2[i])
     if prior2[i]+1 >= len(priorBy16):
       prior2[i] = 0
     elif prior2[i] % 1 == 0:
       prior2[i] = priorBy16[prior2[i].long()]
     else:
       lower = int(prior2[i])
       upper = lower+1
       lower_v = priorBy16[lower]
       upper_v = priorBy16[upper]
       calculatedPrior = (1-(prior2[i]-lower)) * lower_v + ((prior2[i]-lower)) * upper_v
       print(prior2[i], lower, upper, (prior2[i]-lower), (1-(prior2[i]-lower)), lower_v, upper_v, calculatedPrior)
       prior2[i] = calculatedPrior
   
   prior2 = prior2 / prior2.sum()
   
   
   prior_experimental = 0.05 * prior1 + 0.95 * prior2
   return prior_experimental

print(prior_experimental)

#0.,  2.,  4.,  6.,  8., 10., 12., 14.,


figure, axis = plt.subplots(2, 2)
axis[0,0].scatter(grid, prior_experimental)
#axis[0,0].hist(sample, bins=200)
#axis[0,0].hist(sample[contrast==3], bins=200)
#axis[0,1].hist(sample[contrast==4], bins=200)
#axis[1,0].scatter(sample[contrast==3], responses[contrast==3])
#axis[1,1].scatter(sample[contrast==4], responses[contrast==4])
plt.show()
quit()

savePlot(f"figures/{__file__}_BASIS_{GRID}_{FOURIER_BASIS_SIZE}.pdf")
plt.close()



