import glob
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import scipy
import scipy.stats
import sys
import torch
from cosineEstimator import CosineEstimator
from matplotlib import cm
from matplotlib import rc
from matplotlib.backends.backend_pdf import PdfPages
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import ToDevice
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import makeGridIndicesCircular
from util import product
from util import savePlot
from util import sign
from util import toFactor

__file__ = __file__.split("/")[-1]
rc('font', **{'family':'FreeSans'})

OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
assert P > 0
FOLD_HERE = int(sys.argv[2])
REG_WEIGHT = float(sys.argv[3])
assert REG_WEIGHT == 1.0
GRID = int(sys.argv[4])
assert GRID % 180 == 0

FOURIER_BASIS_SIZE = int(sys.argv[5])
assert FOURIER_BASIS_SIZE in [30, 50, 80]
SHOW_PLOT = (len(sys.argv) < 7) or (sys.argv[6] == "SHOW_PLOT")
DEVICE = 'cpu'

FILE = f"logs/CROSSVALID/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"

# Helper Functions dependent on the device

files = sorted(glob.glob("data/Gekas/Exp1/session1/*mat") + glob.glob("data/Gekas/Exp1/session2/*mat"))
files = files + sorted(glob.glob("data/Gekas/Exp2/session1/*mat") + glob.glob("data/Gekas/Exp2/session2/*mat"))

target = []
response = []

sample_total = []
response_total = []
contrast_total = []
subjects_total = []
condition_total = []
centralOrientationPerSubject = {}
for f in files:
 annots = loadmat(f)
 orientation = annots["central_orientation"][0][0]

 transformed_orientation = orientation % 90 + 90

 transformation = transformed_orientation - orientation
 orientation = transformed_orientation
 print(f, orientation)
 data = annots["data"][200:]
 sample = MakeFloatTensor(data[:,0])
 sample = (sample + transformation) % 360
 contrast = MakeLongTensor(data[:,1])
 response = MakeFloatTensor(data[:,4])
 response = (response + transformation) % 360
 color = MakeLongTensor(data[:,9])

 samplesRed = sample[color == 1]
 samplesGreen = sample[color == 2]
 if computeCircularSDWeighted(samplesGreen) < computeCircularSDWeighted(samplesRed):
     bimodalCondition = 2
     condition = color
 else:
     bimodalCondition = 1
     condition = torch.where(color==0, 0, 3-color)

 contrast = torch.where(contrast == 1, 1, torch.where(contrast == 4, 4, condition+1))

 print(f)
 subjectHere = int(f[f.index("sub")+3:f.rfind("ses")])-1
 if "Exp2" in f:
    subjectHere = subjectHere+18
 centralOrientationPerSubject[subjectHere] = orientation
 sample_total.append(sample)
 subjects_total.append(subjectHere + MakeZeros(sample.size()[0]))
 response_total.append(response)
 contrast_total.append(contrast)
 condition_total.append(condition)
 print(subjectHere)

sample = torch.cat(sample_total, dim=0)
responses = torch.cat(response_total, dim=0)
contrast = torch.cat(contrast_total, dim=0)
Subject = torch.cat(subjects_total, dim=0)
condition = torch.cat(condition_total, dim=0)

mask = (condition == 2)
sample = sample[mask]
responses = responses[mask]
condition = condition[mask]
contrast = contrast[mask]
Subject = Subject[mask]

# Store observations
observations_x = sample
observations_y = responses

# Assign folds
N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

Subject_cpu = Subject.cpu()
Fold = 0*Subject_cpu
for i in range(int(min(Subject_cpu)), int(max(Subject_cpu))+1):
    print("Making folds for subject", i)
    trials = [j for j in range(Subject_cpu.size()[0]) if Subject_cpu[j] == i]
    randomGenerator.shuffle(trials)
    foldSize = int(len(trials)/N_FOLDS)
    for k in range(N_FOLDS):
        Fold[trials[k*foldSize:(k+1)*foldSize]] = k
Fold = ToDevice(Fold)

# Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 360

CIRCULAR = True
INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
grid, grid_indices_here = makeGridIndicesCircular(GRID, MIN_GRID, MAX_GRID)
assert grid_indices_here.max() >= GRID, grid_indices_here.max()

# Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

N_SUBJECTS = int(max(Subject))+1
subjectsOrdering = sorted(range(N_SUBJECTS), key=lambda x:centralOrientationPerSubject[x])

# Initialize the model
parameters = {}
with open("logs/CROSSVALID/RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO.py_4_0_1.0_180_50.txt", "r") as inFile:
    l = next(inFile)
    l = next(inFile)
    for l in inFile:
      print(l[:20])
      z, y = l.strip().split("\t")
      parameters[z.strip()] = MakeFloatTensor(json.loads(y))

shapes = {x : y.size() for x, y in parameters.items()}

##############################################
def SQUARED_STIMULUS_DIFFERENCE(x):
    return torch.sin(math.pi*x/180)
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(math.pi*x/180)
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(x)
def SQUARED_SENSORY_DIFFERENCE(x):
    return torch.sin(x)

#############################################################
# Part: Configure the appropriate estimator for minimizing the loss function
assert P >= 2
CosineEstimator.set_parameters(GRID=GRID, OPTIMIZER_VERBOSE=OPTIMIZER_VERBOSE, P=P, SQUARED_SENSORY_DIFFERENCE=SQUARED_SENSORY_DIFFERENCE, SQUARED_SENSORY_SIMILARITY=SQUARED_SENSORY_SIMILARITY)

#############################################################
# Part: Run the model. This function implements the model itself:
## calculating the likelihood of a given dataset under that model
## and---if the computePredictions argument is set to True--- computes
## the bias and variability of the estimate.
def samplePredictions(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, computePredictions=False, sigma_stimulus=None, sigma2_stimulus=None, contrast_=None, folds=None, lossReduce='mean', subject=None):

 motor_variance = torch.exp(- parameters["log_motor_var"][subject])
 sigma2 = 2*torch.sigmoid(sigma_logit)
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 loss = 0
 if True:

  if sigma2_stimulus > 0:
    assert False
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))
    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  # Part: Compute sensory likelihoods. Across both interval and
  ## circular stimulus spaces, this amounts to exponentiaring a
  ## `similarity`
  sensory_likelihoods = torch.softmax(((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2)) + volumeElement.unsqueeze(1).log(), dim=0)

  # Part: If stimulus noise is nonzero, convolve the likelihood with the
  ## stimulus noise.
  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    assert False
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  ## Compute posterior using Bayes' rule. As described in the paper, the posterior is computed
  ## in the discretized stimulus space.
  posterior = prior.unsqueeze(1) * likelihoods.t()
  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  ## Compute the estimator for each m in the discretized sensory space.
  bayesianEstimate = CosineEstimator.apply(grid_indices_here, posterior)
  likelihoods_by_stimulus = likelihoods[:,stimulus_]
  print(likelihoods_by_stimulus.size())
  sampled_estimator = bayesianEstimate[torch.distributions.Categorical(likelihoods_by_stimulus.t()).sample()] * 360 / GRID
  print(sampled_estimator)
  print(sampled_estimator.size())

  motor_noise = scipy.stats.vonmises.rvs(1/float(motor_variance), size=stimulus_.size()[0]) * 180 / math.pi
  print(motor_noise.min(), motor_noise.max())
  sampled_response = (sampled_estimator + motor_noise) % 360

  # Mixture of estimation and uniform response
  uniform_part = torch.sigmoid(parameters["mixture_logit"][subject])
  choose_uniform = (MakeZeros(sampled_response.size()).uniform_(0,1) < uniform_part)
  sampled_response = torch.where(choose_uniform, MakeZeros(sampled_response.size()).uniform_(0,360).float(), sampled_response.float())

  print(sampled_estimator.min())
  print(sampled_estimator.max())

  return sampled_response.float()

trigonometric_basis = torch.stack([SQUARED_STIMULUS_SIMILARITY(i*grid) for i in range(1, FOURIER_BASIS_SIZE+1)] + [SQUARED_STIMULUS_DIFFERENCE(i*grid) for i in range(1, FOURIER_BASIS_SIZE+1)], dim=0)
trigonometric_basis = trigonometric_basis * (GRID / (2*trigonometric_basis.pow(2).sum(dim=1, keepdim=True)))
fourierMultiplier = MakeFloatTensor(list(range(1,FOURIER_BASIS_SIZE+1)) + list(range(1,FOURIER_BASIS_SIZE+1)))

print(fourierMultiplier)
print(trigonometric_basis.pow(2).sum(dim=1))
if False:
  figure, axis = plt.subplots(2*FOURIER_BASIS_SIZE, 1, figsize=(50, 50))
  for i in range(trigonometric_basis.size()[0]):
     axis[i].plot(grid.cpu(), trigonometric_basis[i].cpu())
  savePlot(f"figures/{__file__}_BASIS_{GRID}_{FOURIER_BASIS_SIZE}.pdf")
  plt.close()

byPlot = {}

def plot(ax_, MASK, X, Y, alpha=1, color="gray", group=0, name="?", row=None, size=None, subject=None):
    if group != 1:
      ax_.plot(X[MASK], Y[MASK], alpha=alpha, color='gray')
    if (group,ax_,name,row,color) not in byPlot:
        byPlot[(group,ax_,name,row,color)] = {}
    if subject not in byPlot[(group,ax_,name,row,color)]:
        byPlot[(group,ax_,name,row,color)][subject] = []
    byPlot[(group,ax_,name,row,color)][subject].append((X,Y,MASK))

viridis = cm.get_cmap('Dark2',10)
COLOR1 = viridis(1)
COLOR2 = viridis(3)
COLOR3 = viridis(2)
print(viridis(1))
print(viridis(2))
print(viridis(3))

############################3

# Project the stimuli onto the discrete grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))

yValues = []
for y in observations_y:
   yValues.append(int( torch.argmin((grid - y).abs())))

xValues = MakeLongTensor(xValues)
yValues = MakeLongTensor(yValues)

volume = 2 * math.pi * torch.nn.functional.softmax(parameters["volume"])
prior = torch.nn.functional.softmax(parameters["prior"])

observations_y = MakeZeros(xValues.size())
conditions = MakeZeros(xValues.size())

if True:
   volumeBySubjectRandomAdjustment =MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE) * (parameters["SIGMA_volumeBySubject"]+.1)
   priorBySubjectRandomAdjustment = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE) * (parameters["SIGMA_priorBySubject"]+.1)

   volumeBySubjectFourierTimesWeightInclMean = volumeBySubjectRandomAdjustment + parameters["volumeBySubject"]
   priorBySubjectFourierTimesWeightInclMean = priorBySubjectRandomAdjustment + parameters["priorBySubject"]

   volumeBySubjects = torch.matmul(volumeBySubjectFourierTimesWeightInclMean / fourierMultiplier.unsqueeze(0), trigonometric_basis)
   priorBySubjects = torch.matmul(priorBySubjectFourierTimesWeightInclMean / fourierMultiplier.unsqueeze(0), trigonometric_basis)

for CONTRAST in [1,2,3,4]:
  if not torch.any(contrast == CONTRAST):
       continue
  for SUBJECT in range(N_SUBJECTS):
     volume = 2 * math.pi * torch.nn.functional.softmax(parameters["volume"] + volumeBySubjects[SUBJECT])
     prior_overall = torch.nn.functional.softmax(parameters["prior"] + priorBySubjects[SUBJECT])
     shift_by = GRID-int((grid-centralOrientationPerSubject[SUBJECT]).abs().argmin())
     prior = torch.cat([prior_overall[shift_by:], prior_overall[:shift_by]], dim=0)

     MASK = torch.logical_and(Subject==SUBJECT, contrast==CONTRAST)
     observations_y[MASK] = samplePredictions(xValues[MASK], parameters["sigma_logit"][SUBJECT,CONTRAST], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, computePredictions=True, sigma_stimulus=0, sigma2_stimulus=0, contrast_=CONTRAST, folds=None, lossReduce='sum', subject=SUBJECT)
observations_y = observations_y.float()

SELFID = random.randint(10000, 1000000)

print(xValues)

with open(f"logs/SIMULATED_REPLICATE/{__file__}_{P}_{GRID}.txt", "w") as outFile:
    for z, y in parameters.items():
        print(z, "\t", y.detach().cpu().numpy().tolist(), file=outFile)
    print("=======", file=outFile)
    for i in range(xValues.size()[0]):
      print(int(Subject[i]), int(contrast[i]), round(float(grid[xValues[i]]),1), round(float(observations_y[i]),1), file=outFile)
