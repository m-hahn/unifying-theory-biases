# Gekas



* [Collect quantitative results](evaluateCrossValidationResults_Gekas_Fixed.py), [NLL by exponent (full)](evaluateCrossValidationResults_Gekas_Fixed.py_RelativeLF.pdf), [NLL by exponent (simple)](evaluateCrossValidationResults_Gekas_Fixed.py_simple.pdf)


# Model Components
Explanation of model components:

* `sigma2_stimulus` (1) stimulus noise -- this is unused (zero) in this experiment
* `log_motor_var` (36) -- motor variance, parameterized via logarithm (one for each subject)
* `sigma_logit` (36 x 6) -- sensory noise variance, parameterized via logit (one for each subject)
* `mixture_logit` (36) -- guessing rate, parameterized via logit (one for each subject)
* `prior` (180) -- the fixed-effects component of the prior around the central direction of each subject, parameterized via unnormalized logarithms beta (described in SI Appendix, S2.1.1) 
* `volume` (180) -- the fixed-effects component of the resource allocation, parameterized via unnormalized logarithms alpha (described in SI Appendix, S2.1.1)
* `priorBySubject` (36,2*50) -- per-subject adjustments to the log-prior. 36 is the number of subjects, 100 the number of Fourier components included.
* `volumeBySubject"1 (36,2*50) -- per-subject adjustments to the log-resources. 36 is the number of subjects, 100 the number of Fourier components included.
* `prior_logits_effect_weight` (1) -- used in ELBO-based fitting
* `volume_logits_effect_weight` (1) -- used in ELBO-based fitting
* `SIGMA_priorBySubject` (36, 2*50) -- used in ELBO-based fitting
* `SIGMA_volumeBySubject` (36, 2*50) -- used in ELBO-based fitting


# Fitting


python RunGekas_CosinePLoss_ShiftedPrior_Fourier_MAP_ELBO.py 0 0 1.0 180 50
python RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_MAP_ELBO.py 0 0 1.0 180 50
python RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_MAP_ELBO.py 0 0 1.0 180 50
for p in 2 4 6 8 10
do
python RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_ELBO.py $p 0 1.0 180 50
python RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO.py $p 0 1.0 180 50
python RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_ELBO.py $p 0 1.0 180 50
done


# Calculate Heldout NLL

python RunGekas_CosinePLoss_ShiftedPrior_Fourier_MAP_ELBO_EVAL.py 0 0 1.0 180 50
python RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_MAP_ELBO_EVAL.py 0 0 1.0 180 50
python RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_MAP_ELBO_EVAL.py 0 0 1.0 180 50
for p in 2 4 6 8 10
do
python RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_ELBO_EVAL.py $p 0 1.0 180 50
python RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_EVAL.py $p 0 1.0 180 50
python RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_ELBO_EVAL.py $p 0 1.0 180 50
done

# Visualize

. createSIFigures_Gekas.sh


## Encoding and Prior (no transform)
* Encoding: [resulting plot](RunGekas_CosinePLoss_NoShiftedPrior_Fourier_ELBO_VIZ_Encoding_MainPaper.py_4_0_1.0_180.pdf)
* Prior: [resulting plot](RunGekas_CosinePLoss_NoShiftedPrior_Fourier_ELBO_VIZ_Priors.py_4_0_1.0_180.pdf)


## For Main Paper
* [resulting plot](RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_ACREXP.py_ALL_0_1.0_180_50.pdf)
* [resulting plot](RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_MainPaper.py_4_0_1.0_180.pdf)
* [resulting plot](RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_Stylized.py_4_0_1.0_180.pdf)



## MAP estimator
* [Full model](RunGekas_CosinePLoss_ShiftedPrior_Fourier_MAP_ELBO.py), 
[evaluate](RunGekas_CosinePLoss_ShiftedPrior_Fourier_MAP_ELBO_EVAL.py), 
[visualization script](RunGekas_CosinePLoss_ShiftedPrior_Fourier_MAP_ELBO_VIZ.py), 
[resulting plot](RunGekas_CosinePLoss_ShiftedPrior_Fourier_MAP_ELBO_VIZ.py_0_0_1.0_180.pdf)
* [Uniform prior](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_MAP_ELBO.py), 
[evaluate](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_MAP_ELBO_EVAL.py), 
[visualization script](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_MAP_ELBO_VIZ.py), 
[sample plot](...),
[resulting plot](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_MAP_ELBO_VIZ.py_0_0_1.0_180.pdf)
* [Uniform encoding](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_MAP_ELBO.py), 
[evaluate](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_MAP_ELBO_EVAL.py), 
[visualization script](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_MAP_ELBO_VIZ.py), 
[sample plot](...),
[resulting plot](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_MAP_ELBO_VIZ.py_0_0_1.0_180.pdf)

## Positive exponents
* [Full model](RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO.py), 
[evaluate](RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_EVAL.py), 
[visualization script](RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2.py), 
[batch viz](RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_ACREXP.py),
[resulting plot](figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_ACREXP.py_ALL_0_1.0_180_50.pdf)

* [Uniform prior](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_ELBO.py), 
[evaluate](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_ELBO_EVAL.py), 
[visualization script](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_ELBO_VIZ.py), 
[batch viz](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_ELBO_VIZ_ACREXP.py)
[resulting plot](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_ELBO_VIZ_ACREXP.py_ALL_0_1.0_180_50.pdf)

* [Uniform encoding](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_ELBO.py), 
[evaluate](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_ELBO_EVAL.py), 
[visualization script](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_ELBO_VIZ.py), 
[batch viz](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_ELBO_VIZ_ACREXP.py),
[resulting plot](RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_ELBO_VIZ_ACREXP.py_ALL_0_1.0_180_50.pdf)

## Without transformation:

* [Fitting](RunGekas_CosinePLoss_NoShiftedPrior_Fourier_ELBO.py), 
[visualization of encoding](RunGekas_CosinePLoss_NoShiftedPrior_Fourier_ELBO_VIZ_Encoding_MainPaper.py), 
[visualization of priors](RunGekas_CosinePLoss_NoShiftedPrior_Fourier_ELBO_VIZ_Priors.py)


## On Simulated Data

* [simulate](RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_CreateSimulated.py), [simulated dataset](RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_CreateSimulated.py_4_180_116242.txt),  [fit](RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_OnSimData.py),  [fitted model](logs/SIMULATED_REPLICATE/fits/RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_OnSimData.py_4_0_1.0_180_50_116242.txt), [visualization](RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_OnSimData_VIZ2.py), [resulting plot](RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_OnSimData_VIZ2.py_4_0_1.0_180_116242.pdf)

## Viz for main paper
* [viz](RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_MainPaper.py)
* [contributions from the cardinal group](RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_Cardinal.py), [resulting plot](RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_Cardinal.py_4_0_1.0_180_Magnitudes.pdf), [resulting plot](RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_Cardinal.py_4_0_1.0_180_PPRatio.pdf)
* [stylized](RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_Stylized.py)


Extracting the experimental prior from the dataset: [this script](RunGekas_VisualizePriors.py).

# Simulation

`python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_CreateSimulated.py 4 0 1.0 180 50`

 also need:
	RunGekas_CosinePLoss_ShiftedPrior_Fourier_MAP_ELBO_OnSimData.py
	RunGekas_CosinePLoss_ShiftedPrior_Fourier_MAP_ELBO_OnSimData_EVAL.py


mv RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ_Priors.py ARCHIVE/
mv RunGekas_CosinePLoss_ShiftedPrior_Fourier_MAP_ELBO_OnSimData.py ARCHIVE/
mv RunGekas_CosinePLoss_ShiftedPrior_Fourier_MAP_ELBO_OnSimData_EVAL.py ARCHIVE/
mv RunGekas_CosinePLoss_ShiftedPrior_Fourier_OnlyFixed.py ARCHIVE/
mv RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_OnlyFixed.py ARCHIVE/
mv RunGekas_CosinePLoss_UniformPrior_Fourier_ELBO_OnSimData.py ARCHIVE/
mv RunGekas_CosinePLoss_UniformPrior_Fourier_ELBO_OnSimData_EVAL.py ARCHIVE/
mv RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_OnSimData_EVAL.py ARCHIVE/




