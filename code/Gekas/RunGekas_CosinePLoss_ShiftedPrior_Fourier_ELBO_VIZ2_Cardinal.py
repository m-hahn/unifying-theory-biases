import glob
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sys
import torch
from loadModel import loadModel
from matplotlib import cm
from matplotlib import rc
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import ToDevice
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import makeGridIndicesCircular
from util import product
from util import savePlot
from util import sign

rc('font', **{'family':'FreeSans'})

OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
assert P > 0
FOLD_HERE = int(sys.argv[2])
REG_WEIGHT = float(sys.argv[3])
assert REG_WEIGHT == 1.0
GRID = int(sys.argv[4])
assert GRID % 180 == 0

FOURIER_BASIS_SIZE = int(sys.argv[5])
assert FOURIER_BASIS_SIZE in [30, 50, 80]
SHOW_PLOT = (len(sys.argv) < 7) or (sys.argv[6] == "SHOW_PLOT")
DEVICE = 'cpu'

FILE = f"logs/CROSSVALID/{__file__.replace('_VIZ2_Cardinal','')}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_{FOURIER_BASIS_SIZE}.txt"

if os.path.exists(f"logs/CROSSVALID/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"):
   quit()

# Helper Functions dependent on the device

files = sorted(glob.glob("data/Gekas/Exp1/session1/*mat") + glob.glob("data/Gekas/Exp1/session2/*mat"))
files = files + sorted(glob.glob("data/Gekas/Exp2/session1/*mat") + glob.glob("data/Gekas/Exp2/session2/*mat"))

target = []
response = []

sample_total = []
response_total = []
contrast_total = []
subjects_total = []
condition_total = []
centralOrientationPerSubject = {}
for f in files:
 annots = loadmat(f)
 orientation = annots["central_orientation"][0][0]

 transformed_orientation = orientation % 90 + 90

 transformation = transformed_orientation - orientation
 orientation = transformed_orientation
 print(f, orientation)
 data = annots["data"][200:]
 sample = MakeFloatTensor(data[:,0])
 sample = (sample + transformation) % 360
 contrast = MakeLongTensor(data[:,1])
 response = MakeFloatTensor(data[:,4])
 response = (response + transformation) % 360
 color = MakeLongTensor(data[:,9])

 samplesRed = sample[color == 1]
 samplesGreen = sample[color == 2]
 if computeCircularSDWeighted(samplesGreen) < computeCircularSDWeighted(samplesRed):
     bimodalCondition = 2
     condition = color
 else:
     bimodalCondition = 1
     condition = torch.where(color==0, 0, 3-color)

 contrast = torch.where(contrast == 1, 1, torch.where(contrast == 4, 4, condition+1))

 print(f)
 subjectHere = int(f[f.index("sub")+3:f.rfind("ses")])-1
 if "Exp2" in f:
    subjectHere = subjectHere+18
 centralOrientationPerSubject[subjectHere] = orientation
 sample_total.append(sample)
 subjects_total.append(subjectHere + MakeZeros(sample.size()[0]))
 response_total.append(response)
 contrast_total.append(contrast)
 condition_total.append(condition)
 print(subjectHere)

sample = torch.cat(sample_total, dim=0)
responses = torch.cat(response_total, dim=0)
contrast = torch.cat(contrast_total, dim=0)
Subject = torch.cat(subjects_total, dim=0)
condition = torch.cat(condition_total, dim=0)

mask = (condition == 2)
sample = sample[mask]
responses = responses[mask]
condition = condition[mask]
contrast = contrast[mask]
Subject = Subject[mask]

# Store observations
observations_x = sample
observations_y = responses

# Assign folds
N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

Subject_cpu = Subject.cpu()
Fold = 0*Subject_cpu
for i in range(int(min(Subject_cpu)), int(max(Subject_cpu))+1):
    print("Making folds for subject", i)
    trials = [j for j in range(Subject_cpu.size()[0]) if Subject_cpu[j] == i]
    randomGenerator.shuffle(trials)
    foldSize = int(len(trials)/N_FOLDS)
    for k in range(N_FOLDS):
        Fold[trials[k*foldSize:(k+1)*foldSize]] = k
Fold = ToDevice(Fold)

# Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 360

CIRCULAR = True
INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
grid, grid_indices_here = makeGridIndicesCircular(GRID, MIN_GRID, MAX_GRID)
assert grid_indices_here.max() >= GRID, grid_indices_here.max()

# Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

N_SUBJECTS = int(max(Subject))+1
subjectsOrdering = sorted(range(N_SUBJECTS), key=lambda x:centralOrientationPerSubject[x])

# Initialize the model
init_parameters = {}
init_parameters["sigma2_stimulus"] = MakeFloatTensor([0]).view(1)
init_parameters["log_motor_var"] = MakeFloatTensor(N_SUBJECTS*[0])
init_parameters["sigma_logit"] = -3 + MakeZeros(N_SUBJECTS, 6)
init_parameters["chance_prob_logit"] = MakeFloatTensor([-0]).view(1)
init_parameters["mixture_logit"] = MakeFloatTensor(N_SUBJECTS*[-1])
init_parameters["prior"] = MakeZeros(GRID)
init_parameters["volume"] = MakeZeros(GRID)
init_parameters["priorBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["prior_logits_effect_weight"] = MakeZeros(1)
init_parameters["volumeBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["volume_logits_effect_weight"] = MakeZeros(1)

init_parameters["SIGMA_priorBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["SIGMA_volumeBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)

loadModel(FILE, init_parameters)
assert "volume" in init_parameters

for _, y in init_parameters.items():
    y.requires_grad = True

# Initialize optimizer.
# The learning rate is a user-specified parameter.
learning_rate=.1
optim = torch.optim.SGD([y for _, y in init_parameters.items()], lr=learning_rate)

shapes = {x : y.size() for x, y in init_parameters.items()}

def SQUARED_STIMULUS_DIFFERENCE(x):
    return torch.sin(math.pi*x/180)
def SQUARED_STIMULUS_SIMILARITY(x):
    return torch.cos(math.pi*x/180)
def SQUARED_SENSORY_SIMILARITY(x):
    return torch.cos(x)
def SQUARED_SENSORY_DIFFERENCE(x):
    return torch.sin(x)

# Import the appropriate estimator for minimizing the loss function
SCALE = 50

class LPEstimator(torch.autograd.Function):
    """
    We can implement our own custom autograd Functions by subclassing
    torch.autograd.Function and implementing the forward and backward passes
    which operate on Tensors.
    """

    @staticmethod
    def forward(ctx, grid_indices_here, posterior):
        """
        In the forward pass we receive a Tensor containing the input and return
        a Tensor containing the output. ctx is a context object that can be used
        to stash information for backward computation. You can cache arbitrary
        objects for use in the backward pass using the ctx.save_for_backward method.
        """
        grid_indices_here = grid_indices_here/SCALE
        n_inputs, n_batch = posterior.size()
        initialized = (grid_indices_here.data * posterior).detach().sum(dim=0).data
        result = initialized.clone()

        for itera in range(20):

          if OPTIMIZER_VERBOSE:
             loss = (((result.unsqueeze(0) - grid_indices_here).abs().pow(P)) * posterior.detach()).sum(dim=0).sum() / n_batch
          loss_gradient = ((P * torch.sign(result.unsqueeze(0) - grid_indices_here) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-1)) * posterior.detach()).sum(dim=0) / n_batch
          loss_gradient2 = ((P * (P-1) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-2)) * posterior.detach()).sum(dim=0) / n_batch
          result -= loss_gradient / loss_gradient2
          if OPTIMIZER_VERBOSE:
             print(itera, "max absolute gradient after Newton steps", loss_gradient.abs().max())
          if float(loss_gradient.abs().max()) < 1e-5:
              break

        if float(loss_gradient.abs().max()) >= 1e-5:
            print("WARNING", float(loss_gradient.abs().max()), "after ", itera, " Newton steps")
        if P == 2:
            assert itera == 0
        ctx.save_for_backward(grid_indices_here, posterior, result)

        return SCALE*result

    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor containing the gradient of the loss
        with respect to the output, and we need to compute the gradient of the loss
        with respect to the input.
        """
        grid_indices_here, posterior, result = ctx.saved_tensors
        initialized = (grid_indices_here.data * posterior).detach().sum(dim=0).data

        F = ((P * torch.sign(result.unsqueeze(0) - grid_indices_here) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-1)) * posterior.detach()).sum(dim=0)

        assert F.abs().mean() < 0.001, F
        dF_posterior = ((P * torch.sign(result.unsqueeze(0) - grid_indices_here) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-1)))
        dF_result = ((P * (P-1) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-2)) * posterior.detach()).sum(dim=0)

        gradient = - ((1 / dF_result).unsqueeze(0) * dF_posterior).detach().data

        gradient = grad_output.unsqueeze(0) * gradient

        return None, gradient*SCALE

class CosineEstimator(torch.autograd.Function):
    """
    We can implement our own custom autograd Functions by subclassing
    torch.autograd.Function and implementing the forward and backward passes
    which operate on Tensors.
    """

    @staticmethod
    def forward(ctx, grid_indices_here, posterior):
        """
        In the forward pass we receive a Tensor containing the input and return
        a Tensor containing the output. ctx is a context object that can be used
        to stash information for backward computation. You can cache arbitrary
        objects for use in the backward pass using the ctx.save_for_backward method.
        """
        grid_indices_here = grid_indices_here * 2 * math.pi/GRID

        n_inputs, n_batch = posterior.size()
        initialized = (grid_indices_here.data * posterior).detach().sum(dim=0).data.clone()

        result = initialized.clone()

        momentum = MakeZeros(GRID)

        # Preinitialize using ordinary Lp loss
        if False:
         for itera in range(20):

          if False:
            loss = (((result.unsqueeze(0)/SCALE - grid_indices_here/SCALE).abs().pow(P)) * posterior.detach()).sum(dim=0).sum() / 1
          loss_gradient = ((P * torch.sign(result.unsqueeze(0)/SCALE - grid_indices_here/SCALE) * (result.unsqueeze(0)/SCALE - grid_indices_here/SCALE).abs().pow(P-1)) * posterior.detach()).sum(dim=0) / 1
          loss_gradient2 = ((P * (P-1) * (result.unsqueeze(0)/SCALE - grid_indices_here/SCALE).abs().pow(P-2)) * posterior.detach()).sum(dim=0) / 1
          result -= loss_gradient / loss_gradient2

          if float(loss_gradient.abs().max()) < 1e-5:
              break

        for itera in range(30):

          if OPTIMIZER_VERBOSE:
            loss = (((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2)) * posterior.detach()).sum(dim=0)
          loss_gradient = (P/2 * (SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-1)) * posterior.detach()).sum(dim=0)

          loss_gradient2 = (P/2 * (SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-1)) * posterior.detach()).sum(dim=0) / 1
          if P == 2:
             pass
          else:
             loss_gradient2 = loss_gradient2 + (P/2 * (P/2-1) * (SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)).pow(2) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-2)) * posterior.detach()).sum(dim=0) / 1

          MASK = (loss_gradient2 > 0.001)
          updateGD = - loss_gradient /loss_gradient2.abs().clamp(min=0.0001)
          updateNewton = - loss_gradient/loss_gradient2

          update = torch.where(MASK, updateNewton, updateGD)

          result = result + update
          if OPTIMIZER_VERBOSE:
             print(itera, float(loss.mean()), "max absolute gradient after GD steps", loss_gradient.abs().max(), sum(MASK.float()), "Newton steps", "max update", update.abs().max())
          if float(loss_gradient.abs().max()) < 1e-6:
              break

        if random.random() < 0.01:
            print("Newton Iterations", itera)
        if float(loss_gradient.abs().max()) >= 1e-4:
            print("WARNING", float(loss_gradient.abs().max()), "after ", itera, " Newton steps")
            assert False

        ctx.save_for_backward(grid_indices_here, posterior, result)

        return result * GRID / (2*math.pi)

    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor containing the gradient of the loss
        with respect to the output, and we need to compute the gradient of the loss
        with respect to the input.
        """
        grid_indices_here, posterior, result = ctx.saved_tensors
        initialized = (grid_indices_here.data * posterior).detach().sum(dim=0).data

        n_inputs, n_batch = posterior.size()

        F = (P/2 * (SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-1)) * posterior.detach()).sum(dim=0)

        assert F.abs().mean() < 0.01, (F, F.abs().mean(), F.abs().max()/n_batch, n_batch, ((SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)) * posterior.detach()).sum(dim=0) / n_batch)

        dF_posterior = P/2 * (SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-1))
        dF_result = 0
        dF_result = dF_result + (P/2 * (SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-1)) * posterior.detach()).sum(dim=0)
        if P == 2:
           pass

        else:
           dF_result = dF_result + (P/2 * (P/2-1) * (SQUARED_SENSORY_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)).pow(2) * ((1-SQUARED_SENSORY_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-2)) * posterior.detach()).sum(dim=0)
        if OPTIMIZER_VERBOSE:
           if torch.isnan(result).any():
                print(result.size(), dF_result.size())
                assert False, result
           if torch.isnan(posterior).any():
                print(result.size(), dF_result.size())
                assert False, posterior
           if torch.isnan(dF_result).any():
                print(result[torch.isnan(dF_result)])
                print((SQUARED_STIMULUS_DIFFERENCE(result.unsqueeze(0) - grid_indices_here)).pow(2))
                print(((1+1e-10-SQUARED_STIMULUS_SIMILARITY(result.unsqueeze(0) - grid_indices_here)).pow(P/2-2)))
                print(result.size(), dF_result.size(), posterior.size())
                assert False

        gradient = - ((1 / dF_result).unsqueeze(0) * dF_posterior).detach().data
        if OPTIMIZER_VERBOSE:
           if dF_result.abs().min() < .0001:
                assert False, dF_result.abs().min()
           if torch.isnan(dF_result).any():
                assert False, dF_result
        if OPTIMIZER_VERBOSE:
           if torch.isnan(dF_posterior).any():
                assert False, dF_posterior

        if OPTIMIZER_VERBOSE:
           if torch.isnan(grad_output).any():
                assert False, grad_output
        gradient = grad_output.unsqueeze(0) * gradient
        if OPTIMIZER_VERBOSE:
           if torch.isnan(gradient).any():
                assert False, gradient

        return None, gradient * GRID / (2*math.pi)

# Run the model
def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, computePredictions=False, sigma_stimulus=None, sigma2_stimulus=None, contrast_=None, folds=None, lossReduce='mean', subject=None):

 motor_variance = torch.exp(- parameters["log_motor_var"][subject])
 sigma2 = 2*torch.sigmoid(sigma_logit)
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 loss = 0
 if True:
  folds = MakeLongTensor(folds)
  if subject is not None:
    MASK = torch.logical_and(contrast==contrast_, torch.logical_and((Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0), Subject==subject))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]
  else:
    MASK = torch.logical_and(contrast==contrast_, (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]
  assert stimulus.view(-1).size()[0] > 0, (contrast_)

  if sigma2_stimulus > 0:
    assert False
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))
    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  # Sensory likelihoods
  sensory_likelihoods = torch.nn.functional.softmax(((torch.cos(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(2*sigma2)) / math.sqrt(2*math.pi*sigma2)  + volumeElement.unsqueeze(1).log(), dim=0)

  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    assert False
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  # Compute posterior
  posterior = prior.unsqueeze(1) * likelihoods.t()
  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  # Estimator
  bayesianEstimate = CosineEstimator.apply(grid_indices_here, posterior)
  # Caculate motor likelihood
  error = (SQUARED_STIMULUS_SIMILARITY(360/GRID*bayesianEstimate.unsqueeze(0) - responses.unsqueeze(1)))
  log_normalizing_constant = torch.logsumexp((SQUARED_STIMULUS_SIMILARITY(grid))/motor_variance, dim=0) + math.log(2 * math.pi / GRID)

  log_motor_likelihoods = (error/motor_variance) - log_normalizing_constant
  motor_likelihoods = torch.exp(log_motor_likelihoods)

  # Mixture of estimation and uniform response
  uniform_part = torch.sigmoid(parameters["mixture_logit"][subject])

  motor_likelihoods = (1-uniform_part) * motor_likelihoods + (uniform_part / (2*math.pi) + 0*motor_likelihoods)

  loss = 0
  # Now the loss is obtained by marginalizing out m from the motor likelihood
  if lossReduce == 'mean':
    loss += -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().mean()
  elif lossReduce == 'sum':
    loss += -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().sum()
  else:
    assert False

  if computePredictions:
     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS
     bayesianEstimate_avg_byStimulus = computeCircularMeanWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(bayesianEstimate_byStimulus, likelihoods)

     bayesianEstimate_avg_byStimulus = torch.where((bayesianEstimate_avg_byStimulus-grid).abs()<180, bayesianEstimate_avg_byStimulus, torch.where(bayesianEstimate_avg_byStimulus > 180, bayesianEstimate_avg_byStimulus-360, bayesianEstimate_avg_byStimulus+360))
     assert float(((bayesianEstimate_avg_byStimulus-grid).abs()).max()) < 180, float(((bayesianEstimate_avg_byStimulus-grid).abs()).max())

     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = computeCircularMeanWeighted(posteriorMaxima.unsqueeze(1), likelihoods)
     encodingBias = computeCircularMeanWeighted(grid.unsqueeze(1), likelihoods)
     attraction = (posteriorMaxima-encodingBias)
     attraction1 = attraction
     attraction2 = attraction+360
     attraction3 = attraction-360
     attraction = torch.where(attraction1.abs() < 180, attraction1, torch.where(attraction2.abs() < 180, attraction2, attraction3))
  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction

def retrieveObservations(x, Subject_, Contrast_, meanMethod = "circular"):
     assert Subject_ is None
     y_set = []
     sd_set = []
     for x in x_set:
        y_here = observations_y[(torch.logical_and((xValues == x), contrast==Contrast_))]

        Mean1 = computeCircularMean(y_here)
        Mean2 = computeCenteredMean(y_here, grid[x])
        if abs(Mean1-grid[x]) > 180 and Mean1 > 180:
            Mean1 = Mean1-360
        elif abs(Mean1-grid[x]) > 180 and Mean1 < 180:
            Mean1 = Mean1+360
        if Contrast_ > 1 and abs(Mean1-Mean2) > 180:
            print(Contrast_)
            print(y_here)
            print("Warning: circular and centered means are very different", Mean1, Mean2, grid[x], y_here)
        if meanMethod == "circular":
            Mean = Mean1
        elif meanMethod == "centered":
            Mean = Mean2
        else:
            assert False

        bias = Mean - grid[x]
        if abs(bias) > 180:
            bias = bias+360
        y_set.append(bias)
        sd_set.append(computeCircularSD(y_here))
     return y_set, sd_set

trigonometric_basis = torch.stack([SQUARED_STIMULUS_SIMILARITY(i*grid) for i in range(1, FOURIER_BASIS_SIZE+1)] + [SQUARED_STIMULUS_DIFFERENCE(i*grid) for i in range(1, FOURIER_BASIS_SIZE+1)], dim=0)
trigonometric_basis = trigonometric_basis * (GRID / (2*trigonometric_basis.pow(2).sum(dim=1, keepdim=True)))
fourierMultiplier = MakeFloatTensor(list(range(1,FOURIER_BASIS_SIZE+1)) + list(range(1,FOURIER_BASIS_SIZE+1)))

print(fourierMultiplier)
print(trigonometric_basis.pow(2).sum(dim=1))
if False:
  figure, axis = plt.subplots(2*FOURIER_BASIS_SIZE, 1, figsize=(50, 50))
  for i in range(trigonometric_basis.size()[0]):
     axis[i].plot(grid.cpu(), trigonometric_basis[i].cpu())
  savePlot(f"figures/{__file__}_BASIS_{GRID}_{FOURIER_BASIS_SIZE}.pdf")
  plt.close()

byPlot = {}

def plot(ax_, MASK, X, Y, alpha=1, color="gray", group=0, name="?", row=None):
    if group != 1:
      ax_.plot(X[MASK], Y[MASK], alpha=alpha, color=color)
    if (group,ax_,name,row) not in byPlot:
        byPlot[(group,ax_,name,row)] = []
    byPlot[(group,ax_,name,row)].append((X,Y,MASK))

viridis = cm.get_cmap('Dark2',10)
COLOR1 = viridis(1)
COLOR2 = viridis(3)
COLOR3 = viridis(2)
print(viridis(1))
print(viridis(2))
print(viridis(3))

def model(grid):
  lossesBy500 = []
  crossLossesBy500 = []
  noImprovement = 0
  global optim, learning_rate
  for iteration in range(1):
   parameters = init_parameters

   loss = 0

   if iteration % 500 == 0:
     plt.close()
     gridspec = dict(width_ratios=[1,0.2,1,0.2,1,1,1])
     figure, axis = plt.subplots(3, 7, figsize=(8, 4), gridspec_kw=gridspec)
     plt.tight_layout()
     figure.subplots_adjust(wspace=0.1, hspace=0.2)

     PRI = 2
     ENC = 0
     ATT = 4
     REP = 5
     TOT = 6
     PAD = [1,3]
     for q in range(3):
      for w in PAD:
       axis[q,w].set_visible(False)
     for q in range(3):
       for w in range(7):
         axis[q,w].spines['right'].set_visible(False)
         axis[q,w].spines['top'].set_visible(False)
     for q in range(2):
         for w in range(7):
             axis[q,w].tick_params(labelbottom=False)
     for q in range(3):
         for w in [REP, TOT]:
             axis[q,w].tick_params(labelleft=False)

     x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   volumeBySubjectRandomAdjustment =MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE) * (init_parameters["SIGMA_volumeBySubject"]+.1)
   priorBySubjectRandomAdjustment = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE) * (init_parameters["SIGMA_priorBySubject"]+.1)

   volumeBySubjectFourierTimesWeightInclMean = volumeBySubjectRandomAdjustment + init_parameters["volumeBySubject"]
   priorBySubjectFourierTimesWeightInclMean = priorBySubjectRandomAdjustment + init_parameters["priorBySubject"]

   volumeBySubjects = torch.matmul(volumeBySubjectFourierTimesWeightInclMean / fourierMultiplier.unsqueeze(0), trigonometric_basis)
   priorBySubjects = torch.matmul(priorBySubjectFourierTimesWeightInclMean / fourierMultiplier.unsqueeze(0), trigonometric_basis)

   for I_SUBJECT in range(3):
     MASK = (SQUARED_STIMULUS_SIMILARITY([97.5, 117.5, 137.5][I_SUBJECT]-grid) > .5)
     axis[I_SUBJECT,PRI].plot(grid.cpu()[MASK], 0.03 * torch.maximum(0.025 + MakeZeros(GRID), torch.maximum((0.42 - 0.025 * (grid- [90,112.5,135][I_SUBJECT] - 32).abs()), (0.42 - 0.025 * (grid-[90,112.5,135][I_SUBJECT]+32).abs()))).cpu()[MASK], color=[COLOR1,COLOR2,COLOR3][I_SUBJECT], alpha=1)

   for CONTRAST in [1,2,3,4]:
    if not torch.any(contrast == CONTRAST):
        continue
    for SUBJECT in range(N_SUBJECTS):
     volume = 2 * math.pi * torch.nn.functional.softmax(parameters["volume"] + volumeBySubjects[SUBJECT])
     prior_overall = torch.nn.functional.softmax(parameters["prior"] + priorBySubjects[SUBJECT])
     shift_by = GRID-int((grid-centralOrientationPerSubject[SUBJECT]).abs().argmin())
     prior = torch.cat([prior_overall[shift_by:], prior_overall[:shift_by]], dim=0)

     loss_2_4, bayesianEstimate_2_4, bayesianEstimate_sd_byStimulus_2_4, attraction = computeBias(xValues, init_parameters["sigma_logit"][SUBJECT,CONTRAST], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, computePredictions=(iteration%500 == 0), sigma_stimulus=0, sigma2_stimulus=0, contrast_=CONTRAST, folds=trainFolds, lossReduce='sum', subject=SUBJECT)
     loss += loss_2_4

     if iteration % 500 == 0:
       I_SUBJECT = subjectsOrdering.index(SUBJECT)
       if abs(MakeFloatTensor([centralOrientationPerSubject[SUBJECT]]) - 135) < 15:
           I_SUBJECT=2
       elif abs(MakeFloatTensor([centralOrientationPerSubject[SUBJECT]]) - 135) < 30:
           I_SUBJECT=1
       elif abs(MakeFloatTensor([centralOrientationPerSubject[SUBJECT]]) - 135) < 46:
           I_SUBJECT=0
       else:
           assert False

       if I_SUBJECT != 0:
           continue
       y_here = observations_y[torch.logical_and(Subject == SUBJECT, contrast==CONTRAST)]
       x_here = observations_x[torch.logical_and(Subject == SUBJECT, contrast==CONTRAST)]

       grid_transformed = grid - centralOrientationPerSubject[SUBJECT] + [90,112.5,135][I_SUBJECT]

       if centralOrientationPerSubject[SUBJECT] > 135:
           REVERSE=True
       else:
           REVERSE=False
       if REVERSE:
           grid_transformed = [180,225,270][I_SUBJECT]-grid_transformed
           attraction = -attraction
           bayesianEstimate_2_4 = -bayesianEstimate_2_4+2*grid
           grid_reflected = 270-grid
           centralOrientation_reflected = 270-centralOrientationPerSubject[SUBJECT]
       else:
           grid_reflected = grid
           centralOrientation_reflected = centralOrientationPerSubject[SUBJECT]

       MASK = (SQUARED_STIMULUS_SIMILARITY(centralOrientationPerSubject[SUBJECT]-grid) > .5)

       plot(axis[I_SUBJECT,ENC], MASK, grid_reflected.cpu(), volume.detach().cpu(), alpha=.2, row=I_SUBJECT)
       axis[I_SUBJECT,ENC].scatter([centralOrientation_reflected], [0], s=5, color=[COLOR1,COLOR2,COLOR3][I_SUBJECT])

       OneMinusGuessingRate = float(1-torch.sigmoid(parameters["mixture_logit"][SUBJECT]))

       plot(axis[I_SUBJECT,PRI], MASK, grid_transformed.cpu(), prior.detach().cpu(), alpha=.2, color="gray", row=I_SUBJECT)
       axis[I_SUBJECT,PRI].set_ylim(0, 0.013)
       axis[I_SUBJECT,PRI].set_yticks(ticks=[0])
       plot(axis[I_SUBJECT,ATT], MASK, grid_transformed.cpu(), OneMinusGuessingRate*(attraction).detach().cpu(), alpha=.2, color="gray", row=I_SUBJECT)
       plot(axis[I_SUBJECT,REP], MASK, grid_transformed.cpu(), OneMinusGuessingRate*(bayesianEstimate_2_4-grid-attraction).detach().cpu(), alpha=.2, color="gray", row=I_SUBJECT)

       kappa = 29
       kernel = torch.exp(kappa*SQUARED_STIMULUS_SIMILARITY(x_here.view(-1,1)-grid.view(1,-1))) / (2*math.pi*np.i0(kappa))
       kernel = kernel / kernel.sum(dim=0).max()

       bias = y_here - x_here
       if REVERSE:
           bias = -bias
       bias = torch.where(bias > 180, bias-360, torch.where(bias < -180, bias+360, bias))
       y_smoothed = computeCircularMeanWeighted(bias.unsqueeze(1), kernel)
       y_smoothed = torch.where(y_smoothed > 180, y_smoothed-360, torch.where(y_smoothed < -180, y_smoothed+360, y_smoothed))

       plot(axis[I_SUBJECT][TOT], MASK, grid_transformed.cpu(), y_smoothed.cpu(), alpha=.2, color="gray", group=1, name="human", row=I_SUBJECT)
       plot(axis[I_SUBJECT,TOT], MASK, grid_transformed.cpu(), OneMinusGuessingRate*(bayesianEstimate_2_4-grid).detach().cpu(), alpha=.0, color="gray", row=I_SUBJECT)

       for w in range(7):
         axis[I_SUBJECT,w].set_xticks(ticks=[0,90,180], labels=["0°", "90°", "180°"])

       for w in [ATT,REP,TOT]:
         axis[I_SUBJECT,w].set_ylim(-8, 8)
         axis[I_SUBJECT,w].set_yticks(ticks=[-5,0,5], labels=["-5°","0°","5°"])

       for w in range(7):

         axis[I_SUBJECT,w].set_xlim(0,200)

   biasesAveraged = []
   if iteration % 500 == 0:

     axis[0,ATT].arrow(65, -2, 0, -4, width=2, color="black", head_length=1)
     axis[0,ATT].arrow(115, 2, 0, 4, width=2, color="black", head_length=1)
     axis[1,ATT].arrow(22.5+65, -2, 0, -4, width=2, color="black", head_length=1)
     axis[1,ATT].arrow(22.5+115, 2, 0, 4, width=2, color="black", head_length=1)
     axis[2,ATT].arrow(45+65, -2, 0, -4, width=2, color="black", head_length=1)
     axis[2,ATT].arrow(45+115, 2, 0, 4, width=2, color="black", head_length=1)

     for group, ax_, name, row in byPlot:
           Xs, Ys, MASKs = zip(*byPlot[(group, ax_, name, row)])
           overallX = torch.stack(Xs, dim=0)
           overallY = torch.stack(Ys, dim=0)

           MASK = torch.stack(MASKs, dim=0).any(dim=0)
           minX = 360
           maxX = 0
           for x, m in zip(Xs, MASKs):
               x_ = x[m]
               minX = min(minX, float(x_.min()))
               maxX = max(maxX, float(x_.max()))
           kernel = ((grid.view(-1, 1, 1)-overallX.unsqueeze(0)).abs() < 2).float()

           kernel = kernel / kernel.sum(dim=2).sum(dim=1)
           kernel = torch.where(torch.isnan(kernel), MakeZeros(kernel.size()), kernel)
           if torch.isnan(kernel).any():
               quit()
           smoothed = (overallY.unsqueeze(0) * kernel).sum(dim=2).sum(dim=1)
           MASK = torch.logical_and(grid >= minX, grid<=maxX)

           if group == 1:
               kernel_per_subject = kernel / kernel.sum(dim=2, keepdim=True)
               smoothed_per_subject = (overallY.unsqueeze(0) * kernel_per_subject).sum(dim=2)

               sd = (smoothed_per_subject.pow(2).mean(dim=1) - smoothed_per_subject.mean(dim=1).pow(2)).sqrt() / math.sqrt(36)
               CI_x = grid.detach()[MASK].numpy().tolist()
               CI_y1 = (smoothed_per_subject.mean(dim=1)-sd).detach()[MASK].numpy().tolist()
               CI_y2 = (smoothed_per_subject.mean(dim=1)+sd).detach()[MASK].numpy().tolist()
               poly = plt.Polygon(list(zip(CI_x + CI_x[::-1], CI_y1 + CI_y2[::-1])), facecolor=(.65, .65, .65))
               ax_.add_patch(poly)

               ax_.plot(grid.detach()[MASK], (smoothed_per_subject.mean(dim=1)).detach()[MASK], color=["blue", "red", "green"][group], linewidth=2)
               human = (smoothed_per_subject.mean(dim=1)).detach()
           else:
              ax_.plot(grid.detach()[MASK], smoothed.detach()[MASK], color=["blue", "red", "green"][group], linewidth=2)
              biasesAveraged.append(smoothed.detach())

     axis[0,ENC].set_title("Resources")
     axis[0,PRI].set_title("Prior")
     axis[0,TOT].set_title("Total")

     axis[0,ATT].set_title("Attraction")
     axis[0,REP].set_title("Repulsion")

     print("Saving plot...")
     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.pdf")

   if iteration % 10 == 0:

     from matplotlib import cm
     from matplotlib.colors import ListedColormap, LinearSegmentedColormap
     viridis = cm.get_cmap('viridis', 8)

     figure, axis = plt.subplots(1, 1, figsize=(2,2))
     figure.subplots_adjust(left=0.22, bottom=0.22)
     axis.violinplot((attraction)[MASK].detach().abs(), positions=[0], showextrema=False)
     axis.violinplot((bayesianEstimate_2_4-grid-attraction)[MASK].detach().abs(), positions=[1], showextrema=False)
     plt.xticks([])
     axis.spines['top'].set_visible(False)
     axis.spines['right'].set_visible(False)
     axis.set_yticks(ticks=[0,2.5,5,7.5], labels=[0, "", "5", ""], fontsize=15)

     axis.tick_params(axis='y', labelsize=15, width=0.4)

     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_Magnitudes.pdf")
     figure, axis = plt.subplots(1, 1, figsize=(2,2))
     figure.subplots_adjust(left=0.22, bottom=0.22)
     def createWeightedScatter(ax, x, y, c):
        min_ = y.min()
        max_ = y.max()
        ran = max_-min_
        K = 10
        bins = min_ + torch.arange(0,K+1) / K * ran
        per_bin = ((y.unsqueeze(0) - bins.unsqueeze(1)).abs() < .5*ran/K).float().sum(dim=1)
        bin_for = (y.unsqueeze(0) - bins.unsqueeze(1)).abs().min(dim=0).indices
        bins = [[] for _ in range(K+1)]
        for i in range(len(bin_for)):
          bins[bin_for[i]].append(y[i].item())
        xs = []
        ys = []
        for g in bins:
          random.shuffle(g)
          for q in range(0, len(g)):
             xs.append(x+.4 / per_bin.max() * (q - (len(g)-1)/2))
             ys.append(g[q])
        print("X", xs)

        ax.scatter(xs, ys, s=0.8, alpha=0.5, c=[c])

     createWeightedScatter(axis, 0, (attraction)[MASK].detach().abs(), c=(30/255, 117/255, 179/255, 76/255))
     createWeightedScatter(axis, 1, (bayesianEstimate_2_4-grid-attraction)[MASK].detach().abs(), c=(255/255, 125/255, 12/255, 76/255))
     axis.violinplot((attraction)[MASK].detach().abs(), positions=[0], showextrema=False)
     axis.violinplot((bayesianEstimate_2_4-grid-attraction)[MASK].detach().abs(), positions=[1], showextrema=False)
     plt.xticks([])
     axis.set_xlim(-0.5, 1.5)
     axis.spines['top'].set_visible(False)
     axis.spines['right'].set_visible(False)
     axis.set_yticks(ticks=[0,2.5,5,7.5], labels=[0, "", "5", ""], fontsize=15)

     axis.tick_params(axis='y', labelsize=15, width=0.4)

     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_Magnitudes_Scatter.pdf")

     figure, axis = plt.subplots(1, 1, figsize=(2,2))
     figure.subplots_adjust(left=0.22, bottom=0.22)
     ratio = (prior / volume.pow((P+2)/2)).detach()
     kernel = torch.softmax(-(((grid.unsqueeze(0) - grid.unsqueeze(1))/MAX_GRID)).pow(2)/0.001, dim=0)
     ratio = torch.matmul(ratio.unsqueeze(0), kernel).view(-1)
     prior_smoothed = torch.matmul(prior.unsqueeze(0), kernel).view(-1)
     volume_smoothed = torch.matmul(volume.unsqueeze(0), kernel).view(-1)
     axis.plot([MIN_GRID, MAX_GRID/2], [8,8], color="gray", linestyle='dotted')
     axis.plot([MIN_GRID, MAX_GRID/2], [0,0], color="gray", linestyle='dotted')
     axis.plot([MIN_GRID, MAX_GRID/2], [-8,-8], color="gray", linestyle='dotted')
     axis.plot([MIN_GRID, MAX_GRID/2], [-16,-16], color="gray", linestyle='dotted')
     plt.yticks([])
     axis.set_xticks(ticks=[45, 90, 135], labels=["45", "90", "135"], fontsize=15)
     axis.set_yticks(ticks=[8, 0, -8, -16], labels=["", "", "", ""])

     axis.spines['top'].set_visible(False)
     axis.spines['right'].set_visible(False)

     for w in range(2,GRID):
        y1 = float(ratio[w-1]-ratio[w-2])
        y2 = float(ratio[w]-ratio[w-1])

        repulsive_part = float(biasesAveraged[3][w].cpu().detach())
        attractive_part = float(biasesAveraged[2][w].cpu().detach())

        human_part = float((human)[w].cpu().detach())

        hasAttraction = False
        repulsion = False
        if sign(float(prior_smoothed[w]-prior_smoothed[w-1])) == sign(y2):
            hasAttraction=True
        if sign(float(volume_smoothed[w]-volume_smoothed[w-1])) == -sign(y2):
            repulsion=True
        if hasAttraction and repulsion:
            c = "gray"
        elif hasAttraction:
            c = "green"
        elif repulsion:
            c = "blue"
        else:
            c = "black"

        axis.plot([float(grid[w-1]), float(grid[w])], [0+sign(y2),0+sign(y2)], color=["green", "red", "gray"][{-1 : 0, 0 : 2, 1 : 1}[sign(y2)]])

        y2 = attractive_part
        axis.plot([float(grid[w-1]), float(grid[w])], [-8+sign(y2),-8+sign(y2)], color=["green", "red", "gray"][{-1 : 0, 0 : 2, 1 : 1}[sign(y2)]])

        y2 = repulsive_part
        axis.plot([float(grid[w-1]), float(grid[w])], [-16+sign(y2),-16+sign(y2)], color=["green", "red", "gray"][{-1 : 0, 0 : 2, 1 : 1}[sign(y2)]])

        y2 = human_part
        axis.plot([float(grid[w-1]), float(grid[w])], [8+sign(y2),8+sign(y2)], color=["green", "red", "gray"][{-1 : 0, 0 : 2, 1 : 1}[sign(y2)]])

     axis.set_xlim(45, 135)

     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_PPRatio.pdf")

############################3

# Project the stimuli onto the discrete grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

model(grid)
