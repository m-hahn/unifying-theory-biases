import glob
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sys
import torch
from matplotlib import rc
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import ToDevice
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import makeGridIndicesCircular
from util import product
from util import savePlot

rc('font', **{'family':'FreeSans'})
__file__ = __file__.split("/")[-1]

#############################################################
# Part: Collect arguments
OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
assert P == 0
FOLD_HERE = int(sys.argv[2])
REG_WEIGHT = float(sys.argv[3])
assert REG_WEIGHT == 1.0
GRID = int(sys.argv[4])
assert GRID % 180 == 0

FOURIER_BASIS_SIZE = int(sys.argv[5])
assert FOURIER_BASIS_SIZE in [30, 50, 80]

FILE = f"logs/CROSSVALID/{__file__.replace('_EVAL','')}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_{FOURIER_BASIS_SIZE}.txt"

files = sorted(glob.glob("data/Gekas/Exp1/session1/*mat") + glob.glob("data/Gekas/Exp1/session2/*mat"))
files = files + sorted(glob.glob("data/Gekas/Exp2/session1/*mat") + glob.glob("data/Gekas/Exp2/session2/*mat"))

target = []
response = []

sample_total = []
response_total = []
contrast_total = []
subjects_total = []
condition_total = []
centralOrientationPerSubject = {}
for f in files:
 annots = loadmat(f)
 orientation = annots["central_orientation"][0][0]

 transformed_orientation = orientation % 90 + 90

 transformation = transformed_orientation - orientation
 orientation = transformed_orientation
 print(f, orientation)
 data = annots["data"][200:]

 sample = MakeFloatTensor(data[:,0])
 sample = (sample + transformation) % 360

 contrast = MakeLongTensor(data[:,1])

 response = MakeFloatTensor(data[:,4])
 response = (response + transformation) % 360

 color = MakeLongTensor(data[:,9])

 samplesRed = sample[color == 1]
 samplesGreen = sample[color == 2]

 if computeCircularSDWeighted(samplesGreen) < computeCircularSDWeighted(samplesRed):
     bimodalCondition = 2
     condition = color

 else:
     bimodalCondition = 1
     condition = torch.where(color==0, 0, 3-color)

 contrast = torch.where(contrast == 1, 1, torch.where(contrast == 4, 4, condition+1))

 print(f)
 subjectHere = int(f[f.index("sub")+3:f.rfind("ses")])-1
 if "Exp2" in f:
    subjectHere = subjectHere+18
 centralOrientationPerSubject[subjectHere] = orientation
 sample_total.append(sample)
 subjects_total.append(subjectHere + MakeZeros(sample.size()[0]))
 response_total.append(response)
 contrast_total.append(contrast)
 condition_total.append(condition)
 print(subjectHere)

sample = torch.cat(sample_total, dim=0)
responses = torch.cat(response_total, dim=0)
contrast = torch.cat(contrast_total, dim=0)
Subject = torch.cat(subjects_total, dim=0)
condition = torch.cat(condition_total, dim=0)

mask = (condition == 2)
sample = sample[mask]
responses = responses[mask]
condition = condition[mask]
contrast = contrast[mask]
Subject = Subject[mask]

# Store observations
observations_x = sample
observations_y = responses

#############################################################
# Part: Partition data into folds. As described in the paper,
# this is done within each subject.
N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

Subject_cpu = Subject.cpu()
Fold = 0*Subject_cpu
for i in range(int(min(Subject_cpu)), int(max(Subject_cpu))+1):
    print("Making folds for subject", i)
    trials = [j for j in range(Subject_cpu.size()[0]) if Subject_cpu[j] == i]
    randomGenerator.shuffle(trials)
    foldSize = int(len(trials)/N_FOLDS)
    for k in range(N_FOLDS):
        Fold[trials[k*foldSize:(k+1)*foldSize]] = k
Fold = ToDevice(Fold)

##############################################
# Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 360
RANGE_GRID = MAX_GRID-MIN_GRID

CIRCULAR = True
INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)
assert CIRCULAR

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
grid, grid_indices_here = makeGridIndicesCircular(GRID, MIN_GRID, MAX_GRID)
assert grid_indices_here.max() >= GRID, grid_indices_here.max()

# Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))

xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

N_SUBJECTS = int(max(Subject))+1
subjectsOrdering = sorted(range(N_SUBJECTS), key=lambda x:centralOrientationPerSubject[x])

#############################################################
# Part: Initialize the model
init_parameters = {}
init_parameters["sigma2_stimulus"] = MakeFloatTensor([0]).view(1)
init_parameters["log_motor_var"] = MakeFloatTensor(N_SUBJECTS*[0])
init_parameters["sigma_logit"] = -3 + MakeZeros(N_SUBJECTS, 6)
init_parameters["mixture_logit"] = MakeFloatTensor(N_SUBJECTS*[-1])
init_parameters["prior"] = MakeZeros(GRID)
init_parameters["volume"] = MakeZeros(GRID)
init_parameters["priorBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["prior_logits_effect_weight"] = MakeZeros(1)
init_parameters["volumeBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["volume_logits_effect_weight"] = MakeZeros(1)

init_parameters["SIGMA_priorBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["SIGMA_volumeBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)

with open(FILE, "r") as inFile:
    (next(inFile))
    (next(inFile))
    for l in inFile:
        if l.startswith("==="):
            break
        z, y = l.split("\t")
        try:
          assert init_parameters[z.strip()].size() == MakeFloatTensor(json.loads(y)).size()
        except json.decoder.JSONDecodeError:
          print(y)
          assert False, FILE
        init_parameters[z.strip()] = MakeFloatTensor(json.loads(y))
assert "volume" in init_parameters
for _, y in init_parameters.items():
    y.requires_grad = True

# Initialize optimizer.
# The learning rate is a user-specified parameter.
learning_rate=.1
optim = torch.optim.SGD([y for _, y in init_parameters.items()], lr=learning_rate)

##############################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 2*math.pi

def SQUARED_STIMULUS_DIFFERENCE(x):
    return torch.sin(math.pi*x/180)
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(math.pi*x/180)
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(x)
def SQUARED_SENSORY_DIFFERENCE(x):
    return torch.sin(x)

assert P == 0

# Import the appropriate estimator for minimizing the loss function
SCALE = 50

KERNEL_WIDTH = 0.05

averageNumberOfNewtonSteps = 2

class MAPCircularEstimator(torch.autograd.Function):
    """
    We can implement our own custom autograd Functions by subclassing
    torch.autograd.Function and implementing the forward and backward passes
    which operate on Tensors.
    """

    # For this MAP estimator implementation, there are checks in ExampleGardelle_Inference_Discretized_Likelihood_Nonparam_MotorNoise1_Lp_Scale_NewStrict_NaturalPrior_ZeroExponent4_CROSSVALI_DebugD6_Smoothed2_UnitTest_GD_Check_Unnormalized_Hess_FineGrid_Fix2.py
    @staticmethod
    def forward(ctx, grid_indices_here, posterior):
        """
        In the forward pass we receive a Tensor containing the input and return
        a Tensor containing the output. ctx is a context object that can be used
        to stash information for backward computation. You can cache arbitrary
        objects for use in the backward pass using the ctx.save_for_backward method.
        """
        global averageNumberOfNewtonSteps
        # Step 1: To avoid over-/underflow, rescale the indices
        grid_indices_here = grid_indices_here/SCALE
        n_inputs, n_batch = posterior.size()

        # Step 1: Identify the argmax on the full discretized grid
        kernelFunctionForAll = torch.nn.functional.softmax(-(grid_indices_here.view(GRID, 1, GRID) - grid_indices_here.view(1,GRID,GRID)).pow(2) / (2*(KERNEL_WIDTH**2)), dim=1)
        smoothedPosteriorForAll = (kernelFunctionForAll * posterior.view(1,GRID,GRID)).sum(dim=1)
        resultDiscrete = smoothedPosteriorForAll.argmax(dim=0)

        resultDiscrete = torch.stack([grid_indices_here[resultDiscrete[i],i] for i in range(GRID)], dim =0)
        resultDiscrete1 = resultDiscrete
        assert resultDiscrete1.max() < 720/SCALE
        resultDiscreteFromRaw = resultDiscrete

        # Step 2: Improve estimator by optimizing one finer grid centered around the coarse maximum.
        # A small trust region leads to a finer grid and a better starting point of Newton's.
        # But sometimes the optimum will be found on the boundary of the trust region. In this case, we increase the size of the trust region by a factor of four and try again. NOTE: This behavior only makes sense when the optimum is not at the boundary of the overall stimulus space.
        # One issue that can make the small trust region very important is when there are abrupt spikes in the discretized poosterior.
        Q = 20
        FINE_GRID_SCALE = 2
        for J in range(1):
           environment = (ToDevice(torch.arange(start=-Q, end=Q+1))/(FINE_GRID_SCALE*SCALE)).view(-1, 1) + resultDiscrete.view(1, -1)
           K = environment.size()[0]
           kernelFunction = torch.nn.functional.softmax(-(environment.view(K, 1, GRID) - grid_indices_here.view(1,GRID,GRID)).pow(2) / (2*(KERNEL_WIDTH**2)), dim=1)
           smoothedPosterior = (kernelFunction * posterior.view(1,GRID,GRID)).sum(dim=1)
           smoothedPosteriorInEnvironment = smoothedPosterior.detach()

           argmaxInFineGrid = smoothedPosterior.argmax(dim=0)
           onBoundary = torch.logical_or(argmaxInFineGrid == 0, argmaxInFineGrid== K-1)
           if onBoundary.float().sum() > 0:
               print("Warning: Some points are on the boundary of the trust region in iteration ",J, "Retrying with a larger region" if J == 0 else "Accepting nonetheless", "# of points:", onBoundary.float().sum())
               FINE_GRID_SCALE = .5
           else:
               break
        resultFromIntermediateGrid = ((argmaxInFineGrid-K/2)/(FINE_GRID_SCALE*SCALE) + resultDiscrete)

        PLOT = (random.random() < .1) and False
        if PLOT:
           # Plot the smoothed posterior
           figure, axis = plt.subplots(1, 2)
           axis[0].scatter(SCALE*environment[:,0], smoothedPosterior[:,0])
           axis[1].scatter(SCALE*environment[:,80], smoothedPosterior[:,80])
           Z = max(float(smoothedPosterior[:,0].max()), float(smoothedPosterior[:,80].max()))
           Y = min(float(smoothedPosterior[:,0].min()), float(smoothedPosterior[:,80].min()))

        if PLOT:
           axis[0].plot([SCALE*resultDiscrete[0], SCALE*resultDiscrete[0]], [Y,Z], color="orange")
           axis[0].plot([SCALE*resultFromIntermediateGrid[0], SCALE*resultFromIntermediateGrid[0]], [Y,Z], color="green")
           axis[1].plot([SCALE*resultDiscrete[80], SCALE*resultDiscrete[80]], [Y,Z], color="orange")
           axis[1].plot([SCALE*resultFromIntermediateGrid[80], SCALE*resultFromIntermediateGrid[80]], [Y,Z], color="green")

        # Now use the result from the intermediate grid
        resultDiscrete = resultFromIntermediateGrid
        resultDiscrete = resultDiscrete.clamp(min=MIN_GRID/SCALE, max=MAX_GRID/SCALE)
        assert resultDiscrete.max() < 720/SCALE

        optimizationSequence = []
        if True:
          for i in range(40):
             # First compute P'(theta)
             innerDerivatives = -(resultDiscrete.view(1, GRID) - grid_indices_here) / (KERNEL_WIDTH**2)
             kernelFunction = torch.nn.functional.softmax(-(resultDiscrete.view(1, GRID) - grid_indices_here).pow(2) / (2*(KERNEL_WIDTH**2)), dim=0)
             if OPTIMIZER_VERBOSE:
               smoothedPosterior = (kernelFunction * posterior).sum(dim=0)

             kernelFunctionDerivativeI = innerDerivatives * kernelFunction
             # This sum is needed repeatedly
             kernelFunctionDerivativeISum = kernelFunctionDerivativeI.sum(dim=0, keepdim=True)

             kernelDerivative = kernelFunctionDerivativeI - kernelFunction * kernelFunctionDerivativeISum
             derivativeSmoothedPosterior = (kernelDerivative * posterior).sum(dim=0)

             # First derivative has been verified against Pytorch autograd 

             if True:
               smoothedPosterior = (kernelFunction * posterior).sum(dim=0)
             # Now compute P''(theta)
             kernelFunctionSecondDerivativeI = -1/(KERNEL_WIDTH**2) * kernelFunction + innerDerivatives.pow(2) * kernelFunction
             part1 = kernelFunctionSecondDerivativeI - kernelFunctionDerivativeI * kernelFunctionDerivativeISum
             kernelSecondDerivative = part1 - kernelDerivative * kernelFunctionDerivativeISum - kernelFunction * part1.sum(dim=0, keepdim=True)
             secondDerivativeSmoothedPosterior = (kernelSecondDerivative * posterior).sum(dim=0)
             # Second derivative has been verified against finite differences of the first derivatives
             # These calculations are all bottlenecks. Could perhaps reduce some of the inefficiency.

             hessian = secondDerivativeSmoothedPosterior
             if OPTIMIZER_VERBOSE and i % 10 == 0 or False:
                print(i, smoothedPosterior.mean(), derivativeSmoothedPosterior.abs().mean(), derivativeSmoothedPosterior.abs().max(), "\t", resultDiscrete[10:15]*SCALE)

             # Optimization step
             # For those batch elements where P'' < 0, we do a Newton step.
             # For the others, we do a GD step, with stepsize based 1/P'', cutting off excessively large resulting stepsizes
             # For a well-behaved problem (concave within the trust region and attains its maximum on it), only Newton steps should be required. GD steps are intended as a fallback when this is not satisfied.
             MASK = hessian>=-0.001
             updateHessian =  - derivativeSmoothedPosterior / hessian.clamp(max=-0.001)
             updateGD = .2 * derivativeSmoothedPosterior
             updateGD = derivativeSmoothedPosterior / hessian.abs().clamp(min=0.01)
             # Prevent excessive jumps, and decay the allowed learning rate.
             MAXIMAL_UPDATE_SIZE = 0.1 / (1+i/10)
             update = torch.where(MASK, updateGD, updateHessian).clamp(min=-MAXIMAL_UPDATE_SIZE, max=MAXIMAL_UPDATE_SIZE)
             if random.random() < 0.002:
               print("Maximal update", update.abs().max(), update.abs().median(), (update.abs() >= 0.1).float().sum(), "Doing Non-Newton", MASK.float().sum(), hessian.abs().median())
             resultDiscrete = resultDiscrete + update
             assert resultDiscrete.max() < 720/SCALE
             optimizationSequence.append((resultDiscrete, smoothedPosterior, derivativeSmoothedPosterior, secondDerivativeSmoothedPosterior))
             if PLOT:
                axis[0].plot([SCALE*resultDiscrete[0], SCALE*resultDiscrete[0]], [Y,Z], color="red")
                axis[1].plot([SCALE*resultDiscrete[80], SCALE*resultDiscrete[80]], [Y,Z], color="red")
             if float(derivativeSmoothedPosterior.abs().max()) < 1e-5:
                 break

             # Check whether solution has left trust region
             lowerThanTrustRegion = (environment[0] > resultDiscrete)
             higherThanTrustRegion = (environment[-1] < resultDiscrete)
             if lowerThanTrustRegion.float().sum() + higherThanTrustRegion.float().sum() > 0 and random.random() < 0.005:
                 print("Warning: some batches have left the trust region.", lowerThanTrustRegion.float().sum(), higherThanTrustRegion.float().sum())
        else:
            assert False
        averageNumberOfNewtonSteps = 0.98 * averageNumberOfNewtonSteps + (1-0.98) * i
        if random.random() < 0.003:
           print("Number of Newton iterations", i, "average", averageNumberOfNewtonSteps)
        if i > 20:
            print("Warning: Finding MAP estimator took", i, "iterations. Maximal gradient", float(derivativeSmoothedPosterior.abs().max()))

        if float(derivativeSmoothedPosterior.abs().max()) > 1e-4:
            print("Warning: Finding MAP estimator took", i, "iterations. Maximal gradient", float(derivativeSmoothedPosterior.abs().max()))
            worst = derivativeSmoothedPosterior.abs().argmax()

            print(sorted(derivativeSmoothedPosterior.detach().cpu().numpy().tolist()))
            print(derivativeSmoothedPosterior)
            print("PROBLEM", worst, derivativeSmoothedPosterior[worst], float(derivativeSmoothedPosterior.abs().max()), derivativeSmoothedPosterior[GRID-1])
            plt.close()
            figure, axis = plt.subplots(1, 2, figsize=(15,15))
            axis[0].scatter(SCALE*grid_indices_here[:,0].detach().cpu(), posterior[:,0].detach().cpu(), color="gray")

            kernelFunction = torch.nn.functional.softmax(-(grid_indices_here.view(GRID, 1, GRID) - grid_indices_here.view(1,GRID,GRID)).pow(2) / (2*(KERNEL_WIDTH**2)), dim=1)
            smoothedPosteriorOverAll = (kernelFunction * posterior.view(1,GRID,GRID)).sum(dim=1)

            KernelTimesMinusOnePlusKernel = kernelFunction * (kernelFunction-1)
            kernelFunctionDerivative = 2 * (grid_indices_here.view(GRID, 1, GRID) - grid_indices_here.view(1,GRID,GRID)) / (2*(KERNEL_WIDTH**2)) * KernelTimesMinusOnePlusKernel
            kernelFunctionSecondDerivative = 1 / (KERNEL_WIDTH**2) * KernelTimesMinusOnePlusKernel + ((grid_indices_here.view(GRID, 1, GRID) - grid_indices_here.view(1,GRID,GRID)) / (KERNEL_WIDTH**2)) * (kernelFunctionDerivative * (kernelFunction-1) + kernelFunction*kernelFunctionDerivative)
            hessianOverAll = (kernelFunctionSecondDerivative * posterior).sum(dim=1)
            derivativeSmoothedPosteriorOverAll = (kernelFunctionDerivative * posterior).sum(dim=1)

            axis[0].scatter(SCALE*grid_indices_here[:,0].detach().cpu(), smoothedPosteriorOverAll[:,0].detach().cpu(), color="yellow")
            axis[0].scatter(SCALE*grid_indices_here[:,0].detach().cpu(), derivativeSmoothedPosteriorOverAll[:,0].detach().cpu(), color="purple")

            axis[0].scatter(SCALE*environment[:,0].detach().cpu(), smoothedPosteriorInEnvironment[:,0].detach().cpu())
            axis[1].scatter(SCALE*grid_indices_here[:,worst].detach().cpu(), posterior[:,worst].detach().cpu(), color="gray")
            axis[1].scatter(SCALE*grid_indices_here[:,worst].detach().cpu(), smoothedPosteriorOverAll[:,worst].detach().cpu(), color="yellow")
            axis[1].scatter(SCALE*grid_indices_here[:,worst].detach().cpu(), derivativeSmoothedPosteriorOverAll[:,worst].detach().cpu(), color="purple")
            axis[1].scatter(SCALE*environment[:,worst].detach().cpu(), smoothedPosteriorInEnvironment[:,worst].detach().cpu())
            Z = max(float(smoothedPosteriorInEnvironment[:,0].max()), float(smoothedPosteriorInEnvironment[:,worst].max()))
            Y = min(float(smoothedPosteriorInEnvironment[:,0].min()), float(smoothedPosteriorInEnvironment[:,worst].min()))
            for x, y, z, u in optimizationSequence:
               print(x[0], x[worst], "P", y[worst], "dP", z[worst], "d2P", u[worst], derivativeSmoothedPosterior[worst], resultDiscrete[worst])
               axis[0].plot([SCALE*x[0].detach().cpu(), SCALE*x[0].detach().cpu()], [Y,Z], color="yellow")
               axis[1].plot([SCALE*x[worst].detach().cpu(), SCALE*x[worst].detach().cpu()], [Y,Z], color="yellow")
            axis[0].plot([SCALE*resultDiscrete1[0].detach().cpu(), SCALE*resultDiscrete1[0].detach().cpu()], [Y,Z], color="orange")
            axis[1].plot([SCALE*resultDiscrete1[worst].detach().cpu(), SCALE*resultDiscrete1[worst].detach().cpu()], [Y,Z], color="orange")
            axis[0].plot([SCALE*resultDiscrete[0].detach().cpu(), SCALE*resultDiscrete[0].detach().cpu()], [Y,Z], color="red")
            axis[1].plot([SCALE*resultDiscrete[worst].detach().cpu(), SCALE*resultDiscrete[worst].detach().cpu()], [Y,Z], color="red")

            savePlot(f"figures/DEBUG_{__file__}.pdf")
            plt.close()
            assert False

        if PLOT:
           plt.show()
           plt.close()

        result = resultDiscrete

        ctx.save_for_backward(grid_indices_here, posterior, result)
        return result.detach()*SCALE

    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor containing the gradient of the loss
        with respect to the output, and we need to compute the gradient of the loss
        with respect to the input.
        """
        grid_indices_here, posterior, result = ctx.saved_tensors

        #################
        # g'_j and g''_j are computed as in during the Newton iterations of the forward pass
        innerDerivatives = -(result.view(1, GRID) - grid_indices_here) / (KERNEL_WIDTH**2)
        kernelFunction = torch.nn.functional.softmax(-(result.view(1, GRID) - grid_indices_here).pow(2) / (2*(KERNEL_WIDTH**2)), dim=0)
        kernelFunctionDerivativeI = innerDerivatives * kernelFunction
        # This sum is needed repeatedly
        kernelFunctionDerivativeISum = kernelFunctionDerivativeI.sum(dim=0, keepdim=True)
        kernelDerivative = kernelFunctionDerivativeI - kernelFunction * kernelFunctionDerivativeISum
        derivativeSmoothedPosterior = (kernelDerivative * posterior).sum(dim=0)

        kernelFunctionSecondDerivativeI = -1/(KERNEL_WIDTH**2) * kernelFunction + innerDerivatives.pow(2) * kernelFunction
        part1 = kernelFunctionSecondDerivativeI - kernelFunctionDerivativeI * kernelFunctionDerivativeISum
        kernelSecondDerivative = part1 - kernelDerivative * kernelFunctionDerivativeISum - kernelFunction * part1.sum(dim=0, keepdim=True)
        secondDerivativeSmoothedPosterior = (kernelSecondDerivative * posterior).sum(dim=0)
        hessian = secondDerivativeSmoothedPosterior
        #################

        # Now using implicit differentiation
        gradient_implicit = - kernelDerivative / hessian
        gradient = grad_output.unsqueeze(0) * gradient_implicit

        return None, gradient*SCALE

# Run the model
def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, computePredictions=False, sigma_stimulus=None, sigma2_stimulus=None, contrast_=None, folds=None, lossReduce='mean', subject=None):

 # Part: Obtain the motor variance by exponentiating the appropriate model parameter
 motor_variance = torch.exp(- parameters["log_motor_var"][subject])
 # Part: Obtain the sensory noise variance. We parameterize it as a fraction of the squared volume of the size of the sensory space
 sigma2 = 2*torch.sigmoid(sigma_logit)
 # Part: Obtain the transfer function as the cumulative sum of the discretized resource allocation (referred to as `volume` element due to the geometric interpretation by Wei&Stocker 2015)
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 if True:
  folds = MakeLongTensor(folds)
  if subject is not None:
    MASK = torch.logical_and(contrast==contrast_, torch.logical_and((Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0), Subject==subject))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]
  else:
    MASK = torch.logical_and(contrast==contrast_, (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
    stimulus = stimulus_[MASK]
    responses = responses_[MASK]
  assert stimulus.view(-1).size()[0] > 0, (contrast_)

  if sigma2_stimulus > 0:
    assert False
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))
    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  # Part: Compute sensory likelihoods. Across both interval and
  ## circular stimulus spaces, this amounts to exponentiaring a
  ## `similarity`
  sensory_likelihoods = torch.softmax(((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2)) + volumeElement.unsqueeze(1).log(), dim=0)

  # Part: If stimulus noise is nonzero, convolve the likelihood with the
  ## stimulus noise.
  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    assert False
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  ## Compute posterior using Bayes' rule. As described in the paper, the posterior is computed
  ## in the discretized stimulus space.
  posterior = prior.unsqueeze(1) * likelihoods.t()
  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  # Estimator
  bayesianEstimate = MAPCircularEstimator.apply(grid_indices_here, posterior)

  ## Compute the motor likelihood

  ## `error' refers to the stimulus similarity between the estimator assigned to each m and
  ## the observations found in the dataset.
  ## The Gaussian or von Mises motor likelihood is obtained by exponentiating and normalizing
  error = (SQUARED_STIMULUS_SIMILARITY(360/GRID*bayesianEstimate.unsqueeze(0) - responses.unsqueeze(1)))
  ## The log normalizing constants, for each m in the discretized sensory space
  log_normalizing_constant = torch.logsumexp((SQUARED_STIMULUS_SIMILARITY(grid))/motor_variance, dim=0) + math.log(2 * math.pi / GRID)
  ## The log motor likelihoods, for each pair of sensory encoding m and observed human response
  log_motor_likelihoods = (error/motor_variance) - log_normalizing_constant
  ## Obtaining the motor likelihood by exponentiating.
  motor_likelihoods = torch.exp(log_motor_likelihoods)
  ## Obtain the guessing rate, parameterized via the (inverse) logit transform as described in SI Appendix
  uniform_part = torch.sigmoid(parameters["mixture_logit"][subject])

  ## The full likelihood then consists of a mixture of the motor likelihood calculated before, and the uniform
  ## distribution on the full space.
  motor_likelihoods = (1-uniform_part) * motor_likelihoods + (uniform_part / (2*math.pi) + 0*motor_likelihoods)

  # Now the loss is obtained by marginalizing out m from the motor likelihood
  if lossReduce == 'mean':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().mean()
  elif lossReduce == 'sum':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().sum()
  else:
    assert False

  if computePredictions:
     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS
     bayesianEstimate_avg_byStimulus = computeCircularMeanWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(bayesianEstimate_byStimulus, likelihoods)

     bayesianEstimate_avg_byStimulus = torch.where((bayesianEstimate_avg_byStimulus-grid).abs()<180, bayesianEstimate_avg_byStimulus, torch.where(bayesianEstimate_avg_byStimulus > 180, bayesianEstimate_avg_byStimulus-360, bayesianEstimate_avg_byStimulus+360))
     assert float(((bayesianEstimate_avg_byStimulus-grid).abs()).max()) < 180, float(((bayesianEstimate_avg_byStimulus-grid).abs()).max())

     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = computeCircularMeanWeighted(posteriorMaxima.unsqueeze(1), likelihoods)
     encodingBias = computeCircularMeanWeighted(grid.unsqueeze(1), likelihoods)
     attraction = (posteriorMaxima-encodingBias)
     attraction1 = attraction
     attraction2 = attraction+360
     attraction3 = attraction-360
     attraction = torch.where(attraction1.abs() < 180, attraction1, torch.where(attraction2.abs() < 180, attraction2, attraction3))
  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction

def retrieveObservations(x, Subject_, Contrast_, meanMethod = "circular"):
     assert Subject_ is None
     y_set = []
     sd_set = []
     for x in x_set:
        y_here = observations_y[(torch.logical_and((xValues == x), contrast==Contrast_))]

        Mean1 = computeCircularMean(y_here)
        Mean2 = computeCenteredMean(y_here, grid[x])
        if abs(Mean1-grid[x]) > 180 and Mean1 > 180:
            Mean1 = Mean1-360
        elif abs(Mean1-grid[x]) > 180 and Mean1 < 180:
            Mean1 = Mean1+360
        if Contrast_ > 1 and abs(Mean1-Mean2) > 180:
            print(Contrast_)
            print(y_here)
            print("Warning: circular and centered means are very different", Mean1, Mean2, grid[x], y_here)
        if meanMethod == "circular":
            Mean = Mean1
        elif meanMethod == "centered":
            Mean = Mean2
        else:
            assert False

        bias = Mean - grid[x]
        if abs(bias) > 180:
            bias = bias+360
        y_set.append(bias)
        sd_set.append(computeCircularSD(y_here))
     return y_set, sd_set

trigonometric_basis = torch.stack([SQUARED_STIMULUS_SIMILARITY(i*grid) for i in range(1, FOURIER_BASIS_SIZE+1)] + [SQUARED_STIMULUS_DIFFERENCE(i*grid) for i in range(1, FOURIER_BASIS_SIZE+1)], dim=0)
trigonometric_basis = trigonometric_basis * (GRID / (2*trigonometric_basis.pow(2).sum(dim=1, keepdim=True)))
fourierMultiplier = MakeFloatTensor(list(range(1,FOURIER_BASIS_SIZE+1)) + list(range(1,FOURIER_BASIS_SIZE+1)))

print(fourierMultiplier)
print(trigonometric_basis.pow(2).sum(dim=1))
if False:
  figure, axis = plt.subplots(2*FOURIER_BASIS_SIZE, 1, figsize=(50, 50))
  for i in range(trigonometric_basis.size()[0]):
     axis[i].plot(grid.cpu(), trigonometric_basis[i].cpu())
  savePlot(f"figures/{__file__}_BASIS_{GRID}_{FOURIER_BASIS_SIZE}.pdf")
  plt.close()

def model(grid):
  lossesBy500 = []
  crossLossesBy500 = []
  noImprovement = 0
  global optim, learning_rate
  lossAverageOver500 = 0
  crossValidLossAverageOver500 = 0
  ELBOAverageOver500 = 0

  importance_log_weights = []
  data_NLL = []
  for iteration in range(1):
   parameters = init_parameters

   loss = 0

   if iteration % 500 == 0:
     figure, axis = plt.subplots(N_SUBJECTS+1, 8, figsize=(16, 40))
     x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   if True:
     crossValidLoss = 0
     for CONTRAST in [1,2,3,4]:
      if not torch.any(contrast == CONTRAST):
          continue
      assert CONTRAST != 1
      for SUBJECT in range(N_SUBJECTS):
       volume = SENSORY_SPACE_VOLUME * torch.nn.functional.softmax(parameters["volume"])
       prior_overall = torch.nn.functional.softmax(parameters["prior"])
       shift_by = GRID-int((grid-centralOrientationPerSubject[SUBJECT]).abs().argmin())
       prior = torch.cat([prior_overall[shift_by:], prior_overall[:shift_by]], dim=0)

       loss_2_4, bayesianEstimate_2_4, bayesianEstimate_sd_byStimulus_2_4, attraction = computeBias(xValues, init_parameters["sigma_logit"][SUBJECT,CONTRAST], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, computePredictions=(iteration%100 == 0), sigma_stimulus=0, sigma2_stimulus=0, contrast_=CONTRAST, folds=testFolds, lossReduce='sum', subject=SUBJECT)
       crossValidLoss += loss_2_4

   data_NLL.append(float(crossValidLoss))
   if len(data_NLL) % 10 == 0:
    print(len(data_NLL), data_NLL[-10:])

  data_NLL = MakeFloatTensor(data_NLL)

  marginal_negative_log_likelihood = float(-torch.logsumexp(-data_NLL - math.log(len(data_NLL)), dim=0))
  print(sorted(data_NLL.cpu().numpy().tolist())[:100])
  print(marginal_negative_log_likelihood)
  if True:
        with open(f"losses/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_{FOURIER_BASIS_SIZE}.txt.txt", "w") as outFile:
            print(float(marginal_negative_log_likelihood), file=outFile)
############################3

model(grid)
