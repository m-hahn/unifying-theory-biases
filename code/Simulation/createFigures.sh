# Simulations

# Figure 1

python3 simulate_ExampleAttraction.py 2
python3 simulate_ExampleRepulsion.py 2
python3 simulate_ExampleScalar.py 2
python3 simulate_ExampleEncodingVariability.py 2
python3 simulate_ExampleLoss.py 8
python3 simulate_SensoryNoise.py 8
python3 simulate_StimulusNoise.py 8


# Figure 2

python3 simulate_SlowPrior.py

python3 simulate_FastPrior.py


# Figure S1:

python3 simulate_AdditiveEffect.py
python3 simulate_AdditiveEffect_ColorLegend.py
python3 simulate_StimulusNoise_ByExponent.py
python3 simulate_StimulusNoise_ByExponent_Analytical.py
python3 simulate_SensoryNoise_LargeNoise.py 2
for i in 2 3 4 5 6 7 8 9 10
do
python3 simulate_StimulusNoise_SI.py $i
done
for i in 2 3 4 5 6 7 8 9 10
do
python3 simulate_SensoryNoise_SI.py $i
done
python3 simulate_BoundaryEffect_Detailed2.py 2
python3 simulate_BoundaryEffect_Detailed3.py 2
python3 simulate_BoundaryEffect_Detailed4.py 2
python3 simulate_BoundaryEffect_Detailed3_ByLoss.py
python3 simulate_Comparison_Flat.py 2
python3 simulate_Comparison_Unimodal.py 2
python3 ExampleSubjectiveValue3_Fit3_NoPower_Loss_NoPower_Free_Viz_NoBoundary.py 2 # file doesn't exist!!!

python3 boundaryEffect.py
python3 boundaryEffect_SecondCoeff.py



