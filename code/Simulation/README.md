# Simulations

Use the commands described below to produce plots underlying various figures.
The full set of commands is called by running `. createFigures.sh`.

#### Figure 1

`python3 simulate_ExampleAttraction.py 2`

`python3 simulate_ExampleRepulsion.py 2`

`python3 simulate_ExampleScalar.py 2`

`python3 simulate_ExampleEncodingVariability.py 2`

`python3 simulate_ExampleLoss.py 8`

`python3 simulate_SensoryNoise.py 8`

`python3 simulate_StimulusNoise.py 8`

`python3 simulate_ExampleBoundary.py 2`


#### Figure 2

`python3 simulate_SlowPrior.py`

`python3 simulate_FastPrior.py`


#### Figure S1:

`python3 simulate_AdditiveEffect.py`

`python3 simulate_AdditiveEffect_ColorLegend.py`

#### Figure S2

`python3 simulate_StimulusNoise_ByExponent.py`

#### Figure S3

`python3 simulate_StimulusNoise_ByExponent_Analytical.py`

#### Figure S4

`python3 simulate_SensoryNoise_LargeNoise.py 2`

#### Figure S5

`for i in 2 4 6 8`

`do`

`python3 simulate_SensoryNoise_SI.py $i`

`python3 simulate_StimulusNoise_SI.py $i`

`done`

#### Figure S6
`python3 simulate_BoundaryEffect_Detailed2.py 2`

`python3 simulate_BoundaryEffect_Detailed3.py 2`

`python3 simulate_BoundaryEffect_Detailed4.py 2`

#### Figure S7

`python3 simulate_BoundaryEffect_Detailed3_ByLoss.py`

#### Figure S8

`python3 simulate_Comparison_Flat.py 2`

`python3 simulate_Comparison_Unimodal.py 2`

#### Figure S9
In `../Polania` :

`python3 ExampleSubjectiveValue3_Fit3_NoPower_Loss_NoPower_Free_Viz_NoBoundary.py 2`





