import computations
import json
import math
import matplotlib.pyplot as plt
import random
import scipy.integrate
import sys
import torch
from computations import computeResources
from cosineEstimator import CosineEstimator
from matplotlib import cm
from matplotlib import rc
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from util import MakeFloatTensor
from util import MakeZeros
from util import computeCircularMeanWeighted
from util import computeCircularSDWeighted
from util import derivative
from util import getInverseFisherInformation
from util import getVBias
from util import getWBias
from util import printWithoutLeakageParts
from util import product
from util import savePlot
from util import sech
rc('font', **{'family':'FreeSans'})

OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])

MIN_GRID = 0
MAX_GRID = 360
GRID = 360

INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
def point(p, reference):
    p1 = p
    p2 = p+GRID
    p3 = p-GRID
    ds = [abs(reference-x) for x in [p1,p2,p3]]
    m = min(ds)
    if m == ds[0]:
        return p1
    elif m == ds[1]:
        return p2
    else:
        return p3
grid_indices_here = MakeFloatTensor([[point(y,x) for x in range(GRID)] for y in range(GRID)])

INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x / INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS for x in range(GRID)]) + MIN_GRID
grid.requires_grad = True

SIGMAS = [0.0001, 0.0002, 0.0005, 0.001, 0.002]

##############################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 2*math.pi

# Part: Specify `similariy` or `difference` functions.
## These are negative squared distances (for interval spaces) or
## trigonometric functions (for circular spaces), with
## some extra factors for numerical purposes.
## Exponentiating a `similarity` function and normalizing
## is equivalent to the Gaussian / von Mises density.
## The purpose of specifying these as `closeness` or `distance`,
## rather than simply calling squared or trigonometric
## functions is to  flexibly reuse the same model code for
## both interval and circular spaces.
def SQUARED_STIMULUS_DIFFERENCE(x):
    return torch.sin(math.pi*x/180)
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(math.pi*x/180)
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(x)
def SQUARED_SENSORY_DIFFERENCE(x):
    return torch.sin(x)

##############################################
# Part: Configure the appropriate estimator for minimizing the loss function

assert P >= 2

# Part: Import/define the appropriate estimator for minimizing the loss function
CosineEstimator.set_parameters(GRID=GRID, OPTIMIZER_VERBOSE=OPTIMIZER_VERBOSE, P=P, SQUARED_SENSORY_DIFFERENCE=SQUARED_SENSORY_DIFFERENCE, SQUARED_SENSORY_SIMILARITY=SQUARED_SENSORY_SIMILARITY)

def computeBiasSimulation(stimulus_, sigma2, prior, volumeElement, showLikelihood=False, grid=grid, responses_=None, parameters=None, StimulusSD=None, computePredictions=False, subject=None, sigma_stimulus=None, sigma2_stimulus=None, Duration_=None):

 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 loss = 0
 if True:

  # Part: Apply stimulus noise, if nonzero.
  if sigma2_stimulus > 0:
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))

    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  # Part: Compute sensory likelihoods. Across both interval and
  ## circular stimulus spaces, this amounts to exponentiaring a
  ## `similarity`
  sensory_likelihoods = torch.softmax(((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2))  + volumeElement.unsqueeze(1).log(), dim=0)

  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  if showLikelihood and False:
    figure, axis = plt.subplots(2, 2)
    axis[0,0].plot(grid.detach(), stimulus_likelihoods[:,stimulus].detach())
    axis[0,1].plot(grid.detach(), sensory_likelihoods[:,stimulus].detach())
    axis[0,1].plot(grid.detach(), likelihoods[:,stimulus].detach(), color="orange", linestyle="dotted")
    plt.show()

  posterior = prior.unsqueeze(1) * likelihoods.t()

  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  bayesianEstimate = CosineEstimator.apply(grid_indices_here, posterior)

  if computePredictions:

     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS
     bayesianEstimate_avg_byStimulus = computeCircularMeanWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(bayesianEstimate_byStimulus, likelihoods)

     print(bayesianEstimate_avg_byStimulus.size())
     bayesianEstimate_avg_byStimulus = torch.where((bayesianEstimate_avg_byStimulus-grid).abs()<180, bayesianEstimate_avg_byStimulus, torch.where(bayesianEstimate_avg_byStimulus > 180, bayesianEstimate_avg_byStimulus-360, bayesianEstimate_avg_byStimulus+360))
     print(bayesianEstimate_avg_byStimulus.size())

     assert float(((bayesianEstimate_avg_byStimulus-grid).abs()).max()) < 180, float(((bayesianEstimate_avg_byStimulus-grid).abs()).max())

     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = computeCircularMeanWeighted(posteriorMaxima.unsqueeze(1), likelihoods)
     encodingBias = computeCircularMeanWeighted(grid.unsqueeze(1), likelihoods)
     attraction = (posteriorMaxima-encodingBias)
     attraction1 = attraction
     attraction2 = attraction+360
     attraction3 = attraction-360
     attraction = torch.where(attraction1.abs() < 180, attraction1, torch.where(attraction2.abs() < 180, attraction2, attraction3))
     encodingBias = encodingBias-grid
     encodingBias1 = encodingBias
     encodingBias2 = encodingBias+360
     encodingBias3 = encodingBias-360
     encodingBias = torch.where(encodingBias1.abs() < 180, encodingBias1, torch.where(encodingBias2.abs() < 180, encodingBias2, encodingBias3))

  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction, encodingBias

computations.setData(GRID=GRID, STIMULUS_SPACE_VOLUME=STIMULUS_SPACE_VOLUME)


def computeBias(stimulus_, sigma2, prior, volumeElement, sigma2_stimulus=None, showLikelihood=False):

  loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction, encodingBias = computeBiasSimulation(stimulus_, sigma2, prior, volumeElement, showLikelihood=False, grid=grid, responses_=None, StimulusSD=math.sqrt(sigma2_stimulus), computePredictions=True, subject=None, sigma_stimulus=math.sqrt(sigma2_stimulus), sigma2_stimulus=sigma2_stimulus, Duration_=None)

  bias_results = {}

  bias_results["Attraction"] = attraction.detach()
  bias_results["EncodingBias"] = encodingBias.detach()
  bias_results["Overall"] = (bayesianEstimate_avg_byStimulus - grid).detach()

  inverseFisherInformation = 1/volumeElement.pow(2)
  VBias = getVBias(inverseFisherInformation, grid)
  WBias_prior = getWBias(prior, inverseFisherInformation, grid)

  WBias_prior = WBias_prior * (1+sigma2_stimulus * 180/math.pi * volumeElement.pow(2) * 360)

  bias_results["VBias"] = (INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS * sigma2 * VBias)
  bias_results["WBias"] = (INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS * sigma2 * WBias_prior)
  return bias_results

############################3



prior = 2-torch.sin(grid/180*(math.pi)).abs()
prior = prior / prior.sum().detach()

uniform_prior = 1+0*grid
uniform_prior = uniform_prior / uniform_prior.sum().detach()

volume = 2-torch.sin(2*grid/360*(math.pi)).abs()
volume = volume / volume.sum().detach()


colors = ["green", "blue", "purple"]

reciprocal_kappas = [.01, .05, .1, .2, .3, .4, .5, .6]
SensoryVariances = [kappaI for kappaI in reciprocal_kappas]

figure, axis = plt.subplots(5, len(SensoryVariances), figsize=(16*0.8,8*0.8))
plt.tight_layout()
figure.subplots_adjust(wspace=0.4, hspace=0.2)

v = SENSORY_SPACE_VOLUME*volume



for i in range(len(SensoryVariances)):

     sigma2 = SensoryVariances[i]

     sigm2_stimulus = 0

     bias = computeBias(None, sigma2, prior, v, sigma2_stimulus = 0)
     bias_uniform = computeBias(None, sigma2, uniform_prior, v, sigma2_stimulus = 0)

     full = bias["Overall"]
     attraction = bias["Attraction"]
     VBias = bias["VBias"].detach()
     WBias = bias["WBias"].detach()
     full_uniform = bias_uniform["Overall"]
     encoding = bias_uniform["EncodingBias"]

     F = torch.cat([MakeZeros(1), torch.cumsum(v, dim=0)], dim=0)
     sensory_likelihoods = torch.softmax(((torch.cos(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2)) + v.unsqueeze(1).log(), dim=0)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(grid_indices_here, sensory_likelihoods)

     axis[0,i].set_title("1/κ = "+str(round((sigma2), 2)))

     resources = computeResources(v.detach(), inverse_variance=1/float(sigma2))

     axis[0,i].plot(.5 * grid.detach(), 2 * resources.detach())
     axis[1,i].plot(.5 * grid.detach(), .5 * (bayesianEstimate_sd_byStimulus.view(-1)).detach())

     axis[2,i].plot(.5 * grid.detach(), .5 * (full))
     axis[2,i].plot(.5 * grid.detach(), .5 * (WBias + (P+2)/2 * VBias), linestyle='dotted')

     axis[3,i].plot(.5 * grid.detach(), .5 * (attraction))
     axis[3,i].plot(.5 * grid.detach(), .5 * (WBias), linestyle='dotted')

     axis[4,i].plot(.5 * grid.detach(), .5 * (full-attraction))
     axis[4,i].plot(.5 * grid.detach(), .5 * ((P+2)/2 * VBias), linestyle='dotted')

     axis[0,i].set_ylim(0,0.6)
     axis[1,i].set_ylim(0,50)
     axis[2,i].set_ylim(-30, 30)
     axis[3,i].set_ylim(-30, 30)
     axis[4,i].set_ylim(-30, 30)

for i in range(len(SensoryVariances)):
   if i == 0:
     axis[0,i].set_yticks(ticks=[0,0.2,0.4,0.6], labels=["0.0", "0.2", "0.4", "0.6"])
     axis[1,i].set_yticks(ticks=[0, 20, 40], labels=["0", "20", "40"])
     axis[2,i].set_yticks(ticks=[-20, -10, 0, 10, 20])
     axis[3,i].set_yticks(ticks=[-20, -10, 0, 10, 20])
     axis[4,i].set_yticks(ticks=[-20, -10, 0, 10, 20])
   else:
     axis[0,i].set_yticks(ticks=[0,0.2,0.4,0.6], labels=["","","",""])
     axis[1,i].set_yticks(ticks=[0, 20, 40], labels=["", "", ""])
     axis[2,i].set_yticks(ticks=[-20, -10, 0, 10, 20], labels=["","","","",""])
     axis[3,i].set_yticks(ticks=[-20, -10, 0, 10, 20], labels=["","","","",""])
     axis[4,i].set_yticks(ticks=[-20, -10, 0, 10, 20], labels=["","","","",""])
   if i == 0:
     axis[4,i].set_xticks(ticks=[0,90,180], labels=["0°", "90°", "180°"])
   else:
     axis[4,i].set_xticks(ticks=[0,90,180], labels=["", "", ""])
   axis[0,i].set_xticks(ticks=[0,90,180], labels=["", "", ""])
   axis[1,i].set_xticks(ticks=[0,90,180], labels=["", "", ""])
   axis[2,i].set_xticks(ticks=[0,90,180], labels=["", "", ""])
   axis[3,i].set_xticks(ticks=[0,90,180], labels=["", "", ""])
figure.subplots_adjust(wspace=0.1, hspace=0.1)

savePlot(f"figures/{__file__}_{P}.pdf")

plt.show()
