import torch
from util import MakeFloatTensor
from util import savePlot

class SimpleMedianEstimator(torch.autograd.Function):
     @staticmethod
     def set_parameters(**kwargs):
        global GRID
        GRID = kwargs["GRID"]

     @staticmethod
     def forward(ctx, grid_indices_here, posterior):
        cumulative = torch.cumsum(posterior, dim=0)
        median = (cumulative-.5).abs().argmin(dim=0)
        print(cumulative)
        print(grid_indices_here.size(), posterior.size(), cumulative.size())
        print(median)
        oneBefore = MakeFloatTensor([float(cumulative[median[i]-1,i]) for i in range(GRID)])
        at = MakeFloatTensor([float(cumulative[median[i],i]) for i in range(GRID)])
        oneAfter = MakeFloatTensor([float(cumulative[median[i]+1,i]) for i in range(GRID)])

        medianIsLeft = (at > .5)
        print(medianIsLeft)
        leftOfMedian = torch.where(medianIsLeft, oneBefore, at)
        rightOfMedian = torch.where(medianIsLeft, at, oneAfter)

        grid_indices_at = MakeFloatTensor([float(grid_indices_here[median[i],i]) for i in range(GRID)])
        grid_indices_leftOfMedian = torch.where(medianIsLeft, grid_indices_at-1, grid_indices_at)

        return .5 + grid_indices_leftOfMedian + (.5-leftOfMedian) / (rightOfMedian-leftOfMedian)
