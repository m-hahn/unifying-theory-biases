import math
import matplotlib.pyplot as plt
import scipy
import scipy.integrate as integrate
import torch
from matplotlib import rc
from util import savePlot
rc('font', **{'family':'FreeSans'})

figure, axis = plt.subplots(1, 1, figsize=(3,3))

BOUNDS = [0,0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8,2,2.2,2.4,2.6,2.8,3,3.2,3.4,3.6,3.8,4,5,6]

sigma2s = [0.01, 0.05, 0.1, 0.12, 0.14, 0.16, 0.18, 0.2]
mu = 3

def sign(x):
    if x < 0:
        return -1
    elif x == 0:
        return 0
    else:
        return 1

def integral(function, v0, sigma2):
    i1 = integrate.quad(lambda x: function(x,v0,sigma2), 0, 1)[0]
    i2 = integrate.quad(lambda x: function(x,v0,sigma2), 1, 2)[0]
    i3 = integrate.quad(lambda x: function(x,v0,sigma2), 2, 3)[0]
    i4 = integrate.quad(lambda x: function(x,v0,sigma2), 3, 10)[0]
    i5 = integrate.quad(lambda x: function(x,v0,sigma2), 10, 100)[0]
    i6 = integrate.quad(lambda x: function(x,v0,sigma2), 100, 200)[0]

    return i1+i2+i3+i4+i5

def clamp(x, min=-1e50, max=1e50):
    if x < min:
        return min
    if x > max:
        return max
    return x

for p in [2, 2.5, 3, 3.5, 4, 5, 6]:
    ys = []

    def A0(x, v0, sigma2):
        return math.pow(abs(v0-x),p)
    def A(x, v0, sigma2):
        return p * sign(v0-x) * math.pow(abs(v0-x),p-1)
    def A_(x, v0, sigma2):
        return (p-1) * p * math.pow(abs(v0-x),p-2)

    def B(x, v0, sigma2):
        return math.exp(-math.pow(math.log(x+.1)-mu, 2)/(2*sigma2))

    def C(x, v0, sigma2):
        return math.pow(x+.1, -1)

    def func0(x, v0, sigma2):
        return A0(x, v0, sigma2) + B(x, v0, sigma2) + C(x, v0, sigma2)
    def func(x,v0,sigma2):
        return A(x,v0,sigma2) * B(x,v0,sigma2) * C(x,v0,sigma2)
    def func2(x,v0,sigma2):
        deriv1 = A_(x,v0,sigma2) * B(x,v0,sigma2) * C(x,v0,sigma2)

        return deriv1

    print("Checking gradients")
    print((func(1, 1.31, .5)-func(1, 1.3, .5))/(0.01))
    print(func2(1, 1.3, .5))

    for sigma2 in sigma2s:
     v0 = 20
     for i in range(400):
        loss = integral(func0, v0, sigma2)
        gradient = integral(func, v0, sigma2)
        gradient2 = integral(func2, v0, sigma2)

        update = - gradient/abs(gradient2)

        update = clamp(update, min=-5, max=5)
        v0 = v0 + update
        print(p, sigma2, i, v0,"loss", loss, "gradient", gradient, gradient2, "update", update)
        if abs(gradient) < 1e-5:
            break
     assert abs(gradient) < 1e-4

     ys.append(v0)
    axis.plot(sigma2s, ys)
axis.scatter(sigma2s, [math.exp(mu+sigma2/2) for sigma2 in sigma2s])
axis.scatter(sigma2s, [math.exp(mu-sigma2) for sigma2 in sigma2s])
axis.scatter(sigma2s, [math.exp(mu) for sigma2 in sigma2s])
savePlot(f"figures/{__file__}.pdf")
plt.show()
