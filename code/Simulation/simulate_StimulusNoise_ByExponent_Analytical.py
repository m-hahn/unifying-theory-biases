import computations
import json
import math
import matplotlib.pyplot as plt
import scipy.integrate
import torch
from computations import computeResources
from cosineEstimator11 import CosineEstimator
from matplotlib import cm
from matplotlib import rc
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from util import MakeFloatTensor
from util import MakeZeros
from util import computeCircularMeanWeighted
from util import computeCircularSDWeighted
from util import derivative
from util import getInverseFisherInformation
from util import getVBias
from util import getWBias
from util import printWithoutLeakageParts
from util import product
from util import savePlot
from util import sech
rc('font', **{'family':'FreeSans'})

MIN_GRID = 0
MAX_GRID = 360
GRID = 720

# simulate_StimulusNoise_ByExponent_Analytical.py
# Note: In order to prevent numerical issues at fractional exponents close to 0, we use a large grid (720 points) and a very strict stopping criterion in Newton's method.

INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
def point(p, reference):
    p1 = p
    p2 = p+GRID
    p3 = p-GRID
    ds = [abs(reference-x) for x in [p1,p2,p3]]
    m = min(ds)
    if m == ds[0]:
        return p1
    elif m == ds[1]:
        return p2
    else:
        return p3
grid_indices_here = MakeFloatTensor([[point(y,x) for x in range(GRID)] for y in range(GRID)])

INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x / INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS for x in range(GRID)]) + MIN_GRID
grid.requires_grad = True


##############################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 2*math.pi

# Part: Specify `similariy` or `difference` functions.
## These are negative squared distances (for interval spaces) or
## trigonometric functions (for circular spaces), with
## some extra factors for numerical purposes.
## Exponentiating a `similarity` function and normalizing
## is equivalent to the Gaussian / von Mises density.
## The purpose of specifying these as `closeness` or `distance`,
## rather than simply calling squared or trigonometric
## functions is to  flexibly reuse the same model code for
## both interval and circular spaces.
def SQUARED_STIMULUS_DIFFERENCE(x):
    return torch.sin(math.pi*x/180)
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(math.pi*x/180)
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(x)
def SQUARED_SENSORY_DIFFERENCE(x):
    return torch.sin(x)

##############################################
# Part: Configure the appropriate estimator for minimizing the loss function

OPTIMIZER_VERBOSE = False

def computeBiasSimulation(stimulus_, sigma2, prior, volumeElement, showLikelihood=False, grid=grid, responses_=None, parameters=None, StimulusSD=None, computePredictions=False, subject=None, sigma_stimulus=None, sigma2_stimulus=None, Duration_=None):

 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 loss = 0
 if True:

  # Part: Apply stimulus noise, if nonzero.
  if sigma2_stimulus > 0:
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))

    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  # Part: Compute sensory likelihoods. Across both interval and
  ## circular stimulus spaces, this amounts to exponentiaring a
  ## `similarity`
  sensory_likelihoods = torch.softmax(((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2))  + volumeElement.unsqueeze(1).log(), dim=0)

  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  if showLikelihood and False:
    figure, axis = plt.subplots(1,1)
    axis[0,0].plot(grid.detach(), stimulus_likelihoods[:,stimulus].detach())
    axis[0,1].plot(grid.detach(), sensory_likelihoods[:,stimulus].detach())
    axis[0,1].plot(grid.detach(), likelihoods[:,stimulus].detach(), color="orange", linestyle="dotted")
    plt.show()

  posterior = prior.unsqueeze(1) * likelihoods.t()

  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  bayesianEstimate = CosineEstimator.apply(grid_indices_here, posterior)

  if computePredictions:

     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)
     bayesianEstimate_avg_byStimulus = computeCircularMeanWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(bayesianEstimate_byStimulus, likelihoods)

     print(bayesianEstimate_avg_byStimulus.size())
     bayesianEstimate_avg_byStimulus = torch.where((bayesianEstimate_avg_byStimulus-grid).abs()<180, bayesianEstimate_avg_byStimulus, torch.where(bayesianEstimate_avg_byStimulus > 180, bayesianEstimate_avg_byStimulus-360, bayesianEstimate_avg_byStimulus+360))
     print(bayesianEstimate_avg_byStimulus.size())

     assert float(((bayesianEstimate_avg_byStimulus-grid).abs()).max()) < 180, float(((bayesianEstimate_avg_byStimulus-grid).abs()).max())

     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = computeCircularMeanWeighted(posteriorMaxima.unsqueeze(1), likelihoods)
     encodingBias = computeCircularMeanWeighted(grid.unsqueeze(1), likelihoods)
     attraction = (posteriorMaxima-encodingBias)
     attraction1 = attraction
     attraction2 = attraction+360
     attraction3 = attraction-360
     attraction = torch.where(attraction1.abs() < 180, attraction1, torch.where(attraction2.abs() < 180, attraction2, attraction3))
     encodingBias = encodingBias-grid
     encodingBias1 = encodingBias
     encodingBias2 = encodingBias+360
     encodingBias3 = encodingBias-360
     encodingBias = torch.where(encodingBias1.abs() < 180, encodingBias1, torch.where(encodingBias2.abs() < 180, encodingBias2, encodingBias3))

  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction, encodingBias

computations.setData(GRID=GRID, STIMULUS_SPACE_VOLUME=STIMULUS_SPACE_VOLUME)


def computeBias(stimulus_, sigma2, prior, volumeElement, sigma2_stimulus=None, showLikelihood=False):

  loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction, encodingBias = computeBiasSimulation(stimulus_, sigma2, prior, volumeElement, showLikelihood=False, grid=grid, responses_=None, StimulusSD=math.sqrt(sigma2_stimulus), computePredictions=True, subject=None, sigma_stimulus=math.sqrt(sigma2_stimulus), sigma2_stimulus=sigma2_stimulus, Duration_=None)

  bias_results = {}

  bias_results["Attraction"] = attraction.detach()
  bias_results["EncodingBias"] = encodingBias.detach()
  bias_results["Overall"] = (bayesianEstimate_avg_byStimulus - grid).detach()

  inverseFisherInformation = 1/volumeElement.pow(2)
  VBias = getVBias(inverseFisherInformation, grid)
  WBias_prior = getWBias(prior, inverseFisherInformation, grid)

  WBias_prior = WBias_prior * (1+sigma2_stimulus * 180/math.pi * volumeElement.pow(2) * 360)

  bias_results["VBias"] = (INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS * sigma2 * VBias)
  bias_results["WBias"] = (INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS * sigma2 * WBias_prior)
  return bias_results

############################3



prior = 2-torch.sin(grid/180*(math.pi)).abs()
prior = prior / prior.sum().detach()


volume = 2-torch.sin(2*grid/360*(math.pi)).abs()
volume = volume / volume.sum().detach()


colors = ["green", "blue", "purple", "orange", "red", "yellow", "gray"]



figure, axis = plt.subplots(1, 3, figsize=(1.2*5,1.2*2.5), layout='constrained')

axis[0].set_xlabel("Exponent")
axis[0].set_ylabel("Bias")
axis[1].set_xlabel("Exponent")
axis[2].set_xlabel("Exponent")
#axis[1].set_ylabel("Bias")

v = SENSORY_SPACE_VOLUME*volume



StimulusSigma2 = [0]
print(StimulusSigma2)


init_parameters = {}
init_parameters["sigma_logit"] = MakeZeros(3)
init_parameters["sigma_logit"].data[0] = -6.5
init_parameters["sigma_logit"].data[1] = -5.5
init_parameters["sigma_logit"].data[2] = -4.5

colors = ["blue", "red", "green"]

points = [x/40 for x in range(0,20)]
plotByColor = {x : [] for x in colors}
pointsByColor = {x : [] for x in colors}

# iterate over stimulus noise levels
for i in range(len(StimulusSigma2)):
 # iterate over sensory noise levels
 for j in range(init_parameters["sigma_logit"].size()[0]):
  sigma2 = 2*math.pi*torch.sigmoid(init_parameters["sigma_logit"][j])
  # iterate over loss function exponents
  for P in [0,0.1,0.9,1,1.5,2,2.5,3,3.5,4,4.5,5]: #[0, 0.1, 0.2, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5]:
     sigma2_stimulus = StimulusSigma2[i]
     IsMap = (P == 0)
     if IsMap:
         P = 2
     CosineEstimator.set_parameters(GRID=GRID, OPTIMIZER_VERBOSE=OPTIMIZER_VERBOSE, P=P, SQUARED_SENSORY_DIFFERENCE=SQUARED_SENSORY_DIFFERENCE, SQUARED_SENSORY_SIMILARITY=SQUARED_SENSORY_SIMILARITY, SQUARED_STIMULUS_SIMILARITY=SQUARED_STIMULUS_SIMILARITY, SQUARED_STIMULUS_DIFFERENCE=SQUARED_STIMULUS_DIFFERENCE)

     bias = computeBias(None, sigma2, prior, v, sigma2_stimulus = sigma2_stimulus)

     full = bias["Overall"]
     attraction = bias["Attraction"]
     if IsMap:
         full = attraction + bias["EncodingBias"]
         P = 0
     VBias = bias["VBias"]
     WBias = bias["WBias"]
     print("BIAS TENSOR", bias["WBias"].size())
     X = int(.125*GRID) # this corresponds to 22.5 degrees, as a fraction of 180o degrees

     plotByColor[colors[j]].append((P, .5*full[X]))

     if P % 1 == 0:
       VFactor = (P+2)/2 if P>0 else 0.5

       pointsByColor[colors[j]].append((P, .5*(WBias[X] + VFactor * VBias[X]).detach()))
for x in plotByColor:
  if len(plotByColor[x]) > 0:
    X, Y = zip(*plotByColor[x])

    axis[0].plot(X, Y, color=x)
    axis[2].plot(X, Y, color=x)
    X, Y = zip(*pointsByColor[x])

    axis[1].plot(X, Y, color=x, linestyle='dotted')
    axis[1].scatter(X, Y, color=x)
    axis[2].plot(X, Y, color=x, linestyle='dotted')
    axis[2].scatter(X, Y, color=x)
axis[0].plot([0,5], [0,0], linestyle='dotted', color='black')
axis[1].plot([0,5], [0,0], linestyle='dotted', color='black')
axis[0].plot([0,5], [0,0], linestyle='dotted', color='black')

axis[0].set_title("Simulated")
axis[1].set_title("Analytical Approximation")
axis[2].set_title("Both")
axis[0].set_ylim(-2, 4)
axis[1].set_ylim(-2, 4)
axis[2].set_ylim(-2, 4)

savePlot(f"figures/{__file__}_OVERALL.pdf")

plt.show()
