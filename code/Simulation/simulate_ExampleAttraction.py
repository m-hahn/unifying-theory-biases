import json
import math
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import scipy.integrate
import torch
from lpEstimator import LPEstimator
from matplotlib import cm
from matplotlib import rc
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from util import MakeFloatTensor
from util import MakeZeros
from util import computeCircularSDWeighted
from util import computeMeanWeighted
from util import derivative
from util import getInverseFisherInformation
from util import printWithoutLeakageParts
from util import product
from util import savePlot
from util import sech
rc('font', **{'family':'FreeSans'})

OPTIMIZER_VERBOSE=False

MIN_GRID = -180
MAX_GRID = 540
GRID = 361

INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
def point(p, reference):
    return p
    p1 = p
    p2 = p+GRID
    p3 = p-GRID
    ds = [abs(reference-x) for x in [p1,p2,p3]]
    m = min(ds)
    if m == ds[0]:
        return p1
    elif m == ds[1]:
        return p2
    else:
        return p3
grid_indices_here = MakeFloatTensor([[point(y,x) for x in range(GRID)] for y in range(GRID)])

INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x / INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS for x in range(GRID)]) + MIN_GRID
grid.requires_grad = True

SIGMAS = [0.0001, 0.0002, 0.0005, 0.001, 0.002]

P = 2

init_parameters = {}

init_parameters["sigma2_stimulus"] = MakeFloatTensor([0]).view(1)
init_parameters["log_motor_var"] = MakeFloatTensor([8]).view(1)
init_parameters["sigma_logit"] = MakeFloatTensor(5*[-3]).view(5)
init_parameters["chance_prob_logit"] = MakeFloatTensor([-0]).view(1)
init_parameters["mixture_logit"] = MakeFloatTensor([-1]).view(1)
init_parameters["prior"] = MakeFloatTensor([1]).view(1)
init_parameters["volume"] = MakeZeros(GRID)

SCALE = 50

LPEstimator.set_parameters(GRID=GRID, OPTIMIZER_VERBOSE=OPTIMIZER_VERBOSE, P=P, SQUARED_SENSORY_DIFFERENCE=None, SQUARED_SENSORY_SIMILARITY=None, SCALE=SCALE)

def computeBiasSimulation(stimulus_, sigma2, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, StimulusSD=None, computePredictions=False, subject=None, sigma_stimulus=None, sigma2_stimulus=None, Duration_=None):

 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 inverseFisherInformation = getInverseFisherInformation(volumeElement)

 loss = 0
 if True:

  if sigma2_stimulus > 0:
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))

    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  sensory_likelihoods = torch.exp((-(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)).pow(2))/(2*sigma2)) / math.sqrt(2*math.pi*sigma2)  * volumeElement.unsqueeze(1)

  sensory_likelihoods = sensory_likelihoods / sensory_likelihoods.sum(dim=0, keepdim=True)

  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  if showLikelihood and False:
    figure, axis = plt.subplots(2, 2)
    axis[0,0].plot(grid.detach(), stimulus_likelihoods[:,stimulus].detach())
    axis[0,1].plot(grid.detach(), sensory_likelihoods[:,stimulus].detach())
    axis[0,1].plot(grid.detach(), likelihoods[:,stimulus].detach(), color="orange", linestyle="dotted")
    plt.show()

  posterior = prior.unsqueeze(1) * likelihoods.t()

  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  bayesianEstimate = LPEstimator.apply(grid_indices_here, posterior)

  if computePredictions:

     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS+MIN_GRID
     bayesianEstimate_avg_byStimulus = computeMeanWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(bayesianEstimate_byStimulus, likelihoods)

     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = computeMeanWeighted(posteriorMaxima.unsqueeze(1), likelihoods)
     encodingMean = computeMeanWeighted(grid.unsqueeze(1), likelihoods)
     attraction = (posteriorMaxima-encodingMean)

     encodingBias = encodingMean-grid

  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction, encodingBias

def computeBias(stimulus_, sigma, prior, volumeElement, sigma_logit=None, n_samples=100, sigma2_stimulus=None, showLikelihood=False):
 sigma2 = 20

 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)
 inverseFisherInformation = getInverseFisherInformation(volumeElement)

 with torch.no_grad():

  loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction, encodingBias = computeBiasSimulation(stimulus_, sigma2, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, StimulusSD=math.sqrt(sigma2_stimulus), computePredictions=True, subject=None, sigma_stimulus=math.sqrt(sigma2_stimulus), sigma2_stimulus=sigma2_stimulus, Duration_=None)

  TrueTheta = grid
  L2_Biases = []
  MAPs = []
  Encodings = []
  Medians = []
  L3_Biases = []
  L4_Biases = []

  bias_results = {"leakage" : float('nan')}

  bias_results["Attraction"] = attraction.detach()
  bias_results["L_Encoding"] = encodingBias.detach()
  bias_results["L_L2"] = (bayesianEstimate_avg_byStimulus - grid).detach()

  return bias_results, (inverseFisherInformation / (INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS**3))

############################3

CENTER_OF_PRIOR_INTERVAL = 1023

sigma2_prior = math.sqrt(0.02)

prior = 2-torch.sin(grid/180*(math.pi)).abs()

prior = prior / prior.sum().detach()

uniform_prior = 1+0*grid
uniform_prior = uniform_prior / uniform_prior.sum().detach()

prior_2 = 2-torch.sin(2*grid/360*(math.pi)).abs()
prior_2 = prior_2 / prior_2.sum().detach()

VARIANCE_CHOICE = 0

colors = ["green", "blue", "purple"]

SDs = [0.001, 0.0025, 0.005]

SIGMAS = [x*x for x in SDs]

assigned = ["PRI", None, "ENC", None, "ATT", "REP", "TOT"]
for w, k in enumerate(assigned):
    globals()[k] = w
PAD = [w for w, q in enumerate(assigned) if q is None]
gridspec = dict(width_ratios=[1 if x is not None else .25 for x in assigned])
figure, axis = plt.subplots(1, len(gridspec["width_ratios"]), figsize=(1.7*8, 1.6), gridspec_kw=gridspec)
plt.tight_layout()
figure.subplots_adjust(wspace=0.1, hspace=0.0)

v = 2*math.pi*prior_2

for z in range(len(assigned)):
   axis[z].spines['top'].set_visible(False)
   axis[z].spines['right'].set_visible(False)

StimulusSigma2 = [0.1, 0.5, 1]

viridis = cm.get_cmap('viridis', 8)

prior = torch.exp(-(grid-180).pow(2)/(0.5*(100**2)))
prior = prior / prior.sum()

v = 1+0*grid

axis[PRI].plot(grid.detach(), 0*grid.detach(), color='white')
axis[PRI].plot(grid.detach(), prior.detach(), color='blue')

axis[ENC].plot(grid.detach(), 0*grid.detach(), color='white')
axis[ENC].plot(grid.detach(), (v/math.sqrt(20)).detach(), color="blue")
points = [x/40 for x in range(0,20)]

for i in range(1):
     sigma = .1
     logit_sigma = MakeFloatTensor([math.log(sigma/(1-sigma))])[0]

     bias = computeBias(None, None, prior, v, sigma_logit=logit_sigma, n_samples=1000, sigma2_stimulus = 0)[0]
     bias_uniform = computeBias(None, None, uniform_prior, v, sigma_logit=logit_sigma, n_samples=1000, sigma2_stimulus = 0)[0]

     full = bias["L_L2"]
     attraction = bias["Attraction"]
     full_uniform = bias_uniform["L_L2"]
     encoding = bias_uniform["L_Encoding"]

     axis[ATT].plot(grid.detach(), full, color="blue")
     axis[REP].plot(grid.detach(), full_uniform, color="blue")
     axis[TOT].plot(grid.detach(), full, color="blue")

for w in PAD:
    axis[w].set_visible(False)

for i in [ATT, REP, TOT]:
  axis[i].set_yticks(ticks=[-10, -5, 0, 5, 10], labels=[-10, "", 0, "", 10])
for i in range(1,len(assigned)):
    if assigned[i-1] is not None:
       axis[i].tick_params(labelleft=False)

for i in [ATT, REP, TOT]:
    axis[i].set_ylim(-10,10)

axis[PRI].set_yticks([0])

for i in range(len(assigned)):
    axis[i].set_xlim(0,360)

for i in range(1,7):
  if assigned[i-1] is not None:
    axis[i].tick_params(labelbottom=False)

savePlot(f"figures/{__file__}_OVERALL.pdf", transparent=True)

plt.show()
