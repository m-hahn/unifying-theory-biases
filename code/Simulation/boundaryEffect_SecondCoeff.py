import math
import matplotlib.pyplot as plt
import scipy
import scipy.integrate as integrate
import torch
from matplotlib import rc
from util import savePlot
rc('font', **{'family':'FreeSans'})

figure, axis = plt.subplots(1, 2, figsize=(6,3))

BOUNDS = [0,0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8,2,2.2,2.4,2.6,2.8,3,3.2,3.4,3.6,3.8,4,5,6]

def density(x, bound):
   return (math.exp(-x*x/2) + math.exp(-math.pow(2*bound-x,2)/2))

def integral(f, bound):
    return integrate.quad(f, -100, -10)[0] + integrate.quad(f, -10, -5)[0] + integrate.quad(f, -5, bound)[0]
for p in [2, 4, 6, 8, 10]:
    ys1 = []
    ys2 = []
    def func(x,v0,bound):
        return math.pow(v0-x,p-1) * (math.exp(-x*x/2) + math.exp(-math.pow(2*bound-x,2)/2))
    def func2(x,v0,bound):
        return (p-1) * math.pow(v0-x,p-2) * (math.exp(-x*x/2) + math.exp(-math.pow(2*bound-x,2)/2))
    for bound in BOUNDS:
     v0 = -1
     for i in range(20):
        gradient = integrate.quad(lambda x: func(x,v0,bound), -100, -10)[0] + integrate.quad(lambda x: func(x,v0,bound), -10, -5)[0] + integrate.quad(lambda x: func(x,v0,bound), -5, bound)[0]
        if abs(gradient) < 1e-5:
            break
        gradient2 = integrate.quad(lambda x: func2(x,v0,bound), -100, -10)[0] + integrate.quad(lambda x: func2(x,v0,bound), -10, -5)[0] + integrate.quad(lambda x: func2(x,v0,bound), -5, bound)[0]
        v0 = v0 - gradient / gradient2
     print(p, bound, i, v0, "gradient", gradient, gradient2)

     A = integral(lambda x: math.pow(x-v0,p-1) * x * density(x,bound),bound)
     B = integral(lambda x: math.pow(v0-x,p-2) * x * x * density(x,bound),bound)
     C = (p-1) * integral(lambda x: math.pow(v0-x,p-2) * density(x,bound),bound)

     ys1.append(A/C)
     ys2.append(B/C)
    axis[0].plot(BOUNDS, ys1)
    axis[1].plot(BOUNDS, ys2)
axis[0].set_title("$W_1$")
axis[1].set_title("$W_2$")
savePlot(f"figures/{__file__}.pdf")
plt.show()
