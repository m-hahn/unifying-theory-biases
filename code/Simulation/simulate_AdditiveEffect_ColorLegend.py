import math
import matplotlib.pyplot as plt
import random
import sys
import torch
from lpEstimator import LPEstimator
from mapCircularEstimator3 import MAPCircularEstimator
from matplotlib import cm
from matplotlib import rc
from util import MakeFloatTensor
from util import MakeZeros
from util import computeCenteredMean
from util import computeCircularMeanWeighted
from util import computeCircularSDWeighted
from util import getInverseFisherInformation
from util import getVBias
from util import getWBias
from util import product
from util import savePlot
rc('font', **{'family':'FreeSans'})

OPTIMIZER_VERBOSE = False

MIN_GRID = 0
MAX_GRID = 360
GRID = 360

INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
def point(p, reference):
    p1 = p
    p2 = p+GRID
    p3 = p-GRID
    ds = [abs(reference-x) for x in [p1,p2,p3]]
    m = min(ds)
    if m == ds[0]:
        return p1
    elif m == ds[1]:
        return p2
    else:
        return p3
grid_indices_here = MakeFloatTensor([[point(y,x) for x in range(GRID)] for y in range(GRID)])
grid.requires_grad = True

SIGMAS = [0.0001, 0.0002, 0.0005, 0.001, 0.002]
OVERALL = []

def SQUARED_STIMULUS_SIMILARITY(x):
    return torch.cos(math.pi*x/180)
def DISTANCE(x):
    assert (x<=2*math.pi).all(), x.max()
    return 1-(x.pow(2))

class SimpleMedianEstimator(torch.autograd.Function):
    def forward(ctx, grid_indices_here, posterior):
        cumulative = torch.cumsum(posterior, dim=0)
        median = (cumulative-.5).abs().argmin(dim=0)

        oneBefore = MakeFloatTensor([float(cumulative[median[i]-1,i]) for i in range(GRID)])
        at = MakeFloatTensor([float(cumulative[median[i],i]) for i in range(GRID)])
        oneAfter = MakeFloatTensor([float(cumulative[median[i]+1,i]) for i in range(GRID)])

        medianIsLeft = (at > .5)

        leftOfMedian = torch.where(medianIsLeft, oneBefore, at)
        rightOfMedian = torch.where(medianIsLeft, at, oneAfter)

        grid_indices_at = MakeFloatTensor([float(grid_indices_here[median[i],i]) for i in range(GRID)])
        grid_indices_leftOfMedian = torch.where(medianIsLeft, grid_indices_at-1, grid_indices_at)

        return .5 + grid_indices_leftOfMedian + (.5-leftOfMedian) / (rightOfMedian-leftOfMedian)

SCALE = 50

KERNEL_WIDTH = 0.05

averageNumberOfNewtonSteps = 2

MAPCircularEstimator.set_parameters(GRID=GRID, OPTIMIZER_VERBOSE=OPTIMIZER_VERBOSE, KERNEL_WIDTH=KERNEL_WIDTH, SCALE=SCALE, MIN_GRID=MIN_GRID, MAX_GRID=MAX_GRID, FINE_GRID_SCALE_STATIC=2, UPDATE_DECAY_FACTOR=10)

def computeBiasAnalytical(stimulus_, sigma, prior, volumeElement, n_samples=100):
 sigma2 = sigma**2
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)
 inverseFisherInformation = getInverseFisherInformation(volumeElement)
 VBias = getVBias(inverseFisherInformation, grid)
 WBias_prior = getWBias(prior, inverseFisherInformation, grid)
 stimulus = int(stimulus_*GRID)
 assert stimulus > 0
 return None

def computeBias(stimulus, sigma, prior, volumeElement, n_samples=100, subject=None, StimulusSD=0, sigma_stimulus=0, Duration_=None, sigma2_stimulus=0):
 assert False, "use the other function below"
 log_motor_var = 1
 sigma = 2*math.pi*sigma
 sigma2 = sigma*sigma
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)
 inverseFisherInformation = getInverseFisherInformation(volumeElement)

 loss = 0
 if True:

  if sigma2_stimulus > 0:
    stimulus_log_likelihoods = ((DISTANCE_DEG(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))

    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  sensory_likelihoods = torch.softmax((DISTANCE(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(2*sigma2) + volumeElement.unsqueeze(1).log(), dim=0)

  sensory_likelihoods = sensory_likelihoods / sensory_likelihoods.sum(dim=0, keepdim=True)

  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  posterior = prior.unsqueeze(1) * likelihoods.t()

  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  if P >= 2:
     LPEstimator.set_parameters(GRID=GRID, OPTIMIZER_VERBOSE=OPTIMIZER_VERBOSE, P=P, SQUARED_SENSORY_DIFFERENCE=None, SQUARED_SENSORY_SIMILARITY=None, SCALE=SCALE)
     bayesianEstimate = LPEstimator.apply(grid_indices_here, posterior)
  elif P == 0:
      bayesianEstimate = posterior.argmax(dim=0).float()
  else:
      assert False

  if True:

     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS

     x1 = bayesianEstimate_byStimulus
     x2 = bayesianEstimate_byStimulus+MAX_GRID
     x3 = bayesianEstimate_byStimulus+MAX_GRID+MAX_GRID
     x4 = bayesianEstimate_byStimulus-MAX_GRID
     x5 = bayesianEstimate_byStimulus-MAX_GRID-MAX_GRID
     d1 = (x1-grid.unsqueeze(0)).abs()
     d2 = (x2-grid.unsqueeze(0)).abs()
     d3 = (x3-grid.unsqueeze(0)).abs()
     d4 = (x4-grid.unsqueeze(0)).abs()
     d5 = (x5-grid.unsqueeze(0)).abs()

     minimum = torch.min(torch.stack([d1, d2, d3, d4, d5],dim=0), dim=0).values

     bayesianEstimate_byStimulus = MakeZeros(GRID, GRID)
     bayesianEstimate_byStimulus[d1<1.01*minimum] = x1.expand(-1, GRID)[d1<1.01*minimum]
     bayesianEstimate_byStimulus[d2<1.01*minimum] = x2.expand(-1, GRID)[d2<1.01*minimum]
     bayesianEstimate_byStimulus[d3<1.01*minimum] = x3.expand(-1, GRID)[d3<1.01*minimum]
     bayesianEstimate_byStimulus[d4<1.01*minimum] = x4.expand(-1, GRID)[d4<1.01*minimum]
     bayesianEstimate_byStimulus[d5<1.01*minimum] = x5.expand(-1, GRID)[d5<1.01*minimum]

     bayesianEstimate_byStimulus = torch.where(torch.logical_and(d1<d2, d1<d3), x1, torch.where(d2<d3, x2, x3))

     bayesianEstimate_avg_byStimulus = (bayesianEstimate_byStimulus * likelihoods).sum(dim=0)

     bayesianEstimate_sd_byStimulus = ((bayesianEstimate_byStimulus - bayesianEstimate_avg_byStimulus.unsqueeze(0)).pow(2) * likelihoods).sum(dim=0).sqrt()

  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return bayesianEstimate_avg_byStimulus, inverseFisherInformation

def computeBiasCircular(stimulus, sigma, prior, volumeElement, n_samples=100, subject=None, StimulusSD=0, sigma_stimulus=0, Duration_=None, sigma2_stimulus=0):
 log_motor_var = 1
 sigma = 2*math.pi*sigma
 sigma2 = sigma*sigma
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)
 inverseFisherInformation = getInverseFisherInformation(volumeElement)

 loss = 0
 if True:

  if sigma2_stimulus > 0:
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))

    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  sensory_likelihoods = torch.softmax((torch.cos(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(2*sigma2) + volumeElement.unsqueeze(1).log(), dim=0)

  sensory_likelihoods = sensory_likelihoods / sensory_likelihoods.sum(dim=0, keepdim=True)

  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  posterior = prior.unsqueeze(1) * likelihoods.t()

  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  if P >= 2:
     LPEstimator.set_parameters(GRID=GRID, OPTIMIZER_VERBOSE=OPTIMIZER_VERBOSE, P=P, SQUARED_SENSORY_DIFFERENCE=None, SQUARED_SENSORY_SIMILARITY=None, SCALE=SCALE)
     bayesianEstimate = LPEstimator.apply(grid_indices_here, posterior)
  elif P == 0:
      bayesianEstimate = MAPCircularEstimator.apply(grid_indices_here, posterior)
  elif P == 1:
      bayesianEstimate = SimpleMedianEstimator.apply(grid_indices_here, posterior)
  else:
      assert False

  if True:

     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS
     bayesianEstimate_avg_byStimulus = computeCircularMeanWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(bayesianEstimate_byStimulus, likelihoods)

     bayesianEstimate_avg_byStimulus = torch.where((bayesianEstimate_avg_byStimulus-grid).abs()<180, bayesianEstimate_avg_byStimulus, torch.where(bayesianEstimate_avg_byStimulus > 180, bayesianEstimate_avg_byStimulus-360, bayesianEstimate_avg_byStimulus+360))

  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return bayesianEstimate_avg_byStimulus, inverseFisherInformation

def computeCircularSD(responses_):

      responses_ = responses_.unsqueeze(1)
      R = len(responses_)
      if R == 0:
          return float('nan')
      weights = MakeZeros(R, 1)+1/R
      angles = responses_
      responses_ = torch.stack([torch.cos(responses_/180*math.pi), torch.sin(responses_/180*math.pi)], dim=0)

      averaged = (responses_ * weights.unsqueeze(0)).sum(dim=1)

      resultantLength = averaged.pow(2).sum(dim=0).sqrt()
      circularSD = torch.sqrt(-2*torch.log(resultantLength)) * 180 / math.pi

      return float(circularSD)

def computeCircularMean(responses_):
      if sum(responses_.size()) == 0:
          return float('nan')
      angles = responses_
      responses_ = torch.stack([torch.cos(responses_/180*math.pi), torch.sin(responses_/180*math.pi)], dim=0)
      averaged = responses_.mean(dim=1)
      averaged = averaged / averaged.pow(2).sum().sqrt()
      acosine = torch.acos(averaged[0])/math.pi*180
      asine = torch.asin(averaged[1])/math.pi*180

      if averaged[0] >= 0 and averaged[1] >= 0:
          M = float(acosine)
      elif averaged[0] <= 0 and averaged[1] >= 0:
          M = float(acosine)

      elif averaged[0] <= 0 and averaged[1] <= 0:

          assert float(180-float(asine) - (360-float(acosine))) < .1
          M = 360-float(acosine)

      elif averaged[0] >= 0 and averaged[1] <= 0:
          assert abs((360+float(asine)) - ( 360-float(acosine))) < .1
          M = 360-float(acosine)

      else:
          assert False, (averaged, responses_.mean(dim=1))

      return M

############################3

sigma2_prior_at_m1 = 0.02
prior_at_m1 = 2-torch.sin(grid*2*math.pi/360).abs()
prior_at_m1 = prior_at_m1 / prior_at_m1.sum().detach()
print(prior_at_m1.max())

sigma2_prior_at_0 = 0.02
prior_at_0 = 2-torch.sin(grid*2*math.pi/360)
prior_at_0 = prior_at_0 / prior_at_0.sum().detach()
print(prior_at_0.max())

OFFSET = 3.14

sigma2_prior_at_1 = 0.02
prior_at_1 = 1/(1+2*((180-grid)/100).pow(2)) + .1

prior_at_1 = prior_at_1 / prior_at_1.sum().detach()
print(prior_at_1.max())

prior_uniform = 1+0*grid
prior_uniform = prior_uniform / prior_uniform.sum().detach()

############################3

volume_bimodal = 2-torch.sin(.5*grid*2*math.pi/360).abs()
volume_bimodal = 2*math.pi*volume_bimodal / volume_bimodal.sum().detach()

volume_uniform = 1+0*grid
volume_uniform = 2*math.pi*volume_uniform / volume_uniform.sum().detach()

volume_unimodal = 1/(1+2*((180-grid)/300).pow(2)) + .1
volume_unimodal = 2*math.pi*volume_unimodal / volume_unimodal.sum().detach()

viridis = cm.get_cmap('viridis', 8)
############################3

figure, axis = plt.subplots(1, 1, figsize=(5,1))
plt.tight_layout()

X = int(0.2*GRID)

for P in [0,1,2,3,4,5,6,7,8]:
   axis.plot([P, P+.2], [0,0], c=viridis(P/5), linewidth=10)
   axis.text(P+.0, 20, str(P))
axis.set_xticklabels([])
axis.set_yticklabels([])
axis.set_ylim(-5, 25)
axis.axis('off')
savePlot(f"figures/{__file__}_OVERALL.pdf")

plt.show()
