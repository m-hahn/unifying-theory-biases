import math
import matplotlib.pyplot as plt
import scipy
import scipy.integrate as integrate
import torch
from matplotlib import rc
from util import savePlot
rc('font', **{'family':'FreeSans'})

figure, axis = plt.subplots(1, 1, figsize=(3,3))

BOUNDS = [0,0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8,2,2.2,2.4,2.6,2.8,3,3.2,3.4,3.6,3.8,4,5,6]

for p in [2, 4, 6, 8, 10]:
    ys = []
    def func(x,v0,bound):
        return math.pow(v0-x,p-1) * (math.exp(-x*x/2))
    def func2(x,v0,bound):
        return (p-1) * math.pow(v0-x,p-2) * (math.exp(-x*x/2))
    for bound in BOUNDS:
     v0 = -1
     for i in range(20):
        gradient = integrate.quad(lambda x: func(x,v0,bound), -100, -10)[0] + integrate.quad(lambda x: func(x,v0,bound), -10, -5)[0] + integrate.quad(lambda x: func(x,v0,bound), -5, bound)[0]
        if abs(gradient) < 1e-5:
            break
        gradient2 = integrate.quad(lambda x: func2(x,v0,bound), -100, -10)[0] + integrate.quad(lambda x: func2(x,v0,bound), -10, -5)[0] + integrate.quad(lambda x: func2(x,v0,bound), -5, bound)[0]
        v0 = v0 - gradient / gradient2
     print(p, bound, i, v0, "gradient", gradient, gradient2)
     ys.append(v0)
    axis.plot(BOUNDS, ys)
savePlot(f"figures/{__file__}.pdf")
plt.show()
