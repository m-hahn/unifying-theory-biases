import math
import matplotlib.pyplot as plt
import numpy as np
import random
import torch
from matplotlib import rc
from util import MakeFloatTensor
from util import MakeZeros
from util import savePlot
from util import sech
rc('font', **{'family':'FreeSans'})

bias_observations_x = MakeFloatTensor([0.07*x for x in range(15)])
bias_observations_y = MakeFloatTensor([0.07, 0.06, 0.09, 0.04, 0.03, 0.03, -0.05, -0.01, 0.01, 0.04, -0.03, -0.05, -0.08, -0.08, -0.08])

MIN_GRID = -5
MAX_GRID = 6
GRID = 300

INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid.requires_grad = True

def SQUARED_SENSORY_SIMILARITY(x):
    assert (x<=2*math.pi).all(), x.max()
    return 1-(x.pow(2))

SCALE = 1

class LPEstimator(torch.autograd.Function):
    """
    We can implement our own custom autograd Functions by subclassing
    torch.autograd.Function and implementing the forward and backward passes
    which operate on Tensors.
    """

    @staticmethod
    def forward(ctx, grid_indices_here, posterior):
        """
        In the forward pass we receive a Tensor containing the input and return
        a Tensor containing the output. ctx is a context object that can be used
        to stash information for backward computation. You can cache arbitrary
        objects for use in the backward pass using the ctx.save_for_backward method.
        """
        assert len(grid_indices_here.size()) == 2
        grid_indices_here = grid_indices_here/SCALE
        n_inputs, n_batch = posterior.size()
        initialized = (grid_indices_here.data * posterior).detach().sum(dim=0).data
        result = initialized.clone()

        for itera in range(20):

          loss = (((result.unsqueeze(0) - grid_indices_here).abs().pow(P)) * posterior.detach()).sum(dim=0).sum() / n_batch
          loss_gradient = ((P * torch.sign(result.unsqueeze(0) - grid_indices_here) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-1)) * posterior.detach()).sum(dim=0) / n_batch
          loss_gradient2 = ((P * (P-1) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-2)) * posterior.detach()).sum(dim=0) / n_batch

          result -= loss_gradient / loss_gradient2
          print(itera, "max absolute gradient after Newton steps", loss_gradient.abs().max())
          assert not torch.isnan(result).any(), loss_gradient2
          if float(loss_gradient.abs().max()) < 1e-5:
              break

        if float(loss_gradient.abs().max()) >= 1e-5:
            print("WARNING", float(loss_gradient.abs().max()), "after ", itera, " Newton steps")
        if P == 2:
            assert itera == 0
        ctx.save_for_backward(grid_indices_here, posterior, result)

        assert not torch.isnan(result).any()
        return SCALE*result

    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor containing the gradient of the loss
        with respect to the output, and we need to compute the gradient of the loss
        with respect to the input.
        """
        grid_indices_here, posterior, result = ctx.saved_tensors
        initialized = (grid_indices_here.data * posterior).detach().sum(dim=0).data

        F = ((P * torch.sign(result.unsqueeze(0) - grid_indices_here) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-1)) * posterior.detach()).sum(dim=0)

        assert F.abs().mean() < 0.001, F
        dF_posterior = ((P * torch.sign(result.unsqueeze(0) - grid_indices_here) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-1)))
        dF_result = ((P * (P-1) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-2)) * posterior.detach()).sum(dim=0)

        gradient = - ((1 / dF_result).unsqueeze(0) * dF_posterior).detach().data

        gradient = grad_output.unsqueeze(0) * gradient

        return None, gradient*SCALE

def computeBias(stimulus_, sigma, prior, volumeElement, n_samples=100, showLikelihood=False):

 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 zero = torch.argmin(grid.pow(2))
 one = torch.argmin((grid-1).pow(2))
 massInterval = F[one] - F[zero]
 massFull = F[-1]

 sigma = sigma * (massInterval/massFull)

 sigma2 = sigma**2

 VBias = getVBias(inverseFisherInformation)
 WBias_prior = getWBias(prior, inverseFisherInformation)

 if True:

  sensory_likelihoods = torch.softmax((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(2*sigma2)  + volumeElement.unsqueeze(1).log(), dim=0)

  if True:
    likelihoods = sensory_likelihoods
    assert not torch.isnan(likelihoods).any(), (likelihoods, sigma2)
  posterior = prior.unsqueeze(1) * likelihoods.t()

  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  estimatorByM = LPEstimator.apply(grid.unsqueeze(1), posterior)
  L0Estimator = grid[posterior.argmax(dim=0)]

  posteriorMean = (estimatorByM.unsqueeze(1) * likelihoods).sum(dim=0)
  likelihoodMean = (grid.unsqueeze(1) * likelihoods).sum(dim=0)
  L0EstimatorMean = (L0Estimator.unsqueeze(1) * likelihoods).sum(dim=0)
  variability = ((estimatorByM.pow(2).unsqueeze(1) * likelihoods).sum(dim=0) - posteriorMean.pow(2)).sqrt()

  bias_results = {}
  bias_results["L_L0"] = (L0EstimatorMean - likelihoodMean).detach()
  bias_results["L_L2"] = (posteriorMean - grid).detach()
  bias_results["variability"] = variability.detach()
  assert not torch.isnan(posteriorMean).any()
  return bias_results

############################3

lowestError = 100000

xValues = []
for x in bias_observations_x:
   xValues.append(float( torch.argmin((grid - x).abs()) / GRID))
print(xValues)

with open(f"logs/{__file__.replace('_Viz_NoBoundary', '')}.txt", "r") as inFile:
    next(inFile)
    next(inFile)
    high_sigma = float(next(inFile).split("\t")[1])
    high_sigma2_prior = float(next(inFile).split("\t")[1])
    low_sigma = float(next(inFile).split("\t")[1])
    low_sigma2_prior = float(next(inFile).split("\t")[1])
    power = float(next(inFile).split("\t")[1])
    P = float(next(inFile).split("\t")[1])

for iteration in range(1):

   high_prior = (1/4*high_sigma2_prior)*sech((grid-0.5)/(2*high_sigma2_prior)) + .001
   high_prior = high_prior / high_prior.sum().detach()
   high_prior_2 = high_prior.pow(power)
   high_prior_2 = high_prior_2 / high_prior_2.sum().detach()

   low_prior = (1/4*low_sigma2_prior)*sech((grid-0.5)/(2*low_sigma2_prior)) + .001
   low_prior = low_prior / low_prior.sum().detach()
   low_prior_2 = low_prior.pow(power)
   low_prior_2 = low_prior_2 / low_prior_2.sum().detach()

   VARIANCE_CHOICE = 0

   if True:

      high_results = computeBias(None, high_sigma, high_prior, high_prior_2, n_samples=1000)
      low_results = computeBias(None, low_sigma, low_prior, low_prior_2, n_samples=1000)

      prior_restricted = high_prior.clone()
      prior_restricted[grid>1] = 1e-20
      prior_restricted[grid<0] = 1e-20
      prior_restricted = prior_restricted / prior_restricted.sum().detach()

      high_results_bound = computeBias(None, high_sigma, prior_restricted, high_prior_2, n_samples=1000)
      low_results_bound = computeBias(None, low_sigma, prior_restricted, low_prior_2, n_samples=1000)

      high_observation_bound = (grid+high_results_bound["L_L2"]).detach()
      low_observation_bound =  (grid+low_results_bound["L_L2"]).detach()

      high_observation = (grid+high_results["L_L2"]).detach()
      low_observation =  (grid+low_results["L_L2"]).detach()

      print("Rejecting", low_observation[0], low_observation[-1])
      if low_observation[0] > 0.1 or low_observation[-1] < 0.9:
          continue

      predicted = []
      for i in range(len(bias_observations_y)):
          j = int(torch.argmin((low_observation-xValues[i]).abs()))
          predicted.append((high_observation-low_observation)[j])
      predicted_bound = []
      for i in range(len(bias_observations_y)):
          j = int(torch.argmin((low_observation_bound-xValues[i]).abs()))
          predicted_bound.append((high_observation_bound-low_observation_bound)[j])
      print("PREDICTED", predicted, predicted_bound)
      error = max([math.pow(abs(x-y),1) for x, y  in zip(predicted, bias_observations_y)])
      print("ERROR", error)

      if error < lowestError:
          lowestError = error
          best = [high_sigma, high_sigma2_prior, low_sigma, low_sigma2_prior, power, P]
          figure, axis = plt.subplots(1,8, figsize=(12,2))
          plt.tight_layout()
          axis[0].plot(grid.detach(), high_prior.detach())
          axis[1].plot(grid.detach(), high_prior_2.detach())

          axis[3].plot(grid.detach(), (low_results["L_L2"] - low_results["L_L0"]).detach(), linestyle='solid', color="orange")
          axis[3].plot(grid.detach(), (high_results["L_L2"] - high_results["L_L0"]).detach(), linestyle='solid', color="blue")

          axis[2].plot(grid.detach(), low_results["L_L0"].detach(), linestyle='solid', color="orange")
          axis[2].plot(grid.detach(), high_results["L_L0"].detach(), linestyle='solid', color="blue")

          axis[4].plot(grid.detach(), (low_results_bound["L_L2"]-low_results["L_L2"]).detach(), linestyle='solid', color="orange")
          axis[4].plot(grid.detach(), (high_results_bound["L_L2"]-high_results["L_L2"]).detach(), linestyle='solid', color="blue")

          axis[5].plot(grid.detach(), low_results_bound["L_L2"].detach(), color="orange")
          axis[5].plot(grid.detach(), high_results_bound["L_L2"].detach(), color="blue")

          axis[6].plot(low_observation, high_observation_bound-low_observation_bound, color="green")
          axis[6].plot(low_observation, high_observation-low_observation, color="green", linestyle='dotted')
          axis[7].scatter(bias_observations_x,bias_observations_y, color="green")
          axis[0].set_title("Prior")
          axis[1].set_title("Resources")
          axis[3].set_title("Repulsion")
          axis[2].set_title("Attraction")
          axis[4].set_title("Boundary")
          axis[5].set_title("Full")
          axis[6].set_title("Relative")
          axis[7].set_title("Data")
          for i in [0,1,2,3,4,5,6,7]:
              axis[i].set_xlim(0,1)
          for i in [2,3,4,5,6,7]:
            axis[i].set_ylim(-0.25, 0.25)

          savePlot(f"figures/{__file__}.pdf")
          plt.show()
   print(iteration, lowestError, best)
