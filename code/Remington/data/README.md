The data can be downloaded at https://jazlab.org/resources/

The code expects the content of `LateInferenceDataAndCode.zip` unzipped in `REMINGTON/`, with content: `Datafiles`, `GetStats.m`, `LapseTrials.m`, `Models`.

We also provide a simulated dataset, derived from logs/SIMULATED_REPLICATE/SimulateRemington_Lognormal_Zero.py_0_200.txt, with the order of lines shuffled and stimuli projected onto the discrete grid.
