import os
import random
import subprocess
import sys

script = sys.argv[1]

fits = [x for x in os.listdir("logs/SIMULATED_REPLICATE") if x.startswith("SimulateSynthetic2_Remington")]
random.shuffle(fits)
for f in fits:
  print(f)
  subprocess.call([str(q) for q in ["/proj/mhahn.shadow/SOFTWARE/miniconda3/envs/py39-first/bin/python3.9", "runCrossValidAllRemington_OnlyOneFold.py", script, f]])
#  subprocess.call([str(q) for q in ["/proj/mhahn.shadow/SOFTWARE/miniconda3/envs/py39-first/bin/python3.9", "runCrossValidAllGardelle180_OnlyOneFold.py", "RunSynthetic_FreePrior_Zero_OnSim.py", f]])

