# Plot the fitted models at p=0
python3   RunRemington_Lognormal_Zero_VIZ.py 0 0 0.1 200
python3   RunRemington_Free_Zero_VIZ.py 0 0 0.1 200
python3   RunRemington_Flat_Zero_VIZ.py 0 0 0.1 200
python3   RunRemington_Normal_Zero_VIZ.py 0 0 0.1 200

# Plot the fitted models at p>0 in batch mode
python3   RunRemington_Lognormal_VIZ_ACREXP.py 2 0 0.1 200
python3   RunRemington_Free_VIZ_ACREXP.py 2 0 0.1 200
python3   RunRemington_Flat_VIZ_ACREXP.py 2 0 0.1 200
python3   RunRemington_Normal_VIZ_ACREXP.py 2 0 0.1 200

# Plot human data
python3 RunRemington_Lognormal_VIZ_OnlyHuman_ErrBar.py 2 0 0.1 200 NO_PLOT

# Collect and plot NLL statistics
python3 evaluateCrossValidationResults_Remington_StopNoImpQ.py

# Plot models fitted on simulated data
python3 RunRemington_Lognormal_Zero_OnSim_VIZ.py  0 0 0.1 200
python3 RunRemington_Flat_Zero_OnSim_VIZ.py 0 0 0.1 200

python3 RunRemington_Flat_Zero_VIZ_MainPaper.py 0 0 0.1 200
python3 RunRemington_Lognormal_VIZ_OnlyHuman_MainPaper_ErrBar.py 2 0 0.1 200
python3 RunRemington_Lognormal_Zero_VIZ_MainPaper.py 0 0 0.1 200


python3 evaluateCrossValidationResults_Remington_StopNoImpQ_Flat_OnSim.py ; python3 evaluateCrossValidationResults_Remington_StopNoImpQ_Lognormal_OnSim.py

python3 evaluateCrossValidationResults_Remington_StopNoImpQ_Flat_OnSim.py
python3 evaluateCrossValidationResults_Remington_StopNoImpQ_Free_OnSim.py
python3 evaluateCrossValidationResults_Remington_StopNoImpQ_Lognormal_OnSim.py

