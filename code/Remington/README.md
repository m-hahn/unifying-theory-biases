# Remington et al 2018

## Instructions for Fitting Models

Here, we provide a full walk-through for fitting a model.

##### Step 1: Set up Python environment

We recommend running our code within a virtual environment created using Conda (https://docs.conda.io/en/latest/). To start, create a virtual environment:

`conda create -n perceptual_biases python=3.7 anaconda`

then activate the environment `perceptual_biases`.

Make sure the environment remains activated throughout the subsequent steps.

Inside this environment, install PyTorch (https://pytorch.org).
We developed the codebase under Torch 1.11 (`torch==1.11.0`) and have not tested the codebase thoroughly under Torch 2.0, though we do not anticipate compatibility issues.
You can install Torch 1.11 using instructions at https://pytorch.org/get-started/previous-versions/#v1110
If you have a GPU available, you can accordingly install a PyTorch version compatible with your CUDA version.
All code can in principle be run without a GPU, but acceleration in model fitting can be substantial.

To install the other dependencies, run inside the [`code/` directory](code/):

`pip install -r requirements.txt`


##### Step 2: Download the dataset

The dataset is freely available, see instructions under [data/](data/).

The relevant files then are those from sessions in the ready-set-go task with gain 1: ``data/REMINGTON/Datafiles/RSG_*_100.mat``

After saving the files there, `ls data/REMINGTON/Datafiles/RSG_*_100.mat -1 | wc -l` should result in the output `15` -- i.e., there are files for 15 subjects.

We also include a simple dataset simulated from the fitted model; you can run the code using that one if downloading the original data is not feasible, simply by overwriting the data loading script: `cp iloadRemington_Simulated.py loadRemington.py`.
Note that, in this case, the NLL and visualized fit will differ from those obtained from the original data.
You can always undo such changes by running `git restore` or `git checkout` on the changed files.

##### Step 3: Choose device
Depending on whether you're fitting on a GPU or not, set the environment variable `BIAS_MODEL_DEVICE` to  either ``cuda`` or ``cpu``:

`export BIAS_MODEL_DEVICE=cuda`

or

`export BIAS_MODEL_DEVICE=cpu`

Some vsualization scripts only work in CPU mode; you can control this by setting the environment variable appropriately before running commands.

##### Step 4: Fit a model

The fitting scripts for all model variants are listed below under "Fitting Scripts".


The main paper plots results at p=0 for two parametric priors (normal in sensory space or flat). Results across the unimodal priors, and nonparametrically fitted priors, and across loss functions, are very similar.

For example, you can fit a model with nonparametrically fitted prior at p=2 by running:

`python3 RunRemington_Free.py 2 0 0.1 200`

The four positional arguments describe the loss function exponent (`Zero` in the filename indicates that this script uses the MAP estimator, implemented separately from higher exponents), which of the ten folds to use (0,...,9), the regularization weight lambda (here, for a parametric prior and encoding, its effect is vacuous, though we keep it consistent across all scripts withint a dataset), and the size N of the grid.
The first two parameters are varied systematically for each dataset and model variant; the last two are generally kept constant for a given model.

It takes about 3,500 iterations to reach the final model fit, but longer for full convergence.
The model parameters are stored in intervals of 500 iterations.
On a typical GPU, 500 iterations might take ~10 seconds, and the overall fitting a couple of minutes.
While we ran all models to completion, you may abort the process earlier (Control-C) in order to proceed with the steps below.

##### Step 5: Inspect the model


You can inspect the fitted model here:

`more logs/CROSSVALID/RunRemington_Free.py_2_0_0.1_200.txt`

You could compare to our fit by running:

`git diff logs/CROSSVALID/RunRemington_Free.py_2_0_0.1_200.txt`

The first two lines include information about the losses (held-out NLL in the first line; in-sample losses in the second line).
Subsequent lines indicate fitted numerical parameters, explained under Model Specification below.



You can inspect the final heldout NLL:

`more losses/RunRemington_Free.py_2_0_0.1_200.txt.txt`

If you ran the process to completion, it should be something a bit over 2742. On this dataset, all models assuming a non-flat prior reach a similar heldout NLL. Note that the paper reports relative NLL (Delta NLL) compared to the models reported in the main paper, and averaged across 10 folds. NLL will vary quite a bit over the 10 folds.

You can create a plot of the resulting model by running

`python3 RunRemington_Free_VIZ.py 2 0 0.1 200`

The resulting plot is stored [here](code/figures/RunRemington_Free_VIZ.py_2_0_0.1_200.pdf) at:

`figures/RunRemington_Free_VIZ.py_2_0_0.1_200.pdf`

It should show a unimodal prior, fitted nonparametrically, and a bias dominated by attraction.

You can fit other models similarly, using the scripts listed below.


For the implementation of the algorithm, `RunRemington_Free.py` includes extensive commenting.
The model itself is implemented in the function `computeBias(...)`, which is largely identical across model variants. Other parts of the scripts, however, differ, due to different model parameterizations and because we sometimes adapted optimization schemes (ordinary SG or SignGD; steo size scheme) to model variants to improve convergence behavior. Faster convergence might be achieved with additional tuning of optimization schemes.


To understand differences between scripts, running e.g.
`vimdiff RunRemington_Free.py RunRemington_Lognormal.py`
or 
`vimdiff RunRemington_Free.py RunRemington_Free_Zero.py`
can be helpful.

##### Running on your own data

If you want to apply the model to your own data, you can overwrite the data loading script loadRemington.py: `cp loadRemington_Simulated.py loadRemington.py`, where `loadRemington_Simulated.py` loads the dataset stored [here](data/simulated.txt).
That file contains two columns, the first one listing stimuli and the second one responses.
You can replace these with your own dataset.
Additional columns can be used to indicate conditions; these would then have to be loaded into separate arrays in `loadRemington.py`.


## Quantitative Evaluation

* [Collect quantitative results](evaluateCrossValidationResults_Remington.py), 
* [NLL by exponent (full)](code/figures/evaluateCrossValidationResults_Remington_StopNoImpQ.py_RelativeLF.pdf)
* [NLL by exponent (simple)](code/figures/evaluateCrossValidationResults_Remington_StopNoImpQ.py_simple.pdf)



## Model specification:


* `sigma2_stimulus` (1): stimulus noise, here unused (zero)
* `log_motor_var` (1): motor noise, parameterized via logarithm
* `sigma_logit` (1): log parameterization of sensory noise
* `mixture_logit` (1): guessing rate, parameterized via logit

Parameterization of the resource allocation: The resource allocation only has one free parameter phi, contained in the field `volume[0]`.

Parameterization of the prior:

* The flat prior has no free parameters.
* The freely fitted prior has a parameter `prior` of size (200,).
* The normal priors have scalar parameters `prior_mu` and `prior_sigma2`.

As elsewhere, we generally use  ordinary GD by default, but found SignGD to substantially speed up convergence in some cases.


## Fitting Scripts
### Batch Scripts
* [run model fitting across folds and exponents](runCrossValidAllRemington.py)
* [run all visualization scripts](createSIFigures_Remington.sh)


### p=0
* flat prior: [fitting](RunRemington_Flat_Zero.py), [visualize](RunRemington_Flat_Zero_VIZ.py), [resulting plot](code/figures/RunRemington_Flat_Zero_VIZ.py_0_0_0.1_200.pdf)
* free prior: [fitting](RunRemington_Free_Zero.py), [visualize](RunRemington_Free_Zero_VIZ.py), [resulting plot](code/figures/RunRemington_Free_Zero_VIZ.py_0_0_0.1_200.pdf)
* normal (in sensory space) prior: [fitting](RunRemington_Lognormal_Zero.py), [visualize](RunRemington_Lognormal_Zero_VIZ.py), [resulting plot](code/figures/RunRemington_Lognormal_Zero_VIZ.py_0_0_0.1_200.pdf)
* normal (in stimulus space) prior: [fitting](RunRemington_Normal_Zero.py), [visualize](RunRemington_Normal_Zero_VIZ.py), [resulting plot](code/figures/RunRemington_Normal_Zero_VIZ.py_0_0_0.1_200.pdf)


### Positive Exponents
* flat prior: [fitting](RunRemington_Flat.py), [visualize](RunRemington_Flat_VIZ.py), [batch visualize](RunRemington_Flat_VIZ_ACREXP.py), [resulting plot](code/figures/RunRemington_Flat_VIZ_ACREXP.py_ALL_0_0.1_200.pdf)
* normal (in stimulus space) prior: [fitting](RunRemington_Normal.py), [visualize](RunRemington_Normal_VIZ.py) [batch visualize](RunRemington_Normal_VIZ_ACREXP.py), [resulting plot](code/figures/RunRemington_Normal_VIZ_ACREXP.py_ALL_0_0.1_200.pdf)
* free prior: [fitting](RunRemington_Free.py), [visualize](RunRemington_Free_VIZ.py), [batch visualize](RunRemington_Free_VIZ_ACREXP.py), [resulting plot](code/figures/RunRemington_Free_VIZ_ACREXP.py_ALL_0_0.1_200.pdf)
* normal (in sensory space) prior: [fitting](RunRemington_Lognormal.py), [visualize](RunRemington_Lognormal_VIZ.py), [batch visualize](RunRemington_Lognormal_VIZ_ACREXP.py), [resulting plot](code/figures/RunRemington_Lognormal_VIZ_ACREXP.py_ALL_0_0.1_200.pdf)




## Further Visualization
For Main Paper, Figure 7:

* [magnitudes of attraction and repulsion](code/figures/RunRemington_Lognormal_Zero_VIZ.py_0_0_0.1_200_Magnitudes.pdf), [P/P Ratio](code/figures/RunRemington_Lognormal_Zero_VIZ.py_0_0_0.1_200_PPRatio.pdf)

Plot Original Data:

* [visualize](RunRemington_Lognormal_VIZ_OnlyHuman_ErrBar.py), [resulting plot](code/figures/RunRemington_Lognormal_VIZ_OnlyHuman_ErrBar.py_2_0_0.1_200.pdf)


## Simulate Data
* flat prior: [simulate](SimulateRemington_Flat_Zero.py), [fit model](RunRemington_Flat_Zero_OnSim.py), [visualize](RunRemington_Flat_Zero_OnSim_VIZ.py), [resulting plot](code/figures/RunRemington_Flat_Zero_OnSim_VIZ.py_0_0_0.1_200.pdf)
* normal (in sensory space) prior: [simulate](SimulateRemington_Lognormal_Zero.py), [fit model](RunRemington_Lognormal_Zero_OnSim.py), [visualize](RunRemington_Lognormal_Zero_OnSim_VIZ.py), [resulting plot](code/figures/RunRemington_Lognormal_Zero_OnSim_VIZ.py_0_0_0.1_200.pdf)


python3 evaluateCrossValidationResults_Remington_StopNoImpQ_Flat_OnSim.py ; python3 evaluateCrossValidationResults_Remington_StopNoImpQ_Lognormal_OnSim.py

