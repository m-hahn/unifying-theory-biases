# Code and Fitted Models


This directory contains code and fitted models.


We provide a walk-through for the full workflow for the Remington et al 2018 dataset at [Remington/README.md](code/Remington/README.md).
We also provide all scripts and intermediate results for the other datasets.

* Scripts for [de Gardelle et al 2011](code/Gardelle/README.md), [Tomassini et al 2010](code/Tomassini/README.md), [Gekas et al 2013](code/Gekas/README.md), [Remington et al 2018](code/Remington/README.d), [Xiang et al 2021](Xiang/README.md), [Bae et al 2015](code/Bae/README.md).
* [Lp Estimators](code/Estimators/README.md)
* [Fitted models](code/logs/), [recorded NLL statistics](code/losses/), [visualizations](code/figures/)
* [Numerical Simulations](code/Simulation/)



The datasets for Remington et al 2018 and Xiang et al 2021 are freely available. We obtained datasets for de Gardelle et al 2011, Tomassini et al 2010, Gekas et al 2013 from the respective authors, and do not have permission to disseminate them. For each dataset, we provide instructions where our code expects the original data.


## Workflow

### General Information
We developed and tested the codebase using

* Python 3.7
* torch: PyTorch version `1.11.0`
* matplotlib '3.5.3'

and other dependencies provided [here](requirements.txt).

If you have a GPU available, you can accordingly adapt the version of PyTorch to one compatible with your CUDA version, following instructions at https://www.pytorch.org/. All code can in principle be run without a GPU, but acceleration in model fitting can be substantial.

You can then set an environment variable to indicate the device to be used:

`export BIAS_MODEL_DEVICE=cuda` for GPU

`export BIAS_MODEL_DEVICE=cpu` for CPU

If you do not set it, the model will run without GPU by default.

Visualization scripts often only work in CPU mode; you can control this by setting the environment variable appropriately.

### Running Code
We recommend running our code within a Conda Virtual Environment.
To start, create a virtual environment:

`conda create -n perceptual_biases python=3.7 anaconda`

then activate the environment `perceptual_biases`, and run (inside this directory)

`pip install -r requirements.txt`

We provide a full workflow based on freely available data at [Remington/README.md](Remington/README.md).


## General Notes on Python Scripts

### Components
Each fitting script requires the following core components:

* data loader

* specification of the model's parameters as a dict of PyTorch tensors

* choice and configuration of loss function implementation

* configure GD optimizer: learning rate, momentum

* optimizer routine: standard GD or SignGD; intervals at which held-out NLL is evaluated; learning rate schedule; termination criterion

The general design principle in the codebase is to have separate Python scripts for each model variant (combinations of prior and encoding), which will often only differ in a few lines (a tool such as `vimdiff` is helpful in understanding how two model variants differ).

Fitting scripts primarily differ in preparations specific to the dataset, the parameterization, and the stepsize scheduling applied by the fitting method.


The model itself is implemented in the function `computeBias(...)`, which is largely identical across model variants and (with small adjustments) even across datasets. Other parts of the scripts, however, differ, due to different model parameterizations and because we sometimes adapted optimization schemes (ordinary SG or SignGD; step size scheme) to model variants to improve convergence behavior. We observed consistent results across optimization schemes (except for issues of numerical stability, which the chosen schemes avoid); we designed the step size decay parameters heuristically to achieve fast convergence and avoid numerical problems. Often, schemes had to be adapted for the p=0 special case. The general approach is to decay the step size when the loss hasn't decreased in a while, and stop when this has been happening for a large number of iterations. A systematic investigation of different schemes is beyond the scope of this work due to compositional cost, though it could be quite useful in speeding up convergence.




### Running a script

The fitting method can be substantially accelerated by running on a GPU.

Usually, fitting and visualization scripts expect four positional arguments: a loss function exponent (0,2,...), one of the ten folds used for cross-validation (0,...,9), a regularization weight, and the size N of the discretized grid. The latter two are as follows:

Dataset | Regularization | Grid Size 
-- | -- | -- 
de Gardelle et al 2011 | 10.0 | 180
Tomassini et al 2010 | 0.1 | 180
Gekas et al 2013 | 1.0 | 180
Xiang et al 2021 | 10.0 | 151
Remington et al 2018 | 0.1 | 200
Bae et al 2015 | 1.0 | 180

[Remington/README.md](code/Remington/README.md) has examples for the time interval perception dataset.



Correspondingly, files under `figures/`, `losses/`, and `logs/CROSSVALID/` also have those four suffixes, separated by underscores.
Four models with per-subject adjustment (primarily fitted for the motion direction dataset), there is additionally the number of sine/cosine components in the per-subject adjustments, which is K=50 as described in SI Appendix.


## Visualizations Relevant to Figures in Main Paper

* Figure 1:
[d](code/figures//simulate_ExampleAttraction.py_OVERALL.pdf)
[e](code/figures//simulate_ExampleRepulsion.py_OVERALL.pdf)
[f](code/figures//simulate_ExampleScalar.py_OVERALL.pdf)
[g](code/figures//simulate_ExampleLoss.py_8.pdf)
[h](code/figures//simulate_SensoryNoise.py_8.pdf)
[i](code/figures//simulate_StimulusNoise.py_8.pdf)

* Figure 2: [a](code/figures/simulate_SlowPrior.py_OVERALL.pdf) [b](code/figures/simulate_FastPrior.py_OVERALL.pdf)

* Figure 3: See [source](mainPaper_figures/figure3_ver_free.tex).

* Figure 4: [source](mainPaper_figures/figure4.tex), [a](code/figures/RunGekas_CosinePLoss_NoShiftedPrior_Fourier_ELBO_VIZ_Encoding_MainPaper.py_4_0_1.0_180.pdf)
[b](code/figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_MainPaper.py_4_0_1.0_180.pdf)
[e](code/figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_Stylized.py_4_0_1.0_180.pdf)



* Figure 5: See [source](code/mainPaper_figures/figure5.tex).


* Figure 6: [source](figure6_united.tex), 
[a](code/figures/RunBae_FreePrior_Undelayed_CosineLoss_VIZ3MainPaper.py_4_0_1.0_180.pdf)
[b](code/figures/RunBae_UniformPrior_Undelayed_CosineLoss_VIZ3MainPaper.py_4_0_1.0_180.pdf)
[c](code/figures/RunBae_UniformEncoding_Undelayed_CosineLoss_VIZ3MainPaper.py_4_0_1.0_180.pdf)
[d](code/figures/RunBae_FreePrior_Undelayed_CosineLoss_VIZ3_Regression_VizOnlyBias.py_4_0_1.0_180.pdf)



* Figure 7: See [source](code/mainPaper_figures/figure7.tex).


