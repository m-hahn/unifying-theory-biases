import glob
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sys
import torch
from loadXiang import *
from lpEstimator import LPEstimator
from matplotlib import rc
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import difference
from util import product
from util import savePlot
from util import sech
from util import toFactor

__file__ = __file__.split("/")[-1]
rc('font', **{'family':'FreeSans'})

# Helper Functions dependent on the device

OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
assert P > 0

FOLD_HERE = int(sys.argv[2])
assert FOLD_HERE == 2
REG_WEIGHT = float(sys.argv[3])
GRID = int(sys.argv[4])

# Helper Functions dependent on the device

##############################################
assert (groups == AveStim).all()
assert (observations_x == target).all()
assert (observations_y == response).all()

#############################################################
# Part: Partition data into folds. As described in the paper,
# this is done within each subject.
N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

Fold = 0*Subject
FoldsBySubject = {x : 1000 * list(range(N_FOLDS)) for x in range(int(min(Subject)), int(max(Subject))+1)}
for x in FoldsBySubject:
   randomGenerator.shuffle(FoldsBySubject[x])
IndexBySubject = {x : 0 for x in range(int(min(Subject)), int(max(Subject))+1)}
for i in range(Subject.size()[0]):
    s = int(Subject[i])
    Fold[i] = FoldsBySubject[s][IndexBySubject[s]]
    IndexBySubject[s]+=1

assert (groups == AveStim).all()
assert (observations_x == target).all()
assert (observations_y == response).all()

MIN_GRID = 0
MAX_GRID = 151

CIRCULAR = False

INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
def point(p, reference):
    assert not CIRCULAR
    return p

grid_indices_here = MakeLongTensor([[point(y,x) for x in range(GRID)] for y in range(GRID)])

xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))

xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

stimulus = stimulus_
responses = responses_

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

GROUPS = sorted(list(set(groups.cpu().numpy().tolist())))

SCALE=200
class LPEstimator(torch.autograd.Function):
    """
    We can implement our own custom autograd Functions by subclassing
    torch.autograd.Function and implementing the forward and backward passes
    which operate on Tensors.
    """

    @staticmethod
    def forward(ctx, grid_indices_here, posterior):
        """
        In the forward pass we receive a Tensor containing the input and return
        a Tensor containing the output. ctx is a context object that can be used
        to stash information for backward computation. You can cache arbitrary
        objects for use in the backward pass using the ctx.save_for_backward method.
        """
        grid_indices_here = grid_indices_here/SCALE
        n_inputs, n_batch = posterior.size()
        initialized = (grid_indices_here.data * posterior).detach().sum(dim=0).data
        result = initialized.clone()

        for itera in range(20):

          if OPTIMIZER_VERBOSE:
            loss = (((result.unsqueeze(0) - grid_indices_here).abs().pow(P)) * posterior.detach()).sum(dim=0).sum() / n_batch
          loss_gradient = ((P * torch.sign(result.unsqueeze(0) - grid_indices_here) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-1)) * posterior.detach()).sum(dim=0) / n_batch
          loss_gradient2 = ((P * (P-1) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-2)) * posterior.detach()).sum(dim=0) / n_batch
          loss_gradient2 = loss_gradient2 + (1e-10 if P == 10 else 0)
          if (loss_gradient == 0).any() and False:
             print("Where the loss_gradient is zero: ", posterior.max())
          result -= loss_gradient / loss_gradient2
          if torch.isnan(result).any():
             print(loss_gradient[torch.isnan(result)])
             print(loss_gradient2[torch.isnan(result)])
             print(loss_gradient2.abs().min())
             assert False

          if float(loss_gradient.abs().max()) < 1e-5:
              break

        if float(loss_gradient.abs().max()) >= 1e-5:
            print("WARNING", float(loss_gradient.abs().max()), "after ", itera, " Newton steps")
        if P == 2:
            assert itera == 0
        ctx.save_for_backward(grid_indices_here, posterior, result)

        return SCALE*result

    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor containing the gradient of the loss
        with respect to the output, and we need to compute the gradient of the loss
        with respect to the input.
        """
        grid_indices_here, posterior, result = ctx.saved_tensors
        initialized = (grid_indices_here.data * posterior).detach().sum(dim=0).data

        F = ((P * torch.sign(result.unsqueeze(0) - grid_indices_here) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-1)) * posterior.detach()).sum(dim=0)

        assert F.abs().mean() < 0.001, F
        dF_posterior = ((P * torch.sign(result.unsqueeze(0) - grid_indices_here) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-1)))
        dF_result = ((P * (P-1) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-2)) * posterior.detach()).sum(dim=0)
        if P == 10:
          dF_posterior = dF_posterior+1e-12
          dF_result = dF_result + 1e-9

        gradient = - ((1 / dF_result).unsqueeze(0) * dF_posterior).detach().data
        if torch.isnan(gradient).any():
           print(dF_posterior[torch.isnan(gradient)])
           assert False
        if (dF_result == 0).any():
           assert False

        gradient = grad_output.unsqueeze(0) * gradient

        return None, gradient*SCALE

init_parameters = {}

init_parameters["sigma2_stimulus"] = MakeFloatTensor([0]).view(1)
init_parameters["log_motor_var"] = MakeFloatTensor([0]).view(1)
init_parameters["sigma_logit"] = MakeFloatTensor(2*[-3]).view(2)
init_parameters["mixture_logit"] = MakeFloatTensor([-1]).view(1)
init_parameters["prior"] = MakeZeros(len(GROUPS), GRID)

init_parameters["volume"] = -torch.log(1+grid)
init_parameters["volume"] = MakeZeros(1)
for _, y in init_parameters.items():
    y.requires_grad = True

print("Logs:")
SCRIPT = __file__
print("\n".join(glob.glob(f"logs/CROSSVALID/{SCRIPT}*.txt")))
FILE = f"logs/CROSSVALID/{SCRIPT}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"
if os.path.exists(FILE):
 from loadModel import loadModel
 loadModel(FILE, init_parameters)
else:
    print("Starting from sratch")

assert "volume" in init_parameters
for _, y in init_parameters.items():
    y.requires_grad = True

#############################################################
learning_rate = 0.0001
optim = torch.optim.SGD([y for _, y in init_parameters.items()], lr=learning_rate, momentum=0.3)

shapes = {x : y.size() for x, y in init_parameters.items()}

#############################################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 1

## These are negative squared distances (for interval spaces) or
## trigonometric functions (for circular spaces), with
## some extra factors for numerical purposes.
## Exponentiating a `similarity` function and normalizing
## is equivalent to the Gaussian / von Mises density.
## The purpose of specifying these as `closeness` or `distance`,
## rather than simply calling squared or trigonometric
## functions is to  flexibly reuse the same model code for
## both interval and circular spaces.
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    assert (x<=STIMULUS_SPACE_VOLUME).all(), x.max()
    return -((x/STIMULUS_SPACE_VOLUME).pow(2))/2
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    assert (x<=1).all(), x.max()
    return -(x.pow(2))/2

# Run the model
#############################################################
# Part: Run the model. This function implements the model itself:
## calculating the likelihood of a given dataset under that model
## and---if the computePredictions argument is set to True--- computes
## the bias and variability of the estimate.
def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, StimulusSD=None, computePredictions=False, subject=None, sigma_stimulus=None, sigma2_stimulus=None, group_=None, condition=None, folds=None, lossReduce='mean', log_prior=None):

 # Part: Obtain the motor variance by exponentiating the appropriate model parameter
 motor_variance = 1e-5 + torch.exp(- parameters["log_motor_var"])
 # Part: Obtain the sensory noise variance. We parameterize it as a fraction of the squared volume of the size of the sensory space
 sigma2 = (SENSORY_SPACE_VOLUME * SENSORY_SPACE_VOLUME)*torch.sigmoid(sigma_logit)
 # Part: Obtain the transfer function as the cumulative sum of the discretized resource allocation (referred to as `volume` element due to the geometric interpretation by Wei&Stocker 2015)
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)
 if True:
  # Part: Select data for the relevant fold
  folds = MakeLongTensor(folds)
  MASK = torch.logical_and(torch.logical_and(Condition==condition,groups==group_), (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
  stimulus = stimulus_[MASK]
  responses = responses_[MASK]

  if sigma2_stimulus > 0:
    assert False
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))
    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  likelihoods = torch.softmax((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2)  + volumeElement.unsqueeze(1).log(), dim=0)
  log_likelihoods = torch.log_softmax((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2)  + volumeElement.unsqueeze(1).log(), dim=0)

  log_posterior = log_prior.unsqueeze(1) + log_likelihoods.t()
  if P == 10:
    log_posterior = 0.99 * log_posterior
  posterior = torch.softmax(log_posterior, dim=0)
  if posterior.max() == 1 and False:
     print(log_posterior[:,0])
     print(posterior[0,0])
     print(log_prior[0])
     print(log_likelihoods[:,0])
     print(log_likelihoods[:,1])
     print(posterior[1,1])
     quit()

  if torch.isnan(posterior).any():
     assert False
  if P == 2:
    bayesianEstimate = ((grid[grid_indices_here] * posterior).sum(dim=0))
  else:
    bayesianEstimate = LPEstimator.apply(grid[grid_indices_here], posterior)

  ## Compute the motor likelihood
  motor_variances = motor_variance

  ## `error' refers to the stimulus similarity between the estimator assigned to each m and
  ## the observations found in the dataset.
  ## The Gaussian or von Mises motor likelihood is obtained by exponentiating and normalizing
  error = (SQUARED_STIMULUS_SIMILARITY((bayesianEstimate.unsqueeze(0) - responses.unsqueeze(1)))/motor_variances.unsqueeze(0))

  log_normalizing_constants = torch.logsumexp(SQUARED_STIMULUS_SIMILARITY((grid.view(-1,1) - bayesianEstimate.view(1,-1)))/motor_variances.unsqueeze(0), dim=0)

  log_motor_likelihoods = (error) - log_normalizing_constants.view(1, -1)

  motor_likelihoods = torch.exp(log_motor_likelihoods)

  uniform_part = torch.sigmoid(parameters["mixture_logit"])
  ## The full likelihood then consists of a mixture of the motor likelihood calculated before, and the uniform
  ## distribution on the full space.
  motor_likelihoods = (1-uniform_part) * motor_likelihoods + (uniform_part / (GRID) + 0*motor_likelihoods)

  # Now the loss is obtained by marginalizing out m from the motor likelihood
  if lossReduce == 'mean':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().mean()
  elif lossReduce == 'sum':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().sum()
  else:
    assert False

  if computePredictions:

     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)

     bayesianEstimate_avg_byStimulus = (bayesianEstimate_byStimulus * likelihoods).sum(dim=0)

     bayesianEstimate_sd_byStimulus = (difference(bayesianEstimate_avg_byStimulus.view(1,-1), bayesianEstimate_byStimulus.view(-1, 1)).pow(2) * likelihoods).sum(dim=0).sqrt()
     bayesianEstimate_sd_byStimulus = (bayesianEstimate_sd_byStimulus.pow(2) + motor_variances).sqrt()
     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = (posteriorMaxima.unsqueeze(1) * likelihoods).sum(dim=0)
     encodingBias = (grid.unsqueeze(1) * likelihoods).sum(dim=0)
     attraction = (posteriorMaxima-encodingBias)

  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction

def retrieveObservations(x, group_, condition):
     y_set = []
     sd_set = []
     for x in x_set:
        y_here = observations_y[torch.logical_and(Condition==condition, torch.logical_and(xValues == x, groups==group_))]

        y_set.append(float(y_here.mean() - grid[x]))
        sd_set.append(float(math.sqrt(float(y_here.pow(2).mean() - y_here.mean().pow(2)))))
     y_set = MakeFloatTensor(y_set).cpu()
     sd_set = MakeFloatTensor(sd_set).cpu()
     return y_set, sd_set

def angleDiff(x):

    return -torch.cos(math.pi*x/180)

def model(grid):

  lossesBy500 = []
  crossLossesBy500 = []
  noImprovement = 0
  global optim, learning_rate
  averageLossOver100 = [0]
  for iteration in range(10000000):
   parameters = init_parameters
   ## In each iteration, recompute
   ## - the resource allocation (called `volume' due to a geometric interpretation)
   ## - the prior

   epsilon = 1
   volume = SENSORY_SPACE_VOLUME * torch.softmax(-torch.log(epsilon + grid), dim=0)
   ## For interval datasets, we're norming the size of the sensory space to 1

   loss = 0

   grid_cpu = grid.cpu()
   if iteration % 500 == 0:
     figure, axis = plt.subplots(4, 10, figsize=(20,20))
     axis[0,0].scatter(grid_cpu, volume.detach().cpu())
     axis[0,0].plot([grid_cpu[0], grid_cpu[-1]], [0,0])
     x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   for group_ in GROUPS:
    for condition in [0,1]:
     prior = torch.where((grid-group_).abs() <= 15, 1+MakeZeros(GRID), .001+MakeZeros(GRID))
     prior = prior / prior.sum()
     log_prior = prior.log()
     loss_model, bayesianEstimate_model, bayesianEstimate_sd_byStimulus_model, attraction = computeBias(xValues, init_parameters["sigma_logit"][condition], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, StimulusSD=0, computePredictions=(iteration%500 == 0), subject=None, sigma_stimulus=0, sigma2_stimulus=0, group_=group_, condition=condition, folds=trainFolds, lossReduce='sum', log_prior=log_prior)
     loss += loss_model

     if iteration % 500 == 0:
       axis[(group_-GROUPS[0])//7,3 + (group_-GROUPS[0]) % 7].scatter(grid_cpu, prior.detach().cpu())
       axis[(group_-GROUPS[0])//7,3 + (group_-GROUPS[0]) % 7].set_ylim(0,1.1*float(prior.max()))

       axis[0,0].plot([grid_cpu[0], grid_cpu[-1]], [0,0])
       y_set_data, sd_set_data = retrieveObservations(x, group_, condition)

       axis[condition+2,1].scatter([grid_cpu[q] for q in x_set], y_set_data)
       axis[condition,1].scatter([grid_cpu[q] for q in x_set], (bayesianEstimate_model.detach().cpu()-grid_cpu)[x_set].detach())

       axis[condition+2,2].scatter([grid_cpu[q] for q in x_set], sd_set_data)
       axis[condition,2].scatter([grid_cpu[q] for q in x_set], bayesianEstimate_sd_byStimulus_model[x_set].detach().cpu().detach())

   if iteration % 500 == 0:

     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.pdf")

     plt.close()

   if iteration % 500 == 0 and iteration > 0:
     crossValidLoss = 0
     for group_ in GROUPS:
      for condition in [0,1]:
       prior = torch.where((grid-group_).abs() <= 15, 1+MakeZeros(GRID), .001+MakeZeros(GRID)).cuda()
       prior = prior / prior.sum()
       loss_model, bayesianEstimate_model, bayesianEstimate_sd_byStimulus_model, attraction = computeBias(xValues, init_parameters["sigma_logit"][condition], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, StimulusSD=0, computePredictions=(iteration%500 == 0), subject=None, sigma_stimulus=0, sigma2_stimulus=0, group_=group_, condition=condition, folds=testFolds, lossReduce='sum', log_prior=log_prior)
       crossValidLoss += loss_model

   ## Part: Regularization
   ## Compute the regularization term.
   ## This is only used for nonparametric components, and is zero for parametric model components.
   regularizer_total = 0
   assert not CIRCULAR

   loss = loss * (len(GROUPS)*2/ observations_x.size()[0])
   print("LOSS", loss)
   ## Add regularization
   loss = loss + REG_WEIGHT * regularizer_total

   optim.zero_grad()
   loss.backward()
   maximumGradNorm = []
   largestGradNorm = 0
   for w in init_parameters:
     if init_parameters[w].grad is not None:
      maximumGradNorm.append(w)
      if torch.isnan(init_parameters[w].grad).any():
          print(w)
          print("NAN")
          quit()
      gradNormMax = float(init_parameters[w].grad.abs().max())
      maximumGradNorm.append(gradNormMax)
      largestGradNorm = max(largestGradNorm, float(gradNormMax))
      init_parameters[w].grad.data = torch.sign(init_parameters[w].grad.data)

   print(largestGradNorm, maximumGradNorm)

   optim.step()
   # Experimental scheme that anneals the learning rate based on changes in NLL over 100 iterations.
   averageLossOver100[-1] += float(loss) / 100
   if iteration % 10 == 0:
     print(iteration, averageLossOver100[-3:-1], loss, init_parameters["sigma_logit"], "mixture_logit", init_parameters["mixture_logit"], "log_motor_var", init_parameters["log_motor_var"], learning_rate, sys.argv)
   if iteration % 100 == 0 and iteration > 0:
       averageLossOver100.append(0)

   if iteration % 500 == 0 and iteration > 0:
       lossesBy500.append(float(loss))
       crossLossesBy500.append(float(crossValidLoss))
       if len(lossesBy500) == 1 or float(loss) <= min(lossesBy500):
        with open(f"losses/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt", "w") as outFile:
           print(float(crossValidLoss), file=outFile)
        with open(f"logs/CROSSVALID/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt", "w") as outFile:
           print(float(loss), "CrossValid", float(crossValidLoss), "CrossValidLossesBy500", " ".join([str(q) for q in crossLossesBy500]), file=outFile)
           print(iteration, "LossesBy500", " ".join([str(q) for q in lossesBy500]), file=outFile)
           for z, y in init_parameters.items():
               print(z, "\t", y.detach().cpu().numpy().tolist(), file=outFile)
           print("========", file=outFile)
           print("\t".join([str(q) for q in maximumGradNorm]), file=outFile)
       if largestGradNorm < 1e-5:
          print("Converged to stationary point")
          break
   if iteration % 100 == 0 and iteration > 0:
       if len(averageLossOver100) > 2 and float(averageLossOver100[-2]) >= averageLossOver100[-3]-1e-5:
         learning_rate *= 0.3
         optim = torch.optim.SGD([y for _, y in init_parameters.items()], lr=learning_rate, momentum=0.3)
   if iteration % 100 == 0 and iteration > 0:
       if len(averageLossOver100) > 2 and float(averageLossOver100[-2]) >= min(averageLossOver100[:-2]):
         noImprovement += 1
       else:
         noImprovement = 0
       if noImprovement >= 20:
           print("Stopping", "grad norm", largestGradNorm)
           break

############################3

model(grid)
