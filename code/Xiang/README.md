# Xiang


## Model specification:

* `sigma2_stimulus` (1) logarithmic parameterization of the stimulus noise variance
* `log_motor_var` (1) logarithmic parameterization of the motor variance
* `sigma_logit` (2) logit parameterization of the sensory noise variance
* `mixture_logit` (1) logit parameterization of the guessing rate
* `volume` (1), unused
Prior:
* normal in sensory space: `prior_mu` (21), `prior_s` (1)
* normal in stimulus space: `prior_mu` (21), `prior_s` (21)
* flat: nothing needed

`prior_s` is a logarithmic parameterization of the variance.

The stimulus space includes {0,1,2...,149, 150}, i.e., the stimulus space has size 151, as described in the paper.


## Model scripts

* [Collect quantitative results](evaluateCrossValidationResults_Xiang_AVE.py)
[NLL by exponent (full)](code/figures/evaluateCrossValidationResults_Xiang_AVE.py_RelativeLF.pdf)
[NLL by exponent (simple)](code/figures/evaluateCrossValidationResults_Xiang_AVE.py_simple.pdf)

### MAP estimator

* [Normal in sensory space](RunXiang_ZeroExp_Lognormal_SignGD_AVE.py), 
[viz (three sample intervals)](RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_OnlyModel.py) [plot](code/figures/RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_OnlyModel.py_0_2_10.0_151.pdf);
[viz (all intervals)](RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ.py),
[for Figure 7](RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_Fig7.py)
[magnitues](code/figures/RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_Fig7.py_0_2_10.0_151_Magnitudes.pdf),
[P/P ratio](code/figures/RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_Fig7.py_0_2_10.0_151_PPRatio.pdf)

* [Normal in stimulus space](RunXiang_ZeroExp_Normal_VarSig_SignGD_AVE.py), 
[plot](code/figures/RunXiang_ZeroExp_Normal_VarSig_SignGD_AVE_VIZ.py_0_2_10.0_151.pdf),
[all priors in sensory space](code/figures/RunXiang_ZeroExp_Normal_VarSig_SignGD_AVE_VIZ.py_0_2_10.0_151_SensorySpace.pdf) [in stimulus space](code/figures/RunXiang_ZeroExp_Normal_VarSig_SignGD_AVE_VIZ.py_0_2_10.0_151_StimulusSpace.pdf)

* [Flat](RunXiang_ZeroExp_Flat_SignGD_AVE.py), 
[visualization script](RunXiang_ZeroExp_Flat_SignGD_AVE_VIZ.py), [plot](code/figures/RunXiang_ZeroExp_Flat_SignGD_AVE_VIZ.py_0_2_10.0_151.pdf)


### Positive exponents

* [Normal in sensory space](RunXiang_Lognormal_SignGD_AVE.py), 
[Normal in sensory space, exponent larger than 6](RunXiang_Lognormal_SignGD_HighExponent6_AVE.py)
plots: [p=2](code/figures/RunXiang_Lognormal_SignGD_AVE_VIZForPaper.py_2_2_10.0_151.pdf) [p=4](code/figures/RunXiang_Lognormal_SignGD_AVE_VIZForPaper.py_4_2_10.0_151.pdf) [p=6](code/figures/RunXiang_Lognormal_SignGD_AVE_VIZForPaper.py_6_2_10.0_151.pdf)
[in sensory space](code/figures/RunXiang_Lognormal_SignGD_AVE_VIZ.py_2_2_10.0_151_SensorySpace.pdf), [in stimulus space](code/figures/RunXiang_Lognormal_SignGD_AVE_VIZ.py_2_2_10.0_151_StimulusSpace.pdf)

* [Normal in stimulus space](RunXiang_Normal_VarSig_AVE.py), 
[Normal in stimulus space, exponent equals 10](RunXiang_Normal_VarSig_HighExponent6_AVE.py)
[](RunXiang_Normal_VarSig_AVE.py)
[batch viz](RunXiang_Normal_VarSig_AVE_VIZForPaper_ACREXP.py)
[viz script](RunXiang_Normal_VarSig_AVE_VIZForPaper.py)
[viz priors](RunXiang_Normal_VarSig_AVE_VIZ.py)
[across exponents](code/figures/RunXiang_Normal_VarSig_AVE_VIZForPaper_ACREXP.py_ALL_2_10.0_151.pdf)
[2](code/figures/RunXiang_Normal_VarSig_AVE_VIZForPaper.py_2_2_10.0_151.pdf)
[4](code/figures/RunXiang_Normal_VarSig_AVE_VIZForPaper.py_4_2_10.0_151.pdf)
[6](code/figures/RunXiang_Normal_VarSig_AVE_VIZForPaper.py_6_2_10.0_151.pdf)
[priors (sensory space)](code/figures/RunXiang_Normal_VarSig_AVE_VIZ.py_2_2_10.0_151_SensorySpace.pdf)
[priors (stimulus space)](code/figures/RunXiang_Normal_VarSig_AVE_VIZ.py_2_2_10.0_151_StimulusSpace.pdf)


* [Flat](RunXiang_Flat_NelderMead_AVE.py), 
[Flat, exponent larger than 4](RunXiang_Flat_SignGD_HighExponent6_AVE.py)
[across exponents](code/figures/RunXiang_Flat_SignGD_AVE_VIZForPaper_ACREXP.py_ALL_2_10.0_151.pdf)
[2](code/figures/RunXiang_Flat_SignGD_AVE_VIZForPaper.py_2_2_10.0_151.pdf)
[4](code/figures/RunXiang_Flat_SignGD_AVE_VIZForPaper.py_4_2_10.0_151.pdf)
[6](code/figures/RunXiang_Flat_SignGD_AVE_VIZForPaper.py_6_2_10.0_151.pdf)


#### SG and Nelder-Mead
The discrete nature of the stimulus set leads to difficulties in fitting, especially for higher exponents (p=8, 10) not observed in the other datasets.
As described in SI Appendix, we fitted models using Nelder-Mead initialized from the GD fits at the same p or, when p>8, at p-2, and p=10, the outcome of running Nelder-Mead at p=8.
At lower exponents, we found that Nelder-Mead found equivalent NLL as the gradient-based method.
While Nelder-Mead is much slower than the gradient-based method (even more so when not initialized from an existing fit), it avoids numerical problems at high exponents on this dataset.



## Simulated

* normal in sensory space (p=0): [simulate data](SimulateXiang_Lognormal_SIMULATE_CreateMultiple_Zero.py), [run on simulated data](RunXiang_ZeroExp_Lognormal_SignGD_AVE_OnSim.py), [viz](RunXiang_ZeroExp_Lognormal_SignGD_AVE_OnSim_VIZ_OnlyModel.py), [plot](code/figures/RunXiang_ZeroExp_Lognormal_SignGD_AVE_OnSim_VIZ_OnlyModel.py_0_2_10.0_151.pdf)

* flat (p=0) [simulate data](SimulateXiang_Flat_SIMULATE_CreateMultiple_Zero.py), [run on simulated data](RunXiang_ZeroExp_Flat_SignGD_AVE_OnSim.py), [viz](RunXiang_ZeroExp_Flat_SignGD_AVE_OnSim_VIZ_OnlyModel.py), [plot](code/figures/RunXiang_ZeroExp_Flat_SignGD_AVE_OnSim_VIZ_OnlyModel.py_0_2_10.0_151.pdf)





# Plotting Human Data

* three intervals: [plotting](RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_OnlyHuman_ErrBar.py), [plot](code/figures/RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_OnlyHuman_ErrBar.py_0_2_10.0_151.pdf)

* all intervals: [plotting](RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_AllPriors_OnlyHuman.py), [plot](code/figures/RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_AllPriors_OnlyHuman.py_0_2_10.0_151.pdf)



# Plots for Main Paper

Figure 5 uses:

`python3 RunXiang_ZeroExp_Flat_SignGD_AVE_VIZ_OnlyModel_MainPaper.py 0 2 10.0 151`

`python3 RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_OnlyHuman_MainPaper_ErrBar.py 0 2 10.0 151`

`python3 RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_OnlyModel_MainPaper.py 0 2 10.0 151`

# Simulated

python3 evaluateCrossValidationResults_Xiang_AVE_Flat_OnSim.py ; python3 evaluateCrossValidationResults_Xiang_AVE_Lognormal_OnSim.py
