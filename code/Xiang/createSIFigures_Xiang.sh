python3 RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_OnlyModel.py 0 0 10.0 151 NO_PLOT
python3 RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_OnlyHuman.py 0 0 10.0 151 NO_PLOT

python3 RunXiang_Flat_SignGD_AVE_VIZForPaper_OnlyModel.py  2 0 10.0 151 NO_PLOT
python3 RunXiang_ZeroExp_Flat_SignGD_AVE_VIZ_OnlyModel.py 0 0 10.0 151

python3 RunXiang_Normal_VarSig_AVE_VIZForPaper_ACREXP.py 2 0 10.0 151
python3 RunXiang_Flat_SignGD_AVE_VIZForPaper_ACREXP.py 2 0 10.0 151

python3 RunXiang_Lognormal_NelderMead_AVE_VIZForPaper_ACREXP.py 2 0 10.0 151 NO_PLOT
python3 RunXiang_Normal_NelderMead_AVE_VIZForPaper_ACREXP.py 2 0 10.0 151 NO_PLOT
python3 RunXiang_Flat_NelderMead_AVE_VIZForPaper_ACREXP.py 2 0 10.0 151 NO_PLOT


python3 RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ.py 0 0 10.0 151 NO_PLOT
python3 RunXiang_ZeroExp_Normal_VarSig_SignGD_AVE_VIZ.py 0 0 10.0 151 NO_PLOT
python3 RunXiang_ZeroExp_Flat_SignGD_AVE_VIZ.py 0 0 10.0 151 NO_PLOT
##python3 RunXiang_ZeroExp_SignGD_AVE4_VIZ.py 0 0 10.0 151 NO_PLOT
#for p in 2 4 6 8
#do
###python3 RunXiang_FreePrior_SignGD_HigherP_AVE_VIZForPaper.py $p 0 10.0 151 NO_PLOT
#python3 RunXiang_Lognormal_NelderMead_VIZForPaper.py $p 0 10.0 151 NO_PLOT
#python3 RunXiang_Normal_NelderMead_VIZForPaper.py $p 0 10.0 151 NO_PLOT
#python3 RunXiang_Flat_NelderMead_VIZForPaper.py $p 0 10.0 151 NO_PLOT
#done
python3 evaluateCrossValidationResults_Xiang_AVE.py
python3 RunXiang_Lognormal_SignGD_AVE_VIZ.py 2 0 10.0 151 NO_PLOT
python3 RunXiang_Normal_VarSig_AVE_VIZ.py 2 0 10.0 151 NO_PLOT
##python3 RunXiang_FreePrior_SignGD_HigherP_AVE_VIZ.py 2 0 10.0 151 NO_PLOT

python3 RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_AllPriors_OnlyHuman.py 0 0 10.0 151


python3 RunXiang_Lognormal_SignGD_AVE_VIZForPaper.py 8 0 10.0 151

python3 RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_OnlyModel_MainPaper.py 0 0 10.0 151
python3 RunXiang_ZeroExp_Flat_SignGD_AVE_VIZ_OnlyModel_MainPaper.py 0 0 10.0 151

# For Main Paper, Figure 7
python3 RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_Fig7.py 0 0 10.0 151

python3 evaluateCrossValidationResults_Xiang_AVE_Flat_OnSim.py
python3 evaluateCrossValidationResults_Xiang_AVE_Lognormal_OnSim.py
