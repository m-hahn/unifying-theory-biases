import glob
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sys
import torch
from loadXiang import *
from lpEstimator import LPEstimator
from matplotlib import rc
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import ToDevice
from util import difference
from util import product
from util import savePlot
from util import toFactor

__file__ = __file__.split("/")[-1]
rc('font', **{'family':'FreeSans'})

# Helper Functions dependent on the device

#############################################################
# Part: Collect arguments
OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
assert P > 0

FOLD_HERE = int(sys.argv[2])
#assert FOLD_HERE == 2
REG_WEIGHT = float(sys.argv[3])
GRID = int(sys.argv[4])
SHOW_PLOT = (len(sys.argv) < 6) or (sys.argv[5] == "SHOW_PLOT")
DEVICE = 'cpu'

# Helper Functions dependent on the device

##############################################
assert (groups == AveStim).all()
assert (observations_x == target).all()
assert (observations_y == response).all()

#############################################################
# Part: Partition data into folds. As described in the paper,
# this is done within each subject.
N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

Fold = 0*Subject
FoldsBySubject = {x : 1000 * list(range(N_FOLDS)) for x in range(int(min(Subject)), int(max(Subject))+1)}
for x in FoldsBySubject:
   randomGenerator.shuffle(FoldsBySubject[x])
IndexBySubject = {x : 0 for x in range(int(min(Subject)), int(max(Subject))+1)}
for i in range(Subject.size()[0]):
    s = int(Subject[i])
    Fold[i] = FoldsBySubject[s][IndexBySubject[s]]
    IndexBySubject[s]+=1

assert (groups == AveStim).all()
assert (observations_x == target).all()
assert (observations_y == response).all()

##############################################
# Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 151

CIRCULAR = False
INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
def point(p, reference):
    assert not CIRCULAR
    return p
grid_indices_here = MakeLongTensor([[point(y,x) for x in range(GRID)] for y in range(GRID)])

#############################################################
# Part: Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

stimulus = stimulus_
responses = responses_

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))
for group_ in [35, 40]:
 y_set = []
 sd_set = []
 for x in x_set:
   y_here = observations_y[torch.logical_and(xValues == x, groups==group_)]
   y_set.append(float(y_here.mean() - grid[x]))
   sd_set.append(math.sqrt(float(y_here.pow(2).mean() - y_here.mean().pow(2))))

GROUPS = sorted(list(set(groups.cpu().numpy().tolist())))

# Import/define the appropriate estimator for minimizing the loss function
SCALE=200

#############################################################
# Part: Initialize the model
init_parameters = {}
init_parameters["sigma2_stimulus"] = MakeFloatTensor([0]).view(1)
init_parameters["log_motor_var"] = MakeFloatTensor([0]).view(1)
init_parameters["sigma_logit"] = MakeFloatTensor(2*[-3]).view(2)
init_parameters["mixture_logit"] = MakeFloatTensor([-1]).view(1)
init_parameters["prior_mu"] = MakeZeros(len(GROUPS))+3
init_parameters["prior_s"] = MakeZeros(1)
init_parameters["volume"] = MakeZeros(1)
for _, y in init_parameters.items():
    y.requires_grad = True

print("Logs:")
SCRIPT = __file__.replace("_VIZForPaper", "")
print("\n".join(glob.glob(f"logs/CROSSVALID/{SCRIPT}*.txt")))
FILE = f"logs/CROSSVALID/{SCRIPT}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"
if os.path.exists(FILE):
 from loadModel import loadModel
 loadModel(FILE, init_parameters)
else:
    assert False, FILE
assert "volume" in init_parameters

#############################################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 1

## These are negative squared distances (for interval spaces) or
## trigonometric functions (for circular spaces), with
## some extra factors for numerical purposes.
## Exponentiating a `similarity` function and normalizing
## is equivalent to the Gaussian / von Mises density.
## The purpose of specifying these as `closeness` or `distance`,
## rather than simply calling squared or trigonometric
## functions is to  flexibly reuse the same model code for
## both interval and circular spaces.
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    assert (x<=STIMULUS_SPACE_VOLUME).all(), x.max()
    return -((x/STIMULUS_SPACE_VOLUME).pow(2))/2
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    assert (x<=1).all(), x.max()
    return -(x.pow(2))/2
#############################################################
# Part: Configure the appropriate estimator for minimizing the loss function
# Import/define the appropriate estimator for minimizing the loss function
SCALE=200

LPEstimator.set_parameters(GRID=GRID, OPTIMIZER_VERBOSE=OPTIMIZER_VERBOSE, P=P, SQUARED_SENSORY_SIMILARITY=SQUARED_SENSORY_SIMILARITY, SCALE=SCALE)

# Run the model
#############################################################
# Part: Run the model. This function implements the model itself:
## calculating the likelihood of a given dataset under that model
## and---if the computePredictions argument is set to True--- computes
## the bias and variability of the estimate.
def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, StimulusSD=None, computePredictions=False, subject=None, sigma_stimulus=None, sigma2_stimulus=None, group_=None, condition=None, folds=None, lossReduce='mean'):

 # Part: Obtain the motor variance by exponentiating the appropriate model parameter
 motor_variance = 1e-5 + torch.exp(- parameters["log_motor_var"])
 # Part: Obtain the sensory noise variance. We parameterize it as a fraction of the squared volume of the size of the sensory space
 sigma2 = (SENSORY_SPACE_VOLUME * SENSORY_SPACE_VOLUME)*torch.sigmoid(sigma_logit)
 # Part: Obtain the transfer function as the cumulative sum of the discretized resource allocation (referred to as `volume` element due to the geometric interpretation by Wei&Stocker 2015)
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)
 if True:
  # Part: Select data for the relevant fold
  folds = MakeLongTensor(folds)
  MASK = torch.logical_and(torch.logical_and(Condition==condition,groups==group_), (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
  stimulus = stimulus_[MASK]
  responses = responses_[MASK]

  if sigma2_stimulus > 0:
    assert False
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))
    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  # Part: Compute sensory likelihoods. Across both interval and
  ## circular stimulus spaces, this amounts to exponentiaring a
  ## `similarity`
  sensory_likelihoods = torch.softmax(((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2))  + volumeElement.unsqueeze(1).log(), dim=0)

  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    assert False
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  # Compute posterior
  posterior = prior.unsqueeze(1) * likelihoods.t()
  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  # Estimator
  if P == 0:
    bayesianEstimate = MAPIntervalEstimator.apply(grid[grid_indices_here], posterior)
  elif P == 2:
    bayesianEstimate = ((grid[grid_indices_here] * posterior).sum(dim=0))
  else:
    bayesianEstimate = LPEstimator.apply(grid[grid_indices_here], posterior)
  ## Compute the motor likelihood
  motor_variances = motor_variance

  ## `error' refers to the stimulus similarity between the estimator assigned to each m and
  ## the observations found in the dataset.
  ## The Gaussian or von Mises motor likelihood is obtained by exponentiating and normalizing
  error = (SQUARED_STIMULUS_SIMILARITY((bayesianEstimate.unsqueeze(0) - responses.unsqueeze(1)))/motor_variances.unsqueeze(0))
  log_normalizing_constants = torch.logsumexp(SQUARED_STIMULUS_SIMILARITY((grid.view(-1,1) - bayesianEstimate.view(1,-1)))/motor_variances.unsqueeze(0), dim=0)
  log_motor_likelihoods = (error) - log_normalizing_constants.view(1, -1)

  motor_likelihoods = torch.exp(log_motor_likelihoods)
  ## Obtain the guessing rate, parameterized via the (inverse) logit transform as described in SI Appendix
  uniform_part = torch.sigmoid(parameters["mixture_logit"])
  ## The full likelihood then consists of a mixture of the motor likelihood calculated before, and the uniform
  ## distribution on the full space.
  motor_likelihoods = (1-uniform_part) * motor_likelihoods + (uniform_part / (GRID) + 0*motor_likelihoods)

  # Now the loss is obtained by marginalizing out m from the motor likelihood
  if lossReduce == 'mean':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().mean()
  elif lossReduce == 'sum':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().sum()
  else:
    assert False

  if computePredictions:
     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)
     bayesianEstimate_avg_byStimulus = (bayesianEstimate_byStimulus * likelihoods).sum(dim=0)

     bayesianEstimate_sd_byStimulus = (difference(bayesianEstimate_avg_byStimulus.view(1,-1), bayesianEstimate_byStimulus.view(-1, 1)).pow(2) * likelihoods).sum(dim=0).sqrt()
     bayesianEstimate_sd_byStimulus = (bayesianEstimate_sd_byStimulus.pow(2) +STIMULUS_SPACE_VOLUME*STIMULUS_SPACE_VOLUME * motor_variances).sqrt()
     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = (posteriorMaxima.unsqueeze(1) * likelihoods).sum(dim=0)
     encodingBias = (grid.unsqueeze(1) * likelihoods).sum(dim=0)
     attraction = (posteriorMaxima-encodingBias)

  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction

def retrieveObservations(x, group_, condition):
     x_set_here = []
     y_set = []
     sd_set = []
     for x in x_set:
        y_here = observations_y[torch.logical_and(Condition==condition, torch.logical_and(xValues == x, groups==group_))]
        if sum(y_here.size()) == 0:
            continue
        y_set.append(float(y_here.mean() - grid[x]))
        sd_set.append(float(math.sqrt(float(y_here.pow(2).mean() - y_here.mean().pow(2)))))
        x_set_here.append(float(grid[x]))
     x_set_here = MakeFloatTensor(x_set_here)
     y_set = MakeFloatTensor(y_set)
     sd_set = MakeFloatTensor(sd_set)
     kernel = torch.nn.functional.softmax(-(x_set_here.unsqueeze(0)/MAX_GRID - x_set_here.unsqueeze(1)/MAX_GRID).pow(2) / (2*(0.01)**2), dim=0)
     y_set = (kernel * y_set.unsqueeze(1)).sum(dim=0)
     sd_set = (kernel * sd_set.unsqueeze(1)).sum(dim=0)
     return x_set_here, y_set, sd_set

def plotForGroup(method, group_, ax, xValues, yValues, color=None, args={}):
    if type(xValues) == type([]):
        xValues = MakeFloatTensor(xValues)
    xIndices = torch.logical_and(torch.logical_and(xValues >= group_-15, xValues <= group_+15), xValues % 5 == 0)
    if method == "scatter":
      ax.scatter(xValues[xIndices], yValues[xIndices], **args)
    else:
      ax.plot(xValues[xIndices], yValues[xIndices], **args)

def computeResources(volume, sigma2):

    return volume / torch.sqrt(sigma2) * GRID / (MAX_GRID-MIN_GRID)

def model(grid):

  lossesBy500 = []
  crossLossesBy500 = []
  noImprovement = 0
  global optim, learning_rate
  for iteration in range(1):
   parameters = init_parameters
   ## In each iteration, recompute
   ## - the resource allocation (called `volume' due to a geometric interpretation)
   ## - the prior

   epsilon = 1
   volume = SENSORY_SPACE_VOLUME * torch.softmax(-torch.log(epsilon + grid), dim=0)
   ## For interval datasets, we're norming the size of the sensory space to 1

   loss = 0

   grid_cpu = grid.cpu()
   if iteration % 500 == 0:
     assigned = ["PRI", None, "ENC", None, "ATT", "REP", "TOT", "HUM", None, "VAR", "VAH"]
     for w, k in enumerate(assigned):
         globals()[k] = w
     PAD = [w for w, q in enumerate(assigned) if (q is None or q in ["HUM", "VAH"])]
     gridspec = dict(width_ratios=[1,0.2,1,0.2,1,1,1,0,0.2,1,0])
     figure, axis = plt.subplots(1, len(gridspec["width_ratios"]), figsize=(13,1.9), gridspec_kw=gridspec)
     plt.tight_layout()
     figure.subplots_adjust(wspace=0.1, hspace=0.0)

     for w in PAD:
       axis[w].set_visible(False)

     if True:
       axis[ENC].set_title("Resources", fontsize=16)
       axis[PRI].set_title("Priors", fontsize=16)
       axis[REP].set_title("Repulsion", fontsize=16)
       axis[ATT].set_title("Attraction", fontsize=16)
     if True:
       axis[TOT].set_title("Bias (Model)", fontsize=16)
       axis[HUM].set_title("Bias (Data)", fontsize=16)
       axis[VAR].set_title("Var. (Model)", fontsize=16)
       axis[VAH].set_title("Var. (Data)", fontsize=16)
     figure.subplots_adjust(bottom=0.12,top=0.85)

     for w in range(len(assigned)):
       axis[w].tick_params(axis='both', labelsize=12)
     for condition in [0,1]:
        print("THESE SHOULD MATCH", (math.sqrt(-float(SQUARED_SENSORY_SIMILARITY(torch.ones(1))))), 1/(2*math.pi))
        axis[ENC].plot(grid_cpu, computeResources(volume.detach().cpu(), torch.sigmoid(init_parameters["sigma_logit"][condition]).detach()), linestyle = ["solid", "dotted"][condition], color="gray")
     x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

   ## Separate train and test/heldout partitions of the data
   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   print(GROUPS)
   colors = list(range(100))
   for group_id, group_ in enumerate(GROUPS):
    for condition in [0,1]:
     prior = torch.softmax(-(torch.log(1+grid)-(init_parameters["prior_mu"][group_-GROUPS[0]])).pow(2) / (2 * torch.exp(init_parameters["prior_s"])) - ((1+grid).log()), dim=0)
     loss_model, bayesianEstimate_model, bayesianEstimate_sd_byStimulus_model, attraction = computeBias(xValues, init_parameters["sigma_logit"][condition], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, StimulusSD=0, computePredictions=(iteration%500 == 0), subject=None, sigma_stimulus=0, sigma2_stimulus=0, group_=group_, condition=condition, folds=testFolds, lossReduce='sum')
     loss += loss_model

     if iteration % 500 == 0:
       axis[PRI].plot( grid_cpu, prior.detach().cpu())

       x_set_data, y_set_data, sd_set_data = retrieveObservations(x, group_, condition)

       prior_uniform = 1/GRID + MakeZeros(GRID)
       _, bayesianEstimate_model_uniform, _, _ = computeBias(xValues, init_parameters["sigma_logit"][condition], prior_uniform, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, StimulusSD=0, computePredictions=(iteration%500 == 0), subject=None, sigma_stimulus=0, sigma2_stimulus=0, group_=group_, condition=condition, folds=testFolds, lossReduce='sum')

       plotForGroup("plot", group_, axis[REP], [grid_cpu[q] for q in x_set], (bayesianEstimate_model.detach().cpu()-grid_cpu-attraction)[x_set].detach(), color=colors[group_id], args={"linestyle" : ["solid", "dotted"][condition]})

       plotForGroup("plot", group_, axis[ATT], [grid_cpu[q] for q in x_set], (attraction)[x_set].detach(), color=colors[group_id], args={"linestyle" : ["solid", "dotted"][condition]})
       plotForGroup("plot", group_, axis[TOT], [grid_cpu[q] for q in x_set], (bayesianEstimate_model.detach().cpu()-grid_cpu)[x_set].detach(), color=colors[group_id], args={"linestyle" : ["solid", "dotted"][condition]})
       plotForGroup("plot", group_, axis[HUM], x_set_data, y_set_data, color=colors[group_id], args={"linestyle" : ["solid", "dotted"][condition]})

       plotForGroup("plot", group_, axis[VAR], [grid_cpu[q] for q in x_set], bayesianEstimate_sd_byStimulus_model[x_set].detach().cpu().detach(), color=colors[group_id], args={"linestyle" : ["solid", "dotted"][condition]})
       plotForGroup("plot", group_, axis[VAH], x_set_data, sd_set_data, color=colors[group_id], args={"linestyle" : ["solid", "dotted"][condition]})

       axis[ATT].set_ylim(-20, 10)
       axis[REP].set_ylim(-20, 10)
       axis[TOT].set_ylim(-20, 10)
       axis[HUM].set_ylim(-20, 10)
       axis[VAR].set_ylim(0,15)
       axis[VAH].set_ylim(0,15)

       for z in range(len(assigned)):
         axis[z].spines['right'].set_visible(False)
         axis[z].spines['top'].set_visible(False)
         axis[z].set_xticks(ticks=[0,25,50])
       for z in [ENC]:
         axis[z].set_yticks(ticks=[0,1,2,3,4,5], labels=[0, "", "", "", "", 5])
       for z in [PRI]:
         axis[z].set_yticks(ticks=[0])
       for z in [ATT, REP, TOT, HUM]:
         axis[z].set_yticks(ticks=[-15,-10,-5,0,5], labels=["","‒10","",0,""])
       for z in [VAR, VAH]:
         axis[z].set_yticks(ticks=[0,5,10], labels=[0,"",10])

       for z in [REP, TOT, HUM, VAH]:
           axis[z].tick_params(labelleft=False)

   print("LOSS", loss)
   if iteration % 500 == 0:

     axis[ENC].set_ylim(0, 5)
     for w in range(8):
       axis[w].set_xlim(-1,70)
     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.pdf")

############################3

model(grid)
