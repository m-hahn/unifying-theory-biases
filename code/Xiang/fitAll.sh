for p in 2 4 6 8 10 do
  python3 RunXiang_Flat_NelderMead_AVE.py $p 2 10.0 151
done
for p in 2 4 6 do
  python3 RunXiang_Lognormal_SignGD_AVE.py
done
cp logs/CROSSVALID/RunXiang_Lognormal_SignGD_AVE.py_6_2_10.0_151.txt logs/CROSSVALID/RunXiang_Lognormal_SignGD_HighExponent6_AVE.py_8_2_10.0_151.txt
python3 RunXiang_Lognormal_SignGD_HighExponent6_AVE.py 8 2 10.0 151
cp logs/CROSSVALID/RunXiang_Lognormal_SignGD_HighExponent6_AVE.py_8_2_10.0_151.txt logs/CROSSVALID/RunXiang_Lognormal_SignGD_AVE.py_8_2_10.0_151.txt 
cp logs/CROSSVALID/RunXiang_Lognormal_SignGD_AVE.py_8_2_10.0_151.txt logs/CROSSVALID/RunXiang_Lognormal_SignGD_HighExponent6_AVE.py_10_2_10.0_151.txt
python3 RunXiang_Lognormal_SignGD_HighExponent6_AVE.py 10 2 10.0 151
cp logs/CROSSVALID/RunXiang_Lognormal_SignGD_HighExponent6_AVE.py_10_2_10.0_151.txt logs/CROSSVALID/RunXiang_Lognormal_SignGD_AVE.py_10_2_10.0_151.txt 

for p in 2 4 6 do
  python3 RunXiang_Normal_VarSig_AVE.py
done
cp logs/CROSSVALID/RunXiang_Normal_VarSig_AVE.py_6_2_10.0_151.txt logs/CROSSVALID/RunXiang_Normal_VarSig_HighExponent6_AVE.py_8_2_10.0_151.txt
python3 RunXiang_Normal_VarSig_HighExponent6_AVE.py 8 2 10.0 151
cp logs/CROSSVALID/RunXiang_Normal_VarSig_HighExponent6_AVE.py_8_2_10.0_151.txt logs/CROSSVALID/RunXiang_Normal_VarSig_AVE.py_8_2_10.0_151.txt 
cp logs/CROSSVALID/RunXiang_Normal_VarSig_AVE.py_8_2_10.0_151.txt logs/CROSSVALID/RunXiang_Normal_VarSig_HighExponent6_AVE.py_10_2_10.0_151.txt
python3 RunXiang_Normal_VarSig_HighExponent6_AVE.py 10 2 10.0 151
cp logs/CROSSVALID/RunXiang_Normal_VarSig_HighExponent6_AVE.py_10_2_10.0_151.txt logs/CROSSVALID/RunXiang_Normal_VarSig_AVE.py_10_2_10.0_151.txt 



python3 RunXiang_ZeroExp_Flat_SignGD_AVE.py 0 2 10.0 151
python3 RunXiang_ZeroExp_Lognormal_SignGD_AVE.py 0 2 10.0 151
python3 RunXiang_ZeroExp_Normal_VarSig_SignGD_AVE.py 0 2 10.0 151

