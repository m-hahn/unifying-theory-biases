import torch
import glob
from util import MakeZeros

from util import MakeFloatTensor

from util import MakeLongTensor

from util import ToDevice
from util import toFactor

with open("data/XIANG/Data/datanewfile.csv", "r") as inFile:
    data = [x.split(",") for x in inFile.read().strip().split("\n")]
    header = data[0]
    header = dict(list(zip(header, range(len(header)))))
    data = data[1:]

    Subject = MakeLongTensor([int(x[header["Subject"]]) for x in data])
    blocks = MakeLongTensor([int(x[header["Block"]]) for x in data])
    groups_raw = MakeLongTensor(toFactor(list(zip(Subject.cpu().numpy().tolist(), blocks.cpu().numpy().tolist()))))
    target = MakeFloatTensor([int(x[header["Stimulus"]]) for x in data])
    response = MakeFloatTensor([int(x[header["Response"]]) for x in data])
    Condition = MakeLongTensor([int(x[header["Condition"]]) for x in data])
    AveStim = MakeLongTensor([int(x[header["AveStim"]]) for x in data])

MASK = torch.logical_and(response>=0, response<=150)
print(MASK.float().size())
print(MASK.float().sum())
print(MASK.float().mean())
Subject=Subject[MASK]
blocks=blocks[MASK]
groups_raw=groups_raw[MASK]
target=target[MASK]
response=response[MASK]
Condition=Condition[MASK]
AveStim=AveStim[MASK]

groups = AveStim

observations_x = target
observations_y = response


