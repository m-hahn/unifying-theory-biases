import sys

prefix = ""
inp = sys.argv[1]
haveSavedPlot = False

hasObtainedFold = False
hasObtainedRegWeight = False
hasObtainedGrid = False


#ExampleRemington_Inference_Discretized_Likelihood_Nonparam_MotorNoise7_FreePrior_Weber_Flat_GPU_CROSSVALID_Balance_VIZ_ACREXP.py
with open(inp, "r") as inFile:
 with open(inp.replace(".py", "_ACREXP.py"), "w") as outFile:
   print("from matplotlib.backends.backend_pdf import PdfPages", file=outFile)
   for line in inFile:
       print(haveSavedPlot, line.strip("\n"), line[0] == " ")
       if haveSavedPlot and line[0] == " ":
           continue
       elif haveSavedPlot and line[0] != " " and len(line) > 1:
           haveSavedPlot = False
       if line.strip().startswith("FILE"):
           init = line.index(line.strip()) * " "
           assert prefix == "  "
           print(init + prefix + line.replace("FILE", "BASEFILE").rstrip("\n"), file=outFile)
           print(init + prefix + "FILE = BASEFILE.replace('_ACREXP.py', '.py')", file=outFile)
           continue
       if line.strip().startswith("savePlot"):
           if "Magnitudes" in line or "PPRatio" in line:
               pass
           else:
               indentation = 0
               for indentation in range(len(line)):
                   if line[indentation] != " ":
                       break
               line = line[:indentation] + "pdf.savefig(figure)"
               haveSavedPlot= True
       print((prefix + line).rstrip(), file=outFile)
       if line.replace(" ","").startswith("FOLD_HERE="):
           hasObtainedFold = True
       if line.replace(" ","").startswith("REG_WEIGHT="):
           hasObtainedRegWeight = True
       if line.replace(" ","").startswith("GRID="):
           hasObtainedGrid = True
       if hasObtainedFold and hasObtainedRegWeight and hasObtainedGrid and prefix == "":
           print("with PdfPages(f\"figures/{__file__}_ALL_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.pdf\") as pdf:", file=outFile)
           print(" for P in [2,4,6,8,10]:", file=outFile)
           print("  print(P)", file=outFile)

#           print("  FILE = BASEFILE.replace('.py', '_ACREXP.py')f\"logs/CROSSVALID/{__file__.replace('_VIZ_ACREXP', '')}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt\"", file=outFile)
           prefix = "  "
      

