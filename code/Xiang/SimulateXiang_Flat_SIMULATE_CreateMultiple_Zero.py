import glob
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import scipy
import sys
import torch
from loadXiang import *
from mapIntervalEstimator2 import MAPIntervalEstimator
from matplotlib import rc
from matplotlib.backends.backend_pdf import PdfPages
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import makeGridIndicesCircular
from util import product
from util import savePlot
from util import sech
from util import sign
from util import toFactor
__file__ = __file__.split("/")[-1]
rc('font', **{'family':'FreeSans'})

# Helper Functions dependent on the device

#############################################################
# Part: Collect arguments
OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
assert P == 0
FOLD_HERE = int(sys.argv[2])
#assert FOLD_HERE == 2
REG_WEIGHT = float(sys.argv[3])
GRID = int(sys.argv[4])
SHOW_PLOT = (len(sys.argv) < 6) or (sys.argv[5] == "SHOW_PLOT")
DEVICE = 'cpu'

FILE = f"logs/CROSSVALID/{__file__.replace('_VIZ', '')}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"

# Helper Functions dependent on the device

##############################################
assert (groups == AveStim).all()
assert (observations_x == target).all()
assert (observations_y == response).all()

#############################################################
# Part: Partition data into folds. As described in the paper,
# this is done within each subject.
N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

#############################################################
# Part: Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 151
RANGE_GRID = MAX_GRID-MIN_GRID

CIRCULAR = False
INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)
assert not CIRCULAR

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
def point(p, reference):
    assert not CIRCULAR
    return p
grid_indices_here = MakeLongTensor([[point(y,x) for x in range(GRID)] for y in range(GRID)])

#############################################################
# Part: Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

stimulus = stimulus_
responses = responses_

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

GROUPS = sorted(list(set(groups.cpu().numpy().tolist())))

##############################################
# Initialize the model
parameters = {}
with open("logs/CROSSVALID/RunXiang_ZeroExp_Flat_SignGD_AVE.py_0_2_10.0_151.txt", "r") as inFile:
    (next(inFile))
    (next(inFile))
    for l in inFile:
        if l.startswith("==="):
           break
        z, y = l.split("\t")

        parameters[z.strip()] = MakeFloatTensor(json.loads(y))

##############################################
#############################################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 1

## These are negative squared distances (for interval spaces) or
## trigonometric functions (for circular spaces), with
## some extra factors for numerical purposes.
## Exponentiating a `similarity` function and normalizing
## is equivalent to the Gaussian / von Mises density.
## The purpose of specifying these as `closeness` or `distance`,
## rather than simply calling squared or trigonometric
## functions is to  flexibly reuse the same model code for
## both interval and circular spaces.
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    assert (x<=STIMULUS_SPACE_VOLUME).all(), x.max()
    return -((x/STIMULUS_SPACE_VOLUME).pow(2))/2
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    assert (x<=1).all(), x.max()
    return -(x.pow(2))/2
#############################################################
# Part: Configure the appropriate estimator for minimizing the loss function
SCALE=100
KERNEL_WIDTH = 0.05

averageNumberOfNewtonSteps = 2
W = 100

MAPIntervalEstimator.set_parameters(KERNEL_WIDTH=KERNEL_WIDTH, MIN_GRID=MIN_GRID, MAX_GRID=MAX_GRID, SCALE=SCALE, W=W, GRID=GRID, OPTIMIZER_VERBOSE=False)

# Run the model
def samplePredictions(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, computePredictions=False, subject=None, sigma_stimulus=None, sigma2_stimulus=None, duration_=None, folds=None):
 motor_variance = 1e-5 + torch.exp(- parameters["log_motor_var"])
 # Part: Obtain the sensory noise variance. We parameterize it as a fraction of the squared volume of the size of the sensory space
 sigma2 = (SENSORY_SPACE_VOLUME * SENSORY_SPACE_VOLUME)*torch.sigmoid(sigma_logit)
 # Part: Obtain the transfer function as the cumulative sum of the discretized resource allocation (referred to as `volume` element due to the geometric interpretation by Wei&Stocker 2015)
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 loss = 0
 if True:

  if sigma2_stimulus > 0:
    assert False
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))
    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  # Part: Compute sensory likelihoods. Across both interval and
  ## circular stimulus spaces, this amounts to exponentiaring a
  ## `similarity`
  sensory_likelihoods = torch.softmax(((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2))  + volumeElement.unsqueeze(1).log(), dim=0)

  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    assert False
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  ## Compute posterior using Bayes' rule. As described in the paper, the posterior is computed
  ## in the discretized stimulus space.
  posterior = prior.unsqueeze(1) * likelihoods.t()
  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  ## Compute the estimator for each m in the discretized sensory space.
  if P == 0:
    bayesianEstimate = MAPIntervalEstimator.apply(grid[grid_indices_here], posterior)
  elif P == 2:
    bayesianEstimate = ((grid[grid_indices_here] * posterior).sum(dim=0))
  else:
    bayesianEstimate = LPEstimator.apply(grid[grid_indices_here], posterior)
  ## Compute the motor likelihood
  likelihoods_by_stimulus = likelihoods[:,stimulus_]

  sampled_estimator = bayesianEstimate[torch.distributions.Categorical(likelihoods_by_stimulus.t()).sample()]

  motor_variances = (motor_variance)
  sampled_response = torch.normal(sampled_estimator, STIMULUS_SPACE_VOLUME * motor_variances.sqrt() *torch.ones_like(sampled_estimator)).round()

  # Mixture of estimation and uniform response
  uniform_part = torch.sigmoid(parameters["mixture_logit"])
  choose_uniform = (MakeZeros(sampled_response.size()).uniform_(0,1) < uniform_part)
  sampled_response = torch.where(choose_uniform, MakeZeros(sampled_response.size()).uniform_(MIN_GRID, MAX_GRID).float().round(), sampled_response.float())

  print(sampled_estimator.min())
  print(sampled_estimator.max())

  return sampled_response.float()

lowestError = 100000

xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))

yValues = []
for y in observations_y:
   yValues.append(int( torch.argmin((grid - y).abs())))

xValues = MakeLongTensor(xValues)
yValues = MakeLongTensor(yValues)

epsilon = 1
volume = SENSORY_SPACE_VOLUME * torch.softmax(-torch.log(epsilon + grid), dim=0)

observations_y = MakeZeros(xValues.size())
conditions = MakeZeros(xValues.size())
for group_ in GROUPS:
  for condition in [0,1]:
    prior = torch.where((grid-group_).abs() <= 15, 1+MakeZeros(GRID), .001+MakeZeros(GRID))
    prior = prior / prior.sum()
    MASK = torch.logical_and(Condition==condition,groups==group_)
    observations_y[MASK] = samplePredictions(xValues[MASK], parameters["sigma_logit"][condition], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, computePredictions=False, subject=None, sigma_stimulus=0, sigma2_stimulus=0)
observations_y = observations_y.float()

SELFID = random.randint(10000, 1000000)

print(xValues)

with open(f"logs/SIMULATED_REPLICATE/{__file__}_{P}_{GRID}.txt", "w") as outFile:
    for z, y in parameters.items():
        print(z, "\t", y.detach().cpu().numpy().tolist(), file=outFile)
    print("=======", file=outFile)
    for i in range(xValues.size()[0]):
      print(int(groups[i]), int(Condition[i]), round(float(grid[xValues[i]]),1), round(float(observations_y[i]),1), file=outFile)
