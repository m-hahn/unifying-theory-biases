import glob
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sys
import torch
from loadXiang import *
from mapIntervalEstimator2 import MAPIntervalEstimator
from matplotlib import rc
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import computeMeanWeighted, computeSDWeighted
from util import difference
from util import product
from util import savePlot
from util import sign
from util import toFactor

rc('font', **{'family':'FreeSans'})

# Helper Functions dependent on the device

#############################################################
# Part: Collect arguments
OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
assert P == 0

FOLD_HERE = int(sys.argv[2])
#assert FOLD_HERE == 0
REG_WEIGHT = float(sys.argv[3])
GRID = int(sys.argv[4])
SHOW_PLOT = (len(sys.argv) < 6) or (sys.argv[5] == "SHOW_PLOT")
DEVICE = 'cpu'

# Helper Functions dependent on the device

##############################################
assert (groups == AveStim).all()
assert (observations_x == target).all()
assert (observations_y == response).all()

#############################################################
# Part: Partition data into folds. As described in the paper,
# this is done within each subject.
N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

Fold = 2+0*Subject

assert (groups == AveStim).all()
assert (observations_x == target).all()
assert (observations_y == response).all()

##############################################
# Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 151
RANGE_GRID = MAX_GRID-MIN_GRID

CIRCULAR = False

INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
def point(p, reference):
    assert not CIRCULAR
    return p

grid_indices_here = MakeLongTensor([[point(y,x) for x in range(GRID)] for y in range(GRID)])

#############################################################
# Part: Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))

xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

stimulus = stimulus_
responses = responses_

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))
for group_ in [35, 40]:
 y_set = []
 sd_set = []
 for x in x_set:
   y_here = observations_y[torch.logical_and(xValues == x, groups==group_)]

   y_set.append(float(y_here.mean() - grid[x]))
   sd_set.append(math.sqrt(float(y_here.pow(2).mean() - y_here.mean().pow(2))))

GROUPS = sorted(list(set(groups.cpu().numpy().tolist())))

# Import/define the appropriate estimator for minimizing the loss function
# Initialize the model
init_parameters = {}
init_parameters["sigma2_stimulus"] = MakeFloatTensor([0]).view(1)
init_parameters["log_motor_var"] = MakeFloatTensor([0]).view(1)
init_parameters["sigma_logit"] = MakeFloatTensor(2*[-3]).view(2)
init_parameters["mixture_logit"] = MakeFloatTensor([-1]).view(1)
init_parameters["prior_mu"] = MakeZeros(len(GROUPS))+3
init_parameters["prior_s"] = MakeZeros(1)
init_parameters["volume"] = MakeZeros(1)
for _, y in init_parameters.items():
    y.requires_grad = True

print("Logs:")
SCRIPT = __file__.replace("_VIZ_OnlyHuman_MainPaper", "")
print("\n".join(glob.glob(f"logs/CROSSVALID/{SCRIPT}*.txt")))
FILE = f"logs/CROSSVALID/{SCRIPT}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"
if os.path.exists(FILE):
 from loadModel import loadModel
 loadModel(FILE, init_parameters)
else:
    print("Initialized from scratch")

assert "volume" in init_parameters

#############################################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 1

## These are negative squared distances (for interval spaces) or
## trigonometric functions (for circular spaces), with
## some extra factors for numerical purposes.
## Exponentiating a `similarity` function and normalizing
## is equivalent to the Gaussian / von Mises density.
## The purpose of specifying these as `closeness` or `distance`,
## rather than simply calling squared or trigonometric
## functions is to  flexibly reuse the same model code for
## both interval and circular spaces.
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    assert (x<=STIMULUS_SPACE_VOLUME).all(), x.max()
    return -((x/STIMULUS_SPACE_VOLUME).pow(2))/2
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    assert (x<=1).all(), x.max()
    return -(x.pow(2))/2
#############################################################
# Part: Configure the appropriate estimator for minimizing the loss function
# Import/define the appropriate estimator for minimizing the loss function

SCALE=100
KERNEL_WIDTH = 0.05

averageNumberOfNewtonSteps = 2
W = 100

MAPIntervalEstimator.set_parameters(KERNEL_WIDTH=KERNEL_WIDTH, MIN_GRID=MIN_GRID, MAX_GRID=MAX_GRID, SCALE=SCALE, W=W, GRID=GRID, OPTIMIZER_VERBOSE=False)

# Run the model
#############################################################
# Part: Run the model. This function implements the model itself:
## calculating the likelihood of a given dataset under that model
## and---if the computePredictions argument is set to True--- computes
## the bias and variability of the estimate.
def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, StimulusSD=None, computePredictions=False, subject=None, sigma_stimulus=None, sigma2_stimulus=None, group_=None, condition=None, folds=None, lossReduce='mean'):

 # Part: Obtain the motor variance by exponentiating the appropriate model parameter
 motor_variance = 1e-5 + torch.exp(- parameters["log_motor_var"])
 # Part: Obtain the sensory noise variance. We parameterize it as a fraction of the squared volume of the size of the sensory space
 sigma2 = (SENSORY_SPACE_VOLUME * SENSORY_SPACE_VOLUME)*torch.sigmoid(sigma_logit)
 # Part: Obtain the transfer function as the cumulative sum of the discretized resource allocation (referred to as `volume` element due to the geometric interpretation by Wei&Stocker 2015)
 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 loss = 0
 if True:

  folds = MakeLongTensor(folds)

  MASK = torch.logical_and(torch.logical_and(Condition==condition,groups==group_), (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))

  stimulus = stimulus_[MASK]
  responses = responses_[MASK]
  print(group_, stimulus.min(), stimulus.max())
  if sigma2_stimulus > 0:
    assert False
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))

    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  # Part: Compute sensory likelihoods. Across both interval and
  ## circular stimulus spaces, this amounts to exponentiaring a
  ## `similarity`
  sensory_likelihoods = torch.softmax(((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2))  + volumeElement.unsqueeze(1).log(), dim=0)

  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    assert False
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  posterior = prior.unsqueeze(1) * likelihoods.t()

  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  if P == 2:
    bayesianEstimate = ((grid[grid_indices_here] * posterior).sum(dim=0))
  else:
    bayesianEstimate = MAPIntervalEstimator.apply(grid[grid_indices_here], posterior)

  motor_variances = (motor_variance)

  ## `error' refers to the stimulus similarity between the estimator assigned to each m and
  ## the observations found in the dataset.
  ## The Gaussian or von Mises motor likelihood is obtained by exponentiating and normalizing
  error = (SQUARED_STIMULUS_SIMILARITY((bayesianEstimate.unsqueeze(0) - responses.unsqueeze(1)))/motor_variances.unsqueeze(0))
  ## The log normalizing constants, for each m in the discretized sensory space
  log_normalizing_constants = torch.logsumexp(SQUARED_STIMULUS_SIMILARITY((grid.view(-1,1) - bayesianEstimate.view(1,-1)))/motor_variances.unsqueeze(0), dim=0)
  ## The log motor likelihoods, for each pair of sensory encoding m and observed human response
  log_motor_likelihoods = (error) - log_normalizing_constants.view(1, -1)

  ## Obtaining the motor likelihood by exponentiating.
  motor_likelihoods = torch.exp(log_motor_likelihoods)
  ## Obtain the guessing rate, parameterized via the (inverse) logit transform as described in SI Appendix
  uniform_part = torch.sigmoid(parameters["mixture_logit"])
  ## The full likelihood then consists of a mixture of the motor likelihood calculated before, and the uniform
  ## distribution on the full space.
  motor_likelihoods = (1-uniform_part) * motor_likelihoods + (uniform_part / (GRID) + 0*motor_likelihoods)

  # Now the loss is obtained by marginalizing out m from the motor likelihood
  if lossReduce == 'mean':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().mean()
  elif lossReduce == 'sum':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().sum()
  else:
    assert False

  ## If computePredictions==True, compute the bias and variability of the estimate
  if computePredictions:

     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)

     bayesianEstimate_avg_byStimulus = (bayesianEstimate_byStimulus * likelihoods).sum(dim=0)

     bayesianEstimate_sd_byStimulus = (difference(bayesianEstimate_avg_byStimulus.view(1,-1), bayesianEstimate_byStimulus.view(-1, 1)).pow(2) * likelihoods).sum(dim=0).sqrt()
     bayesianEstimate_sd_byStimulus = (bayesianEstimate_sd_byStimulus.pow(2) +STIMULUS_SPACE_VOLUME*STIMULUS_SPACE_VOLUME * motor_variances).sqrt()
     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = (posteriorMaxima.unsqueeze(1) * likelihoods).sum(dim=0)
     encodingBias = (grid.unsqueeze(1) * likelihoods).sum(dim=0)
     attraction = (posteriorMaxima-encodingBias)

  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction

def retrieveAndSmoothObservations(x, group_, condition):
     x_set_here = []
     y_set = []
     sd_set = []
     for x in x_set:
        y_here = observations_y[torch.logical_and(Condition==condition, torch.logical_and(xValues == x, groups==group_))]

        if sum(y_here.size()) == 0:
            continue
        y_set.append(float(y_here.mean() - grid[x]))
        sd_set.append(float(math.sqrt(float(y_here.pow(2).mean() - y_here.mean().pow(2)))))
        x_set_here.append(float(grid[x]))
     x_set_here = MakeFloatTensor(x_set_here)
     y_set = MakeFloatTensor(y_set)
     sd_set = MakeFloatTensor(sd_set)
     kernel = torch.nn.functional.softmax(-(x_set_here.unsqueeze(0)/MAX_GRID - x_set_here.unsqueeze(1)/MAX_GRID).pow(2) / (2*(0.01)**2), dim=0)
     y_set = (kernel * y_set.unsqueeze(1)).sum(dim=0)
     sd_set = (kernel * sd_set.unsqueeze(1)).sum(dim=0)
     return x_set_here, y_set, sd_set

def retrieveAndSmoothObservationsDirect(x,  group_, condition):
   y_set_boot = []
   sd_set_boot = []
   for _ in range(100):
     boot = torch.randint(low=0, high=observations_y.size()[0]-1, size=observations_y.size())
     observations_y_ = observations_y[boot]
     Condition_ = Condition[boot]
     groups_ = groups[boot]
     xValues_ = xValues[boot]
     MASK = torch.logical_and(Condition_==condition, groups_==group_)
     y_sets = []
     sd_sets = []
     subjects = sorted(list(set(list(Subject.numpy().tolist()))))
     if True:
       ## select observations from each cell
       kernel = torch.softmax(5000 * SQUARED_STIMULUS_SIMILARITY(grid[x_set].unsqueeze(0) - grid[xValues_[MASK]].unsqueeze(1)), dim=0)
       y_here = observations_y_[MASK]

       y_smoothed = computeMeanWeighted(y_here.unsqueeze(1), kernel)

       y_set_boot.append(y_smoothed)

       sd_set = []
       for x in x_set:
        y_here = observations_y_[torch.logical_and(MASK, (xValues_ == x))]
        if y_here.size()[0] > 0:
           sd_set.append(computeSDWeighted(y_here))
        else:
           sd_set.append(float('nan'))
       sd_set = MakeFloatTensor(sd_set)

       kernel = torch.softmax(5000 * SQUARED_STIMULUS_SIMILARITY(grid[x_set].unsqueeze(0) - grid[x_set].unsqueeze(1)) - 1e20*torch.isnan(sd_set.unsqueeze(1)).float(), dim=0)

       sd_set[torch.isnan(sd_set)] = 0
       sd_set = (sd_set.unsqueeze(1) * kernel).sum(dim=0)

       sd_set_boot.append(sd_set)

   y_set = torch.stack(y_set_boot, dim=0)
   sd_set = torch.stack(sd_set_boot, dim=0)
   y_set_var = computeSDWeighted(y_set)

   y_set_var = torch.stack([torch.quantile(y_set, q=0.025, dim=0), torch.quantile(y_set, q=0.975, dim=0)], dim=1)
   sd_set_var = torch.stack([torch.quantile(sd_set, q=0.025, dim=0), torch.quantile(sd_set, q=0.975, dim=0)], dim=1)

   y_set = computeMeanWeighted(y_set)
   sd_set = computeMeanWeighted(sd_set)

   print(y_set.size(), len(x_set))
   y_set = y_set - grid[x_set]
   y_set_var = y_set_var - grid[x_set].unsqueeze(1)

   print(x_set)
   print(y_set)
   print(sd_set)
   print(sd_set_var)
   return grid[x_set], y_set, sd_set, y_set_var, sd_set_var

def plotForGroup(method, group_, ax, xValues, yValues, color, args={}):
    if type(xValues) == type([]):
        xValues = MakeFloatTensor(xValues)
    xIndices = torch.logical_and(xValues >= group_-15, xValues <= group_+15)
    args["linewidth"] = 2.5
    if method == "scatter":
      ax.scatter(xValues[xIndices], yValues[xIndices], color=color, **args)
    else:
      ax.plot(xValues[xIndices], yValues[xIndices], color=color, **args)

def model(grid):

  lossesBy500 = []
  crossLossesBy500 = []
  noImprovement = 0
  global optim, learning_rate
  for iteration in range(1):
   parameters = init_parameters
   ## In each iteration, recompute
   ## - the resource allocation (called `volume' due to a geometric interpretation)
   ## - the prior

   epsilon = 1
   volume = SENSORY_SPACE_VOLUME * torch.softmax(-torch.log(epsilon + grid), dim=0)
   ## For interval datasets, we're norming the size of the sensory space to 1

   loss = 0

   grid_cpu = grid.cpu()
   if iteration % 500 == 0:
     assigned = ["ENC", None, None, "PRI", None, None,  "ATT", None, "REP", None, None, "HUM", None, None, "VAH"]
     for w, k in enumerate(assigned):
         globals()[k] = w
     PAD = [w for w, q in enumerate(assigned) if (q not in ["HUM", "VAH"])]
     gridspec = dict(width_ratios=[1 if x is not None else .2 for x in assigned])
     figure, axis = plt.subplots(1, len(gridspec["width_ratios"]), figsize=(14,2.1), gridspec_kw=gridspec)
     plt.tight_layout()
     figure.subplots_adjust(wspace=0.1, hspace=0.0)

     for w in PAD:
       axis[w].set_visible(False)

     if True:
       axis[ENC].set_title("Resources", fontsize=16)
       axis[PRI].set_title("Priors", fontsize=16)
       axis[REP].set_title("Repulsion", fontsize=16)
       axis[ATT].set_title("Attraction", fontsize=16)
     if True:
       axis[HUM].set_title("Bias (data)", fontsize=22)
       axis[VAH].text(5,17, "Variability", fontsize=22)
       axis[VAH].text(16, 13, "(data)", fontsize=22)
     figure.subplots_adjust(bottom=0.12,top=0.85)

     for w in range(len(assigned)):
       axis[w].tick_params(axis='both', labelsize=16)
     for condition in [0,1]:
        axis[ENC].plot(grid_cpu, volume.detach().cpu() / torch.sqrt(torch.sigmoid(init_parameters["sigma_logit"][condition])).detach(), linestyle = ["solid", "dotted"][condition], color="blue")

     x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   print(GROUPS)

   colors = ["blue", "orange", "green"]
   for group_id, group_ in enumerate([30, 40, 50]):
    for condition in [0,1]:
     prior = torch.softmax(-(torch.log(1+grid)-(init_parameters["prior_mu"][group_-GROUPS[0]])).pow(2) / (2 * torch.exp(init_parameters["prior_s"])) - ((1+grid).log()), dim=0)
     loss_model, bayesianEstimate_model, bayesianEstimate_sd_byStimulus_model, attraction = computeBias(xValues, init_parameters["sigma_logit"][condition], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, StimulusSD=0, computePredictions=(iteration%500 == 0), subject=None, sigma_stimulus=0, sigma2_stimulus=0, group_=group_, condition=condition, folds=testFolds, lossReduce='sum')
     loss += loss_model

     if iteration % 500 == 0:

       x_set_data, y_set_data, sd_set_data, y_errbar, sd_errbar = retrieveAndSmoothObservationsDirect(x, group_, condition)

       x_set_subset = torch.logical_and(grid[x_set] >= group_-15, grid[x_set] <= group_+15)

       print("Y ERR BAR", y_errbar[x_set_subset])

       CI_x = [float(grid[x]) for x in MakeLongTensor(x_set)[x_set_subset]]
       CI_y1 = ((y_errbar[:,0]))[x_set_subset].detach().numpy().tolist()
       CI_y2 = ((y_errbar[:,1]))[x_set_subset].detach().numpy().tolist()
       poly = plt.Polygon(list(zip(CI_x + CI_x[::-1], CI_y1 + CI_y2[::-1])), facecolor=(.65, .65, .65), alpha=0.5)
       axis[HUM].add_patch(poly)

       CI_x = [float(grid[x]) for x in MakeLongTensor(x_set)[x_set_subset]]
       CI_sd1 = ((sd_errbar[:,0]))[x_set_subset].detach().numpy().tolist()
       CI_sd2 = ((sd_errbar[:,1]))[x_set_subset].detach().numpy().tolist()
       poly = plt.Polygon(list(zip(CI_x + CI_x[::-1], CI_sd1 + CI_sd2[::-1])), facecolor=(.65, .65, .65), alpha=0.5)

       axis[VAH].add_patch(poly)

       plotForGroup("plot", group_, axis[HUM], x_set_data, y_set_data, color=colors[group_id], args={"linestyle" : ["solid", "dotted"][condition]})

       plotForGroup("plot", group_, axis[VAH], x_set_data, sd_set_data, color=colors[group_id], args={"linestyle" : ["solid", "dotted"][condition]})

       axis[ATT].set_ylim(-20, 10)
       axis[REP].set_ylim(-20, 10)
       axis[HUM].set_ylim(-20, 10)
       axis[VAH].set_ylim(0,15)

       for z in range(len(assigned)):
         axis[z].spines['right'].set_visible(False)
         axis[z].spines['top'].set_visible(False)
         axis[z].set_xticks(ticks=[0,25,50])
       for z in [ENC]:
         axis[z].set_yticks(ticks=[0,5,10,15], labels=[0, "", 10, ""])
       for z in [PRI]:
         axis[z].set_yticks(ticks=[0,0.01, 0.02, 0.03], labels=[0, "", "", 0.03])
       for z in [ATT, REP, HUM]:
         axis[z].set_yticks(ticks=[-15,-10,-5,0,5], labels=["","‒10","",0,""])
       for z in [VAH]:
         axis[z].set_yticks(ticks=[0,5,10], labels=[0,"",10])

       for z in [REP]:
           axis[z].tick_params(labelleft=False)
       for z in range(len(assigned)):
           axis[z].tick_params(labelbottom=False)

   print("LOSS", loss)
   if iteration % 500 == 0:

     axis[ENC].set_ylim(0, 5)
     for w in range(8):
       axis[w].set_xlim(-1,70)

     for i in range(len(assigned)):
        plt.subplots_adjust(bottom=0.2,top=0.8)

     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.pdf", transparent=True)

   if iteration % 500 == 0:
     from matplotlib import cm
     from matplotlib.colors import ListedColormap, LinearSegmentedColormap
     viridis = cm.get_cmap('viridis', 8)

     figure, axis = plt.subplots(1, 1, figsize=(1.5,1.5))
     axis.plot([0,0], [0, (bayesianEstimate_model-grid-attraction).detach().pow(2).mean()], linewidth=50)
     axis.plot([1,1], [0, (attraction).detach().pow(2).mean()], linewidth=50)
     plt.xticks([])
     plt.yticks([])
     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_Magnitudes.pdf")
     if SHOW_PLOT:

        plt.show()
     plt.close()

     figure, axis = plt.subplots(1, 1, figsize=(1.8,1.8))
     plt.tight_layout()
     ratio = (prior / volume.pow((P+2)/2)).detach()
     kernel = torch.softmax(-(((grid.unsqueeze(0) - grid.unsqueeze(1))/MAX_GRID)).pow(2)/0.001, dim=0)
     ratio = torch.matmul(ratio.unsqueeze(0), kernel).view(-1)
     prior_smoothed = torch.matmul(prior.unsqueeze(0), kernel).view(-1)
     volume_smoothed = torch.matmul(volume.unsqueeze(0), kernel).view(-1)
     axis.set_xlim(float(x_set_data[0]), float(x_set_data[-1]))
     axis.plot([float(x_set_data[0]), float(x_set_data[-1])], [8,8], color="gray", linestyle='dotted')
     axis.plot([float(x_set_data[0]), float(x_set_data[-1])], [0,0], color="gray", linestyle='dotted')
     axis.plot([float(x_set_data[0]), float(x_set_data[-1])], [-8,-8], color="gray", linestyle='dotted')
     axis.plot([float(x_set_data[0]), float(x_set_data[-1])], [-16,-16], color="gray", linestyle='dotted')
     plt.yticks([])

     axis.set_yticks(ticks=[8, 0, -8, -16], labels=["A", "B", "C", "D"])

     for w in range(2,GRID):
        if grid[w] < x_set_data[0] or grid[w] > x_set_data[-1]:
            continue
        y1 = float(ratio[w-1]-ratio[w-2])
        y2 = float(ratio[w]-ratio[w-1])

        repulsive_part = float((bayesianEstimate_model-grid-attraction)[w].cpu().detach())
        attractive_part = float((attraction)[w].cpu().detach())
        repulsivePredominance = math.pow(repulsive_part, 2) / (math.pow(repulsive_part,2) + math.pow(attractive_part,2))

        hasAttraction = False
        repulsion = False
        if sign(float(prior_smoothed[w]-prior_smoothed[w-1])) == sign(y2):
            hasAttraction=True
        if sign(float(volume_smoothed[w]-volume_smoothed[w-1])) == -sign(y2):
            repulsion=True
        if hasAttraction and repulsion:
            c = "gray"
        elif hasAttraction:
            c = "green"
        elif repulsion:
            c = "blue"
        else:
            c = "black"

        axis.plot([float(grid[w-1]), float(grid[w])], [0+sign(y2),0+sign(y2)], color=["green", "red", "gray"][{-1 : 0, 0 : 2, 1 : 1}[sign(y2)]])

        y2 = repulsive_part
        axis.plot([float(grid[w-1]), float(grid[w])], [-8+sign(y2),-8+sign(y2)], color=["green", "red", "gray"][{-1 : 0, 0 : 2, 1 : 1}[sign(y2)]])

        y2 = attractive_part
        axis.plot([float(grid[w-1]), float(grid[w])], [-16+sign(y2),-16+sign(y2)], color=["green", "red", "gray"][{-1 : 0, 0 : 2, 1 : 1}[sign(y2)]])

     for w in range(2, len(x_set)):
        y2 = y_set[w]
        axis.plot([float(grid[x_set[w-1]]), float(grid[x_set[w]])], [8+sign(y2),8+sign(y2)], color=["green", "red", "gray"][{-1 : 0, 0 : 2, 1 : 1}[sign(y2)]])

     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_PPRatio.pdf")
     if SHOW_PLOT:

        plt.show()
     plt.close()

############################3

lowestError = 100000

xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))

yValues = []
for y in observations_y:
   yValues.append(int( torch.argmin((grid - y).abs())))

xValues = MakeLongTensor(xValues)
yValues = MakeLongTensor(yValues)

model(grid)
