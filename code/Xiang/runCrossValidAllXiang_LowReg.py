import os
import random
import subprocess
import sys
from util import savePlot
script = sys.argv[1]

Ps = [0,2,4,6]
folds = [2]

for _ in range(1000):
  P = random.choice(Ps)
  if P % 2 == 1:
      continue

  if (P == 0) != ("Zero" in script):
       continue
  fold = random.choice(folds)
  if "osine" in script and P == 3:
    continue
  print(f"logs/CROSSVALID/{script}_{P}_{fold}_{0.1}_{151}.txt")
  if os.path.exists(f"logs/CROSSVALID/{script}_{P}_{fold}_{0.1}_{151}.txt"):
     continue
  subprocess.call([str(q) for q in ["/u/nlp/anaconda/main/anaconda3/envs/py37-mhahn/bin/python", script, P, fold, 0.1, 151]])
