import glob
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sys
import torch
from loadXiang import *
from lpEstimator import LPEstimator
from matplotlib import rc
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import ToDevice
from util import difference
from util import product
from util import savePlot
from util import toFactor

__file__ = __file__.split("/")[-1]
rc('font', **{'family':'FreeSans'})

# Helper Functions dependent on the device

#############################################################
# Part: Collect arguments
OPTIMIZER_VERBOSE = False

P = 2
assert P > 0

FOLD_HERE = 2

REG_WEIGHT = 10.0
GRID = 151
SHOW_PLOT = (len(sys.argv) < 6) or (sys.argv[5] == "SHOW_PLOT")
DEVICE = 'cpu'

# Helper Functions dependent on the device

##############################################
assert (groups == AveStim).all()
assert (observations_x == target).all()
assert (observations_y == response).all()

#############################################################
# Part: Partition data into folds. As described in the paper,
# this is done within each subject.
N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

Fold = 0*Subject
FoldsBySubject = {x : 1000 * list(range(N_FOLDS)) for x in range(int(min(Subject)), int(max(Subject))+1)}
for x in FoldsBySubject:
   randomGenerator.shuffle(FoldsBySubject[x])
IndexBySubject = {x : 0 for x in range(int(min(Subject)), int(max(Subject))+1)}
for i in range(Subject.size()[0]):
    s = int(Subject[i])
    Fold[i] = FoldsBySubject[s][IndexBySubject[s]]
    IndexBySubject[s]+=1

assert (groups == AveStim).all()
assert (observations_x == target).all()
assert (observations_y == response).all()

##############################################
# Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 151

CIRCULAR = False
INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)
assert not CIRCULAR

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
def point(p, reference):
    assert not CIRCULAR
    return p
grid_indices_here = MakeLongTensor([[point(y,x) for x in range(GRID)] for y in range(GRID)])

#############################################################
# Part: Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

stimulus = stimulus_
responses = responses_

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))
for group_ in [35, 40]:
 y_set = []
 sd_set = []
 for x in x_set:
   y_here = observations_y[torch.logical_and(xValues == x, groups==group_)]
   y_set.append(float(y_here.mean() - grid[x]))
   sd_set.append(math.sqrt(float(y_here.pow(2).mean() - y_here.mean().pow(2))))

GROUPS = sorted(list(set(groups.cpu().numpy().tolist())))

#############################################################
# Part: Initialize the model
init_parameters = {}
init_parameters["sigma2_stimulus"] = MakeFloatTensor([0]).view(1)
init_parameters["log_motor_var"] = MakeFloatTensor([0]).view(1)
init_parameters["sigma_logit"] = MakeFloatTensor(2*[-3]).view(2)
init_parameters["mixture_logit"] = MakeFloatTensor([-1]).view(1)
init_parameters["prior_mu"] = MakeZeros(len(GROUPS))+3
init_parameters["prior_s"] = MakeZeros(1)
init_parameters["volume"] = MakeZeros(1)
for _, y in init_parameters.items():
    y.requires_grad = True

print("Logs:")
SCRIPT = __file__
print("\n".join(glob.glob(f"logs/CROSSVALID/{SCRIPT}*.txt")))
FILE = f"logs/CROSSVALID/{SCRIPT}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"
if os.path.exists(FILE):
 from loadModel import loadModel
 loadModel(FILE, init_parameters)
else:

    print("Starting from sratch")
assert "volume" in init_parameters
for _, y in init_parameters.items():
    y.requires_grad = True

#############################################################
# Part: Initialize optimizer.
# The learning rate is a user-specified parameter.
learning_rate = 0.01
optim = torch.optim.SGD([y for _, y in init_parameters.items()], lr=learning_rate, momentum=0.3)

shapes = {x : y.size() for x, y in init_parameters.items()}

#############################################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 1

## These are negative squared distances (for interval spaces) or
## trigonometric functions (for circular spaces), with
## some extra factors for numerical purposes.
## Exponentiating a `similarity` function and normalizing
## is equivalent to the Gaussian / von Mises density.
## The purpose of specifying these as `closeness` or `distance`,
## rather than simply calling squared or trigonometric
## functions is to  flexibly reuse the same model code for
## both interval and circular spaces.
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    assert (x<=STIMULUS_SPACE_VOLUME).all(), x.max()
    return -((x/STIMULUS_SPACE_VOLUME).pow(2))/2
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    assert (x<=1).all(), x.max()
    return -(x.pow(2))/2
#############################################################
# Part: Configure the appropriate estimator for minimizing the loss function
# Import/define the appropriate estimator for minimizing the loss function
SCALE=200

LPEstimator.set_parameters(GRID=GRID, OPTIMIZER_VERBOSE=OPTIMIZER_VERBOSE, P=P, SQUARED_SENSORY_SIMILARITY=SQUARED_SENSORY_SIMILARITY, SCALE=SCALE)

# Run the model
#############################################################
# Part: Run the model. This function implements the model itself:
## calculating the likelihood of a given dataset under that model
## and---if the computePredictions argument is set to True--- computes
## the bias and variability of the estimate.
def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, StimulusSD=None, computePredictions=False, subject=None, sigma_stimulus=None, sigma2_stimulus=None, group_=None, condition=None, folds=None, lossReduce='mean'):
  MASK = (torch.logical_and(Condition==condition,groups==group_))
##  MASK = (Condition==condition)
  return MASK.float().sum(), (groups==group_).float().sum(), (Condition==condition).float().sum()

def model(grid):

  lossesBy500 = []
  crossLossesBy500 = []
  noImprovement = 0
  global optim, learning_rate
  averageLossOver100 = [0]
  for iteration in range(1):
   parameters = init_parameters

   ## Separate train and test/heldout partitions of the data
   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   ## Iterate over the conditions and possibly subjects, if parameters are fitted separately.
   for group_ in GROUPS:
    for condition in [0,1]:
     ## Run the model at its current parameter values.

     print(group_, condition, computeBias(xValues, init_parameters["sigma_logit"][condition], None, None, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, StimulusSD=0, computePredictions=(iteration%500 == 0), subject=None, sigma_stimulus=0, sigma2_stimulus=0, group_=group_, condition=condition, folds=trainFolds, lossReduce='sum'))

############################3

model(grid)
