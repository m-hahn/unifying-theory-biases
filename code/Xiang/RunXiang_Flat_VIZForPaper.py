import glob
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sys
import torch
from matplotlib import rc
from scipy.io import loadmat
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import difference
from util import product
from util import savePlot
from util import sech
from util import toFactor

rc('font', **{'family':'FreeSans'})

# Helper Functions dependent on the device

OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
assert P > 0

FOLD_HERE = int(sys.argv[2])
assert FOLD_HERE == 2
REG_WEIGHT = float(sys.argv[3])
GRID = int(sys.argv[4])
SHOW_PLOT = (len(sys.argv) < 6) or (sys.argv[5] == "SHOW_PLOT")
DEVICE = 'cpu'

with open("data/XIANG/Data/datanewfile.csv", "r") as inFile:
    data = [x.split(",") for x in inFile.read().strip().split("\n")]
    header = data[0]
    header = dict(list(zip(header, range(len(header)))))
    data = data[1:]

    Subject = MakeLongTensor([int(x[header["Subject"]]) for x in data])
    blocks = MakeLongTensor([int(x[header["Block"]]) for x in data])
    groups_raw = MakeLongTensor(toFactor(list(zip(Subject.cpu().numpy().tolist(), blocks.cpu().numpy().tolist()))))
    target = MakeFloatTensor([int(x[header["Stimulus"]]) for x in data])
    response = MakeFloatTensor([int(x[header["Response"]]) for x in data])

    Condition = MakeLongTensor([int(x[header["Condition"]]) for x in data])
    AveStim = MakeLongTensor([int(x[header["AveStim"]]) for x in data])

MASK = torch.logical_and(response>=0, response<=150)
print(MASK.float().size())
print(MASK.float().sum())
print(MASK.float().mean())

Subject=Subject[MASK]
blocks=blocks[MASK]
groups_raw=groups_raw[MASK]
target=target[MASK]
response=response[MASK]
Condition=Condition[MASK]
AveStim=AveStim[MASK]

N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)

Fold = 0*Subject
FoldsBySubject = {x : 1000 * list(range(N_FOLDS)) for x in range(int(min(Subject)), int(max(Subject))+1)}
for x in FoldsBySubject:
   randomGenerator.shuffle(FoldsBySubject[x])
IndexBySubject = {x : 0 for x in range(int(min(Subject)), int(max(Subject))+1)}
for i in range(Subject.size()[0]):
    s = int(Subject[i])
    Fold[i] = FoldsBySubject[s][IndexBySubject[s]]
    IndexBySubject[s]+=1

groups = AveStim

observations_x = target
observations_y = response

MIN_GRID = 0
MAX_GRID = 150
RANGE_GRID = MAX_GRID-MIN_GRID

CIRCULAR = False

INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
def point(p, reference):
    assert not CIRCULAR
    return p

grid_indices_here = MakeLongTensor([[point(y,x) for x in range(GRID)] for y in range(GRID)])

xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))

xValues = MakeLongTensor(xValues)

stimulus_ = xValues
responses_=observations_y

sigma_stimulus = 4
sigma2_stimulus = sigma_stimulus
stimulus = stimulus_
responses = responses_

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))
for group_ in [35, 40]:
 y_set = []
 sd_set = []
 for x in x_set:
   y_here = observations_y[torch.logical_and(xValues == x, groups==group_)]

   y_set.append(float(y_here.mean() - grid[x]))
   sd_set.append(math.sqrt(float(y_here.pow(2).mean() - y_here.mean().pow(2))))

GROUPS = sorted(list(set(groups.cpu().numpy().tolist())))

SCALE=100
class LPEstimator(torch.autograd.Function):
    """
    We can implement our own custom autograd Functions by subclassing
    torch.autograd.Function and implementing the forward and backward passes
    which operate on Tensors.
    """

    @staticmethod
    def forward(ctx, grid_indices_here, posterior):
        """
        In the forward pass we receive a Tensor containing the input and return
        a Tensor containing the output. ctx is a context object that can be used
        to stash information for backward computation. You can cache arbitrary
        objects for use in the backward pass using the ctx.save_for_backward method.
        """
        grid_indices_here = grid_indices_here/SCALE
        n_inputs, n_batch = posterior.size()
        initialized = (grid_indices_here.data * posterior).detach().sum(dim=0).data
        result = initialized.clone()

        for itera in range(20):

          if OPTIMIZER_VERBOSE:
            loss = (((result.unsqueeze(0) - grid_indices_here).abs().pow(P)) * posterior.detach()).sum(dim=0).sum() / n_batch
          loss_gradient = ((P * torch.sign(result.unsqueeze(0) - grid_indices_here) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-1)) * posterior.detach()).sum(dim=0) / n_batch
          loss_gradient2 = ((P * (P-1) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-2)) * posterior.detach()).sum(dim=0) / n_batch
          result -= loss_gradient / loss_gradient2

          if float(loss_gradient.abs().max()) < 1e-5:
              break

        if float(loss_gradient.abs().max()) >= 1e-5:
            print("WARNING", float(loss_gradient.abs().max()), "after ", itera, " Newton steps")
        if P == 2:
            assert itera == 0
        ctx.save_for_backward(grid_indices_here, posterior, result)

        return SCALE*result

    @staticmethod
    def backward(ctx, grad_output):
        """
        In the backward pass we receive a Tensor containing the gradient of the loss
        with respect to the output, and we need to compute the gradient of the loss
        with respect to the input.
        """
        grid_indices_here, posterior, result = ctx.saved_tensors
        initialized = (grid_indices_here.data * posterior).detach().sum(dim=0).data

        F = ((P * torch.sign(result.unsqueeze(0) - grid_indices_here) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-1)) * posterior.detach()).sum(dim=0)

        assert F.abs().mean() < 0.001, F
        dF_posterior = ((P * torch.sign(result.unsqueeze(0) - grid_indices_here) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-1)))
        dF_result = ((P * (P-1) * (result.unsqueeze(0) - grid_indices_here).abs().pow(P-2)) * posterior.detach()).sum(dim=0)

        gradient = - ((1 / dF_result).unsqueeze(0) * dF_posterior).detach().data

        gradient = grad_output.unsqueeze(0) * gradient

        return None, gradient*SCALE

init_parameters = {}

init_parameters["sigma2_stimulus"] = MakeFloatTensor([0]).view(1)
init_parameters["log_motor_var"] = MakeFloatTensor([0]).view(1)
init_parameters["sigma_logit"] = MakeFloatTensor(2*[-3]).view(2)
init_parameters["chance_prob_logit"] = MakeFloatTensor([-0]).view(1)
init_parameters["mixture_logit"] = MakeFloatTensor([-1]).view(1)
init_parameters["prior"] = MakeZeros(len(GROUPS), GRID)

init_parameters["volume"] = -torch.log(1+grid)
for _, y in init_parameters.items():
    y.requires_grad = True

print("Logs:")
print("\n".join(glob.glob(f"logs/CROSSVALID/{__file__.replace('_VIZForPaper', '')}*.txt")))
FILE = f"logs/CROSSVALID/{__file__.replace('_VIZForPaper', '')}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"
if os.path.exists(FILE):
 from loadModel import loadModel
 loadModel(FILE, init_parameters)
else:
    assert False, FILE
assert "volume" in init_parameters
for _, y in init_parameters.items():
    y.requires_grad = True

learning_rate = 0.1
optim = torch.optim.SGD([y for _, y in init_parameters.items()], lr=learning_rate)

shapes = {x : y.size() for x, y in init_parameters.items()}

def SQUARED_STIMULUS_SIMILARITY(x):
    if not ((x<2*MAX_GRID).all()):
        print("WARNING 255:", x.max())
    return -(x/MAX_GRID).pow(2)
def COSINE_RAD(x):
    assert (x<=2*math.pi).all(), x.max()
    return -(x/(2*math.pi)).pow(2)

def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, StimulusSD=None, computePredictions=False, subject=None, sigma_stimulus=None, sigma2_stimulus=None, group_=None, condition=None, folds=None, lossReduce='mean'):
 motor_variance = torch.exp(- parameters["log_motor_var"])
 sigma2 = torch.sigmoid(sigma_logit)

 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 loss = 0
 if True:

  folds = MakeLongTensor(folds)
  MASK = torch.logical_and(torch.logical_and(Condition==condition,groups==group_), (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
  stimulus = stimulus_[MASK]
  responses = responses_[MASK]

  if sigma2_stimulus > 0:
    assert False
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))

    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  sensory_likelihoods = torch.exp((COSINE_RAD(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(2*sigma2)) / math.sqrt(2*math.pi*sigma2)  * volumeElement.unsqueeze(1)

  sensory_likelihoods = sensory_likelihoods / sensory_likelihoods.sum(dim=0, keepdim=True)

  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  posterior = prior.unsqueeze(1) * likelihoods.t()

  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  if P == 2:
    bayesianEstimate = ((grid[grid_indices_here] * posterior).sum(dim=0))
  else:
    bayesianEstimate = LPEstimator.apply(grid[grid_indices_here], posterior)

  motor_variances = (motor_variance)

  error = (SQUARED_STIMULUS_SIMILARITY((bayesianEstimate.unsqueeze(0) - responses.unsqueeze(1)))/motor_variances.unsqueeze(0))

  log_normalizing_constants = torch.logsumexp(SQUARED_STIMULUS_SIMILARITY((grid.view(-1,1) - bayesianEstimate.view(1,-1)))/motor_variances.unsqueeze(0), dim=0)

  log_motor_likelihoods = (error) - log_normalizing_constants.view(1, -1)

  motor_likelihoods = torch.exp(log_motor_likelihoods)

  uniform_part = 0

  motor_likelihoods = (1-uniform_part) * motor_likelihoods + (uniform_part / ((MAX_GRID-MIN_GRID)) + 0*motor_likelihoods)

  # Now the loss is obtained by marginalizing out m from the motor likelihood
  if lossReduce == 'mean':
    loss += -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().mean()
  elif lossReduce == 'sum':
    loss += -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().sum()
  else:
    assert False

  if computePredictions:

     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)

     bayesianEstimate_avg_byStimulus = (bayesianEstimate_byStimulus * likelihoods).sum(dim=0)

     bayesianEstimate_sd_byStimulus = (difference(bayesianEstimate_avg_byStimulus.view(1,-1), bayesianEstimate_byStimulus.view(-1, 1)).pow(2) * likelihoods).sum(dim=0).sqrt()
     bayesianEstimate_sd_byStimulus = (bayesianEstimate_sd_byStimulus.pow(2) + motor_variances).sqrt()

     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = (posteriorMaxima.unsqueeze(1) * likelihoods).sum(dim=0)
     encodingBias = (grid.unsqueeze(1) * likelihoods).sum(dim=0)
     attraction = (posteriorMaxima-encodingBias)
     attraction1 = attraction
     attraction2 = attraction+360
     attraction3 = attraction-360
     attraction = torch.where(attraction1.abs() < 180, attraction1, torch.where(attraction2.abs() < 180, attraction2, attraction3))

  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
 if float(loss) != float(loss):
     print("NAN!!!!")
     quit()
 return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction

def retrieveObservations(x, group_, condition):
     x_set_here = []
     y_set = []
     sd_set = []
     for x in x_set:
        y_here = observations_y[torch.logical_and(Condition==condition, torch.logical_and(xValues == x, groups==group_))]

        if sum(y_here.size()) == 0:
            continue
        y_set.append(float(y_here.mean() - grid[x]))
        sd_set.append(float(math.sqrt(float(y_here.pow(2).mean() - y_here.mean().pow(2)))))
        x_set_here.append(float(grid[x]))
     x_set_here = MakeFloatTensor(x_set_here)
     y_set = MakeFloatTensor(y_set)
     sd_set = MakeFloatTensor(sd_set)
     kernel = torch.nn.functional.softmax(-(x_set_here.unsqueeze(0)/MAX_GRID - x_set_here.unsqueeze(1)/MAX_GRID).pow(2) / (2*(0.01)**2), dim=0)
     y_set = (kernel * y_set.unsqueeze(1)).sum(dim=0)
     sd_set = (kernel * sd_set.unsqueeze(1)).sum(dim=0)
     return x_set_here, y_set, sd_set

def angleDiff(x):

    return -torch.cos(math.pi*x/180)

def plotForGroup(method, group_, ax, xValues, yValues, color=None, args={}):
    if type(xValues) == type([]):
        xValues = MakeFloatTensor(xValues)
    xIndices = torch.logical_and(xValues >= group_-15, xValues <= group_+15)
    if method == "scatter":
      ax.scatter(xValues[xIndices], yValues[xIndices], **args)
    else:
      ax.plot(xValues[xIndices], yValues[xIndices], **args)

def computeResources(volume, sigma2):
    alphaReciprocal = (math.sqrt(-float(SQUARED_SENSORY_SIMILARITY(torch.ones(1)))))
    return volume / torch.sqrt(sigma2) * GRID / (MAX_GRID-MIN_GRID) * alphaReciprocal

def model(grid):

  lossesBy500 = []
  crossLossesBy500 = []
  noImprovement = 0
  global optim, learning_rate
  for iteration in range(1):
   parameters = init_parameters

   volume = 2 * math.pi * torch.nn.functional.softmax(parameters["volume"], dim=0).detach()

   loss = 0

   grid_cpu = grid.cpu()
   if iteration % 500 == 0:
     assigned = ["ENC", None, "PRI", None, "ATT", "REP", "TOT", "HUM", None, "VAR", "VAH"]
     for w, k in enumerate(assigned):
         globals()[k] = w
     PAD = [w for w, q in enumerate(assigned) if q is None]
     gridspec = dict(width_ratios=[1,0.2,1,0.2,1,1,1,1,0.2,1,1])
     figure, axis = plt.subplots(1, len(gridspec["width_ratios"]), figsize=(16,2.5), gridspec_kw=gridspec)
     plt.tight_layout()
     figure.subplots_adjust(wspace=0.1, hspace=0.0)

     for w in PAD:
       axis[w].set_visible(False)

     if True:
       axis[ENC].set_title("Resources", fontsize=16)
       axis[PRI].set_title("Priors", fontsize=16)
       axis[REP].set_title("Repulsion", fontsize=16)
       axis[ATT].set_title("Attraction", fontsize=16)
     if True:
       axis[TOT].set_title("Bias (Model)", fontsize=16)
       axis[HUM].set_title("Bias (Data)", fontsize=16)
       axis[VAR].set_title("Var. (Model)", fontsize=16)
       axis[VAH].set_title("Var. (Data)", fontsize=16)
     figure.subplots_adjust(bottom=0.12,top=0.85)

     for w in range(len(assigned)):
       axis[w].tick_params(axis='both', labelsize=12)
     for condition in [0,1]:
        print("THESE SHOULD MATCH", (math.sqrt(-float(SQUARED_SENSORY_SIMILARITY(torch.ones(1))))), 1/(2*math.pi))
        axis[ENC].plot(grid_cpu, computeResources(volume.detach().cpu(), torch.sigmoid(init_parameters["sigma_logit"][condition]).detach()), linestyle = ["solid", "dotted"][condition], color="blue")

     x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   print(GROUPS)

   colors = list(range(100))
   for group_id, group_ in enumerate(GROUPS):
    for condition in [0,1]:
     prior = torch.where((grid-group_).abs() <= 15, 1+MakeZeros(GRID), .001+MakeZeros(GRID))
     loss_model, bayesianEstimate_model, bayesianEstimate_sd_byStimulus_model, attraction = computeBias(xValues, init_parameters["sigma_logit"][condition], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, StimulusSD=0, computePredictions=(iteration%500 == 0), subject=None, sigma_stimulus=0, sigma2_stimulus=0, group_=group_, condition=condition, folds=testFolds, lossReduce='sum')
     loss += loss_model

     if iteration % 500 == 0:
       axis[PRI].plot( grid_cpu, (.95+group_id*0.03)*prior.detach().cpu())

       x_set_data, y_set_data, sd_set_data = retrieveObservations(x, group_, condition)

       prior_uniform = 1/GRID + MakeZeros(GRID)
       _, bayesianEstimate_model_uniform, _, _ = computeBias(xValues, init_parameters["sigma_logit"][condition], prior_uniform, volume, n_samples=1000, grid=grid, responses_=observations_y, parameters=parameters, StimulusSD=0, computePredictions=(iteration%500 == 0), subject=None, sigma_stimulus=0, sigma2_stimulus=0, group_=group_, condition=condition, folds=testFolds, lossReduce='sum')

       plotForGroup("plot", group_, axis[REP], [grid_cpu[q] for q in x_set], (bayesianEstimate_model_uniform.detach().cpu()-grid_cpu)[x_set].detach(), color=colors[group_id], args={"linestyle" : ["solid", "dotted"][condition]})

       plotForGroup("plot", group_, axis[ATT], [grid_cpu[q] for q in x_set], 0*(attraction)[x_set].detach(), color=colors[group_id], args={"linestyle" : ["solid", "dotted"][condition]})
       plotForGroup("plot", group_, axis[TOT], [grid_cpu[q] for q in x_set], (bayesianEstimate_model.detach().cpu()-grid_cpu)[x_set].detach(), color=colors[group_id], args={"linestyle" : ["solid", "dotted"][condition]})
       plotForGroup("plot", group_, axis[HUM], x_set_data, y_set_data, color=colors[group_id], args={"linestyle" : ["solid", "dotted"][condition]})

       plotForGroup("plot", group_, axis[VAR], [grid_cpu[q] for q in x_set], bayesianEstimate_sd_byStimulus_model[x_set].detach().cpu().detach(), color=colors[group_id], args={"linestyle" : ["solid", "dotted"][condition]})
       plotForGroup("plot", group_, axis[VAH], x_set_data, sd_set_data, color=colors[group_id], args={"linestyle" : ["solid", "dotted"][condition]})

       axis[ATT].set_ylim(-20, 10)
       axis[REP].set_ylim(-20, 10)
       axis[TOT].set_ylim(-20, 10)
       axis[HUM].set_ylim(-20, 10)
       axis[VAR].set_ylim(0,15)
       axis[VAH].set_ylim(0,15)

       for z in range(len(assigned)):
         axis[z].spines['right'].set_visible(False)
         axis[z].spines['top'].set_visible(False)
         axis[z].set_xticks(ticks=[0,25,50])
       for z in [ENC]:
         axis[z].set_yticks(ticks=[0,1,2,3,4,5], labels=[0, "", "", "", "", 5])

       for z in [PRI]:
         axis[z].set_yticks(ticks=[0])
       for z in [ATT, REP, TOT, HUM]:
         axis[z].set_yticks(ticks=[-15,-10,-5,0,5], labels=["","‒10","",0,""])
       for z in [VAR, VAH]:
         axis[z].set_yticks(ticks=[0,5,10], labels=[0,"",10])

       for z in [REP, TOT, HUM, VAH]:
           axis[z].tick_params(labelleft=False)

   print("LOSS", loss)
   if iteration % 500 == 0:

     axis[0].set_ylim(0, 20)
     for w in range(8):
       axis[w].set_xlim(-1,70)
     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.pdf")
     if SHOW_PLOT:
        plt.show()
     plt.close()

############################3

lowestError = 100000

xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))

yValues = []
for y in observations_y:
   yValues.append(int( torch.argmin((grid - y).abs())))

xValues = MakeLongTensor(xValues)
yValues = MakeLongTensor(yValues)

model(grid)
