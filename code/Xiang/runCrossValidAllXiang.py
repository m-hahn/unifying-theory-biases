import os
import random
import subprocess
import sys
from util import savePlot
script = sys.argv[1]

Ps = [0,2,4,6,8,10]
folds = list(range(10))

for _ in range(1000):
  P = random.choice(Ps)
  if P % 2 == 1:
      continue

  if (P == 0) != ("Zero" in script):
       continue
  if P>6 and "Nelder" not in script:
       continue
  fold = random.choice(folds)
  if "osine" in script and P == 3:
    continue
  print("Considering", f"logs/CROSSVALID/{script}_{P}_{fold}_{10.0}_{151}.txt")
  if os.path.exists(f"logs/CROSSVALID/{script}_{P}_{fold}_{10.0}_{151}.txt"):
     continue
  subprocess.call([str(q) for q in ["python3", script, P, fold, 10.0, 151]])
