cd ../Gardelle/
python3 RunGardelle_NaturalPrior_CosineLoss_VIZ_OnlyModel.py 8 0 10.0 180
python3 RunGardelle_UniformPrior_CosineLoss_VIZ_OnlyModel.py 8 0 10.0 180
python3 RunGardelle_UniformPrior_CosineLoss_VIZ_OnlyHuman_ErrBar.py 8 0 10.0 180
python3 RunGardelle_NaturalPrior_CosineLoss_VIZ2_OnlyVar.py 2 0 10.0 180
python3 RunGardelle_NaturalPrior_CosineLoss_VIZ2_OnlyVar.py 4 0 10.0 180
python3 RunGardelle_NaturalPrior_CosineLoss_VIZ2_OnlyVar.py 6 0 10.0 180
python3 RunGardelle_NaturalPrior_CosineLoss_VIZ2_OnlyVar.py 8 0 10.0 180
python3 RunGardelle_NaturalPrior_CenteredLoss_Viz_Human_CircStat_OnlyVar_ErrBar.py
cd ../Tomassini/
python3 RunTomassini_CosinePLoss_NaturalisticPrior_VIZ_OnlyModel_MainPaper.py 8 0 0.1 180
python3 RunTomassini_CosinePLoss_NaturalisticPrior_VIZ_OnlyHuman_MainPaper_ErrBar.py 8 0 0.1 180
python3 RunTomassini_CosinePLoss_UniPrior_VIZ_OnlyModel_MainPaper.py 8 0 0.1 180
cd ../mainPaper_figures/
