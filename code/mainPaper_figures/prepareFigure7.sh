cd ../Gardelle/
python3 RunGardelle_NaturalPrior_CosineLoss_VIZ.py 8 0 10.0 180
#RunGardelle_NaturalPrior_CosineLoss_VIZ.py_8_0_10.0_180_Magnitudes.pdf}};
cd ../Tomassini/
python3 RunTomassini_CosinePLoss_NaturalisticPrior_VIZ.py 8 0 0.1 180
#RunTomassini_CosinePLoss_NaturalisticPrior_VIZ.py_8_0_0.1_180_Magnitudes.pdf}};
cd ../Gekas/
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_Cardinal.py 4 0 1.0 180 50
#RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_Cardinal.py_4_0_1.0_180_Magnitudes.pdf}};
cd ../Bae/
python3 RunBae_FreePrior_Undelayed_CosineLoss_VIZ3MainPaper.py 4 0 1.0 180
#RunBae_FreePrior_Undelayed_CosineLoss_VIZ3MainPaper.py_4_0_1.0_180_Magnitudes.pdf}};
cd ../Remington/
python3 RunRemington_Lognormal_Zero_VIZ.py 0 0 0.1 200
#RunRemington_Lognormal_Zero_VIZ.py_0_0_0.1_200_Magnitudes_Scatter.pdf}};
cd ../Xiang/
python3 RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_Fig7.py 0 2 10.0 151
#RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_Fig7.py_0_2_10.0_151_Magnitudes.pdf} };
cd ../Gardelle/
python3 evaluateCrossValidationResults_Gardelle_180.py
cd ../Tomassini/
python3 evaluateCrossValidationResults_Tomassini_180.py
cd ../Gekas/
python3 evaluateCrossValidationResults_Gekas_Fixed.py
cd ../Bae/
python3 evaluateCrossValidationResults_Bae_New.py
cd ../Remington/
python3 evaluateCrossValidationResults_Remington_StopNoImpQ.py
cd ../Xiang/
python3 evaluateCrossValidationResults_Xiang_AVE.py
cd ../mainPaper_figures/

