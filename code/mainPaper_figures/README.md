# Figures for the main paper

* [Figure 1](figure1_ver4c_reverse_editable.pdf)
* [Figure 2](figure2_ver2_reverse.pdf)
* [Figure 3](figure3_ver_free.pdf)
* [Figure 4](figure4.pdf)
* [Figure 5](figure5.pdf)
* [Figure 6](figure6_united.pdf)
* [Figure 7](figure7.pdf)

