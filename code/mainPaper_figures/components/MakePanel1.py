import matplotlib
import colormath
import colormath.color_conversions
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import scipy.special
import sys
import torch
from matplotlib import rc
rc('font', **{'family':'FreeSans'})
from colormath.color_objects import XYZColor, sRGBColor
from colormath.color_conversions import convert_color

figure, axis = plt.subplots(1, 1, figsize=(1, 1))

# Colors picked from original paper using https://www.ginifab.com/feeds/pms/color_picker_from_image.php

plt.tight_layout()
for q in range(16):
  for w in range(16):
   color = ((127 + random.gauss(0,2))/255, (152 + random.gauss(0,2))/255, (98 + random.gauss(0,2))/255)
   color = ((105 + random.gauss(0,2))/255, (157 + random.gauss(0,2))/255, (109+ random.gauss(0,2))/255)
   color = ((87 + random.gauss(0,2))/255, (160 + random.gauss(0,2))/255, (126 + random.gauss(0,2))/255)

   r = matplotlib.patches.Rectangle((q,w), width=1, height=1, color=color)
   axis.add_patch(r)
axis.set_xlim(0,16)
axis.set_ylim(0,16)
axis.spines['right'].set_visible(False)
axis.spines['left'].set_visible(False)
axis.spines['top'].set_visible(False)
axis.spines['bottom'].set_visible(False)
figure.subplots_adjust(bottom=0, top=1, left=0, right=1)
plt.savefig(f"{__file__}_150.png")
plt.show()


