import colormath
import colormath.color_conversions
import computations
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import scipy.special
import sys
import torch
from colormath.color_conversions import convert_color
from colormath.color_objects import XYZColor, sRGBColor
from computations import computeResources
from cosineEstimator import CosineEstimator
from loadBae_All import *
from matplotlib import rc
from matplotlib.backends.backend_pdf import PdfPages
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import ToDevice
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import makeGridIndicesCircular
from util import mean
from util import product
from util import savePlot
from util import toFactor
__file__ = __file__.split("/")[-1]
rc('font', **{'family':'FreeSans'})

OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
assert P > 0
FOLD_HERE = int(sys.argv[2])
REG_WEIGHT = float(sys.argv[3])
GRID = int(sys.argv[4])
assert GRID == 180
SHOW_PLOT = (len(sys.argv) < 6) or (sys.argv[5] == "SHOW_PLOT")
DEVICE = 'cpu'

FILE = f"logs/CROSSVALID/{__file__.replace('_VIZ3', '')}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"

# Helper Functions dependent on the device

N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)
Subject=subjects
seed = random.randint(1000,100000)

set_of_subjects = sorted(list(set(Subject.cpu().numpy().tolist())))
subjects_resampled = random.choices(set_of_subjects, k=len(set_of_subjects))
print(subjects_resampled)

data_indices = ToDevice(torch.arange(0, Subject.size()[0]))
observations_per_subject = {i : data_indices[(Subject == i)] for i in set_of_subjects}

subject_resampled = []
boot = []
for i in range(len(subjects_resampled)):
   trials = observations_per_subject[subjects_resampled[i]]
   subject_resampled = subject_resampled + [i for _ in range(observations_per_subject[subjects_resampled[i]].size()[0])]
   boot = boot + trials.cpu().numpy().tolist()

assert len(boot) == sum([observations_per_subject[i].size()[0] for i in subjects_resampled])
boot = ToDevice(MakeLongTensor(boot))

Subject = ToDevice(MakeLongTensor(subject_resampled))

target = target[boot]
bias = bias[boot]
condition = condition[boot]

print("Determining folds")
Fold = 0*Subject
for i in range(min(Subject), max(Subject)+1):
    trials = [j for j in range(Subject.size()[0]) if Subject[j] == i]
    randomGenerator.shuffle(trials)
    foldSize = int(len(trials)/N_FOLDS)
    for k in range(N_FOLDS):
        Fold[trials[k*foldSize:(k+1)*foldSize]] = k
print("Finished determining folds")

observations_x = target
observations_y = (observations_x + bias) % 360

# 4. SET UP THE GRID

# Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 360

CIRCULAR = True
INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
grid, grid_indices_here = makeGridIndicesCircular(GRID, MIN_GRID, MAX_GRID)

# Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

# 5. SET UP THE PARAMETERS AND OPTIMIZER

# Initialize the model
fitted_parameters_byRun = []
lossCurves = []
with open("bootstrap/logs/CROSSVALID/RunBae_FreePrior_Both_CosineLoss_BootstrapHier2.py_2_0_1.0_180.txt", "r") as inFile:
 try:
  while True:
    l = next(inFile)
    if l.startswith("#"):
        fitted_parameters_byRun.append({})
        u = l.strip().split(" ")
        assert u[5] == "CrossValidLossesBy500", u[:7]
        lossCurves.append([float(q) for q in u[6:]])

        l = next(inFile)
        l = next(inFile)
    z, y = l.strip().split("\t")
    fitted_parameters_byRun[-1][z.strip()] = MakeFloatTensor(json.loads(y))
 except StopIteration:
     pass
print("Number of runs", len(fitted_parameters_byRun))
print(lossCurves)

figure, axis = plt.subplots(1, 1, figsize=(6,2))
plt.tight_layout()
for c in lossCurves:
  axis.plot([500*i for i in range(1,len(c)+1)], c)
savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_LossCurve.pdf")

plt.close()

figure, axis = plt.subplots(1, 1, figsize=(1.5,1.5))
plt.tight_layout()
for j in range(1):
  axis.hist([1/(4*torch.sigmoid(q["sigma_logit"][j])).item() for q in fitted_parameters_byRun])

savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_sensoryKappa.pdf")

plt.close()

##############################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 2*math.pi

computations.setData(STIMULUS_SPACE_VOLUME=STIMULUS_SPACE_VOLUME, GRID=GRID)

figure, axis = plt.subplots(1, 1, figsize=(3,2))
plt.tight_layout()
for i in range(len(lossCurves)):
   resources_computed_overall = MakeZeros(grid.size())
   volume = 2 * math.pi * torch.nn.functional.softmax(fitted_parameters_byRun[i]["volume"], dim=0)

   for condition_ in [0,1]:
     sensory_variance = 2*torch.sigmoid(fitted_parameters_byRun[i]["sigma_logit"][condition_])
     resources_computed = computeResources(volume, 1/sensory_variance)
     resources_computed_overall = resources_computed_overall + 0.5 * resources_computed
   axis.plot(grid.view(-1), resources_computed_overall, color="gray", alpha=0.02)
axis.set_ylim(bottom=0)

axis.set_xticks(ticks=[0,180,360], labels=["0°", "180°", "360°"])

savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_volume.pdf")

plt.close()

figure, axis = plt.subplots(1, 1, figsize=(3,2))
plt.tight_layout()
for i in range(len(lossCurves)):
  axis.plot(grid.view(-1), torch.softmax(fitted_parameters_byRun[i]["prior"].view(-1), dim=0), color="gray", alpha=0.02)
axis.set_ylim(bottom=0)
axis.set_yticks(ticks=[0])
axis.set_xticks(ticks=[0,180,360], labels=["0°", "180°", "360°"])

savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_prior.pdf")

plt.close()


