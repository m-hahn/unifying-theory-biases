from matplotlib.backends.backend_pdf import PdfPages
import colormath
import colormath.color_conversions
import computations
import getObservations
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import scipy.integrate
import sys
import torch
from colormath.color_conversions import convert_color
from colormath.color_objects import XYZColor, sRGBColor
from computations import computeResources
from cosineEstimator import CosineEstimator
from getObservations import retrieveObservationsDirect
from loadBae_All import *
from matplotlib import rc
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import ToDevice
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import makeGridIndicesCircular
from util import mean
from util import product
from util import savePlot
from util import sech
from util import toFactor

rc('font', **{'family':'FreeSans'})

OPTIMIZER_VERBOSE = False

P = int(sys.argv[1])
assert P != 3
assert P > 0
FOLD_HERE = int(sys.argv[2])
REG_WEIGHT = float(sys.argv[3])
GRID = int(sys.argv[4])
assert GRID % 180 == 0
FOURIER_BASIS_SIZE = int(sys.argv[5])
assert FOURIER_BASIS_SIZE in [30, 50, 80, 89]
FILE = f"logs/CROSSVALID/{__file__.replace('_VIZ', '')}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_{FOURIER_BASIS_SIZE}.txt"

print("SUBJECTS", sorted(list(set(subjects))))

N_SUBJECTS = 1+int(subjects.max())
N_OBSERV = 9/10 * subjects.size()[0]
N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)
Subject=subjects
Fold = 0*Subject
for i in range(min(Subject), max(Subject)+1):
    trials = [j for j in range(Subject.size()[0]) if Subject[j] == i]
    randomGenerator.shuffle(trials)
    foldSize = int(len(trials)/N_FOLDS)
    for k in range(N_FOLDS):
        Fold[trials[k*foldSize:(k+1)*foldSize]] = k

observations_x = target
observations_y = (observations_x + bias) % 360

# 4. SET UP THE GRID

# Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 360
RANGE_GRID = MAX_GRID-MIN_GRID

INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
grid, grid_indices_here = makeGridIndicesCircular(GRID, MIN_GRID, MAX_GRID)

# Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

# 5. SET UP THE PARAMETERS AND OPTIMIZER

# Initialize the model
init_parameters = {}
init_parameters["log_motor_var"] = MakeFloatTensor(N_SUBJECTS*[0])
init_parameters["sigma_logit"] = -1 + MakeZeros(N_SUBJECTS)
init_parameters["mixture_logit"] = MakeFloatTensor(N_SUBJECTS*[-1])
init_parameters["prior"] = MakeZeros(GRID)
init_parameters["volume"] = MakeZeros(GRID)
init_parameters["priorBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["prior_logits_effect_weight"] = MakeZeros(1)
init_parameters["volumeBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["volume_logits_effect_weight"] = MakeZeros(1)

init_parameters["SIGMA_priorBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
init_parameters["SIGMA_volumeBySubject"] = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)

with open(FILE, "r") as inFile:
    (next(inFile))
    (next(inFile))
    for l in inFile:
        z, y = l.split("\t")
        assert init_parameters[z.strip()].size() == MakeFloatTensor(json.loads(y)).size(), (z)
        init_parameters[z.strip()] = MakeFloatTensor(json.loads(y))
assert "volume" in init_parameters
lossesBy500 = []
crossLossesBy500 = []

for _, y in init_parameters.items():
    y.requires_grad = True

##############################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 2*math.pi

# Part: Specify `similariy` or `difference` functions.
## These are negative squared distances (for interval spaces) or
## trigonometric functions (for circular spaces), with
## some extra factors for numerical purposes.
## Exponentiating a `similarity` function and normalizing
## is equivalent to the Gaussian / von Mises density.
## The purpose of specifying these as `closeness` or `distance`,
## rather than simply calling squared or trigonometric
## functions is to  flexibly reuse the same model code for
## both interval and circular spaces.
def SQUARED_STIMULUS_DIFFERENCE(x):
    return torch.sin(math.pi*x/180)
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(math.pi*x/180)
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(x)
def SQUARED_SENSORY_DIFFERENCE(x):
    return torch.sin(x)

##############################################
# Import/define the appropriate estimator for minimizing the loss function
# 6. SET UP THE LOSS FUNCTION
# Import/define the appropriate estimator for minimizing the loss function

CosineEstimator.set_parameters(GRID=GRID, OPTIMIZER_VERBOSE=OPTIMIZER_VERBOSE, P=P, SQUARED_SENSORY_DIFFERENCE=SQUARED_SENSORY_DIFFERENCE, SQUARED_SENSORY_SIMILARITY=SQUARED_SENSORY_SIMILARITY)

# 7. SPECIFY THE MODEL (DATA LIKELIHOOD AND BIAS COMPUTATION)
#############################################################
# Part: Run the model. This function implements the model itself:
## calculating the likelihood of a given dataset under that model
## and---if the computePredictions argument is set to True--- computes
## the bias and variability of the estimate.
def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, computePredictions=False, parameters=None, condition_=None, folds=None, lossReduce='mean', sigma2_stimulus=0, subject=None):

 # Part: Obtain the motor variance by exponentiating the appropriate model parameter
 motor_variance = torch.exp(- parameters["log_motor_var"][subject])
 # Part: Obtain the sensory noise variance.
 sigma2 = 2*torch.sigmoid(sigma_logit[subject])
 # Part: Obtain the transfer function as the cumulative sum of the discretized resource allocation (referred to as `volume` element due to the geometric interpretation by Wei&Stocker 2015)

 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 if True:
  # Part: Select data for the relevant fold
  folds = MakeLongTensor(folds)
  MASK = torch.logical_and(subjects == subject, (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
  stimulus = stimulus_[MASK]
  responses = responses_[MASK]

  # Part: Compute sensory likelihoods. Across both interval and
  ## circular stimulus spaces, this amounts to exponentiaring a
  ## `similarity`
  sensory_likelihoods = torch.softmax(((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2))  + volumeElement.unsqueeze(1).log(), dim=0)

  # Part: If stimulus noise is nonzero, convolve the likelihood with the
  ## stimulus noise.
  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    ## On this dataset, this is zero, so the
    ## code block will not be used.
    assert False
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  ## Compute posterior using Bayes' rule. As described in the paper, the posterior is computed
  ## in the discretized stimulus space.
  posterior = prior.unsqueeze(1) * likelihoods.t()
  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  ## Compute the estimator for each m in the discretized sensory space.
  bayesianEstimate = CosineEstimator.apply(grid_indices_here, posterior)

  ## Compute the motor likelihood
  ## `error' refers to the stimulus similarity between the estimator assigned to each m and
  ## the observations found in the dataset.
  ## The Gaussian or von Mises motor likelihood is obtained by exponentiating and normalizing

  # Caculate motor likelihood
  error = (SQUARED_STIMULUS_SIMILARITY(360/GRID*bayesianEstimate.unsqueeze(0) - responses.unsqueeze(1)))
  ## The log normalizing constants, for each m in the discretized sensory space
  log_normalizing_constant = torch.logsumexp((SQUARED_STIMULUS_SIMILARITY(grid))/motor_variance, dim=0) + math.log(2 * math.pi / GRID)
  ## The log motor likelihoods, for each pair of sensory encoding m and observed human response
  log_motor_likelihoods = (error/motor_variance) - log_normalizing_constant
  ## Obtaining the motor likelihood by exponentiating.
  motor_likelihoods = torch.exp(log_motor_likelihoods)

  ## Obtain the guessing rate, parameterized via the (inverse) logit transform as described in SI Appendix
  # Mixture of estimation and uniform response
  uniform_part = torch.sigmoid(parameters["mixture_logit"][subject])
  ## The full likelihood then consists of a mixture of the motor likelihood calculated before, and the uniform
  ## distribution on the full space.
  motor_likelihoods = (1-uniform_part) * motor_likelihoods + (uniform_part / (2*math.pi) + 0*motor_likelihoods)

  # Now the loss is obtained by marginalizing out m from the motor likelihood
  if lossReduce == 'mean':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().mean()
  elif lossReduce == 'sum':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().sum()
  else:
    assert False

  ## If computePredictions==True, compute the bias and variability of the estimate
  if computePredictions:
     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS
     bayesianEstimate_avg_byStimulus = computeCircularMeanWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = torch.sqrt(bayesianEstimate_sd_byStimulus.pow(2) + motor_variance * 3282.806)

     bayesianEstimate_avg_byStimulus = torch.where((bayesianEstimate_avg_byStimulus-grid).abs()<180, bayesianEstimate_avg_byStimulus, torch.where(bayesianEstimate_avg_byStimulus > 180, bayesianEstimate_avg_byStimulus-360, bayesianEstimate_avg_byStimulus+360))
     assert float(((bayesianEstimate_avg_byStimulus-grid).abs()).max()) < 180, float(((bayesianEstimate_avg_byStimulus-grid).abs()).max())

     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = computeCircularMeanWeighted(posteriorMaxima.unsqueeze(1), likelihoods)
     encodingBias = computeCircularMeanWeighted(grid.unsqueeze(1), likelihoods)
     attraction = (posteriorMaxima-encodingBias)
     attraction1 = attraction
     attraction2 = attraction+360
     attraction3 = attraction-360
     attraction = torch.where(attraction1.abs() < 180, attraction1, torch.where(attraction2.abs() < 180, attraction2, attraction3))
     encodingBias = encodingBias-grid
     encodingBias1 = encodingBias
     encodingBias2 = encodingBias+360
     encodingBias3 = encodingBias-360
     encodingBias = torch.where(encodingBias1.abs() < 180, encodingBias1, torch.where(encodingBias2.abs() < 180, encodingBias2, encodingBias3))
  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
     encodingBias = None

  if float(loss) != float(loss):
      print("NAN 526476!!!!")
      quit()
  return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction, encodingBias

computations.setData(STIMULUS_SPACE_VOLUME=STIMULUS_SPACE_VOLUME, GRID=GRID)

getObservations.setData(x_set=x_set, observations_y=observations_y, xValues=xValues, condition=condition, grid=grid, GRID=GRID, SQUARED_STIMULUS_SIMILARITY=SQUARED_STIMULUS_SIMILARITY, subjects=subjects)

trigonometric_basis = torch.stack([SQUARED_STIMULUS_SIMILARITY(i*grid) for i in range(1, FOURIER_BASIS_SIZE+1)] + [SQUARED_STIMULUS_DIFFERENCE(i*grid) for i in range(1, FOURIER_BASIS_SIZE+1)], dim=0)
trigonometric_basis = trigonometric_basis * (GRID / (2*trigonometric_basis.pow(2).sum(dim=1, keepdim=True)))
fourierMultiplier = (MakeFloatTensor(list(range(1,FOURIER_BASIS_SIZE+1)) + list(range(1,FOURIER_BASIS_SIZE+1))))

print(fourierMultiplier)
print(trigonometric_basis.pow(2).sum(dim=1))
if False:
  figure, axis = plt.subplots(2*FOURIER_BASIS_SIZE, 1, figsize=(50, 50))
  for i in range(trigonometric_basis.size()[0]):
     axis[i].plot(grid.cpu(), trigonometric_basis[i].cpu())
  savePlot(f"figures/{__file__}_BASIS_{GRID}_{FOURIER_BASIS_SIZE}.pdf")
  plt.close()

def model(grid):
  lossesBy500 = []
  crossLossesBy500 = []
  noImprovement = 0
  global optim, learning_rate
  for iteration in range(1):
   parameters = init_parameters

   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   volumeBySubjectRandomAdjustment = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)
   priorBySubjectRandomAdjustment = MakeZeros(N_SUBJECTS,2*FOURIER_BASIS_SIZE)

   volumeBySubjectFourierTimesWeightInclMean = volumeBySubjectRandomAdjustment + init_parameters["volumeBySubject"]
   priorBySubjectFourierTimesWeightInclMean = priorBySubjectRandomAdjustment + init_parameters["priorBySubject"]

   volumeBySubjects = torch.matmul(volumeBySubjectFourierTimesWeightInclMean / fourierMultiplier.unsqueeze(0), trigonometric_basis)
   priorBySubjects = torch.matmul(priorBySubjectFourierTimesWeightInclMean / fourierMultiplier.unsqueeze(0), trigonometric_basis)

   loss = 0
   with PdfPages(f"figures/{__file__}_{P}_ALL_{REG_WEIGHT}_{GRID}.pdf") as pdf:
    for SUBJECT in range(0, N_SUBJECTS):
     if iteration % 500 == 0:
       figure, axis = plt.subplots(1, 6, figsize=(12,2))
       plt.tight_layout()
     volume = 2 * math.pi * torch.nn.functional.softmax(parameters["volume"] + (volumeBySubjects[SUBJECT] if SUBJECT >= 0 else 0))
     prior = torch.nn.functional.softmax(parameters["prior"] + (priorBySubjects[SUBJECT] if SUBJECT >= 0 else 0))
     loss_, bayesianEstimate, bayesianEstimate_sd, attraction, encodingBias = computeBias(xValues, init_parameters["sigma_logit"], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, computePredictions=(iteration % 500 == 0), parameters=parameters, condition_=None, subject=SUBJECT, lossReduce='sum', folds=testFolds)
     loss = loss + loss_
     if iteration % 500 == 0:
       _, y_set, sd_set, y_errbar, sd_errbar = retrieveObservationsDirect(None, subject=SUBJECT)

       CI_x = [float(grid[x]) for x in x_set]
       CI_sd1 = ((sd_errbar[:,0])).detach().numpy().tolist()
       CI_sd2 = ((sd_errbar[:,1])).detach().numpy().tolist()
       poly = plt.Polygon(list(zip(CI_x + CI_x[::-1], CI_sd1 + CI_sd2[::-1])), facecolor=(.65, .65, .65), alpha=0.5)

       axis[5].add_patch(poly)

       CI_x = [float(grid[x]) for x in x_set]
       CI_y1 = ((y_errbar[:,0])).detach().numpy().tolist()
       CI_y2 = ((y_errbar[:,1])).detach().numpy().tolist()
       poly = plt.Polygon(list(zip(CI_x + CI_x[::-1], CI_y1 + CI_y2[::-1])), facecolor=(.65, .65, .65), alpha=0.5)

       axis[4].add_patch(poly)

       sensory_variance = 2*torch.sigmoid(init_parameters["sigma_logit"][SUBJECT])
       resources_computed = computeResources(volume, 1/sensory_variance)
       axis[0].scatter([grid[0], grid[-1]], [0,0], c='white')
       axis[1].scatter([grid[0], grid[-1]], [0,0], c='white')
       axis[5].scatter([grid[0], grid[-1]], [0,0], c='white')

       axis[0].set_ylim(0, 1.1*float(resources_computed.max()))
       axis[1].set_ylim(0, 1.1*float(prior.max()))

       axis[1].set_yticks(ticks=[0])

       LINEWIDTH = 3
       PLOTTING_STEPS = 3
       for i in range(PLOTTING_STEPS,GRID-1,PLOTTING_STEPS):
          color = categories_color[int(180*i/GRID)]
          L = 70
          a = 38 * math.cos(2*math.pi*i/GRID)
          b = 38 * math.sin(2*math.pi*i/GRID)
          color = colormath.color_conversions.convert_color(colormath.color_objects.LabColor(L,a,b), target_cs=colormath.color_objects.AdobeRGBColor)
          color = (color.rgb_r, color.rgb_g, color.rgb_b)

          axis[0].plot([grid[i-PLOTTING_STEPS].cpu(), grid[i].cpu()], [resources_computed[i-3].cpu().detach(), resources_computed[i].cpu().detach()],  c=color, linewidth=LINEWIDTH)

          axis[1].plot([grid[i-PLOTTING_STEPS].cpu(), grid[i].cpu()], [prior[i-PLOTTING_STEPS].cpu().detach(), (prior)[i].cpu().detach()], c=color, linewidth=LINEWIDTH)

          axis[2].plot([grid[i-PLOTTING_STEPS].cpu(), grid[i].cpu()], [(bayesianEstimate-grid-attraction)[i-PLOTTING_STEPS].cpu().detach(), (bayesianEstimate-grid-attraction)[i].cpu().detach()], c=color, linewidth=LINEWIDTH)

          axis[3].plot([grid[i-PLOTTING_STEPS].cpu(), grid[i].cpu()], [attraction[i-PLOTTING_STEPS].cpu().detach(), attraction[i].cpu().detach()], c=color, linewidth=LINEWIDTH)

          axis[4].plot([grid[i-PLOTTING_STEPS].cpu(), grid[i].cpu()], [(bayesianEstimate-grid)[i-PLOTTING_STEPS].cpu().detach(), (bayesianEstimate-grid)[i].cpu().detach()], c=color, linewidth=LINEWIDTH)

          axis[5].plot([grid[i-PLOTTING_STEPS].cpu(), grid[i].cpu()], [bayesianEstimate_sd[i-PLOTTING_STEPS].cpu().detach(), bayesianEstimate_sd[i].cpu().detach()], c=color, linewidth=LINEWIDTH)
       if False:
         axis[0].plot([130, 150, 150, 130, 130], [0.1, 0.1, 0.5, 0.5, 0.1], color="gray")
       lastStart = 0
       for i in range(1,len(x_set)-1,2):
          if ((grid[x_set[lastStart]] - grid[x_set[i]])/360).pow(2) + ((y_set[lastStart] - y_set[i])/20).pow(2) >= 0.01:
             L = 70
             j = (i-lastStart)/2 + lastStart
             a = 38 * math.sin(2*math.pi*j/GRID)
             b = 38 * math.cos(2*math.pi*j/GRID)
             color = colormath.color_conversions.convert_color(colormath.color_objects.LabColor(L,a,b), target_cs=colormath.color_objects.AdobeRGBColor)
             color = (color.rgb_r, color.rgb_g, color.rgb_b)
             axis[4].plot(grid[x_set[lastStart:i+1]].cpu(), y_set[lastStart:i+1].cpu().detach(), c=color, linestyle='dotted', linewidth=LINEWIDTH)
             lastStart = i

       lastStart = 0
       for i in range(1,len(x_set)-1,2):
          if ((grid[x_set[lastStart]] - grid[x_set[i]])/360).pow(2) + ((sd_set[lastStart] - sd_set[i])/15).pow(2) >= 0.01:
             L = 70
             j = (i-lastStart)/2 + lastStart
             a = 38 * math.sin(2*math.pi*j/GRID)
             b = 38 * math.cos(2*math.pi*j/GRID)
             color = colormath.color_conversions.convert_color(colormath.color_objects.LabColor(L,a,b), target_cs=colormath.color_objects.AdobeRGBColor)
             color = (color.rgb_r, color.rgb_g, color.rgb_b)
             axis[5].plot(grid[x_set[lastStart:i+1]].cpu(), sd_set[lastStart:i+1].cpu().detach(), c=color, linestyle='dotted', linewidth=LINEWIDTH)
             lastStart = i

       axis[2].set_ylim(-15,15)
       axis[3].set_ylim(-15,15)
       axis[4].set_ylim(-15,15)

       for q in range(6):
         axis[q].set_xticks(ticks=[0,180,360], labels=["0°", "180°", "360°"])

     print("Held-out NLL LOSS", loss)
     regularizer1 = ((init_parameters["volume"][1:] - init_parameters["volume"][:-1]).pow(2).sum() + (init_parameters["volume"][0] - init_parameters["volume"][-1]).pow(2))/GRID
     regularizer2 = ((init_parameters["prior"][1:] - init_parameters["prior"][:-1]).pow(2).sum() + (init_parameters["prior"][0] - init_parameters["prior"][-1]).pow(2))/GRID

     loss = loss + REG_WEIGHT * regularizer1 * N_OBSERV
     loss = loss + REG_WEIGHT * regularizer2 * N_OBSERV

     if iteration % 10 == 0:
       print(iteration, loss, init_parameters["sigma_logit"], torch.sigmoid(parameters["mixture_logit"]) )
     if iteration % 500 == 0:
       axis[0].set_title("Resources")
       axis[1].set_title("Prior")
       axis[2].set_title("Repulsion")
       axis[3].set_title("Attraction")
       axis[4].set_title("Bias")
       axis[5].set_title("Variability")
       pdf.savefig(figure)
#       plt.show()
 #      plt.close()

############################3

############################3

model(grid)
