import matplotlib.pyplot as plt
import math
import torch
from util import computeCircularMean, computeCenteredMean, computeCircularSD, MakeFloatTensor, computeCircularMeanWeighted, computeCircularMeanWeighted, mean, computeMeanWeighted, computeCircularSDWeighted, computeCircularConfidenceInterval, bringCircularBiasCloseToZero
def setData(**kwargs):
    global x_set
    x_set = kwargs["x_set"]
    global observations_y
    observations_y=kwargs["observations_y"]
    global xValues
    xValues=kwargs["xValues"]
    global condition
    condition=kwargs["condition"]
    global grid
    grid=kwargs["grid"]
    global GRID
    GRID = grid.size()[0]
    global SQUARED_STIMULUS_SIMILARITY
    if "SQUARED_STIMULUS_SIMILARITY" in kwargs:
       SQUARED_STIMULUS_SIMILARITY=kwargs["SQUARED_STIMULUS_SIMILARITY"]
    global subjects
    if "subjects" in kwargs:
       subjects=kwargs["subjects"]


def retrieveObservationsDirect(x, meanMethod = "circular", condition_=None, subject=None):
   y_set_boot = []
   sd_set_boot = []
   for _ in range(100):
     boot = torch.randint(low=0, high=observations_y.size()[0]-1, size=observations_y.size())
     observations_y__ = observations_y[boot]
     xValues__ = xValues[boot]
     condition__ = condition[boot]
     if subject is not None:
        subjects__ = subjects[boot]
     y_sets = []
     sd_sets = []
     if True:
       if subject is None and condition_ is None:
          MASK = (condition__==condition__) # i.e., True
       elif subject is None:
          MASK = (condition__==condition_)
       else:
          MASK = (subjects__==subject)



       kernel = torch.softmax(20 * SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0) - grid[xValues__[MASK]].unsqueeze(1)), dim=0)
       y_here = observations_y__[MASK]
       y_smoothed = computeCircularMeanWeighted(y_here.unsqueeze(1), kernel)
  
  
       y_set_boot.append(y_smoothed)

       sd_set = []
       for x in x_set:
        if subject is None and condition_ is None:
          y_here = observations_y__[xValues__ == x]
        elif subject is None:
          y_here = observations_y__[(torch.logical_and((xValues__ == x), condition__==condition_))]
        else:
          y_here = observations_y__[(torch.logical_and((xValues__ == x), subjects__==subject))]
        sd_set.append(computeCircularSD(y_here))
       sd_set = torch.FloatTensor(sd_set)
       kernel = torch.softmax(20 * SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0) - grid[x_set].unsqueeze(1)) - 100000*torch.isnan(sd_set.unsqueeze(1)).float(), dim=0)
       sd_set[torch.isnan(sd_set)] = 0
       sd_set = (sd_set.unsqueeze(1) * kernel).sum(dim=0)
       sd_set_boot.append(sd_set)
    
   y_set = torch.stack(y_set_boot, dim=0)
   sd_set = torch.stack(sd_set_boot, dim=0)
   if True:
     y_set_var = computeCircularConfidenceInterval(y_set) 
   else:
     y_set_var = computeCircularSDWeighted(y_set) 
   if True:
     sd_set_var = torch.stack([sd_set.quantile(q=.025, dim=0), sd_set.quantile(q=.975, dim=0)], dim=1)
   else:
     sd_set_var = (sd_set.pow(2).mean(dim=0) - sd_set.mean(dim=0).pow(2)).sqrt()

   y_set = computeCircularMeanWeighted(y_set)


   y_set = bringCircularBiasCloseToZero(y_set - grid)
   y_set_var[:,0] = bringCircularBiasCloseToZero(y_set_var[:,0] - grid)
   y_set_var[:,1] = bringCircularBiasCloseToZero(y_set_var[:,1] - grid)


   return x_set, y_set, sd_set.mean(dim=0), y_set_var, sd_set_var


