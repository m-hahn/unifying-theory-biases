from util import MakeZeros
from util import MakeFloatTensor
from util import MakeLongTensor
from util import ToDevice


with open("data/BAE/ParamEst_JEPG.csv", "r") as inFile:
    data = [x.split("\t") for x in inFile.read().strip().split("\n")]
    head = [x.strip() for x in data[0]]
    data = data[1:]
    categories = [int(x[head.index("Category")]) for x in data]
    categories_names = [x[head.index("CatNames")] for x in data]
categories_color = []
for x in categories_names:
    x = "".join([y for y in x.lower() if not y.isdigit()])
    if "border" in x or "focal" in x:
        x = "black"
    if x == "oragne":
        x = "orange"
    elif x == "greem":
        x = "green"
    categories_color.append(x)

target = []
bias = []
subject = []
condition = []
for group, f in enumerate(["data/BAE/SumNewColorPerception.csv"]):
  with open(f, "r") as inFile:
    data = [x.split("\t") for x in inFile.read().strip().split("\n")]
    header = [x.strip() for x in data[0]]
    data = [[float(q) for q in w] for w in data[1:]]
    Subject = header.index("subject_Num")
    TargetColor = header.index("TargetColor")
    TargetAngle = header.index("TargetAngle")
    ReportAngle = header.index("ReportAngle")
    Rotation = header.index("Rotation")
    target += [2*x[TargetColor] for x in data]
    bias += [(x[TargetAngle]-x[ReportAngle]) for x in data]
    subject += [10*group + int(x[Subject]) for x in data]
    condition += [group for x in data]

target = MakeFloatTensor(target)
bias = MakeFloatTensor(bias)
condition = MakeLongTensor(condition)
subject = MakeLongTensor(subject)

observations_x = target
observations_y = (observations_x + bias) % 360

