import os
import random
import subprocess
import sys
from util import savePlot
script = sys.argv[1]

Ps = [0,2,4,6,8,10]
folds = list(range(10))

for _ in range(1000):
  P = random.choice(Ps)
  if P%2 == 1:
     continue
  if P == 3 and "Cosine" in script:
     continue
  if P == 0 and "Zero" not in script:
     continue
  if P > 0 and P < 1 and "SmallExponent" not in script:
    continue
  if P > 1 and ("SmallExponent" in script or "Zero" in script):
    continue
  fold = random.choice(folds)
  REG = random.choice([1.0])
  print(f"logs/CROSSVALID/{script.replace('_GPU', '')}_{P}_{fold}_{REG}_180.txt")
  if os.path.exists(f"logs/CROSSVALID/{script.replace('_GPU', '')}_{P}_{fold}_{REG}_180.txt"):
     print("Exists")
     continue
  subprocess.call([str(q) for q in ["/proj/mhahn.shadow/SOFTWARE/miniconda3/envs/py39-first/bin/python3.9", script, P, fold, REG, 180]])
