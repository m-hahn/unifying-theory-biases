import colormath
import colormath.color_conversions
import computations
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sys
import torch
from colormath.color_conversions import convert_color
from colormath.color_objects import XYZColor, sRGBColor
from computations import computeResources, computeResourcesWithStimulusNoise_TakesVariance
from cosineEstimator import CosineEstimator
from matplotlib import rc
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import makeGridIndicesCircular
from util import mean
from util import product
from util import savePlot
from util import sech
from util import sign

rc('font', **{'family':'FreeSans'})
# Helper Functions dependent on the device

OPTIMIZER_VERBOSE = False

# 1. OBTAIN ARGUMENTS
P = int(sys.argv[1])
assert P > 0
FOLD_HERE = int(sys.argv[2])
REG_WEIGHT = float(sys.argv[3])
GRID = int(sys.argv[4])
assert GRID in [180,181]
SHOW_PLOT = (len(sys.argv) < 6) or (sys.argv[5] == "SHOW_PLOT")

FILE = f"logs/CROSSVALID/{__file__.replace('_VIZ3_Regression_VizOnlyBias', '')}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"
if os.path.exists(f"losses/{FILE.replace('logs/CROSSVALID/', '')}.txt") and not SHOW_PLOT:
   print("Already done")
   assert False
if not os.path.exists(FILE):
   print("Missing model")
   assert False, FILE

with open("data/BAE/ParamEst_JEPG.csv", "r") as inFile:
    data = [x.split("\t") for x in inFile.read().strip().split("\n")]
    head = [x.strip() for x in data[0]]

    data = data[1:]
    categories = [int(x[head.index("Category")]) for x in data]
    categories_names = [x[head.index("CatNames")] for x in data]
categories_color = []
for x in categories_names:
    x = "".join([y for y in x.lower() if not y.isdigit()])

    if "border" in x or "focal" in x:
        x = "black"
    if x == "oragne":
        x = "orange"
    elif x == "greem":
        x = "green"
    categories_color.append(x)

target = []
bias = []
subject = []
condition = []
for group, f in enumerate(["data/BAE/SumNewColorPerception.csv"]):
  with open(f, "r") as inFile:
    data = [x.split("\t") for x in inFile.read().strip().split("\n")]
    header = [x.strip() for x in data[0]]
    data = [[float(q) for q in w] for w in data[1:]]
    Subject = header.index("subject_Num")
    TargetColor = header.index("TargetColor")
    TargetAngle = header.index("TargetAngle")
    ReportAngle = header.index("ReportAngle")
    Rotation = header.index("Rotation")
    target += [2*x[TargetColor] for x in data]
    bias += [(x[TargetAngle]-x[ReportAngle]) for x in data]
    subject += [10*group + int(x[Subject]) for x in data]
    condition += [group for x in data]

target = MakeFloatTensor(target)
bias = MakeFloatTensor(bias)
condition = MakeLongTensor(condition)
subject = MakeLongTensor(subject)

N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)
Subject=subject
Fold = 0*Subject
for i in range(min(Subject), max(Subject)+1):
    trials = [j for j in range(Subject.size()[0]) if Subject[j] == i]
    randomGenerator.shuffle(trials)
    foldSize = int(len(trials)/N_FOLDS)
    for k in range(N_FOLDS):
        Fold[trials[k*foldSize:(k+1)*foldSize]] = k

observations_x = target
observations_y = (observations_x + bias) % 360

# 4. SET UP THE GRID

# Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 360

CIRCULAR = True
INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
grid, grid_indices_here = makeGridIndicesCircular(GRID, MIN_GRID, MAX_GRID)

# Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))

xValues = MakeLongTensor(xValues)

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

# 5. SET UP THE PARAMETERS AND OPTIMIZER

# Initialize the model
init_parameters = {}
init_parameters["log_motor_var"] = MakeFloatTensor([0]).view(1)
init_parameters["sigma_logit"] = MakeFloatTensor(2*[-1]).view(2)
init_parameters["mixture_logit"] = MakeFloatTensor([-1]).view(1)
init_parameters["prior"] = MakeZeros(GRID)
init_parameters["volume"] = MakeZeros(GRID)

with open(FILE, "r") as inFile:
    (next(inFile))
    (next(inFile))
    for l in inFile:
        z, y = l.split("\t")
        assert init_parameters[z.strip()].size() == MakeFloatTensor(json.loads(y)).size(), (z)
        init_parameters[z.strip()] = MakeFloatTensor(json.loads(y))
assert "volume" in init_parameters
for _, y in init_parameters.items():
    y.requires_grad = True

##############################################
# Initialize optimizer.
# The learning rate is a user-specified parameter.
learning_rate = 0.2
optim = torch.optim.SGD([y for _, y in init_parameters.items()], lr=learning_rate)

##############################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 2*math.pi

# Part: Specify `similariy` or `difference` functions.
## These are negative squared distances (for interval spaces) or
## trigonometric functions (for circular spaces), with
## some extra factors for numerical purposes.
## Exponentiating a `similarity` function and normalizing
## is equivalent to the Gaussian / von Mises density.
## The purpose of specifying these as `closeness` or `distance`,
## rather than simply calling squared or trigonometric
## functions is to  flexibly reuse the same model code for
## both interval and circular spaces.
def SQUARED_STIMULUS_DIFFERENCE(x):
    return torch.sin(math.pi*x/180)
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(math.pi*x/180)
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(x)
def SQUARED_SENSORY_DIFFERENCE(x):
    return torch.sin(x)

##############################################
# Import/define the appropriate estimator for minimizing the loss function
# 6. SET UP THE LOSS FUNCTION
# Import/define the appropriate estimator for minimizing the loss function

CosineEstimator.set_parameters(GRID=GRID, OPTIMIZER_VERBOSE=OPTIMIZER_VERBOSE, P=P, SQUARED_SENSORY_DIFFERENCE=SQUARED_SENSORY_DIFFERENCE, SQUARED_SENSORY_SIMILARITY=SQUARED_SENSORY_SIMILARITY)

# 7. SPECIFY THE MODEL (DATA LIKELIHOOD AND BIAS COMPUTATION)
#############################################################
# Part: Run the model. This function implements the model itself:
## calculating the likelihood of a given dataset under that model
## and---if the computePredictions argument is set to True--- computes
## the bias and variability of the estimate.
def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, parameters=None, StimulusSD=None, computePredictions=False, sigma_stimulus=0, sigma2_stimulus=0, condition_=None, folds=None, lossReduce='mean'):

 # Part: Obtain the motor variance by exponentiating the appropriate model parameter
 motor_variance = torch.exp(- parameters["log_motor_var"])
 # Part: Obtain the sensory noise variance.
 sigma2 = 2*torch.sigmoid(sigma_logit)
 # Part: Obtain the transfer function as the cumulative sum of the discretized resource allocation (referred to as `volume` element due to the geometric interpretation by Wei&Stocker 2015)

 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 if True:
  # Part: Select data for the relevant fold
  folds = MakeLongTensor(folds)
  MASK = torch.logical_and(condition==condition_, (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
  stimulus = stimulus_[MASK]
  responses = responses_[MASK]
  if sigma2_stimulus > 0:
    stimulus_log_likelihoods = ((SQUARED_STIMULUS_SIMILARITY(grid.unsqueeze(0)-grid.unsqueeze(1)))/(sigma2_stimulus))
    stimulus_likelihoods = torch.nn.Softmax(dim=0)(stimulus_log_likelihoods)

  # Part: Compute sensory likelihoods. Across both interval and
  ## circular stimulus spaces, this amounts to exponentiaring a
  ## `similarity`
  sensory_likelihoods = torch.softmax(((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2))  + volumeElement.unsqueeze(1).log(), dim=0)

  # Part: If stimulus noise is nonzero, convolve the likelihood with the
  ## stimulus noise.
  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)
  inverse_encoding_variability = 1/(((grid.unsqueeze(1)).pow(2) * likelihoods).sum(dim=0) - (grid.unsqueeze(1) * likelihoods).sum(dim=0).pow(2)).sqrt()

  ## Compute posterior using Bayes' rule. As described in the paper, the posterior is computed
  ## in the discretized stimulus space.
  posterior = prior.unsqueeze(1) * likelihoods.t()
  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  ## Compute the estimator for each m in the discretized sensory space.
  bayesianEstimate = CosineEstimator.apply(grid_indices_here, posterior)

  ## Compute the motor likelihood
  ## `error' refers to the stimulus similarity between the estimator assigned to each m and
  ## the observations found in the dataset.
  ## The Gaussian or von Mises motor likelihood is obtained by exponentiating and normalizing

  # Caculate motor likelihood
  error = (SQUARED_STIMULUS_SIMILARITY(360/GRID*bayesianEstimate.unsqueeze(0) - responses.unsqueeze(1)))
  ## The log normalizing constants, for each m in the discretized sensory space
  log_normalizing_constant = torch.logsumexp((SQUARED_STIMULUS_SIMILARITY(grid))/motor_variance, dim=0) + math.log(2 * math.pi / GRID)
  ## The log motor likelihoods, for each pair of sensory encoding m and observed human response
  log_motor_likelihoods = (error/motor_variance) - log_normalizing_constant
  ## Obtaining the motor likelihood by exponentiating.
  motor_likelihoods = torch.exp(log_motor_likelihoods)

  ## Obtain the guessing rate, parameterized via the (inverse) logit transform as described in SI Appendix
  # Mixture of estimation and uniform response
  uniform_part = torch.sigmoid(parameters["mixture_logit"])
  ## The full likelihood then consists of a mixture of the motor likelihood calculated before, and the uniform
  ## distribution on the full space.
  motor_likelihoods = (1-uniform_part) * motor_likelihoods + (uniform_part / (2*math.pi) + 0*motor_likelihoods)

  # Now the loss is obtained by marginalizing out m from the motor likelihood
  if lossReduce == 'mean':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().mean()
  elif lossReduce == 'sum':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().sum()
  else:
    assert False

  ## If computePredictions==True, compute the bias and variability of the estimate
  if computePredictions:
     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS
     bayesianEstimate_avg_byStimulus = computeCircularMeanWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = torch.sqrt(bayesianEstimate_sd_byStimulus.pow(2) + motor_variance * 3282.806)

     bayesianEstimate_avg_byStimulus = torch.where((bayesianEstimate_avg_byStimulus-grid).abs()<180, bayesianEstimate_avg_byStimulus, torch.where(bayesianEstimate_avg_byStimulus > 180, bayesianEstimate_avg_byStimulus-360, bayesianEstimate_avg_byStimulus+360))

     assert float(((bayesianEstimate_avg_byStimulus-grid).abs()).max()) < 180, float(((bayesianEstimate_avg_byStimulus-grid).abs()).max())

     print(likelihoods.size(), bayesianEstimate.size())
     threshold75 = grid.clone()

     for h in range(400):
         predicted_psychometric_f75_at_theta = (((threshold75.view(1,GRID) < bayesianEstimate.view(GRID,1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS).float() * likelihoods).sum(dim=0)).detach()
         higher = threshold75+1/(1+h/10)
         lower = threshold75-1/(1+h/10)
         threshold75 = torch.where(predicted_psychometric_f75_at_theta<0.75, lower, higher)
     threshold50 = grid.clone()
     for h in range(400):
         predicted_psychometric_f50_at_theta = (((threshold50.view(1,GRID) < bayesianEstimate.view(GRID,1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS).float() * likelihoods).sum(dim=0)).detach()
         higher = threshold50+1/(1+h/10)
         lower = threshold50-1/(1+h/10)
         threshold50 = torch.where(predicted_psychometric_f50_at_theta<0.50, lower, higher)

     if False:
        plt.close()
        figure, axis = plt.subplots(3, 3, figsize=(20,20))
        axis[0,0].plot(grid.detach(), predicted_psychometric_f75_at_theta)
        axis[0,1].plot(grid.detach(), predicted_psychometric_f50_at_theta)
        axis[1,0].set_title("Bias")
        axis[1,0].plot(grid.detach(), (threshold50-grid).detach(), color="blue")
        axis[1,0].plot(grid.detach(), (bayesianEstimate_avg_byStimulus-grid).detach(), color="red")
        axis[1,1].set_title("Threshold")
        axis[1,1].plot(grid.detach(), -(threshold75-threshold50).detach())
        axis[2,0].plot(grid.detach(), F.detach()[:-1])
        axis[2,1].plot(grid.detach(), volumeElement.detach())

        X__ = (grid-150).abs().argmin()
        MASK = torch.logical_and(grid>100, grid<200)
        for Q, color in zip([torch.matmul(posterior, likelihoods)[:,X__], prior, likelihoods[:,X__]], ["red", "green", "blue"]):
            Q = Q[MASK]
            Xs__ = (grid_indices_here.detach()[:,X__]/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS)[MASK]
            axis[1,2].plot(Xs__, (Q/Q.max()).detach(), c=color)

        axis[0,0].set_xlim(130, 150)
        axis[0,1].set_xlim(130, 150)
        axis[1,0].set_xlim(130, 150)

        axis[1,1].set_xlim(130, 150)
        axis[2,0].set_xlim(130, 150)
        axis[1,2].set_xlim(130, 150)
        axis[1,0].set_ylim(-8, 8)
        axis[1,1].set_ylim(0,6)
        plt.show()

     threshold75 = -(threshold75-threshold50)
     bias_psychometric = threshold50-grid

     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = computeCircularMeanWeighted(posteriorMaxima.unsqueeze(1), likelihoods)
     encodingBias = computeCircularMeanWeighted(grid.unsqueeze(1), likelihoods)
     attraction = (posteriorMaxima-encodingBias)
     attraction1 = attraction
     attraction2 = attraction+360
     attraction3 = attraction-360
     attraction = torch.where(attraction1.abs() < 180, attraction1, torch.where(attraction2.abs() < 180, attraction2, attraction3))
     encodingBias = encodingBias-grid
     encodingBias1 = encodingBias
     encodingBias2 = encodingBias+360
     encodingBias3 = encodingBias-360
     encodingBias = torch.where(encodingBias1.abs() < 180, encodingBias1, torch.where(encodingBias2.abs() < 180, encodingBias2, encodingBias3))
  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
     encodingBias = None

  if float(loss) != float(loss):
      print("NAN!!!!")
      quit()
  return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction, encodingBias, threshold75, bias_psychometric, inverse_encoding_variability

computations.setData(STIMULUS_SPACE_VOLUME=STIMULUS_SPACE_VOLUME, GRID=GRID)

def model(grid):
  lossesBy500 = []
  crossLossesBy500 = []
  noImprovement = 0
  global optim, learning_rate
  for iteration in range(1):
   parameters = init_parameters
   ## In each iteration, recompute
   ## - the resource allocation (called `volume' due to a geometric interpretation)
   ## - the prior

   volume = 2 * math.pi * torch.nn.functional.softmax(parameters["volume"], dim=0)
   prior = torch.nn.functional.softmax(-((grid-140)/360).pow(2) / (2*0.001)) + 1e-10
   prior = prior/prior.sum()

   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   if iteration % 500 == 0:

     assigned = ["PRI", None, None, "ENC", None, None, "ATT", None, "REP", None, "TOT", None, "HUM"]
     for w, k in enumerate(assigned):
         globals()[k] = w
     PAD = [w for w, q in enumerate(assigned) if q is None]
     gridspec = dict(width_ratios=[1 if x is not None else .15 for x in assigned])
     figure, axis = plt.subplots(1, len(gridspec["width_ratios"]), figsize=(10, 1.3*0.8*1.8), gridspec_kw=gridspec)

     plt.tight_layout()
     figure.subplots_adjust(wspace=0.1, hspace=0.0)

   loss = 0
   if iteration % 500 == 0:
       axis[PRI].set_ylim(0, 1.1*float(prior.max()))
       axis[PRI].set_yticks(ticks=[0])
       axis[ENC].set_yticks(ticks=[0,0.1,0.2], labels=["0","0.1","0.2"])

   for condition_ in range(3):

     SIGMA_LOGIT = [-5, -4.3, -5][condition_]
     init_parameters["sigma_logit"].data[0] = SIGMA_LOGIT
     SIGMA2_STIMULUS = [0.0, 0, 0.02][condition_]
     loss_, bayesianEstimate, bayesianEstimate_sd, attraction, encodingBias, threshold75, bias_psychometric, inverse_encoding_variability = computeBias(xValues, init_parameters["sigma_logit"][0], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, computePredictions=(iteration % 500 == 0), parameters=parameters, condition_=0, folds=testFolds, lossReduce='sum', sigma2_stimulus=SIGMA2_STIMULUS)
     sensory_variance = 2*torch.sigmoid(SIGMA_LOGIT + MakeZeros(1))

     for i in range(1,GRID-1):
          color = categories_color[int(180*i/GRID)]
          L = 70
          a = 38 * math.cos(2*math.pi*i/GRID)
          b = 38 * math.sin(2*math.pi*i/GRID)
          color = colormath.color_conversions.convert_color(colormath.color_objects.LabColor(L,a,b), target_cs=colormath.color_objects.AdobeRGBColor)
          color = (color.rgb_r, color.rgb_g, color.rgb_b)

          axis[PRI].plot([grid[i-1].cpu(), grid[i].cpu()], [prior[i-1].cpu().detach(), (prior)[i].cpu().detach()], c="gray")

     print(bayesianEstimate-grid)

     loss = loss + loss_
     volume_here = 1/((SIGMA2_STIMULUS * math.pow(180/math.pi,2)) + sensory_variance * math.pow(STIMULUS_SPACE_VOLUME/GRID,2)/(( volume.pow(2)))).sqrt()
     resources_computed = computeResourcesWithStimulusNoise_TakesVariance(volume, 1/sensory_variance, (SIGMA2_STIMULUS * math.pow(180/math.pi,2)))
     print("RESOURCES COMPUTED", resources_computed)
     assert (volume_here-resources_computed).abs().max() < 0.01

     print("@@@@@@@@@@@", condition_, sensory_variance)
     print(volume_here)
     for i in range(1,GRID-1):
          color = ["red", "blue", "green"][condition_]

          axis[ENC].plot([grid[i-1].cpu(), grid[i].cpu()], (resources_computed[i-1:i+1]).cpu().detach(),  c=color)

          ## For purposes of sanity-checking, we can also plot the inverse encoding variability, which is closely related to the inverse sqrt FI
##          axis[ENC].plot([grid[i-1].cpu(), grid[i].cpu()], (inverse_encoding_variability[i-1:i+1]).cpu().detach(),  c=color, linestyle='dotted')

          if grid[i] < 130 or grid[i] > 150:
             continue

          axis[REP].plot([grid[i-1].cpu(), grid[i].cpu()], [(bayesianEstimate-grid-attraction)[i-1].cpu().detach(), (bayesianEstimate-grid-attraction)[i].cpu().detach()], c=color)

          axis[ATT].plot([grid[i-1].cpu(), grid[i].cpu()], [attraction[i-1].cpu().detach(), attraction[i].cpu().detach()], c=color)

          axis[TOT].plot([grid[i-1].cpu(), grid[i].cpu()], [(bayesianEstimate-grid)[i-1].cpu().detach(), (bayesianEstimate-grid)[i].cpu().detach()], c=color)

     axis[PRI].text(110, 0.03, "prob.", size=14, rotation='vertical')
     axis[ATT].text(105, 0.00, "bias", size=14, rotation='vertical',va="center")

     axis[HUM].errorbar([130+0.25, 140+0.25, 150+0.25], [(1.2641139772988623+1.8713761463177878)/2, (-0.2830801119045603+-0.6710032640042751)/2, (-3.1015845383063514+-2.060919519567335)/2], yerr=[1.55/2,1.53/2,1.63/2], c="red")
     axis[HUM].errorbar([130+0, 140+0, 150+0], [3.734864209462981, -0.6188636621670014, -3.725175906167852], yerr=[2.01/2,1.98/2,2.00/2], c="blue")
     axis[HUM].errorbar([130+.5, 140+.5, 150+.5], [5.645438226600479, 0.7525648499354123, -3.5750887906748634], yerr=[3.47/2,3.47/2,3.51/2], c="green")

     axis[ENC].set_ylim(bottom=0)

     for s in [ATT, REP , TOT, HUM]:
       axis[s].set_ylim(-8, 8)
     for i in [ENC, PRI, ATT, REP, TOT, HUM]:
       axis[i].set_xlim(120, 160)
     for i in range(len(assigned)):
       axis[i].set_xticks(ticks=[120,140,160], labels=["120", "140", "[deg]"])
       axis[i].spines['right'].set_visible(False)
       axis[i].spines['top'].set_visible(False)
     for i in [ENC, ATT]:
            axis[i].tick_params(labelbottom=False)

     for i in range(len(assigned)):
       axis[i].tick_params(labelsize=13)

     for i in range(1,len(assigned)):
        if i != ENC:
            axis[i].tick_params(labelbottom=False)
     for i in range(1,len(assigned)):
        if i not in [ENC, PRI, ATT]:
            axis[i].tick_params(labelleft=False)

   if iteration % 500 == 0:
     for w in PAD:
       axis[w].set_visible(False)
     axis[ENC].set_title("Resources", fontsize=13)
     axis[PRI].set_title("Prior", fontsize=13)
     axis[REP].set_title("Repulsion", fontsize=13)
     axis[ATT].set_title("Attraction", fontsize=13)
     axis[TOT].set_title("Bias (model)", fontsize=13)
     axis[HUM].set_title("Bias (data)", fontsize=13)

     figure.subplots_adjust(bottom=0.23,top=0.77)
     savePlot(f"figures/{__file__}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.pdf", transparent=True)


############################3

model(grid)
