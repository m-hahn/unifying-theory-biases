# Bae

. createACREXP_ALL.sh

python3 RunBae_UniformPrior_Both_CosineLoss_VIZ3Averaged_ACREXP.py 2 0 1.0 180

python3 RunBae_UniformEncoding_Both_CosineLoss_VIZ3Averaged_ACREXP.py 2 0 1.0 180
python3 RunBae_FreePrior_Both_CosineLoss_VIZ3Averaged_ACREXP.py 2 0 1.0 180
python3 RunBae_FreePrior_Both_VIZ3Averaged_ACREXP.py 2 0 1.0 180

python3 RunBae_FreePrior_Both_Zero_VIZ3.py 0 0 1.0 180
python3 RunBae_UniformEncoding_Both_Zero_VIZ3.py 0 0 1.0 180
python3 RunBae_UniformPrior_Both_Zero_VIZ3.py 0 0 1.0 180
for p in 2 4 6 8 
do
python3 RunBae_UniformEncoding_Both_CosineLoss_VIZ3.py $p 0 1.0 180
python3 RunBae_UniformPrior_Both_CosineLoss_VIZ3.py $p 0 1.0 180
python3 RunBae_FreePrior_Both_CosineLoss_VIZ3.py $p 0 1.0 180
python3 RunBae_FreePrior_Both_VIZ3.py $p 0 1.0 180
done
python3 RunBae_CosineLoss_BySubjEffects_Fourier_ELBO_VIZ.py 2 0 0.0 180 50
python3 evaluateCrossValidationResults_Bae_New_Both.py

python3 RunBae_FreePrior_Both_CosineLoss_VIZ3MainPaper.py 2 0 1.0 180
python3 RunBae_UniformEncoding_Both_CosineLoss_VIZ3MainPaper.py 2 0 1.0 180
python3 RunBae_UniformPrior_Both_CosineLoss_VIZ3MainPaper.py 2 0 1.0 180


python3 RunBae_FreePrior_Both_CosineLoss_OnSim_VIZ3.py 2 0 1.0 180 876774

python3 RunBae_FreePrior_Both_CosineLoss_VIZ3Averaged_ACREXP.py 2 0 1.0 180
for p in 2 4 6 8 
do
python3 RunBae_FreePrior_Both_CosineLoss_VIZ3Averaged.py $p 0 1.0 180
done


python3 RunBae_FreePrior_Both_CosineLoss_VIZ3_ByCondition.py 2 0 1.0 180


python3 RunBae_FreePrior_Both_CosineLoss_VIZ3Averaged_ACREXP.py 2 0 1.0 180
python3 RunBae_UniformEncoding_Both_CosineLoss_VIZ3Averaged_ACREXP.py 2 0 1.0 180
python3 RunBae_UniformPrior_Both_CosineLoss_VIZ3Averaged_ACREXP.py 2 0 1.0 180




python3 RunBae_FreePrior_Both_Zero_VIZ3Averaged.py 0 0 1.0 180
python3 RunBae_UniformPrior_Both_Zero_VIZ3Averaged.py 0 0 1.0 180
python3 RunBae_UniformEncoding_Both_Zero_VIZ3Averaged.py 0 0 1.0 180


python3 evaluateCrossValidationResults_Bae_New_Both_Synthetic.py SimulateSynthetic_Bae_Parameterized_Both.py_2_12345_FITTED_FITTED.txt

python3 RunSynthetic_Bae_FreePrior_Both_CosineLoss_OnSim_VIZAveraged.py 2 0 1.0 180 SimulateSynthetic_Bae_Parameterized_Both.py_2_12345_FITTED_FITTED.txt
python3 RunBae_FreePrior_Both_CosineLoss_VIZ3Averaged.py 2 0 1.0 180

python3 RunBae_FreePrior_Both_CosineLoss_VIZ3Averaged_ACREXP.py 2 0 1.0 180

python3 RunBae_CosineLoss_BySubjEffects_Fourier_ELBO_VIZ.py 2 0 0.0 180 50

python3 RunBae_FreePrior_Both_CosineLoss_VIZ3_Regression_VizOnlyBias.py 2 0 1.0 180

python3 evaluateCrossValidationResults_Bae_New_Both_Synthetic.py SimulateSynthetic_Bae_Parameterized_Both.py_2_12345_FITTED_FITTED.txt


