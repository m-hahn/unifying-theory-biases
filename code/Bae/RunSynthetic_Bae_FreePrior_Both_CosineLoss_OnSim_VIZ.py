import colormath
import colormath.color_conversions
import computations
import json
import math
import matplotlib.pyplot as plt
import numpy as np
import os
import random
import sys
import torch
import computations
from computations import computeResources
from cosineEstimator import CosineEstimator
from loadBae_All import *
from matplotlib import rc
from util import MakeFloatTensor
from util import MakeLongTensor
from util import MakeZeros
from util import computeCenteredMean
from util import computeCircularMean
from util import computeCircularMeanWeighted
from util import computeCircularSD
from util import computeCircularSDWeighted
from util import makeGridIndicesCircular
from util import mean
from util import product
from util import savePlot
from util import sign

__file__ = __file__.split("/")[-1]
rc('font', **{'family':'FreeSans'})
# Helper Functions dependent on the device

OPTIMIZER_VERBOSE = False

# 1. OBTAIN ARGUMENTS
P = int(sys.argv[1])
assert P > 0
FOLD_HERE = int(sys.argv[2])
REG_WEIGHT = float(sys.argv[3])
GRID = int(sys.argv[4])
assert GRID == 180
DEVICE = 'cuda'
FIT = sys.argv[5] #f"SimulateSynthetic_Parameterized.py_8_12345_UNIFORM_UNIFORM.txt"


noiseConditions = "12345"
assert "_"+noiseConditions+"_" in FIT

FILE = f"logs/CROSSVALID/{__file__.replace('_VIZ', '')}_{FIT}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.txt"

if not os.path.exists(FILE):
   print("Missing model")
   assert False, FILE

# 2. LOAD DATA

##############################################

with open(f"logs/SIMULATED_REPLICATE/{FIT}", "r") as inFile:
  data = [z.split(" ") for z in inFile.read().strip().split("=======\n")[1].split("\n")         ]

condition__, sample__, responses__ = zip(*data)
condition__ = MakeLongTensor([int(q) for q in condition__])
assert (condition__ == condition).all()
sample = MakeFloatTensor([float(q) for q in sample__])
responses = MakeFloatTensor([float(q) for q in responses__])

observations_x = sample
observations_y = responses

# Assign folds
N_FOLDS = 10
assert FOLD_HERE < N_FOLDS
randomGenerator = random.Random(10)
Subject=subjects
Fold = 0*Subject
for i in range(min(Subject), max(Subject)+1):
    trials = [j for j in range(Subject.size()[0]) if Subject[j] == i]
    randomGenerator.shuffle(trials)
    foldSize = int(len(trials)/N_FOLDS)
    for k in range(N_FOLDS):
        Fold[trials[k*foldSize:(k+1)*foldSize]] = k

assert (observations_y >= 0).all()
assert (observations_y <= 360).all()

# 4. SET UP THE GRID

# Set up the discretized grid
MIN_GRID = 0
MAX_GRID = 360

CIRCULAR = True
INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS = GRID/(MAX_GRID-MIN_GRID)

grid = MakeFloatTensor([x/GRID * (MAX_GRID-MIN_GRID) for x in range(GRID)]) + MIN_GRID
grid_indices = MakeFloatTensor([x for x in range(GRID)])
grid, grid_indices_here = makeGridIndicesCircular(GRID, MIN_GRID, MAX_GRID)

# Project observed stimuli onto grid
xValues = []
for x in observations_x:
   xValues.append(int( torch.argmin((grid - x).abs())))
xValues = MakeLongTensor(xValues)

x_set = sorted(list(set(xValues.cpu().numpy().tolist())))

# 5. SET UP THE PARAMETERS AND OPTIMIZER

# Initialize the model
init_parameters = {}
init_parameters["log_motor_var"] = MakeFloatTensor([0]).view(1)
init_parameters["sigma_logit"] = MakeFloatTensor(2*[-1]).view(2)
init_parameters["mixture_logit"] = MakeFloatTensor([-1]).view(1)
init_parameters["prior"] = MakeZeros(GRID)
init_parameters["volume"] = MakeZeros(GRID)
with open(FILE, "r") as inFile:
    (next(inFile))
    (next(inFile))
    for l in inFile:
        z, y = l.split("\t")
        assert init_parameters[z.strip()].size() == MakeFloatTensor(json.loads(y)).size(), (z)
        init_parameters[z.strip()] = MakeFloatTensor(json.loads(y))
assert "volume" in init_parameters
for _, y in init_parameters.items():
    y.requires_grad = True

##############################################
# Part: Specify `similarity` or `difference` functions.

STIMULUS_SPACE_VOLUME = MAX_GRID-MIN_GRID
SENSORY_SPACE_VOLUME = 2*math.pi

# Part: Specify `similariy` or `difference` functions.
## These are negative squared distances (for interval spaces) or
## trigonometric functions (for circular spaces), with
## some extra factors for numerical purposes.
## Exponentiating a `similarity` function and normalizing
## is equivalent to the Gaussian / von Mises density.
## The purpose of specifying these as `closeness` or `distance`,
## rather than simply calling squared or trigonometric
## functions is to  flexibly reuse the same model code for
## both interval and circular spaces.
def SQUARED_STIMULUS_DIFFERENCE(x):
    return torch.sin(math.pi*x/180)
def SQUARED_STIMULUS_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    stimulus space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(math.pi*x/180)
def SQUARED_SENSORY_SIMILARITY(x):
    """ Given a difference x between two stimuli, compute the `similarity` in
    sensory space. Generally, this is cos(x) for circular spaces and something
    akin to 1-x^2 for interval spaces, possibly defined with additional factors
    to normalize by the size of the space. The resulting values are exponentiated
    and normalized to obtain a Gaussian or von Mises density."""
    return torch.cos(x)
def SQUARED_SENSORY_DIFFERENCE(x):
    return torch.sin(x)

##############################################
# Import/define the appropriate estimator for minimizing the loss function
# 6. SET UP THE LOSS FUNCTION
# Import/define the appropriate estimator for minimizing the loss function

CosineEstimator.set_parameters(GRID=GRID, OPTIMIZER_VERBOSE=OPTIMIZER_VERBOSE, P=P, SQUARED_SENSORY_DIFFERENCE=SQUARED_SENSORY_DIFFERENCE, SQUARED_SENSORY_SIMILARITY=SQUARED_SENSORY_SIMILARITY)

# 7. SPECIFY THE MODEL (DATA LIKELIHOOD AND BIAS COMPUTATION)
#############################################################
# Part: Run the model. This function implements the model itself:
## calculating the likelihood of a given dataset under that model
## and---if the computePredictions argument is set to True--- computes
## the bias and variability of the estimate.
def computeBias(stimulus_, sigma_logit, prior, volumeElement, n_samples=100, showLikelihood=False, grid=grid, responses_=None, computePredictions=False, parameters=None, condition_=None, folds=None, lossReduce='mean', sigma2_stimulus=0):

 # Part: Obtain the motor variance by exponentiating the appropriate model parameter
 motor_variance = torch.exp(- parameters["log_motor_var"])
 # Part: Obtain the sensory noise variance.
 sigma2 = 2*torch.sigmoid(sigma_logit)
 # Part: Obtain the transfer function as the cumulative sum of the discretized resource allocation (referred to as `volume` element due to the geometric interpretation by Wei&Stocker 2015)

 F = torch.cat([MakeZeros(1), torch.cumsum(volumeElement, dim=0)], dim=0)

 if True:
  # Part: Select data for the relevant fold
  folds = MakeLongTensor(folds)
  MASK = torch.logical_and(condition==condition_, (Fold.unsqueeze(0) == folds.unsqueeze(1)).any(dim=0))
  stimulus = stimulus_[MASK]
  responses = responses_[MASK]
  # Part: Compute sensory likelihoods. Across both interval and
  ## circular stimulus spaces, this amounts to exponentiaring a
  ## `similarity`
  sensory_likelihoods = torch.softmax(((SQUARED_SENSORY_SIMILARITY(F[:-1].unsqueeze(0) - F[:-1].unsqueeze(1)))/(sigma2))  + volumeElement.unsqueeze(1).log(), dim=0)

  # Part: If stimulus noise is nonzero, convolve the likelihood with the
  ## stimulus noise.
  if sigma2_stimulus == 0:
    likelihoods = sensory_likelihoods
  else:
    ## On this dataset, this is zero, so the
    ## code block will not be used.
    assert False
    likelihoods = torch.matmul(sensory_likelihoods, stimulus_likelihoods)

  ## Compute posterior using Bayes' rule. As described in the paper, the posterior is computed
  ## in the discretized stimulus space.
  posterior = prior.unsqueeze(1) * likelihoods.t()
  posterior = posterior / posterior.sum(dim=0, keepdim=True)

  ## Compute the estimator for each m in the discretized sensory space.
  bayesianEstimate = CosineEstimator.apply(grid_indices_here, posterior)

  ## Compute the motor likelihood
  ## `error' refers to the stimulus similarity between the estimator assigned to each m and
  ## the observations found in the dataset.
  ## The Gaussian or von Mises motor likelihood is obtained by exponentiating and normalizing

  # Caculate motor likelihood
  error = (SQUARED_STIMULUS_SIMILARITY(360/GRID*bayesianEstimate.unsqueeze(0) - responses.unsqueeze(1)))
  ## The log normalizing constants, for each m in the discretized sensory space
  log_normalizing_constant = torch.logsumexp((SQUARED_STIMULUS_SIMILARITY(grid))/motor_variance, dim=0) + math.log(2 * math.pi / GRID)
  ## The log motor likelihoods, for each pair of sensory encoding m and observed human response
  log_motor_likelihoods = (error/motor_variance) - log_normalizing_constant
  ## Obtaining the motor likelihood by exponentiating.
  motor_likelihoods = torch.exp(log_motor_likelihoods)

  ## Obtain the guessing rate, parameterized via the (inverse) logit transform as described in SI Appendix
  # Mixture of estimation and uniform response
  uniform_part = torch.sigmoid(parameters["mixture_logit"])
  ## The full likelihood then consists of a mixture of the motor likelihood calculated before, and the uniform
  ## distribution on the full space.
  motor_likelihoods = (1-uniform_part) * motor_likelihoods + (uniform_part / (2*math.pi) + 0*motor_likelihoods)

  # Now the loss is obtained by marginalizing out m from the motor likelihood
  if lossReduce == 'mean':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().mean()
  elif lossReduce == 'sum':
    loss = -torch.gather(input=torch.matmul(motor_likelihoods, likelihoods),dim=1,index=stimulus.unsqueeze(1)).squeeze(1).log().sum()
  else:
    assert False

  ## If computePredictions==True, compute the bias and variability of the estimate
  if computePredictions:
     bayesianEstimate_byStimulus = bayesianEstimate.unsqueeze(1)/INVERSE_DISTANCE_BETWEEN_NEIGHBORING_GRID_POINTS
     bayesianEstimate_avg_byStimulus = computeCircularMeanWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = computeCircularSDWeighted(bayesianEstimate_byStimulus, likelihoods)
     bayesianEstimate_sd_byStimulus = torch.sqrt(bayesianEstimate_sd_byStimulus.pow(2) + motor_variance * 3282.806)

     bayesianEstimate_avg_byStimulus = torch.where((bayesianEstimate_avg_byStimulus-grid).abs()<180, bayesianEstimate_avg_byStimulus, torch.where(bayesianEstimate_avg_byStimulus > 180, bayesianEstimate_avg_byStimulus-360, bayesianEstimate_avg_byStimulus+360))
     assert float(((bayesianEstimate_avg_byStimulus-grid).abs()).max()) < 180, float(((bayesianEstimate_avg_byStimulus-grid).abs()).max())
     posteriorMaxima = grid[posterior.argmax(dim=0)]
     posteriorMaxima = computeCircularMeanWeighted(posteriorMaxima.unsqueeze(1), likelihoods)
     encodingBias = computeCircularMeanWeighted(grid.unsqueeze(1), likelihoods)
     attraction = (posteriorMaxima-encodingBias)
     attraction1 = attraction
     attraction2 = attraction+360
     attraction3 = attraction-360
     attraction = torch.where(attraction1.abs() < 180, attraction1, torch.where(attraction2.abs() < 180, attraction2, attraction3))
     encodingBias = encodingBias-grid
     encodingBias1 = encodingBias
     encodingBias2 = encodingBias+360
     encodingBias3 = encodingBias-360
     encodingBias = torch.where(encodingBias1.abs() < 180, encodingBias1, torch.where(encodingBias2.abs() < 180, encodingBias2, encodingBias3))
  else:
     bayesianEstimate_avg_byStimulus = None
     bayesianEstimate_sd_byStimulus = None
     attraction = None
     encodingBias = None

  if float(loss) != float(loss):
      print("NAN 526476!!!!")
      quit()
  return loss, bayesianEstimate_avg_byStimulus, bayesianEstimate_sd_byStimulus, attraction, encodingBias

computations.setData(STIMULUS_SPACE_VOLUME=STIMULUS_SPACE_VOLUME, GRID=GRID)

# 8. ROUTINE FOR RETRIEVING DATA
def retrieveAndSmoothObservations(x, meanMethod = "circular", condition_=None):
     y_set = []
     sd_set = []
     subject_set = set(list(Subject.numpy().tolist()))
     for x in x_set:
        y_here = observations_y[torch.logical_and(xValues == x, condition==condition_)]

        Mean1 = computeCircularMean(y_here)
        Mean2 = computeCenteredMean(y_here, grid[x])
        if abs(Mean1-grid[x]) > 180 and Mean1 > 180:
            Mean1 = Mean1-360
        elif abs(Mean1-grid[x]) > 180 and Mean1 < 180:
            Mean1 = Mean1+360
        if abs(Mean1-Mean2) > 180:
            print(Duration_)
            print(y_here)
            print(Mean1, Mean2, grid[x])
            assert False
        if meanMethod == "circular":
            Mean = Mean1
        elif meanMethod == "centered":
            Mean = Mean2
        else:
            assert False

        bias = Mean - grid[x]
        if abs(bias) > 180:
            bias = bias+360
        y_set.append(bias)

        sd_by_subject = []
        for s in subject_set:
            y_here_s = observations_y[torch.logical_and(xValues == x, torch.logical_and(Subject == s, condition==condition_))]
            if y_here_s.size()[0] > 0:
               sd_by_subject.append(computeCircularSD(y_here_s))
        sd_set.append(mean(sd_by_subject))
     x_set_tensor = MakeFloatTensor(x_set) * 360 / GRID
     y_set_tensor = MakeFloatTensor(y_set)
     sd_set_tensor = MakeFloatTensor(sd_set)
     kappa = 20
     KERNEL = torch.nn.functional.softmax(kappa * SQUARED_STIMULUS_SIMILARITY(x_set_tensor.view(-1,1) - x_set_tensor.view(1,-1)), dim=0)
     y_set = (y_set_tensor.view(-1, 1) * KERNEL).sum(dim=0)
     sd_set = (sd_set_tensor.view(-1, 1) * KERNEL).sum(dim=0)
     return x_set, y_set, sd_set

# 9. CODE FOR OPTIMIZING OR VISUALIZING
def model(grid):
  lossesBy500 = []
  crossLossesBy500 = []
  noImprovement = 0
  for iteration in range(1):
   parameters = init_parameters
   ## In each iteration, recompute
   ## - the resource allocation (called `volume' due to a geometric interpretation)
   ## - the prior

   volume = 2 * math.pi * torch.nn.functional.softmax(parameters["volume"], dim=0)
   prior = torch.nn.functional.softmax(parameters["prior"], dim=0)
   trainFolds = [i for i in range(N_FOLDS) if i!=FOLD_HERE]
   testFolds = [FOLD_HERE]

   if iteration % 500 == 0:

     assigned = ["ENC", None, "PRI", None, "ATT", "REP", "TOT", None, "VAR"]
     for w, k in enumerate(assigned):
         globals()[k] = w
     PAD = [w for w, q in enumerate(assigned) if q is None]
     gridspec = dict(width_ratios=[1 if x is not None else .2 for x in assigned])
     figure, axis = plt.subplots(2, len(gridspec["width_ratios"]), figsize=(10, 4), gridspec_kw=gridspec)

     plt.tight_layout()
     figure.subplots_adjust(wspace=0.1, hspace=0.0)
   loss = 0
   for condition_ in [0,1]:
     loss_, bayesianEstimate, bayesianEstimate_sd, attraction, encodingBias = computeBias(xValues, init_parameters["sigma_logit"][condition_], prior, volume, n_samples=1000, grid=grid, responses_=observations_y, computePredictions=(iteration % 500 == 0), parameters=parameters, condition_=condition_, folds=testFolds, lossReduce='sum')

     loss = loss + loss_
     if iteration % 500 == 0:
       x_set, y_set, sd_set = retrieveAndSmoothObservations(None, condition_=condition_)

       sensory_variance = 2*torch.sigmoid(init_parameters["sigma_logit"][0])
       resources_computed = computeResources(volume, 1/sensory_variance)
       axis[condition_,0].scatter([grid[0], grid[-1]], [0,0], c='white')
       axis[condition_,1].scatter([grid[0], grid[-1]], [0,0], c='white')
       axis[condition_,5].scatter([grid[0], grid[-1]], [0,0], c='white')

       axis[condition_,ENC].set_ylim(0, 1.1*float(resources_computed.max()))
       axis[condition_,PRI].set_ylim(0, 1.1*float(prior.max()))

       axis[condition_,PRI].set_yticks(ticks=[0])

       LINEWIDTH = 3
       PLOTTING_STEPS = 3
       for i in range(PLOTTING_STEPS,GRID-1,PLOTTING_STEPS):
          color = categories_color[int(180*i/GRID)]
          L = 70
          a = 38 * math.cos(2*math.pi*i/GRID)
          b = 38 * math.sin(2*math.pi*i/GRID)
          color = colormath.color_conversions.convert_color(colormath.color_objects.LabColor(L,a,b), target_cs=colormath.color_objects.AdobeRGBColor)
          color = (color.rgb_r, color.rgb_g, color.rgb_b)

          axis[condition_,ENC].plot([grid[i-PLOTTING_STEPS].cpu(), grid[i].cpu()], [resources_computed[i-3].cpu().detach(), resources_computed[i].cpu().detach()],  c=color, linewidth=LINEWIDTH)

          axis[condition_,PRI].plot([grid[i-PLOTTING_STEPS].cpu(), grid[i].cpu()], [prior[i-PLOTTING_STEPS].cpu().detach(), (prior)[i].cpu().detach()], c=color, linewidth=LINEWIDTH)

          axis[condition_,REP].plot([grid[i-PLOTTING_STEPS].cpu(), grid[i].cpu()], [(bayesianEstimate-grid-attraction)[i-PLOTTING_STEPS].cpu().detach(), (bayesianEstimate-grid-attraction)[i].cpu().detach()], c=color, linewidth=LINEWIDTH)

          axis[condition_,ATT].plot([grid[i-PLOTTING_STEPS].cpu(), grid[i].cpu()], [attraction[i-PLOTTING_STEPS].cpu().detach(), attraction[i].cpu().detach()], c=color, linewidth=LINEWIDTH)

          axis[condition_,TOT].plot([grid[i-PLOTTING_STEPS].cpu(), grid[i].cpu()], [(bayesianEstimate-grid)[i-PLOTTING_STEPS].cpu().detach(), (bayesianEstimate-grid)[i].cpu().detach()], c=color, linewidth=LINEWIDTH)

          axis[condition_,VAR].plot([grid[i-PLOTTING_STEPS].cpu(), grid[i].cpu()], [bayesianEstimate_sd[i-PLOTTING_STEPS].cpu().detach(), bayesianEstimate_sd[i].cpu().detach()], c=color, linewidth=LINEWIDTH)
       if False:
         axis[condition_,0].plot([130, 150, 150, 130, 130], [0.1, 0.1, 0.5, 0.5, 0.1], color="gray")
       lastStart = 0
       for i in range(1,len(x_set)-1,2):
          if ((grid[x_set[lastStart]] - grid[x_set[i]])/360).pow(2) + ((y_set[lastStart] - y_set[i])/20).pow(2) >= 0.01:
             L = 70
             j = (i-lastStart)/2 + lastStart
             a = 38 * math.sin(2*math.pi*j/GRID)
             b = 38 * math.cos(2*math.pi*j/GRID)
             color = colormath.color_conversions.convert_color(colormath.color_objects.LabColor(L,a,b), target_cs=colormath.color_objects.AdobeRGBColor)
             color = (color.rgb_r, color.rgb_g, color.rgb_b)
             axis[condition_,TOT].plot(grid[x_set[lastStart:i+1]].cpu(), y_set[lastStart:i+1].cpu().detach(), c=color, linestyle='dotted', linewidth=LINEWIDTH)
             lastStart = i

       lastStart = 0
       for i in range(1,len(x_set)-1,2):
          if ((grid[x_set[lastStart]] - grid[x_set[i]])/360).pow(2) + ((sd_set[lastStart] - sd_set[i])/15).pow(2) >= 0.01:
             L = 70
             j = (i-lastStart)/2 + lastStart
             a = 38 * math.sin(2*math.pi*j/GRID)
             b = 38 * math.cos(2*math.pi*j/GRID)
             color = colormath.color_conversions.convert_color(colormath.color_objects.LabColor(L,a,b), target_cs=colormath.color_objects.AdobeRGBColor)
             color = (color.rgb_r, color.rgb_g, color.rgb_b)
             axis[condition_,VAR].plot(grid[x_set[lastStart:i+1]].cpu(), sd_set[lastStart:i+1].cpu().detach(), c=color, linestyle='dotted', linewidth=LINEWIDTH)
             lastStart = i

       axis[condition_,ATT].set_ylim(-10,10)
       axis[condition_,REP].set_ylim(-10,10)
       axis[condition_,TOT].set_ylim(-10,10)
       axis[condition_,VAR].set_ylim(0,15)

   regularizer1 = ((init_parameters["volume"][1:] - init_parameters["volume"][:-1]).pow(2).sum() + (init_parameters["volume"][0] - init_parameters["volume"][-1]).pow(2))/GRID
   regularizer2 = ((init_parameters["prior"][1:] - init_parameters["prior"][:-1]).pow(2).sum() + (init_parameters["prior"][0] - init_parameters["prior"][-1]).pow(2))/GRID
   regularizer_total = regularizer1 + regularizer2

   loss = loss * (1/observations_y.size()[0])
   loss = loss + REG_WEIGHT * regularizer_total

   if iteration % 10 == 0:
     print(iteration, loss, init_parameters["sigma_logit"], torch.sigmoid(parameters["mixture_logit"]) )
   if iteration % 500 == 0:

     for condition_ in [0,1]:  
      for w in PAD:
       axis[condition_,w].set_visible(False)

     axis[0,ENC].set_title("Resources", fontsize=16)
     axis[0,PRI].set_title("Prior", fontsize=16)
     axis[0,REP].set_title("Repulsion", fontsize=16)
     axis[0,ATT].set_title("Attraction", fontsize=16)
     axis[0,TOT].set_title("Bias", fontsize=16)
     axis[0,VAR].set_title("Variability", fontsize=16)

     for condition_ in [0,1]:  
      for w in range(9):
         axis[condition_,w].set_xticks(ticks=[0,180,360], labels=["0°", "180°", "360°"])
         axis[condition_,w].spines['right'].set_visible(False)
         axis[condition_,w].spines['top'].set_visible(False)
      for w in range(9):
         axis[condition_,w].tick_params(labelbottom=False)
      for w in [ATT,REP,TOT]:
          axis[condition_,w].set_yticks([-10,0,10])
      for w in [REP,TOT]:
         axis[condition_,w].tick_params(labelleft=False)
      for w in [ENC]:
          axis[condition_,w].set_yticks([0,0.2])
      for w in [VAR]:
          axis[condition_,w].set_yticks([0,10])
      figure.subplots_adjust(bottom=0.08,top=0.85)
     savePlot(f"figures/{__file__}_{FIT}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}.pdf")
     plt.close()

   if iteration % 500 == 0:
     from matplotlib import cm
     from matplotlib.colors import ListedColormap, LinearSegmentedColormap
     viridis = cm.get_cmap('viridis', 8)

     figure, axis = plt.subplots(1, 1, figsize=(1.5,1.5))
     axis.plot([0,0], [0, (bayesianEstimate-grid-attraction).detach().pow(2).mean()], linewidth=50)
     axis.plot([1,1], [0, (attraction).detach().pow(2).mean()], linewidth=50)
     plt.xticks([])
     plt.yticks([])
     savePlot(f"figures/{__file__}_{FIT}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_Magnitudes.pdf")
     plt.close()

     figure, axis = plt.subplots(1, 1, figsize=(1.8,1.8))
     plt.tight_layout()
     ratio = (prior / volume.pow((P+2)/2)).detach()
     kernel = torch.softmax(-(((grid.unsqueeze(0) - grid.unsqueeze(1))/MAX_GRID)).pow(2)/0.001, dim=0)
     ratio = torch.matmul(ratio.unsqueeze(0), kernel).view(-1)
     prior_smoothed = torch.matmul(prior.unsqueeze(0), kernel).view(-1)
     volume_smoothed = torch.matmul(volume.unsqueeze(0), kernel).view(-1)
     axis.plot([MIN_GRID, MAX_GRID/2], [8,8], color="gray", linestyle='dotted')
     axis.plot([MIN_GRID, MAX_GRID/2], [0,0], color="gray", linestyle='dotted')
     axis.plot([MIN_GRID, MAX_GRID/2], [-8,-8], color="gray", linestyle='dotted')
     axis.plot([MIN_GRID, MAX_GRID/2], [-16,-16], color="gray", linestyle='dotted')
     plt.yticks([])
     axis.set_xticks(ticks=[0,90,180], labels=["0°", "90°", "180°"])
     axis.set_yticks(ticks=[8, 0, -8, -16], labels=["A", "B", "C", "D"])
     for w in range(2,GRID):
        y1 = float(ratio[w-1]-ratio[w-2])
        y2 = float(ratio[w]-ratio[w-1])

        repulsive_part = float((bayesianEstimate-grid-attraction)[w].cpu().detach())
        attractive_part = float((attraction)[w].cpu().detach())
        repulsivePredominance = math.pow(repulsive_part, 2) / (math.pow(repulsive_part,2) + math.pow(attractive_part,2))

        hasAttraction = False
        repulsion = False
        if sign(float(prior_smoothed[w]-prior_smoothed[w-1])) == sign(y2):
            hasAttraction=True
        if sign(float(volume_smoothed[w]-volume_smoothed[w-1])) == -sign(y2):
            repulsion=True
        if hasAttraction and repulsion:
            c = "gray"
        elif hasAttraction:
            c = "green"
        elif repulsion:
            c = "blue"
        else:
            c = "black"
        axis.plot([float(grid[w-1])/2, float(grid[w])/2], [0+sign(y2),0+sign(y2)], color=["green", "red", "gray"][{-1 : 0, 0 : 2, 1 : 1}[sign(y2)]])

        y2 = repulsive_part
        axis.plot([float(grid[w-1])/2, float(grid[w])/2], [-8+sign(y2),-8+sign(y2)], color=["green", "red", "gray"][{-1 : 0, 0 : 2, 1 : 1}[sign(y2)]])

        y2 = attractive_part
        axis.plot([float(grid[w-1])/2, float(grid[w])/2], [-16+sign(y2),-16+sign(y2)], color=["green", "red", "gray"][{-1 : 0, 0 : 2, 1 : 1}[sign(y2)]])

     for w in range(2, len(x_set)):
        y2 = y_set[w]
        axis.plot([float(grid[x_set[w-1]])/2, float(grid[x_set[w]])/2], [8+sign(y2),8+sign(y2)], color=["green", "red", "gray"][{-1 : 0, 0 : 2, 1 : 1}[sign(y2)]])

     savePlot(f"figures/{__file__}_{FIT}_{P}_{FOLD_HERE}_{REG_WEIGHT}_{GRID}_PPRatio.pdf")
     plt.close()

############################3

model(grid)
