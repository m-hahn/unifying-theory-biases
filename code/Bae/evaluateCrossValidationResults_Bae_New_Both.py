import math
import matplotlib.pyplot as plt
from evaluateCrossValidationResults2 import crossValidResults
from matplotlib import rc
from util import savePlot
rc('font', **{'family':'FreeSans'})

def mean(x):
    return sum(x)/len(x)

def round_(x):
    if str(x).lower() == "nan":
        return "--"
    else:
        return round(x)

def deltaDiff(x,y):
    if len(x) < 10 or len(y) < 10:
        return "--"
    return round(mean([x[i]-y[i] for i in range(len(x))]),1)

def deltaSD(x,y):
    if len(x) < 10 or len(y) < 10:
        return "--"
    mu = mean([x[i]-y[i] for i in range(len(x))])
    muSquared = mean([math.pow(x[i]-y[i],2) for i in range(len(x))])
    return round(math.sqrt(muSquared - math.pow(mu, 2)) / math.sqrt(10),1)

curves = {}
def plot(color, style, loss, result):
    if (color,style) not in curves:
       curves[(color, style)] = []
    if result[2] != result[2]:
        return
    curves[(color, style)].append((loss, result[2], result[3], result[4]))

curvesRelative = {}
def plotRelative(color, style, loss, result, resultRef):
    if (color,style) not in curvesRelative:
       curvesRelative[(color, style)] = []
    if result[2] != result[2]:
        return
    sd = deltaSD(result[4],resultRef[4])
    if sd == '--':
        return
    curvesRelative[(color, style)].append((loss, result[2]-resultRef[2], sd))

curvesRelativeLF = {}
def plotEffectOfLossFunction(color, style, loss, result, reference):
    if result is None:
        return
    if (color,style) not in curvesRelativeLF:
       curvesRelativeLF[(color, style)] = []
    meanRelative = result[2] - reference[2]
    sd = deltaSD(result[4], reference[4])
    if sd == '--':
        return
    curvesRelativeLF[(color, style)].append((loss, meanRelative, sd))

for loss in range(0,20):
    if loss == 0:
      uniformEncoding = crossValidResults(f"RunBae_UniformEncoding_Both_Zero.py_0_*_1.0_180", STRICT=True)
      uniformPrior = crossValidResults(f"RunBae_UniformPrior_Both_Zero.py_0_*_1.0_180", STRICT=True)

      full = crossValidResults(f"RunBae_FreePrior_Both_Zero.py_0_*_1.0_180", STRICT=True)
      cosine_uniformEncoding = uniformEncoding
      cosine_uniformPrior = uniformPrior

      cosine_full = full
    else:

      uniformEncoding = crossValidResults(f"RunBae_UniformEncoding_Both.py_{loss}_*_1.0_180", STRICT=True)
      uniformPrior = crossValidResults(f"RunBae_UniformPrior_Both.py_{loss}_*_1.0_180", STRICT=True)

      full = crossValidResults(f"RunBae_FreePrior_Both.py_{loss}_*_1.0_180", STRICT=True)
      print(full)
      cosine_uniformEncoding = crossValidResults(f"RunBae_UniformEncoding_Both_CosineLoss.py_{loss}_*_1.0_180", STRICT=True)
      cosine_uniformPrior = crossValidResults(f"RunBae_UniformPrior_Both_CosineLoss.py_{loss}_*_1.0_180", STRICT=True)
      cosine_full = crossValidResults(f"RunBae_FreePrior_Both_CosineLoss.py_{loss}_*_1.0_180", STRICT=True)

    COLOR_FREE = "green"
    COLOR_UNIFORM_PRIOR = "red"
    COLOR_UNIFORM_ENCODING = "blue"
    COLOR_HARD1 = "purple"
    COLOR_HARD2 = "orange"

    plot(COLOR_FREE, "solid", loss, full)
    plot(COLOR_FREE, "dotted", loss, cosine_full)

    plot(COLOR_UNIFORM_PRIOR, "solid", loss, uniformPrior)
    plot(COLOR_UNIFORM_PRIOR, "dotted", loss, cosine_uniformPrior)

    plot(COLOR_UNIFORM_ENCODING, "solid", loss, uniformEncoding)
    plot(COLOR_UNIFORM_ENCODING, "dotted", loss, cosine_uniformEncoding)

    plotRelative("green", "solid", loss, full, full)
    plotRelative("green", "dotted", loss, cosine_full, cosine_full)
    plotRelative("red", "solid", loss, uniformPrior, full)
    plotRelative("red", "dotted", loss, cosine_uniformPrior, cosine_full)
    plotRelative("blue", "solid", loss, uniformEncoding, full)
    plotRelative("blue", "dotted", loss, cosine_uniformEncoding, cosine_full)

    plotEffectOfLossFunction(COLOR_FREE, "solid", loss, full, crossValidResults(f"RunBae_FreePrior_Both_CosineLoss.py_2_*_1.0_180", STRICT=True))
    plotEffectOfLossFunction(COLOR_FREE, "dotted", loss, cosine_full, crossValidResults(f"RunBae_FreePrior_Both_CosineLoss.py_2_*_1.0_180", STRICT=True))
    plotEffectOfLossFunction(COLOR_UNIFORM_PRIOR, "solid", loss, uniformPrior, crossValidResults(f"RunBae_FreePrior_Both_CosineLoss.py_2_*_1.0_180", STRICT=True))
    plotEffectOfLossFunction(COLOR_UNIFORM_PRIOR, "dotted", loss, cosine_uniformPrior, crossValidResults(f"RunBae_FreePrior_Both_CosineLoss.py_2_*_1.0_180", STRICT=True))
    plotEffectOfLossFunction(COLOR_UNIFORM_ENCODING, "solid", loss, uniformEncoding, crossValidResults(f"RunBae_FreePrior_Both_CosineLoss.py_2_*_1.0_180", STRICT=True))
    plotEffectOfLossFunction(COLOR_UNIFORM_ENCODING, "dotted", loss, cosine_uniformEncoding, crossValidResults(f"RunBae_FreePrior_Both_CosineLoss.py_2_*_1.0_180", STRICT=True))

    results = []
    results.append(loss)
    results.append(round_(full[2]))

    results.append(round_(uniformEncoding[2]))

    results.append(round_(uniformPrior[2]))

    results.append(round_(cosine_full[2]))

    results.append(round_(cosine_uniformEncoding[2]))

    results.append(round_(cosine_uniformPrior[2]))

    print(" & ".join([str(q) for q in results])+"\\\\")

minY = 100000000000000
maxY = -100000000000000
figure, axis = plt.subplots(1,1, figsize=(0.9*2,0.9*2))
figure.subplots_adjust(left=0.25, bottom=0.25)
for key, values in curvesRelativeLF.items():
    color, style = key
    if color != COLOR_FREE or style != "dotted":
        continue
    if len(values) == 0:
        continue
    x, y, errors = zip(*values)
    print(x, y, errors)
    color = "gray"
    axis.plot(x, y, color=color, linestyle='solid', linewidth=0.5)

    minY = min(minY, min(y))
    maxY = max(maxY, max(y))
    (_, caps, _) = axis.errorbar(x, y, yerr=[z for z in errors], color=color, fmt='none', linewidth=0.5, capsize=2)
    for cap in caps:
       cap.set_markeredgewidth(0.5)
    ## can add argument , fmt='none') to errorbar so the line isn't drawn twice
## done plotting
print(minY, maxY)
axis.set_xlim(-1,11)
axis.set_ylim(minY-20, maxY+20)
axis.spines['top'].set_visible(False)
axis.spines['right'].set_visible(False)
axis.set_yticks(ticks=[-20,-10,0,10,20, 30], labels=["\N{MINUS SIGN}20", "", 0, "", 20, ""])
axis.set_xticks(ticks=[0,5,10])
axis.tick_params(labelsize=14, width=0.4)

savePlot(f"figures/{__file__}_simple.pdf")
plt.show()

#######################

with open(f"output/{__file__}.tsv", "w") as outFile:
 figure, axis = plt.subplots(1,2, figsize=(6,3))
 for key, values in curves.items():
    color, style = key
    if len(values) == 0:
        continue
    x, y, errors, raw = zip(*values)
    print(x, y, errors)
    i = ["solid", "dotted"].index(style)
    axis[i].plot(x, y, color=color, linestyle=style)
    axis[i].scatter(x, y, color=color)
    axis[i].errorbar(x, y, yerr=errors, color=color)
    print(key, x, raw)
    for i, exponent in enumerate(x):
        for fold in range(len(raw[i])):
            print("\t".join([str(q) for q in [key[0], key[1], exponent, fold, raw[i][fold]]]), file=outFile)

savePlot(f"figures/{__file__}.pdf")

counter = 0
figure, axis = plt.subplots(1, 2, figsize=(6,3))
for key, values in curvesRelative.items():
    counter += 1
    color, style = key
    if len(values) == 0:
        continue
    x, y, errors = zip(*values)
    print(x, y, errors)
    i = ["solid", "dotted"].index(style)
    axis[i].plot(x, y, color=color, linestyle=style)
    axis[i].scatter(x, y, color=color)
    axis[i].errorbar(x, y, yerr=errors, color=color)

axis[0].set_xlim(-1, 15)
axis[1].set_xlim(-1, 15)
savePlot(f"figures/{__file__}_Relative.pdf")
plt.show()

minY = 100000000000000
maxY = -100000000000000
figure, axis = plt.subplots(1, 2, figsize=(6,3), layout='constrained')

with open(f"output/{__file__}.txt", "w") as outFile:
 for key, values in curvesRelativeLF.items():
    color, style = key
    if len(values) == 0:
        continue
    x, y, errors = zip(*values)
    print(x, y, errors)
    i = ["solid", "dotted"].index(style)
    minY = min(minY, min(y))
    maxY = max(maxY, max(y))
    axis[i].plot(x, y, color=color, linestyle=style)
    axis[i].scatter(x, y, color=color, s=10)
    axis[i].errorbar(x, y, yerr=errors, color=color)
    print(color, style, [round(q) for q in y], file=outFile)

for i in range(2):
 axis[i].plot([0,16], [0,0], color="gray", linestyle="dotted")
 axis[i].set_xlabel("Exponent")
axis[0].set_ylabel("Δ NLL")
axis[0].set_ylim(minY-10, maxY+10)
axis[1].set_ylim(minY-10, maxY+10)
axis[0].set_xlim(-1, 13)
axis[1].set_xlim(-1, 13)
axis[0].set_title("Centered Loss")
axis[1].set_title("Cosine Loss")
savePlot(f"figures/{__file__}_RelativeLF.pdf")
plt.show()
