\subsubsection{Falsifiability of Modeling Framework}\label{sec:falsifiability}

Our fitting procedure provides a powerful method of fitting encoding-decoding models to perceptual estimation data without restrictive parametric assumptions often made in prior work.
This raises the question whether our modeling framework -- that is, the model described in Main Text, Figure 1a -- is falsifiable as a scientific hypothesis \citep{Popper1959TheLO}, or whether it can fit any dataset.
Indeed, Bayesian modeling has sometimes been criticized as being sufficiently unconstrained to easily provide post-hoc stories for any observation \citep{Bowers2012BayesianJS}, and this concern might seem even more pressing when fitting a rich nonparametric model as we are doing here.
In this section, we illustrate the falsifiability of our modeling framework, by making simple modifications to real data and showing that the model ceases to provide a fit.


\paragraph*{Noise Characteristics}
Our theory predicts that sensory (internal) and stimulus (external) noise differentially affect biases.
In order to show that this is falsifiable prediction, we fitted our model to the orientation perception dataset collected by \citet[][]{deGardelle2010AnOI}, but under the counterfactual assumption that the different noise levels had been produced by varying stimulus noise rather than exposure duration.
Resulting fits are shown in  Figure~\ref{fig:falsify}A.
Across loss functions, the model cannot fit the data: As our theory predicts that stimulus noise reduces repulsion, the model consistently fits a smaller bias for the high-noise condition (blue curve), in disagreement with the data.
We analogously fitted to the data collected by \citet{Tomassini2010OrientationUR} exchanging the two condition manipulations -- assigning the effect of exposure duration to stimulus noise, and the effect of stimulus noise to sensory noise; the qualitative pattern is again not recovered despite the model's flexibility.
Remarkably, we had not explicitly imposed the constraint that the oblique bias had to be modeled as repulsion, rather than attraction:
A modeling approach that only considers the bias could in principle account for the pattern in the counterfactual dataset -- at least the one based on \citet[][]{deGardelle2010AnOI} -- by assuming an (albeit implausible) prior peaking at oblique directions.
In contrast, our model, fitting to the full response distribution in the trial-by-trial data, shows that the counterfactual dataset is incompatible with the theory.


\paragraph*{Adding Constant}
We added a constant to the response in the orientation dataset collected by \citet{deGardelle2010AnOI}, making the bias close to nonnegative everywhere.
A bias of constant (nonzero) sign appears implausible under Bayesian models on a circular stimulus space, and cannot be fitted (Figure~\ref{fig:falsify}B).

\paragraph*{Shifting Patterns between Noise Conditions}
% (with the largest shift when noise was largest)
Next, in the same dataset, we shifted stimuli and responses by -- depending on the exposure duration -- 10, 20, 30, 40, 50 degrees.\footnote{As described in Materials\&Methods, the experiment in \citet{deGardelle2010AnOI} has five exposure durations. As described in Materials\&Methods, throughout the paper, we follow \citet{Wei2015ABO} in not plotting the condition with largest noise (shortest exposure) of this dataset due to difficulty obtaining consistent Kernel smoothing estimates; however, we always include all five durations in model fitting.}
Our theoretical analysis of the coding model predicts that different conditions should vary only by the scaling of bias, not in its sign.
As a consequence, such a pattern cannot be fitted (Figure~\ref{fig:falsify}C).




\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont



                \begin{tikzpicture}
\node (label) at (2.4,4-2.5+2)[draw=none, align=left, anchor=center]{\textbf{A. Exchanging stimulus noise and sensory noise}};



\node (label) at (1,2.4-2.5+0.3+2)[draw=none, align=left, anchor=center]{\includegraphics[page=1,width=.6\textwidth]{{figures/RunFalseGardelle_FreePrior_CosineLoss_NoiseType_VIZ.py_8_0_10.0_180}.pdf}};
\node (label) at (4,2.5-2.5+0.3+2-0.1)[draw=none, align=left, anchor=center]{\includegraphics[page=1,width=.6\textwidth]{{figures/RunGardelle_UniformPrior_CosineLoss_VIZ_OnlyHuman_ErrBar.py_8_0_10.0_180}.pdf}};


\node (label) at (1,2.4-2.5+0.3)[draw=none, align=left, anchor=center]{\includegraphics[page=1,width=.6\textwidth]{{figures/RunFalseTomassini_CosinePLoss_FreePrior_VIZ.py_8_0_0.1_180}.pdf}};
\node (label) at (4,2.5-2.5+0.3-0.1)[draw=none, align=left, anchor=center]{\includegraphics[page=1,width=.6\textwidth]{{figures/RunTomassini_CosinePLoss_NaturalisticPrior_VIZ_OnlyHuman_MainPaper_ErrBar.py_8_0_0.1_180}.pdf}};






			\node (label) at (2.4,-2.4+1)[draw=none, align=left, anchor=center]{\textbf{B. Adding a Constant}};
			\node (label) at (1,-2.4+0-0.4+0.3)[draw=none, align=left, anchor=center]{\includegraphics[page=1,width=.6\textwidth]{{figures/RunFalseGardelle_FreePrior_CosineLoss_AddConstant_VIZ.py_8_0_10.0_180}.pdf}};
			\node (label) at (4,-2.4+0.2-0.4+0.3-0.1-0.1)[draw=none, align=left, anchor=center]{\includegraphics[page=1,width=.6\textwidth]{{figures/RunFalseGardelle_FreePrior_CosineLoss_AddConstant_Veri_VIZ.py_8_0_10.0_180}.pdf}};


			\node (label) at (2.4,-4.8+1-0.1)[draw=none, align=left, anchor=center]{\textbf{C. Shifting pattern between noise conditions}};
			\node (label) at (1,-4.8+0-0.4-0.3+0.3)[draw=none, align=left, anchor=center]{\includegraphics[page=1,width=.6\textwidth]{{figures/RunFalseGardelle_FreePrior_CosineLoss_Shift_VIZ.py_8_0_10.0_180}.pdf}};
			\node (label) at (4,-4.8+0.2-0.4-0.3+0.3-0.1-0.1)[draw=none, align=left, anchor=center]{\includegraphics[page=1,width=.6\textwidth]{{figures/RunFalseGardelle_FreePrior_CosineLoss_Shift_Veri_VIZ.py_8_0_10.0_180}.pdf}};

		\end{tikzpicture}
	\end{center}

	\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Reproduced with:
python3 RunGardelle_UniformPrior_CosineLoss_VIZ_OnlyHuman.py 8 0 10.0 180
python3 RunFalseGardelle_FreePrior_CosineLoss_NoiseType_VIZ.py 8 0 10.0 180
python3 RunFalseGardelle_FreePrior_CosineLoss_AddConstant_VIZ.py 8 0 10.0 180
python3 RunFalseGardelle_FreePrior_CosineLoss_AddConstant_Veri_VIZ.py 8 0 10.0 180
python3 RunFalseGardelle_FreePrior_CosineLoss_Shift_VIZ.py 8 0 10.0 180
python3 RunFalseGardelle_FreePrior_CosineLoss_Shift_Veri_VIZ.py 8 0 10.0 180
\end{Verbatim}
\fi                                                                                      

		\caption{Falsifiability analysis: In each row, we show the bias arising from counterfactual manipulations (right) of the data collected by \citet{deGardelle2010AnOI} (A first; B; C) or \citet{Tomassini2010OrientationUR} (A second), together with attempted fits from the model (left). Colors for conditions are as in the main paper. 
		When exchanging conditions manipulating sensory and stimulus noise (A), adding a constant to the responses (B), or shifting the pattern between noise conditions (C), the model ceases to provide a fit:
Even though the model has broad flexibility in fitting resources and priors, it cannot reproduce the qualitative patterns in the counterfactually manipulated datasets.
For the counterfactual datasets, we plot means and bootstrapped 95\% confidence intervals over $n$=120 trials at each stimulus value and condition (a, bottom), and means with bootstrapped 95\% confidence bands (a top, b, c).
		}\label{fig:falsify}

\end{figure}


