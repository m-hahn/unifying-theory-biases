\subsection{Illustrating Theorems 1--3}


%vimdiff simulate_StimulusNoise_ByExponent.py simulate_SensoryNoise_LargeNoise.py simulate_SensoryNoise_SI.py simulate_StimulusNoise_SI.py simulate_StimulusNoise_ByExponent_Analytical.py

In this section, we provide simulations illustrating the implications of Theorems 1--3 (see Main Text, Equations (2--3) and theorems in Materials and Methods).
\begin{enumerate}
	\item \emph{(Additive Decomposition)} Theorem 1 describes an additive decomposition of biases into attraction and repulsion; repulsion but not attraction depends on the loss function. We illustrate this in Figure~\ref{fig:additive-prior-likelihood}, where we show the bias for all combinations of a set of encoding resource allocations with a set of priors.

	\item \emph{(Sensory Noise and Loss Function)} Theorem 1 implies that sensory noise increases both repulsion and attraction. It further states that repulsion -- but not attraction -- scales linearly (factor $\frac{p+2}{2}$) with the exponent $p \geq 1$.
		This functional form does not hold for $p=0$, where the factor is $\frac{1}{4}$ instead of expected $\frac{1}{2}$.
Figure~\ref{fig:bias-by-loss} exemplifies this at the example of orientation estimation.
Figure~\ref{fig:bias-by-loss2} shows the scaling of bias with sensory noise and with loss function exponents, including non-integer exponents.
Results transfer to non-integer exponents; furthermore, there is a nonlinearity close to $0$, corresponding to the difference in functional form.


	\item \emph{(Behavior at Large Sensory Noise)}
Theorem 1 describes the bias in the limit when noise is small.
In Figure~\ref{fig:bias-by-noise}, we examine the behavior of the bias as sensory noise increases, for the same point as selected in Figure~\ref{fig:bias-by-loss2}.
For small noise, biases scale quadratically with $\sigma$ as predicted by the analytical approximation.
For large noise, biases grow more slowly and saturate, reflecting averaging out of local variation.

\item \emph{(Role of Stimulus Noise)} While sensory noise increases both components of bias according to Theorem 1, Theorem 2 shows that the action of stimulus noise is different for attractive and repulsive components: Stimulus noise always increases prior attraction. It leaves likelihood repulsion unchanged when $p=2$, but decreases it when $p>2$.
	These analytical results are verified by the simulation in Figure~\ref{eq:simulation-stimulus-noise}.

\item \emph{(Boundary Effects)}  Theorem 3 describes how abrupt truncation of the prior leads to both a regression effect into the interior, and modulation of attractive and repulsive components close to the boundary. This is illustrated by simulations in Figure~\ref{fig:sim-boundary}.
	Whereas prior attraction is independent of the loss function, the boundary effect increases with the loss function exponent (Figure~\ref{fig:sim-bpoundary-by-loss}).
\end{enumerate}


\begin{figure}
    \centering

	        \begin{tikzpicture}
			\node (label) at (1,0)[draw=none, align=left, anchor=center]{ \includegraphics[width=0.98\textwidth]{{figures/simulate_AdditiveEffect.py_OVERALL}.pdf}};
			\node (label) at (1,-7)[draw=none, align=left, anchor=center]{ \includegraphics[width=0.58\textwidth]{{figures/simulate_AdditiveEffect_ColorLegend.py_OVERALL}.pdf}};
			\node (label) at (-2.85,1.4-0.2)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{ Repulsion};
			\node (label) at (-1.8+0.2,2.2)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center, rotate=90]{ Attraction};

			\node (label) at (1,-5.8)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{ Loss Function Exponent:};
			\node (label) at (3.5,6)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{ \large{Prior}};
			\node (label) at (-7.5,-2.0)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center, rotate=90]{ \large{Encoding}};
		\end{tikzpicture}

		\ifshowcode
\begin{Verbatim}
Reproduced via:
python3 simulate_AdditiveEffect.py
python3 simulate_AdditiveEffect_ColorLegend.py
\end{Verbatim}
\fi



	\caption{Additive effects of prior attraction and likelihood repulsion on biases at a stimulus $\theta$, as a function of encoding resource allocation, prior, and sensory SD (quantified as the SD of the likelihood). All biases are numerically simulated. \textbf{Rows} represent three different sample encoding resource allocations ($F'(\theta) \propto \sqrt{\FI}$ is plotted in the left columns): one is uniform, one is bimodal, one is unimodal.
    The position of the stimulus $\theta$ in stimulus space is indicated by the orange line.
In order to make biases comparable across the three encoding resource allocations, we chose $\theta$ so that $\J(\theta)$ is approximately constant across the three encodings.
	For each encoding, we plot the corresponding likelihood at the different levels of sensory noise.
	The resource allocations give rise to different likelihood repulsion biases at $\theta$, plotted in the second column, as a function of sensory SD $\sigma$ (from $0$ to $20$, expressed as the SD of the likelihood projected into the stimulus space $\mathcal{X}$).  \textbf{Columns} indicate four different priors (plotted in the first row), with corresponding prior attraction biases at $\theta$ (plotted in the second row, again as a function of likelihood SD).
    The remaining cells indicate the simulated overall bias determined by encoding and prior.
 As predicted by Theorem 1, each bias arises as the sum of the attractive and repulsive biases corresponding to its row and column.
    Prior attraction is independent of the loss function, whereas the strength of likelihood repulsion increases with $p$.
    }
    \label{fig:additive-prior-likelihood}
    \end{figure}



\begin{figure}
	\centering


	\includegraphics[width=0.85\textwidth]{{figures/simulate_StimulusNoise_ByExponent.py_OVERALL}.pdf}



	% the color scheme is called tab10
	\definecolor{col1}{rgb}{0.12156862745098039, 0.4666666666666667, 0.7058823529411765}
	\definecolor{col2}{rgb}{1.0, 0.4980392156862745, 0.054901960784313725}
	\definecolor{col3}{rgb}{0.17254901960784313, 0.6274509803921569, 0.17254901960784313}
	\definecolor{col4}{rgb}{0.8392156862745098, 0.15294117647058825, 0.1568627450980392}
	\definecolor{col5}{rgb}{0.5803921568627451, 0.403921568627451, 0.7411764705882353}
	\definecolor{col6}{rgb}{0.5490196078431373, 0.33725490196078434, 0.29411764705882354}

	{\fontfamily{phv}\selectfont Loss Function Exponent:}

	{\huge\textcolor{col1}{---}} $p=0$,\ \ \ \ 
	{\huge\textcolor{col2}{---}} $p=1$,\ \ \ \ 
	{\huge\textcolor{col3}{---}} $p=2$,\ \ \ \ 
	{\huge\textcolor{col4}{---}} $p=3$,\ \ \ \ 
	{\huge\textcolor{col5}{---}} $p=4$,\ \ \ \ 
	{\huge\textcolor{col6}{---}} $p=5$

\ifshowcode
\begin{Verbatim}
Reproduced via:
python3 simulate_StimulusNoise_ByExponent.py
\end{Verbatim}
\fi


	\caption{Role of sensory noise magnitude and loss function exponent, at the example of orientation estimation.
	For a model of orientation perception with matched prior and encoding precision (compare Main Text, Figure 1D 5--7), we show simulated biases for three levels of sensory noise (rows) and loss function exponents from 0 to 5 (colors).
	As predicted by Theorem 1, sensory noise increases attractive and repulsive components.
	Loss function exponent increases repulsive components, but not prior attraction.
	In this case, the overall bias is attractive when $p=0$ and repulsive when $p\geq 1$.
	The arrow refers to the stimulus analyzed further in Figure~\ref{fig:bias-by-loss2}. % and \ref{fig:bias-by-noise}.
	}\label{fig:bias-by-loss}
\end{figure}

\begin{figure}
\centering

	\definecolor{pltPurple}{rgb}{0.5019607843137255, 0.0, 0.5019607843137255}
	\definecolor{pltRed}{rgb}{1.0, 0.0, 0.0}
	\definecolor{pltGreen}{rgb}{0.0, 0.5019607843137255, 0.0}
	\definecolor{pltOrange}{rgb}{1.0, 0.6470588235294118, 0.0}
	\definecolor{pltBlue}{rgb}{0.0, 0.0, 1.0}
	\definecolor{pltGray}{rgb}{0.5, 0.5, 0.5}




	\includegraphics[width=0.85\textwidth]{{figures/simulate_StimulusNoise_ByExponent_Analytical.py_OVERALL}.pdf}
%


	{\fontfamily{phv}\selectfont \textcolor{pltBlue}{----} low sensory noise} %Noise SD XXX \% of sensory space

	{\fontfamily{phv}\selectfont \textcolor{pltRed}{----} medium sensory noise} %^Noise SD XXX \% of sensory space

	{\fontfamily{phv}\selectfont \textcolor{pltGreen}{----} high sensory noise} %Noise SD XXX \% of sensory space

\ifshowcode
\begin{Verbatim}
Reproduced via:
python3 simulate_StimulusNoise_ByExponent_Analytical.py
\end{Verbatim}
\fi


	\caption{Role of sensory noise magnitude and loss function exponent, at the example of orientation estimation.
	Bias (at the $22.5^\circ$ peak of the repulsive bias in orientation estimation, indicated by the arrows in Figure~\ref{fig:bias-by-loss}) as a function of the loss function exponent $P$, for three levels of sensory noise.
We show simulated bias including fractional exponents (left), analytical approximations for integer exponents (center), and both superimposed (right).
	As predicted by Theorem 1, the bias is a monotonic function of the exponent, and approximately linear when noise and exponent are small; for larger noise and exponent, the finite size of the stimulus space leads to sublinear growth, seen most distinctly when noise is large (green curve).
	The monotonic relationship between exponent and bias, shown for integer exponents in Theorem 1, smoothly extends to fractional exponents.
	There is a very rapid jump as $P \rightarrow 0$; in the analytical expression, this is reflected in a special expression for the MAP estimator's bias (Equation [3] in the main text).
	The bias is attractive when $P$ is $0$ or very close to $0$, and repulsive otherwise.
	}\label{fig:bias-by-loss2}
\end{figure}


\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont


	        \begin{tikzpicture}
			\node (label) at (1,-1.5)[draw=none, align=left, anchor=center]{ \includegraphics[width=0.88\textwidth]{{figures/simulate_SensoryNoise_LargeNoise.py_2}.pdf}};
			\node (label) at (-7.3,1.4)[draw=none, align=left, anchor=center]{ Resources $\sqrt{\FI}$};
			\node (label) at (-7.3,0)[draw=none, align=left, anchor=center]{ Likelihood SD};
			\node (label) at (-7.3,-1.4)[draw=none, align=left, anchor=center]{ Bias};
			\node (label) at (-7.3,-2.8)[draw=none, align=left, anchor=center]{ Attraction};
			\node (label) at (-7.3,-4.2)[draw=none, align=left, anchor=center]{ Repulsion};
		\end{tikzpicture}

\definecolor{pltBlue}{rgb}{0.0, 0.0, 1.0}
\definecolor{pltOrange}{rgb}{1.0, 0.6470588235294118, 0.0}


		\textcolor{pltBlue}{\large{-----}} Simulated

		\textcolor{pltOrange}{\large{- - -}} Analytical approximation (Theorem 1)
	\end{center}

	\ifshowcode
\begin{Verbatim}
Reproduced via:
python3 simulate_SensoryNoise_LargeNoise.py 2
\end{Verbatim}
\fi


	\caption{Scaling of bias in the example of Figure~\ref{fig:bias-by-loss} as a function of the sensory noise magnitude $\sigma$ at $p=2$.
	We describe noise magnitudes in terms of $\frac{1}{\kappa}$ ($\kappa$ the parameter of the von Mises distribution), which corresponds to the variance of the likelihood in $\mathcal{Y} = [0,2\pi]$ in the low-noise limit.
	\textbf{First row:} $\sqrt{\FI}$.
	\textbf{Second row:} SD of the likelihood transformed back into the stimulus space $\mathcal{X} = [0^\circ,180^\circ]$.
	\textbf{Third row:} Bias of the estimator at $p=2$, simulated (solid) and predicted by Theorem 1 (dotted).
	\textbf{Fourth row:} Attractive component, given by analytical formula (dotted) or operationalized numerically as the simulated decoding bias of the MAP estimator (Materials\&Methods, solid).
	\textbf{Fifth row:} Repulsive component, given by analytical formula (dotted) or operationalized as difference between full bias and attractive component.
	For small noise, biases scale quadratically with the noise magnitude, as $\frac{1}{\FI} = \frac{\sigma^2}{\J} \propto \sigma^2 \sim \frac{1}{\kappa}$. For larger noise magnitude, biases grow more slowly and saturate. Hence, when noise is very large (likelihood SD $\gtrapprox 40^\circ$), biases are smaller than predicted by Theorem 1, though with the same qualitative behavior.}\label{fig:bias-by-noise}
\end{figure}

\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont



       	        \begin{tikzpicture}
			\begin{scope}[shift={(0,5.8)}]
				\def\right{8.35}
			\node (label) at (-6,0)[draw=none, align=left, anchor=center, text depth=0pt]{\footnotesize{Prior}}; % text depth=0pt makes tikz ignore descenders, so center anchor has good vertical alignment
			\node (label) at (-4.5,0)[draw=none, align=left, anchor=center, text depth=0pt]{\footnotesize{Resources}};
			\node (label) at (-3,0)[draw=none, align=left, anchor=center, text depth=0pt]{\footnotesize{Attraction}};
			\node (label) at (-1.5,0)[draw=none, align=left, anchor=center, text depth=0pt]{\footnotesize{Repulsion}};
			\node (label) at (-0.2,0)[draw=none, align=left, anchor=center, text depth=0pt]{\footnotesize{Total}};

			\node (label) at (\right+-6,0)[draw=none, align=left, anchor=center, text depth=0pt]{\footnotesize{Prior}}; % text depth=0pt makes tikz ignore descenders, so center anchor has good vertical alignment
			\node (label) at (\right+-4.5,0)[draw=none, align=left, anchor=center, text depth=0pt]{\footnotesize{Resources}};
			\node (label) at (\right+-3,0)[draw=none, align=left, anchor=center, text depth=0pt]{\footnotesize{Attraction}};
			\node (label) at (\right+-1.5,0)[draw=none, align=left, anchor=center, text depth=0pt]{\footnotesize{Repulsion}};
			\node (label) at (\right+-0.2,0)[draw=none, align=left, anchor=center, text depth=0pt]{\footnotesize{Total}};
			\end{scope}

		\node (label) at (1,0)[draw=none, align=left, anchor=center]{ \begin{minipage}[b]{0.48\linewidth}
		\centering

		{\textbf{Varying Sensory Noise}}

	$p=2$

	\ \ 

			\ \ 


	\includegraphics[width=0.97\textwidth]{{figures/simulate_SensoryNoise_SI.py_2}.pdf}

	$p=3$

	\includegraphics[width=0.97\textwidth]{{figures/simulate_SensoryNoise_SI.py_3}.pdf}

$p=4$

	\includegraphics[width=0.97\textwidth]{{figures/simulate_SensoryNoise_SI.py_4}.pdf}

	$p=5$

	\includegraphics[width=0.97\textwidth]{{figures/simulate_SensoryNoise_SI.py_5}.pdf}

$p=6$

	\includegraphics[width=0.97\textwidth]{{figures/simulate_SensoryNoise_SI.py_6}.pdf}

$p=7$

	\includegraphics[width=0.97\textwidth]{{figures/simulate_SensoryNoise_SI.py_7}.pdf}

	$p=8$

	\includegraphics[width=0.97\textwidth]{{figures/simulate_SensoryNoise_SI.py_8}.pdf}



	\end{minipage}
	\ \ \ \ 
        \begin{minipage}[b]{0.48\linewidth}
		\centering

		{\textbf{Varying Stimulus Noise }}



	$p=2$

	\ \ 

		\ \ 

	\includegraphics[width=0.97\textwidth]{{figures/simulate_StimulusNoise_SI.py_2}.pdf}

$p=3$

	\includegraphics[width=0.97\textwidth]{{figures/simulate_StimulusNoise_SI.py_3}.pdf}

$p=4$

	\includegraphics[width=0.97\textwidth]{{figures/simulate_StimulusNoise_SI.py_4}.pdf}

$p=5$

	\includegraphics[width=0.97\textwidth]{{figures/simulate_StimulusNoise_SI.py_5}.pdf}

$p=6$

	\includegraphics[width=0.97\textwidth]{{figures/simulate_StimulusNoise_SI.py_6}.pdf}

$p=7$

	\includegraphics[width=0.97\textwidth]{{figures/simulate_StimulusNoise_SI.py_7}.pdf}

	$p=8$

	\includegraphics[width=0.97\textwidth]{{figures/simulate_StimulusNoise_SI.py_8}.pdf}




	\end{minipage}};
	\end{tikzpicture}


        \definecolor{pltGreen}{rgb}{0.0, 0.5019607843137255, 0.0}

{\Huge\textcolor{pltGreen}{-----}} Low Noise\ \ \ \ \ \ \ \ 
{\Huge\textcolor{yellow}{-----}} High Noise
\end{center}

\ifshowcode
\begin{Verbatim}
Reproduced via:
python3 simulate_SensoryNoise_SI.py {2-8}
python3 simulate_StimulusNoise_SI.py {2-8}
\end{Verbatim}
\fi


	\caption{
		Effect of sensory (internal or neural) and stimulus (external) noise on bias.
		Simulated bias in orientation perception, assuming prior and encoding matched to natural image statistics.
\textbf{Left:} Varying sensory noise at zero stimulus noise.
Increasing sensory noise (yellow) increases both attraction and repulsion, independently of the loss function, as predicted by Theorems 1 and 2.
\textbf{Right:} Varying stimulus noise while keeping sensory noise fixed. % (SD 5\% of the sensory space).
Increasing stimulus noise (yellow) increases prior attraction independently of the loss function.
	While it leaves likelihood repulsion approximately unchanged at $p=2$, it decreases the repulsive component for high exponents, as predicted by Theorem 2.
		}\label{eq:simulation-stimulus-noise}
\end{figure}

\begin{figure}
	\begin{center}
		\fontfamily{phv}\selectfont


A.	Boundary Effect

\ \ 

\includegraphics[width=0.8\textwidth]{{figures/simulate_BoundaryEffect_Detailed3.py_OVERALL}.pdf}

\ \ 

\ \ 

B. Boundary Effect and Prior Attraction

\ \ 

\includegraphics[width=0.8\textwidth]{{figures/simulate_BoundaryEffect_Detailed2.py_OVERALL}.pdf}

\ \ 

\ \ 

C. Boundary Effect and Likelihood Repulsion

\ \ 

\includegraphics[width=0.8\textwidth]{{figures/simulate_BoundaryEffect_Detailed4.py_OVERALL}.pdf}
	\end{center}

	\ifshowcode
\begin{Verbatim}
Reproduced via:
python3 simulate_BoundaryEffect_Detailed2.py 2
python3 simulate_BoundaryEffect_Detailed3.py 2
python3 simulate_BoundaryEffect_Detailed4.py 2
\end{Verbatim}
\fi


	\caption{Boundary effects (simulated, at $p=2$): Within a large stimulus space $\mathcal{X} = [-10,10]$ with uniform or periodic prior and resource allocation (here, only showing $[0,1.2]$), we consider the effect of truncating the prior at $\theta_{Max}=1$: A. In the vicinity of the boundary, a regression effect into the interior is observed. As sensory noise increases, both the magnitude of the bias and the area affected increase.
	B. Truncating a nontrivial prior at the boundary leads to a combination of prior attraction in the interior and a regression effect close to the boundary; the regression effect prevails close to the boundary.
	C. The same happens to likelihood repulsion.
	}\label{fig:sim-boundary}
\end{figure}


\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont


\includegraphics[width=0.5\textwidth]{{figures/simulate_BoundaryEffect_Detailed3_ByLoss.py_OVERALL}.pdf}

	\definecolor{col1}{rgb}{0.12156862745098039, 0.4666666666666667, 0.7058823529411765}
	\definecolor{col2}{rgb}{1.0, 0.4980392156862745, 0.054901960784313725}
	\definecolor{col3}{rgb}{0.17254901960784313, 0.6274509803921569, 0.17254901960784313}
	\definecolor{col4}{rgb}{0.8392156862745098, 0.15294117647058825, 0.1568627450980392}
	\definecolor{col5}{rgb}{0.5803921568627451, 0.403921568627451, 0.7411764705882353}
	\definecolor{col6}{rgb}{0.5490196078431373, 0.33725490196078434, 0.29411764705882354}

\ \

	Loss Function Exponent:

	{\huge\textcolor{col1}{---}} $p=0$,\ \ \ \ 
	{\huge\textcolor{col2}{---}} $p=1$,\ \ \ \ 
	{\huge\textcolor{col3}{---}} $p=2$,\ \ \ \ 
	{\huge\textcolor{col4}{---}} $p=3$,\ \ \ \ 
	{\huge\textcolor{col5}{---}} $p=4$,\ \ \ \ 
	{\huge\textcolor{col6}{---}} $p=5$, etc.
	\end{center}

\ifshowcode
\begin{Verbatim}
Reproduced via:
python3 simulate_BoundaryEffect_Detailed3_ByLoss.py
\end{Verbatim}
\fi



	\caption[Loss Function and Boundary]{Effect of loss function on the boundary effect. We show the bias resulting at the boundary in Figure~\ref{fig:sim-boundary}A as a function of the exponent. In the analytical expressions, this quantity corresponds to Equation~(\ref{eq:regression-lagrange-form}); it reflects a local average of the coefficient $H_{1,p,D}$ plotted in Figure~\ref{fig:v0} for positive even exponents.
	As predicted theoretically, the magnitude of bias increases with the loss function exponent.
	This explains simulation results in \citet[][Figure 5]{Jazayeri2010TemporalCC}, where the boundary effect was much more pronounced at $p=2$ (their \textit{Bayesian Least Squares} [BLS]) than at $p=0$ (MAP).
	This dependence on the loss function is caused by the abrupt discontinuous truncation of the prior, qualitatively different from ordinary prior attraction to a smooth prior (as described by Theorem 1), which is approximately independent of the loss function.
	\citet{Jazayeri2010TemporalCC} concluded that $p=2$ provided a better model of their data; however, our results show that this task is better accounted for by a smooth unimodal prior, with fit largely independent of the loss function (Section~\ref{sec:si-remington}).
	}\label{fig:sim-bpoundary-by-loss}
\end{figure}


\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont

	\textbf{Flat Prior}

	\ \ 

\includegraphics[width=0.95\textwidth]{{figures/simulate_Comparison_Flat.py_OVERALL}.pdf}

\ \ 

\ \ 

	\textbf{Unimodal Prior}

\ \ 


\includegraphics[width=0.95\textwidth]{{figures/simulate_Comparison_Unimodal.py_OVERALL}.pdf}
	\end{center}

	\ifshowcode
\begin{Verbatim}
Reproduced via:
python3 simulate_Comparison_Flat.py 2
python3 simulate_Comparison_Unimodal.py 2
\end{Verbatim}
\fi



	\caption{Comparison between flat and unimodal prior. Both kinds of prior can give rise to central tendency effect, either as a boundary effect (flat prior, \citep[e.g.][]{Jazayeri2010TemporalCC}) or prior attraction (unimodal prior, \citep[e.g.][]{Petzschner2011IterativeBE}). An important difference is visible in the variability: The unimodal prior may not impact the variability; the flat prior leads to reduced variability at the boundary.}
\end{figure}


\clearpage
\subsection{Distinguishing Prior Attraction from Boundary Effects}

Here, we consider an example where regression to the mean reflects boundary effects, rather than attraction to a smooth prior.
\citet{Polana2018EfficientCO} measured the relative bias in subjective value judgments on a bounded scale when sensory noise was high or low, finding an outwards bias around the center, and an inwards bias elsewhere.
While directly fitting the encoding model on this data is infeasible as the ground truth values (i.e., subjects' subjective values) were not measured, we simulated our model based on the encoding and prior determined in independent experiments by \citet{Polana2018EfficientCO}.
Results are shown in Figure~\ref{fig:subjective-value-simulation}.
Importantly, even though the prior peaks in the middle of the range, the regression effect cannot be explained as prior attraction here: If prior attraction were responsible for the regression to the mean, biases should be attractive towards the center throughout the entire range.
The fact that the regression effect only appears along the boundary shows that boundary effects are instead responsible.
While prior attraction shows a broadly similar qualitative pattern as regression to the mean, the simultaneous apperance of attraction on the boundary and repulsion in the center is absent when disregarding the boundary effect.

\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont

        \includegraphics[width=0.95\textwidth]{{figures/ExampleSubjectiveValue3_Fit3_NoPower_Loss_NoPower_Free_Viz_NoBoundary.py}.pdf}

	\textcolor{orange}{---} {{Low sensory noise}}\ \ \ \  \textcolor{blue}{---} {{High sensory noise}}
	\end{center}

	\ifshowcode
\begin{Verbatim}
Reproduced via:
python3 ExampleSubjectiveValue3_Fit3_NoPower_Loss_NoPower_Free_Viz_NoBoundary.py 2
\end{Verbatim}
\fi



	\caption{Bias in a bounded scale of subjective value ratings: \citet{Polana2018EfficientCO} measured the relative bias between short and long exposure times. The prior and resource allocation were parameterized based on \citet{Polana2018EfficientCO}. Numerical parameters were fitted to match reported summary statistics. In the ``Relative'' facet, the dotted curve indicates the corresponding prediction without boundary effects. The predicted relative bias (solid) shows a repulsive bias away from the center when close to the center, and a bias towards it further out. This bias towards the center cannot be explained as prior attraction, because attraction would have to also dominate in the center if it dominated the repulsion away from it.
Rather,	the pattern is correctly accounted for in terms of the regression away from the boundary, similar to Figure~\ref{fig:sim-boundary}C: Likelihood repulsion accounts for biases in the interior, whereas the regression effect dominates closer to the boundary. 
The	dotted line indicates relative bias in the absence of a boundary; this would be repulsive everywhere.
}
    \label{fig:subjective-value-simulation}
\end{figure}


%
%We note that, for interval data (though not circular data), the space has hard boundaries at the minimum and maximum of the range (unless it is infinite). However, in the examples we have, experimental stimuli are in a small part of that space, quite far from the boundary (if measured in sensory space).
%As a consequence, the hard boundary is essentially irrelevant in determining biases in such datasets.

