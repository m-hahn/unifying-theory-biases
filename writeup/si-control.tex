

We next asked whether the models we fitted across modalities are identifiable in principle, by simulating synthetic datasets from fitted models, and running our fitting procedure on the resulting simulated datasets.
If these models can be reliably identified from estimation data, the resulting fit should resemble the original fit.
We further fitted the models across loss functions to validate the extent to which the loss function is identifiable.
Results for each of the datasets, together with discussion in figure captions, are provided in Figures \ref{fig:recovery-gardelle-1}--\ref{fig:recovery-6}.
Taken together, we consistently found that the original fit was closely matched by the re-fitted model (Figures \ref{fig:recovery-gardelle-1}--\ref{fig:recovery-6}), and that loss functions can be recovered in those cases where the original dataset shows substantially different NLL for different loss functions.
Overall, this shows that, despite the flexibility of our model, ground truth models can be recovered using the amount of observations available in these datasets.
In particular, all aspects of the model fits relevant to our conclusions (e.g., approximately uniform recovered prior in orientation perception, or the distinctive roles of encoding and prior in color perception) are well recovered.





\begin{figure}
%	\centering
{
\fontfamily{phv}\selectfont

	\textbf{1. Naturalistic Prior, Arbitrary Encoding}

\ 

	        \begin{tikzpicture}
			\node (label) at (1,2)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/RunGardelle_NaturalPrior_CosineLoss_VIZ.py_8_0_10.0_180.pdf}};
			\node (label) at (1,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/RunGardelle_NaturalPrior_CosineLoss_OnSim_VIZ.py_8_0_10.0_180.pdf}};
			\node (label) at (9,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/evaluateCrossValidationResults_Gardelle_180_NaturalPrior_OnSim.py_RelativeLF.pdf}};
			\node (label) at (1,3.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Original Fit};
			\node (label) at (1,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Fit at ground-truth loss function:};
			\node (label) at (9,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{NLL by loss function:};
		\end{tikzpicture}


\ 



	\textbf{2. Uniform Prior, Arbitrary Encoding}

\ 

	        \begin{tikzpicture}
			\node (label) at (1,2)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/RunGardelle_UniformPrior_CosineLoss_VIZ.py_8_0_10.0_180.pdf}};
			\node (label) at (1,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/RunGardelle_UniformPrior_CosineLoss_OnSim_VIZ.py_8_0_10.0_180.pdf}};
			\node (label) at (9,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/evaluateCrossValidationResults_Gardelle_180_UniformPrior_OnSim.py_RelativeLF.pdf}};
			\node (label) at (1,3.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Original Fit};
			\node (label) at (1,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Fit at ground-truth loss function:};
			\node (label) at (9,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{NLL by loss function:};
		\end{tikzpicture}
}
	\caption{Recoverability analysis: Data from \citet{deGardelle2010AnOI}, for models in Main Text, Figure 3b--c ($p=8$).
See Figure~\ref{fig:recovery-gardelle-2} for nonparametrically fitted prior.
For each model variant, we show the original fitted model used as the basis for simulating a synthetic dataset (top), the NLL across loss function exponents when fitting on that synthetic dataset (bottom right).
Models can be closely recovered when simulating from (and fitting with) naturalistic, uniform, or freely fitted priors (Figures \ref{fig:recovery-gardelle-1}--\ref{fig:recovery-gardelle-2}).
In close agreement with what is observed when fitting on the original raw dataset (Figure~\ref{fig:nll-Gardelle}),  low exponents can be ruled out in all three situations (Figures~\ref{fig:recovery-gardelle-1}--\ref{fig:recovery-gardelle-2}).
}\label{fig:recovery-gardelle-1}
\end{figure}



\begin{figure}
%	\centering
{
\fontfamily{phv}\selectfont

	
	\textbf{3. Arbitrary Prior, Arbitrary Encoding}

\ 

	        \begin{tikzpicture}
			\node (label) at (1,2)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/RunGardelle_FreePrior_CosineLoss_VIZ.py_8_0_10.0_180.pdf}};
			\node (label) at (1,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/RunGardelle_FreePrior_CosineLoss_OnSim_VIZ.py_8_0_10.0_180.pdf}};
			\node (label) at (9,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/evaluateCrossValidationResults_Gardelle_180_FreePrior_OnSim.py_RelativeLF.pdf}};
			\node (label) at (1,3.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Original Fit};
			\node (label) at (1,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Fit at ground-truth loss function:};
			\node (label) at (9,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{NLL by loss function:};
		\end{tikzpicture}
}
	\caption{Recoverability analysis: Data from \citet{deGardelle2010AnOI}. Continuation of Figure~\ref{fig:recovery-gardelle-1}.}\label{fig:recovery-gardelle-2}
\end{figure}







\begin{figure}
{
\fontfamily{phv}\selectfont


	\textbf{1. Naturalistic Prior, Arbitrary Encoding}

\ 

        \begin{tikzpicture}
			\node (label) at (1,2)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/RunTomassini_CosinePLoss_NaturalisticPrior_VIZ.py_8_0_0.1_180.pdf}};
			\node (label) at (1,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/RunTomassini_CosinePLoss_NaturalisticPrior_OnSimData_VIZ.py_8_0_0.1_180.pdf}};
			\node (label) at (9,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/evaluateCrossValidationResults_Tomassini_180_NaturalisticPrior_OnSimData.py_RelativeLF.pdf}};
			\node (label) at (1,3.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Original Fit};
			\node (label) at (1,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Fit at ground-truth loss function:};
			\node (label) at (9,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{NLL by loss function:};
		\end{tikzpicture}

\  

	\textbf{2. Uniform Prior, Arbitrary Encoding}

\ 

        \begin{tikzpicture}
			\node (label) at (1,2)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/RunTomassini_CosinePLoss_UniPrior_VIZ.py_8_0_0.1_180.pdf}};
			\node (label) at (1,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/RunTomassini_CosinePLoss_UniPrior_OnSimData_VIZ.py_8_0_0.1_180.pdf}};
			\node (label) at (9,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/evaluateCrossValidationResults_Tomassini_180_UniPrior_OnSimData.py_RelativeLF.pdf}};
			\node (label) at (1,3.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Original Fit};
			\node (label) at (1,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Fit at ground-truth loss function:};
			\node (label) at (9,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{NLL by loss function:};
		\end{tikzpicture}

}


	\ifshowcode
\begin{Verbatim}
Reproduced via:
python3 RunTomassini_CosinePLoss_NaturalisticPrior_VIZ.py 8 0 0.1 180
python3 RunTomassini_CosinePLoss_NaturalisticPrior_OnSimData_VIZ.py 8 0 0.1 180
python3 RunTomassini_CosinePLoss_UniPrior_VIZ.py 8 0 0.1 180 2
python3 RunTomassini_CosinePLoss_UniPrior_OnSimData_VIZ.py 8 0 0.1 180 
\end{Verbatim}
\fi
	\caption{Recoverability analysis: Data from \citet{Tomassini2010OrientationUR}, for models in Main Text, Figure 3g--h ($p=8$).
See Figure~\ref{fig:recovery-tomassinib} for nonparametrically fitted prior.
For each model variant, model components are well recovered at the ground-truth loss function ($p=8$) underlying the simulated datasets (Figure~\ref{fig:recovery-tomassini}--\ref{fig:recovery-tomassinib}).
Low exponents can be ruled out when simulating from (and fitting with) naturalistic and uniform priors.
When simulating from (and fitting with) the freely fitted model (Figure~\ref{fig:recovery-tomassinib}), NLL does not difer much between high and low exponents, mirroring what is found when fitting on the original raw data (Figure~\ref{fig:nll-Tomassini}).
However, a low exponent cannot reproduce the experimentally observed effect of stimulus noise, and requires a prior peaking at oblique directions, at odds with both experimental stimulus statistics and naturalistic image statistics (Figure~\ref{fig:tomassini-free}).
In contrast, the larger dataset collected by \citet{deGardelle2010AnOI} (10K trials vs 2K trials) is sufficient to confidently identify high exponents (Figures \ref{fig:gardelle-free}). %When constraining the model to fit an approximately uniform prior as identified on that dataset, small exponents are ruled out (Figures \ref{fig:tomassini-uniprior}).
}\label{fig:recovery-tomassini}
\end{figure}



\begin{figure}
{
\fontfamily{phv}\selectfont



\textbf{3. Arbitrary Prior, Arbitrary Encoding:}

\ 

        \begin{tikzpicture}
			\node (label) at (1,2)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/RunTomassini_CosinePLoss_FreePrior_VIZ.py_8_0_0.1_180.pdf}};
			\node (label) at (1,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/RunTomassini_CosinePLoss_FreePrior_OnSimData_VIZ.py_8_0_0.1_180.pdf}};
			\node (label) at (9,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/evaluateCrossValidationResults_Tomassini_180_FreePrior_OnSimData.py_RelativeLF.pdf}};
			\node (label) at (1,3.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Original Fit};
			\node (label) at (1,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Fit at ground-truth loss function:};
			\node (label) at (9,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{NLL by loss function:};
		\end{tikzpicture}
}
	\caption{Continuation of Figure~\ref{fig:recovery-tomassini}.
}\label{fig:recovery-tomassinib}
\end{figure}


\begin{figure}

        \begin{tikzpicture}
			\node (label) at (1,2)[draw=none, align=left, anchor=center]{\includegraphics[height=0.25\textheight]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_ACREXP.py_ALL_0_1.0_180_50.pdf}};
			\node (label) at (1,-4.5)[draw=none, align=left, anchor=center]{\includegraphics[height=0.25\textheight]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_OnSimData_VIZ2.py_4_0_1.0_180.pdf}};
			\node (label) at (9,-2.8)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/evaluateCrossValidationResults_Gekas_Fixed_OnSim.py_RelativeLF.pdf}};
			\node (label) at (1,5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Original Fit};
			\node (label) at (1,-1.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Fit at ground-truth loss function:};
			\node (label) at (9,-1.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{NLL by loss function:};
		\end{tikzpicture}


	\caption{Recoverability analysis: Motion direction dataset collected by \citet{gekas2013complexityas}, for model in Main Text, Figure 4 b--d ($p=4$).
We show the original fit (top), and results for fitting on data simulated from that fit: NLL by exponent (bottom right), and the model components (left).
Model components and loss function are well recovered. 
}\label{fig:recovery-gekas}
\end{figure}



\begin{figure}


        \begin{tikzpicture}
			\node (label) at (1,2)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/RunBae_FreePrior_Both_CosineLoss_VIZ3MainPaper.py_2_0_1.0_180.pdf}};
			\node (label) at (1,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/RunSynthetic_Bae_FreePrior_Both_CosineLoss_OnSim_VIZAveraged.py_SimulateSynthetic_Bae_Parameterized_Both.py_2_12345_FITTED_FITTED.txt_2_0_1.0_180.pdf}};
			\node (label) at (9,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/evaluateCrossValidationResults_Bae_New_Both_Synthetic.py_SimulateSynthetic_Bae_Parameterized_Both.py_2_12345_FITTED_FITTED.txt_RelativeLF.pdf}};
			\node (label) at (1,3.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Original Fit};
			\node (label) at (1,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Fit at ground-truth loss function:};
			\node (label) at (9,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{NLL by loss function:};
		\end{tikzpicture}

	\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Reproduced via:
python3 RunBae_FreePrior_Undelayed_CosineLoss_VIZ3_ACREXP.py 4 0 1.0 180 
python3 RunBae_FreePrior_Undelayed_CosineLoss_OnSim_VIZ3.py 4 0 1.0 180
\end{Verbatim}
\fi


	\caption{Recoverability analysis: color hue dataset collected by \citet{Bae2015WhySC}, for model in Main Text, Figure 6a ($p=2$).
The encoding is closely recovered, and the loss function used for simulation (p=2) is well identified.
The bimodal nature of the prior is identified. While the relative heights of the peaks is not recovered, the shape of the resulting attrative bias is reproduced.
}\label{fig:recovery-bae}
\end{figure}



\begin{figure}
{
\fontfamily{phv}\selectfont


	\textbf{1. Normal (in Sensory Space) Prior}


\
        \begin{tikzpicture}
			\node (label) at (1,2)[draw=none, align=left, anchor=center]{\includegraphics[height=0.09\textheight]{figures/RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_OnlyModel.py_0_2_10.0_151.pdf}};
			\node (label) at (1,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.09\textheight]{figures/RunXiang_ZeroExp_Lognormal_SignGD_AVE_OnSim_VIZ_OnlyModel.py_0_2_10.0_151.pdf}};
			\node (label) at (9,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/evaluateCrossValidationResults_Xiang_AVE_Lognormal_OnSim.py_RelativeLF.pdf}};
			\node (label) at (1,3.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Original Fit};
			\node (label) at (1,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Fit at ground-truth loss function:};
			\node (label) at (9,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{NLL by loss function:};
		\end{tikzpicture}

 


\ 


	\textbf{2. Flat Prior}

\ 

        \begin{tikzpicture}
			\node (label) at (1,2)[draw=none, align=left, anchor=center]{\includegraphics[height=0.09\textheight]{figures/RunXiang_ZeroExp_Flat_SignGD_AVE_VIZ_OnlyModel.py_0_2_10.0_151.pdf}};
			\node (label) at (1,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.09\textheight]{figures/RunXiang_ZeroExp_Flat_SignGD_AVE_OnSim_VIZ_OnlyModel.py_0_2_10.0_151.pdf}};
			\node (label) at (9,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/evaluateCrossValidationResults_Xiang_AVE_Flat_OnSim.py_RelativeLF.pdf}};
			\node (label) at (1,3.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Original Fit};
			\node (label) at (1,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Fit at ground-truth loss function:};
			\node (label) at (9,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{NLL by loss function:};
		\end{tikzpicture}
}

	\ifshowcode
\begin{Verbatim}
Reproduced via:
python3 RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_OnlyModel.py 0 2 10.0 151
python3 RunXiang_ZeroExp_Lognormal_SignGD_AVE_OnSim_VIZ_OnlyModel.py 0 2 10.0 151
python3 RunXiang_ZeroExp_Flat_SignGD_AVE_VIZ_OnlyModel.py 0 2 10.0 151
python3 RunXiang_ZeroExp_Flat_SignGD_AVE_OnSim_VIZ_OnlyModel.py 0 2 10.0 151
\end{Verbatim}
\fi


	\caption{Recoverability analysis: Numerosity perception (data from \citet{Xiang2021ConfidenceAC}), for models in Main Text, Figure 5 c--d ($p=0$).
The parametric priors (top) are closely recovered.
For a normal (in sensory space) prior, NLL does not vary much with the exponent, similar to the original fit.
For the flat prior, the model is recovered; however, such a model is clearly ruled out by inferior fit on the original dataset (Figure~\ref{fig:nll-Xiang}).
}\label{fig:recovery-xiang}
\end{figure}




\begin{figure}
{
\fontfamily{phv}\selectfont


	\textbf{1. Normal (in Sensory Space) Prior}


\ 

        \begin{tikzpicture}
			\node (label) at (1,2)[draw=none, align=left, anchor=center]{\includegraphics[height=0.09\textheight]{figures/RunRemington_Lognormal_Zero_VIZ.py_0_0_0.1_200.pdf}};
			\node (label) at (1,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.09\textheight]{figures/RunRemington_Lognormal_Zero_OnSim_VIZ.py_0_0_0.1_200.pdf}};
			\node (label) at (9,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/evaluateCrossValidationResults_Remington_StopNoImpQ_Lognormal_OnSim.py_RelativeLF.pdf}};
			\node (label) at (1,3.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Original Fit};
			\node (label) at (1,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Fit at ground-truth loss function:};
			\node (label) at (9,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{NLL by loss function:};
		\end{tikzpicture}


\ 


	\textbf{2. Flat Prior}

\ 


        \begin{tikzpicture}
			\node (label) at (1,2)[draw=none, align=left, anchor=center]{\includegraphics[height=0.09\textheight]{figures/RunRemington_Flat_Zero_VIZ.py_0_0_0.1_200.pdf}};
			\node (label) at (1,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.09\textheight]{figures/RunRemington_Flat_Zero_OnSim_VIZ.py_0_0_0.1_200.pdf}};
			\node (label) at (9,-1)[draw=none, align=left, anchor=center]{\includegraphics[height=0.1\textheight]{figures/evaluateCrossValidationResults_Remington_StopNoImpQ_Flat_OnSim.py_RelativeLF.pdf}};
			\node (label) at (1,3.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Original Fit};
			\node (label) at (1,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{Fit at ground-truth loss function:};
			\node (label) at (9,0.5)[font=\fontfamily{phv}\selectfont,draw=none, align=left, anchor=center]{NLL by loss function:};
		\end{tikzpicture}



}

	\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Reproduced via:
python3 RunRemington_Lognormal_Zero_VIZ.py 0 0 0.1 200
python3 RunRemington_Lognormal_Zero_OnSim_VIZ.py 0 0 0.1 200
python3 RunRemington_Flat_Zero_VIZ.py 0 0 0.1 200
python3 RunRemington_Flat_Zero_OnSim_VIZ.py 0 0 0.1 200
\end{Verbatim}
\fi


	\caption{Recoverability analysis: Data from \citet{Remington2018LateBI}, for models in Main Text, Figure 5g--h ($p=0$).
%The parametric priors are closely recovered.
For the normal (in sensory space) prior, NLL barely varies with the exponent, similar to the original fit (Figure~\ref{fig:nll-Remington}).
}\label{fig:recovery-6}
\end{figure}


