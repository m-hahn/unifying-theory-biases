\subsection{Fitting Procedure}\label{sec:fitting}

\subsubsection{Model Specification} % and Implementation}


Here, we recapitulate all elements of the model for reference.
The parameter set $\Theta$ of the model is given by:
\begin{enumerate}
	\item The stimulus space $\mathcal{X}$, which is an interval or the circle, and the sensory space $\mathcal{Y}$, which has the same topology as the stimulus space.
Without loss of generality, the length of $\mathcal{Y}$ can be assumed to be 1 (for intervals) or $2\pi$ (for the circle).\footnote{Rescaling it simply results in rescaling of $F$ and $\sigma$. This has no effect on $\FI$, or any other observed quantities. While the space can be an infinite or semi-infinite interval in theory, it is always truncated to a finite interval in implementation.}
	\item $p \geq 0$, the loss function exponent
	\item $\Prior(\theta)$, a function on the stimulus space
	\item $F(\theta)$, a function monotonically mapping from the stimulus space to the sensory space.
	\item $\sigma^2$, the variance of sensory (internal) noise
	\item $\tau^2$, the variance of stimulus (external) noise
	\item $\rho^2$, the variance of motor noise
	\item $\xi$, the guessing rate
\end{enumerate}
When noise is von Mises (for circular stimulus spaces), $\sigma^2, \tau^2, \rho^2$ are identified as the reciprocal of the ``inverse temperature'' parameter $\kappa$ of the von Mises distribution; this equals the variance in the small-noise regime.

When there are multiple noise conditions (e.g., different exposure durations) within an experiment, there correspondingly are multiple noise parameters corresponding to the conditions (e.g., one value of $\sigma^2$ per exposure duration), while all other parameters are kept fixed.
Details for each dataset are provided in Section~\ref{sec:details-datasets}.


If $\Theta$ is the full parameter set of the model, then the generative model generating a response $\theta'$ given a stimulus $\theta$ is as follows:
\begin{enumerate}
	\item Stochastic encoding:
		\begin{equation}\label{eq:model1}
			m = F(\theta + \epsilon) + \delta
		\end{equation}
		where $\epsilon \sim N(0,\tau^2)$, $\delta \sim N(0,\sigma^2)$.
	\item Bayes estimator:
		\begin{equation}\label{eq:model2}
			\widehat{\theta}_{m} := \arg_{\widehat\theta\in\mathcal{X}} \min \int
			\ell(\widehat{\theta}, \theta)
	\Probcap(\theta|m) d\theta
		\end{equation}
		where the posterior $\Probcap(\theta|m)$ is determined by the model parameters $\Theta$, specifically the encoding likelihood and the prior, and where $\ell$ is an $L^p$ loss function as described in Materials and Methods.
	\item Response: With probability $\xi$, the response is uniform on the stimulus space; with probability $(1-\xi)$ it is $\widehat{\theta}_{\ell} + \epsilon_{Motor}$ where $\epsilon_{Motor} \sim N(0,\rho^2)$ (or analogously von Mises, if the space is circular).
\end{enumerate}
Given model parameters $\Theta$, the log-likelihood of a single trial with response $\theta'$ and true stimulus $\theta$ is thus given as
\begin{equation}\label{eq:trial-lik}
	\log \Probmin(\theta'|\theta) = \log \left\{\frac{\xi}{\operatorname{Vol}(\mathcal{X})} + (1-\xi) \cdot \int_{\mathcal{X}} \int_{\mathcal{Y}} P_{Normal}(\theta'|\widehat{\theta}_{m=F(\theta + \epsilon) + \delta}, \rho^2) P_{Normal}(\epsilon|0, \tau^2) P_{Normal}(\delta|0,\sigma^2) d\epsilon d\delta\right\}
\end{equation}
where $\widehat{\theta}_{m=F(\theta + \epsilon) + \delta}$ is the estimator given the encoding $m=F(\theta + \epsilon) + \delta$, and where $P_{Normal}$ is a Gaussian density, replaced by the appropriate von Mises distribution when $\mathcal{X}$ is circular.
The overall negative log-likelihood is the sum over all datapoints $(\theta_{True,i}, \theta'_i)$ in the dataset:
\begin{equation}\label{eq:trial-lik-sum}
-	\sum_{i=1}^K \log \Probmin(\theta'_i|\theta_{True,i})
\end{equation}
This definition of the likelihood pools data from all subjects, irrespective of systematic differences between subjects.
The model can also be fitted with subject-specific effects to account for such subject differences; see Section~\ref{sec:mixed-effects} for this version.

\subsubsection{Implementation}






\begin{figure}
\begin{center}
\begin{tabular}{c|ccccc}
& $\mathcal{X}$ & $\operatorname{Vol}(\mathcal{X})$ & $\operatorname{Vol}(\mathcal{Y})$ & N \\ \hline
\citet{deGardelle2010AnOI} & [0,360]$^*$ & 360$^*$ & $2\pi$ & 180\\
\citet{Tomassini2010OrientationUR} & [0,360]$^*$ & 360$^*$ & $2\pi$ & 180\\
\citet{Bae2015WhySC} & [0,360] & 360 & $2\pi$ & 180\\ 
\citet{gekas2013complexityas} &  [0,360]  & 360       &$2\pi$ & 180\\ \hline
\citet{Xiang2021ConfidenceAC} & [0,151]$^\dagger$ & 151$^\dagger$ & 1 & 151\\
\citet{Remington2018LateBI} & [0,3] & 3 & 1 & 200\\
\end{tabular}
\end{center}


\caption{Stimulus and sensory spaces as used in our model implementation.
We consistently use sensory spaces of length $2\pi$ for circular stimulus spaces, and of length $1$ for interval stimulus spaces.
$^*$For orientation perception, we internally reparameterized to [0,360] for convenience, but always plot results in the standard [0,180]-based parameterization.
$^\dagger$In numerosity perception, the observations are all discrete; conceptually, the stimulus space consists of $\{0,\dots, 150\}$, which contains 151 integers, leading to a volume of 151.
Note that for the interval stimulus spaces, $\mathcal{X}$ conceptually is $[0, \infty)$, but truncation to a finite space is needed for computational implementation.
$N$ denotes the number of points on the discretized grid, as described in Materials and Methods.
}\label{fig:spaces-volumes}
\end{figure}


\begin{figure}
\begin{center}
\begin{tabular}{c|cccccccccc}
& Parameter & Parameterized using... \\ \hline
Stimulus Space & $\mathcal{X}$ & $\theta_1, \dots, \theta_N$ \\
Exponent & $p$ & $p$ \\
Prior & $p_{prior}(\theta)$ & $\beta(\theta_1), \dots, \beta(\theta_N) \in \mathbb{R}$ (Eq. \ref{eq:prior-param})\\
Transfer & $F$ & $\alpha(\theta_1), \dots, \alpha(\theta_N) \in \mathbb{R}$ (Eq. \ref{eq:fprime-param})\\
Noise Parameters & $\sigma^2$ & $\gamma \in \mathbb{R}$ \\
                 & $\tau^2$ & $\phi \in \mathbb{R}$ \\
                 & $\rho^2$ & $\eta \in \mathbb{R}$ \\
                 & $\xi$ & $logit(\xi) \in \mathbb{R}$ \\
\end{tabular}
\end{center}

\caption{The set $\Theta$ of parameters of the model, and their parameterization in the implementation. All parameters subject to model fitting are parameterized using unconstrained real variables.}
\end{figure}

As described in Materials and Methods, the model is implemented computationally on a regularly spaced discretized grid $\theta_1, \dots, \theta_N$.
Conceptually, the implemented version is obtained by replacing all integrals in the definition of the model by sums over the grid.
Here, we provide the relevant computations in detail.
Define $\operatorname{Vol}(\mathcal{X})$ and $\operatorname{Vol}(\mathcal{Y})$ as the volumes (length, in the case of an interval) of the stimulus and sensory spaces, respectively.
In the implementation, we defined the sensory and stimulus spaces for each of the datasets as in Figure~\ref{fig:spaces-volumes}; note that the choice of $\operatorname{Vol}(\mathcal{Y})$ is purely conventional and does not affect any predictions about observable quantities.

\paragraph*{Encoding}
The encoding resources and prior are encoded by assigning one value of $F'$, $\Prior$ to each point $\theta_i$ in the discretized grid.
In parametric fits (e.g., naturalistic prior for orientation, or encoding based on Weber's law), these can be directly computed.
In nonparametric fits, they are parameterized using the softmax transform, which ensures nonnegativity and normalization:
\begin{align}\label{eq:fprime-param}
	F'(\theta_i) =& \operatorname{Vol}(\mathcal{Y}) \frac{\exp(\alpha(\theta_i))}{\sum_{j=1}^N\exp(\alpha(\theta_j))} \\
\label{eq:prior-param}
	\Prior(\theta_i) =& \frac{\exp(\beta(\theta_i))}{\sum_{j=1}^N\exp(\beta(\theta_j))}
\end{align}
for $\theta$ on the grid; here, $\alpha(\theta_i)$ and $\beta(\theta_i)$ ($i=1,\dots, N$) are real-valued parameters of the model. 

Conceptually, the transfer function is given as $F(\theta) = \int_{\theta_{Min}}^\theta F'(\theta')d\theta'$ where $\theta_{Min}$ is the lower end of the stimulus space; it is implemented as the cumulative sum of the discretized $F'$:\footnote{When $\mathcal{X}$ -- and equivalently $\mathcal{Y}$ -- is circular, say $[0,2\pi]$, $\theta_{Min}$ can be taken as $0$, and the output $F(\theta) \in [0,2\pi]$ is interpreted as a point on the circle.}
\begin{equation}\label{eq:cum-sum}
	F(\theta_i) = \sum_{j=1}^{i-1} F'(\theta_j)
\end{equation}
The input stimulus $\theta$ is mapped to the closest element $\theta_i$ of the discretized grid, and is then encoded as $F(\theta_i)$.
The likelihood is projected onto the discretized grid; sensory noise results in another point on the grid.
Formally, the sensory noise likelihood is, for stimuli on the discretized grid $\theta_i, \theta_j$ ($1 \leq i, j \leq N$), given by the discrete probability distribution:
\begin{equation}
	\Probmin(\theta_j|\theta_i) \propto F'(\theta_j) \exp(\frac{d_{\mathcal{Y}}(F(\theta_i)-F(\theta_j))}{\sigma^2})
\end{equation}
where $F'(\theta_i)$ is parameterized by (\ref{eq:fprime-param}), and we set:
\begin{align}
	d_{\mathcal{Y}}(x) := -\frac{x^2}{2} & \ \ \ \text{if }\mathcal{Y}\text{ is interval} \\
	d_{\mathcal{Y}}(x) := \cos(x) & \ \ \ \text{if }\mathcal{Y}\text{ is circular}
\end{align}
In the sensory space, this is a (discretized) Gaussian with variance $\sigma^2$ if $X$ is an interval, and von Mises with parameter $\kappa = \sigma^{-2}$ if $X$ is circular.
An encoding $\theta_j$ is then sampled from this discrete distribution.

\paragraph*{Inference}
Given an encoding $\theta_j$, the posterior is computed on the discrete grid using exact Bayesian inference:
\begin{equation}
	\Probmin_{posterior}(\theta_k|\theta_j) = \frac{p_{prior}(\theta_k) \Probmin(\theta_j|\theta_k)}{\sum_{l=1}^N p_{prior}(\theta_l) \Probmin(\theta_j|\theta_l)}
\end{equation}
The estimator $\widehat{\theta}$ is computed by loss function minimization, discretizing (\ref{eq:model2}):
\begin{equation}\label{eq:discretized-loss}
\widehat{\theta} = \arg_{\widehat{\theta}\in\mathcal{X}} \min \sum_{k=1}^N \ell(\widehat{\theta},\theta_k) \Probmin_{posterior}(\theta_k|\theta_j)
\end{equation}
where $\widehat{\theta}\in\mathcal{X}$ ranges over the entire continuous input space, not just the discretized grid, and where $\ell$ is an $L^p$ loss function as described in Materials and Methods.
For this, we use Newton's method.\footnote{When $p > 1$, we apply Newton's method to minimize the $L^p$ loss; for $p=0$, we use it to maximize the posterior. When the second derivative in loss minimization (similarly for posterior maximization) has the wrong sign in some iteration (negative in the case of minimization), we take a gradient descent step rather than a Newton step; this avoids divergence or convergence to a \emph{maximum} of the loss when the loss is not convex.}
See below for implementation at $p=0$ (MAP estimator).





\paragraph*{Parameterization of Noise Parameters}
To faciliate the fitting procedure, we parameterize the variances $\sigma^2, \rho^2, \tau^2$, constrained to nonnegative values, using unconstrained real-valued parameters $\gamma, \eta, \phi$, as follows.
We parameterize sensory variance via the inverse-logit transformation:
\begin{equation}\label{eq:sigma2-logit}
\sigma^2 :=	\frac{C_\sigma}{1+\exp(-\gamma)}
\end{equation}
where $\gamma$ is a fitted parameter.
\begin{figure}
\begin{center}
\begin{tabular}{c|ccccc}
& $C_\sigma$ \\ \hline
\citet{deGardelle2010AnOI} &  4 \\
\citet{Tomassini2010OrientationUR} & $\operatorname{Vol}(\mathcal{Y})^2 = 4\pi^2$\\
\citet{Bae2015WhySC} &  2 \\ 
\citet{gekas2013complexityas}  & $2$ \\ \hline
\citet{Xiang2021ConfidenceAC}  & $\operatorname{Vol}(\mathcal{Y})^2=1$\\
\citet{Remington2018LateBI}   & $\operatorname{Vol}(\mathcal{Y})^2=1$\\
\end{tabular}
\end{center}
\caption{The coefficient $C_\sigma$ used for each dataset (Equation~\ref{eq:sigma2-logit}). We chose $(\operatorname{Vol}(\mathcal{Y})^2$ by default, but found that smaller values avoided convergence issues in some datasets.  }
\end{figure}
By default, we took $C_\sigma := (\operatorname{Vol}(\mathcal{Y})^2$, so $\sigma$ describes a fraction of sensory space volume, but we found that smaller values of $C_\sigma$ could sometimes speed up convergence.
The guessing rate $\xi \in [0,1]$ is also parameterized using the inverse-logit transform, via the corresponding log-odds.
Define
\begin{align}
	d_{\mathcal{X}}(x) := -\frac{(x/\operatorname{Vol}(X))^2}{2} & \ \ \ \text{if }\mathcal{X}\text{ is an interval} \\
	d_{\mathcal{X}}(x) := \cos(\frac{2\pi x}{\operatorname{Vol}(X)}) & \ \ \ \text{if }\mathcal{X}\text{ is circular} 
\end{align}
We parameterize motor variance via its logarithm, so that the motor likelihood is (for $x \in \mathcal{X}$, not just the discretized grid):
\begin{equation}\label{eq:discretized-motor-lik}
	\Probmin_{motor}(x|\widehat{\theta}) \propto \frac{\exp(\frac{d_{\mathcal{X}}(x-\widehat{\theta})}{\exp(-\eta)})}{ \frac{Vol(X)}{N}  \sum_{i=1}^N \exp(\frac{d_{\mathcal{X}}(\theta_i-\widehat{\theta})}{\exp(-\eta)})}  
\end{equation}
where the denominator is a numerical approximation to the normalizing constant $\int  \exp(\frac{d_{\mathcal{X}}(\theta-\widehat{\theta})}{\exp(-\eta)}) d\theta$.
For circular (but not interval) spaces, the normalization constant is independent of $\widehat{\theta}$ and has a known analytical expression, which we verified is closely matched by the discrete approximation.
The motor variance $\rho^2$ is 
\begin{align}
\rho^2 = Vol(X)^2 \exp(-\eta) \text{ for an interval space} \\
\rho^2 = \frac{Vol(X)^2}{4\pi^2} \exp(-\eta) \text{ for a circular space}
\end{align}
Finally, when stimulus noise is nonzero, we analogously parameterize $\tau^2$ as $\exp(-\phi)$:
\begin{equation}
	\Probmin_{stimulus}(\theta_j|\theta_i) \propto \exp(\frac{d_{\mathcal{X}}(\theta_i-\theta_j)}{\exp(-\phi)})
\end{equation}
The resulting variance $\tau^2$ is $\operatorname{Vol}(X)^2 \exp(-\phi)$ for an interval space, and $\frac{\operatorname{Vol}(X)^2}{4\pi^2} \exp(-\phi)$ for a circular space.
When stimulus noise is zero, $\Probmin_{stimulus}(\theta_j|\theta_i) = \delta_{ij}$.

Overall, the log-likelihood (Equation~\ref{eq:trial-lik}) for a response $x\in\mathcal{X}$ given a stimulus $\theta\in\mathcal{X}$ closest to grid point $\theta_i$ ($1 \leq i \leq N$) is obtained by discretizing the two integrals in (\ref{eq:trial-lik}):
\begin{equation}\label{eq:discretized-trial-lik}
	\log \Probmin(x|\theta_i) = \log \left\{\frac{\xi}{\operatorname{Vol}(X)} + (1-\xi) \sum_{j}\sum_k \Probmin_{stimulus}(\theta_j|\theta_i)  \Probmin_{sensory}(\theta_k|\theta_j) \Probmin_{motor}(x|\widehat{\theta}_{\theta_k})\right\}
\end{equation}
where $\widehat{\theta}_{\theta_k}$ is the estimator (\ref{eq:discretized-loss}) derived from the encoding $\theta_k$.\footnote{Equivalently, in implementation, the normalizing constant $\operatorname{Vol}(X)$ can replaced by $N$ in (\ref{eq:discretized-motor-lik}, \ref{eq:discretized-trial-lik}), without changing the optimal model parameters maximizing the log-likelihood. As this changes the log-likelihood by an additive constant, it leaves the $\Delta NLL$ between two model variants on the same data unaffected.}








\subsubsection{Fitting Procedure}
\paragraph*{Mitigating Overfitting}
In order to mitigate and prevent overfitting, we added a regularization term to~(\ref{eq:trial-lik-sum}):
\begin{equation}\label{eq:regularization}
	\frac{\lambda}{N}	\sum_{i,j \text{ neighbors}} \left(\alpha(\theta_i)-\alpha(\theta_j)\right)^2
\end{equation}
	and analogously for $\beta$, where $i$ and $j$ are neighbors if $j=i+1$ or (in the circular case), $i=N$, $j=1$.
	The weight $\lambda$, shared across $\alpha$ and $\beta$, was determined for each dataset using cross-validation in preliminary experiments.

\paragraph*{Parameter Fitting}
We fitted the model separately for each loss function exponent $p$.
We compute derivatives of the log-likelihood (\ref{eq:trial-lik-sum}) w.r.t. the model parameters using automated differentiation as implemented in PyTorch \citep{DBLP:conf/nips/PaszkeGMLBCKLGA19} and apply gradient descent to optimize the model parameters to minimize the objective function consisting of the negative log-likelihood (\ref{eq:trial-lik-sum}) and the regularization terms (\ref{eq:regularization}) for $\alpha$ and $\beta$.
Parameters were initialized at plausible values while maintaining numerical stability; the fitting procedure can also be run with random initialization (Figure~\ref{fig:stable-convergence}).
On circular data, we found the optimization to be robust to different learning rate scheduling schemes; we typically used vanilla gradient descent with learning rate decayed whenever loss did not decrease over several hundred steps. 
For interval data, we found the optimization scheme to matter at the boundary, where fewer observations are available.
Here, we found that Sign-GD \citep[e.g.][]{DBLP:journals/corr/abs-2002-08056} often provided rapid convergence compared to other common schemes.
When needed to avoid slow convergence or numerically invalid results (NaN), learning rate schemes were adjusted for individual model variants and datasets.\footnote{Published code has individual scripts for each model variant, containing the corresponding learning rate scheme.}
Simplex-based (e.g., Nelder-Mead) or Quasi-Newton (e.g., BFGS) methods did not offer improved convergence compared to these simple gradient descent schemes.\footnote{However, we resorted to Nelder-Mead to avoid numerical issues in numerosity perception, which has a discrete stimulus and response set; see Section~\ref{sec:si-xiang} for details.}
While the optimization problem is in general not convex, we found the problem to be well-behaved when treated with these simple gradient descent methods (Figure~\ref{fig:stable-convergence}).


\begin{figure}
	\centering
{
\fontfamily{phv}\selectfont

\begin{tikzpicture}
%	\node (label) at (-4,3)[draw=none, align=left, anchor=center]{\large{A}};
%	\node (label) at (4.5,3)[draw=none, align=left, anchor=center]{\large{B}};
%	\node (label) at (-4,0.2)[draw=none, align=left, anchor=center]{\large{C}};

%	\node (label) at (0,2)[draw=none, align=left, anchor=center]{\includegraphics[width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_8_0_10.0_180_LossCurve.pdf}};
%\node (label) at (8,2)[draw=none, align=left, anchor=center]{\includegraphics[width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_8_0_10.0_180_sensoryKappa.pdf}};

\node (label) at (0,1)[draw=none, align=left, anchor=center]{Random Initialization};
\node (label) at (7,1)[draw=none, align=left, anchor=center]{Resulting Fit};


\node (label) at (-4,-0)[draw=none, align=left, anchor=center]{Run 1};
\node (label) at (-4,-1*1.5)[draw=none, align=left, anchor=center]{Run 2};
\node (label) at (-4,-2*1.5)[draw=none, align=left, anchor=center]{Run 3};
\node (label) at (-4,-3*1.5)[draw=none, align=left, anchor=center]{Run 4};
\node (label) at (-4,-4*1.5)[draw=none, align=left, anchor=center]{Run 5};
\node (label) at (-4,-5*1.5)[draw=none, align=left, anchor=center]{Run 6};
\node (label) at (-4,-6*1.5)[draw=none, align=left, anchor=center]{Run 7};
\node (label) at (-4,-7*1.5)[draw=none, align=left, anchor=center]{Run 8};

\node (label) at (0,0)[draw=none, align=left, anchor=center]{\includegraphics[page=1,width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_ALL_0_10.0_180.pdf}};
\node (label) at (7,0)[draw=none, align=left, anchor=center]{\includegraphics[page=2,width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_ALL_0_10.0_180.pdf}};
\node (label) at (0,-1.5)[draw=none, align=left, anchor=center]{\includegraphics[page=3,width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_ALL_0_10.0_180.pdf}};
\node (label) at (7,-1.5)[draw=none, align=left, anchor=center]{\includegraphics[page=4,width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_ALL_0_10.0_180.pdf}};
\node (label) at (0,-3)[draw=none, align=left, anchor=center]{\includegraphics[page=5,width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_ALL_0_10.0_180.pdf}};
\node (label) at (7,-3)[draw=none, align=left, anchor=center]{\includegraphics[page=6,width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_ALL_0_10.0_180.pdf}};
\node (label) at (0,-4.5)[draw=none, align=left, anchor=center]{\includegraphics[page=7,width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_ALL_0_10.0_180.pdf}};
\node (label) at (7,-4.5)[draw=none, align=left, anchor=center]{\includegraphics[page=8,width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_ALL_0_10.0_180.pdf}};
\node (label) at (0,-6)[draw=none, align=left, anchor=center]{\includegraphics[page=9,width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_ALL_0_10.0_180.pdf}};
\node (label) at (7,-6)[draw=none, align=left, anchor=center]{\includegraphics[page=10,width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_ALL_0_10.0_180.pdf}};
\node (label) at (0,-7.5)[draw=none, align=left, anchor=center]{\includegraphics[page=11,width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_ALL_0_10.0_180.pdf}};
\node (label) at (7,-7.5)[draw=none, align=left, anchor=center]{\includegraphics[page=12,width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_ALL_0_10.0_180.pdf}};
\node (label) at (0,-9)[draw=none, align=left, anchor=center]{\includegraphics[page=13,width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_ALL_0_10.0_180.pdf}};
\node (label) at (7,-9)[draw=none, align=left, anchor=center]{\includegraphics[page=14,width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_ALL_0_10.0_180.pdf}};
\node (label) at (0,-10.5)[draw=none, align=left, anchor=center]{\includegraphics[page=15,width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_ALL_0_10.0_180.pdf}};
\node (label) at (7,-10.5)[draw=none, align=left, anchor=center]{\includegraphics[page=16,width=0.4\textwidth]{figures/RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py_ALL_0_10.0_180.pdf}};
\end{tikzpicture}
}

\ifshowcode
	\begin{verbatim}
for i in `seq 1 8`; do python3 RunGardelle_FreePrior_CosineLoss_InitControl3.py 8 0 10.0 180 ; done

python3 RunGardelle_FreePrior_CosineLoss_InitControl3_VIZ.py 8 0 10.0 180
	\end{verbatim}
\fi

	\caption{
		Convergence of estimates across initializations, on the dataset collected by \citet{deGardelle2010AnOI}: 
		We randomly initialized parameters by (1) initializing $\alpha, \beta$ as sinusoidal functions,
%\begin{equation}
%	\eta_1 \sin(\frac{2\pi}{L} \theta) + \eta_2 \cos(\frac{2\pi}{L} \theta)
%\end{equation}
%where $L = 180$ is the size of the stimulus space, $\eta_1, \theta_2 \in [-.5,.5]$ uniformly at random.
%
%
	(2) uniformly sampling each sensory SD logit from a large interval covering the fitted values, %independently for each condition when there were multiple. % TODO problem with some extra implementations where 2\pi or so
%This is the range of values fitted -- initializing at small or large values leads to numerical problems
%
	(3) initializing the logit of $\xi$ in $[-1,1]$, (4) log motor variance  in $[-1,0]$.
%
%Across initializations, we show:		(A) Heldout NLL by the number of steps taken by the fitting algorithm; (B) Sensory noise $\kappa$ over the course of fitting for the five duration conditions; (C)
	We show eight randomly selected runs, each in one row, with the initialization (left column) and the fitted model (right column); colors are as in the main text.
The fitting method converges to a unique optimum, across initializations.
}\label{fig:stable-convergence}
\end{figure}






\paragraph*{Gradient Computation for $L^p$ Estimator ($p>0$)}
The only component of the generative model (\ref{eq:model1}--\ref{eq:trial-lik-sum} and \ref{eq:discretized-trial-lik}) that is not straightforwardly differentiable using standard automated differentiation methods is the estimator $\widehat{\theta}$ (\ref{eq:model2}, \ref{eq:discretized-loss}), which is a function of the posterior $P$ (and thus of the model parameters $\Theta$).
Differentiating through Newton's method is inefficient and prone to accumulation of numerical errors; we thus chose a different route.
Writing the loss in (\ref{eq:discretized-loss}) as $L(\theta, P_1, \dots, P_N)$ where $P_i = \Probmin_{posterior}(\theta_i|\theta)$, and $M := \partial_\theta L$, (where $N$ is the number of discretization points), the estimator's gradient w.r.t. the posterior is, using the implicit function theorem \citep{Rudin1964PrinciplesOM}:
\begin{equation}
	\frac{\partial \widehat{\theta}}{\partial P_i}  = - \left(\frac{\partial M}{\partial \theta}\right)^{-1} \cdot \frac{\partial M}{\partial P_i}
\end{equation}
evaluated at $\theta = \widehat{\theta}$.
For the ordinary distance $|x-y|^p$ ($p>0$), we thus have
\footnote{$M=	\partial_\theta L = p\cdot \sum_{i=1}^N P_i \operatorname{sign}(\theta-x_i) |\theta - x_i|^{p-1}$; hence, $	\frac{\partial M}{\partial \theta}= p \cdot (p-1) \cdot	\sum_{i=1}^N P_i |\theta - x_i|^{p-2} $ and $\frac{\partial M}{\partial P_i} = p\cdot \operatorname{sign}(\theta-x_i) |\theta - x_i|^{p-1} $.
}
\begin{align}\label{eq:lp-gradient}
	\frac{\partial \widehat{\theta}}{\partial P_i}  = & - \frac{ \operatorname{sign}(\theta-x_i) |\theta - x_i|^{p-1}}{ (p-1) \cdot        \sum_{i=1}^N P_i |\theta - x_i|^{p-2}}
\end{align}
This is fully applicable when $p>1$; for $0<p<1$, it is applicable whenever $\theta$ does not equal any of the grid points.
For the cosine loss $\ell(x,y) =     (1-\cos(x-y))^{p/2}$, the result is\footnote{Note
\begin{equation}
       \frac{d}{dx} \ell(x,y) = \frac{p}{2} (1-\cos(x-y))^{\frac{p}{2}-1} \sin(x-y)
\end{equation}
and
\begin{equation}
        \frac{d^2}{dx^2} \ell(x,y) = \frac{p}{2} \frac{p-2}{2} (1-\cos(x-y))^{\frac{p}{2}-2} \sin(x-y)^2 + \frac{p}{2} (1-\cos(x-y))^{\frac{p}{2}-1} \cos(x-y)
\end{equation}
}
\begin{equation}
	\frac{\partial \widehat{\theta}}{\partial P_i} = -\frac{ (1-\cos(\theta-x_i))^{\frac{p}{2}-1} \sin(\theta-x_i)}{ \sum_{j=1}^N  P_j \left[   \frac{p-2}{2} (1-\cos(\theta-x_j))^{\frac{p}{2}-2} \sin(\theta-x_j)^2 +  (1-\cos(\theta-x_j))^{\frac{p}{2}-1} \cos(\theta-x_j)\right]}
\end{equation}
which is applicable for $p=2,4,5,6,\dots$.\footnote{
At $p=3$, the denominator is unbounded when $\theta=x_j$, and the estimator is not differentiable.
The denominator is finite when $p \geq 4$.
	At $p=2$, the quantity simplifies to
\begin{equation}
	\frac{\partial \widehat{\theta}}{\partial P_i} = -\frac{ \sin(\theta-x_i)}{  \sum_{j=1}^N  \cos(\theta-x_j)}.
\end{equation}
}

\paragraph*{Gradient Computation for MAP Estimator}
On a discrete grid, the MAP estimator is always a grid point and thus piecewise constant (hence, its gradient is zero or undefined) as a functional of the posterior.
In order to obtain meaningful gradients for the MAP estimator, it is thus necessary to consider the continuous limit.
In general, for an arbitrary smooth posterior $P$, the functional derivative of the MAP estimator is the tempered distribution\footnote{Whereas  (\ref{eq:lp-gradient}) was derived by applying implicit differentation to the loss function, (\ref{eq:functional-derivative}) can be derived by applying implicit differentiation to $P$, starting from the equation $P'(\theta_{MAP}) = 0$: Let $\theta_P := \operatorname{argmax} P$; then, at $\epsilon=0$ and for any test function $\phi$, we have $0 = \int \frac{\delta P'(\theta_P)}{\delta P} \phi dx = \frac{d}{d\epsilon} \left[(P+\epsilon\phi)'(\theta_{P+\epsilon\phi})\right] = \frac{d}{d\epsilon} [ P'(\theta_{P+\epsilon\phi})+\epsilon\phi'(\theta_{P+\epsilon\phi})] =  P''(\theta) \frac{d}{d\epsilon} \theta_{P+\epsilon\phi}+\phi'(\theta_{P}) =  P''(\theta_P) \int \frac{\delta \theta_{P}}{\delta P} \phi dx +\phi'(\theta_{P}) = \int \left(P''(\theta_P)  \frac{\delta \theta_{P}}{\delta P}  -\delta'(x-\theta_{P})\right) \phi dx.$ Setting the last term equal to zero for any test function $\phi$ yields the result.
The resulting functional derivative is a tempered distribution, not an ordinary function; we note that, in contrast, for $p>1$, the functional derivative is an ordinary function, analoguous to (\ref{eq:lp-gradient}).
}
\begin{equation}\label{eq:functional-derivative}
	\frac{\delta \widehat{\theta}_{MAP}}{\delta P} =\frac{\delta'(x-\widehat{\theta})}{P''(\widehat{\theta})}
\end{equation}
where $\frac{\delta}{\delta P}$ on the left-hand-side indicates the functional derivative, and $\delta'$ on the right-hand side is the distributional derivative of the Dirac distribution.
In order to evaluate this on the discretized grid, we interpolate $P_1,\dots, P_N$ using a kernel:
$f(P,\theta) := \sum_{i=1}^N g_i(\theta) P_i$
where $g_i \geq 0$, $\sum_i g_i(x) = 1$.
Then by (\ref{eq:functional-derivative})\footnote{On the finite-dimensional function space parameterized by $P_1, \dots, P_N$, the functional $\delta'(x-\theta)$ is given by the linear form $[-g_1'(\theta), \dots, -g_N(\theta]$.} or by the implicit function theorem applied to $\deriv{\theta} f(P,\theta) = 0$, the gradient $\frac{\partial \widehat{\theta}_{MAP}}{\partial P_i}$ is
\begin{equation}\label{eq:functional-derivative-findim}
	\frac{d \widehat{\theta}_{MAP}}{dP_i} = -\frac{g_i'(\widehat{\theta})}{\sum_{i=1}^N g_i''(\widehat{\theta}) P_i}
\end{equation}
We chose the RBF kernel\footnote{
	Defined by
\begin{align*}
	g_i(x) = \frac{\exp(-\frac{(x-x_i)^2}{2\sigma^2})}{\sum_{j=1}^N \exp(-\frac{(x-x_j)^2}{2\sigma^2})}
\end{align*}
with bandwidth $\sigma$ chosen to provide close fit to the discrete posterior. 
}
and further added a smooth approximation of the function $\log 1_{\theta_{Min} \leq x \leq \theta_{Max}}$ to smoothly restrict $f$ to the stimulus space in those datasets where $\mathcal{X}$ has a boundary\footnote{We chose $-\frac{1}{10} \exp(-(x-\theta_{Min})) - \frac{1}{10} \exp(x-\theta_{Max})$. This can be viewed as another kernel function $g_i$, and thus its contribution to the gradients can be absorbed into (\ref{eq:functional-derivative-findim}). While it would be possible to simply constrain $\widehat{\theta}$ to be in the stimulus space, such an approach would lead to incorrect gradients $\deriv{P}\widehat{\theta}$ at the boundary.}.
We use this smoothed version $f(\theta,P)$ both for computing the MAP estimator (using Newton's method) and its gradients~(\ref{eq:functional-derivative-findim}).

\subsubsection{Parameterization with Subject-Specific Adjustments}\label{sec:mixed-effects}




As described in the main text, the motion direction dataset \citep{gekas2013complexityas} has different experimental stimulus distributions for each subject, which may lead to subject-specific adaptation of encoding and prior.
We thus analyzed that dataset using subject-specific adjustments to encoding and prior.
Here, we describe the appropriate version of our fitting procedure.
In standard parametric mixed-effects models extending linear regression \citep{Bates2014FittingLM}, intercepts and slopes receive per-subject adjustments that are regularized towards zero using a Gaussian shrinkage prior.
In our setting, adjustments to encoding and prior will be functions rather than individual numbers:
As described in Section~\ref{sec:fitting}, our model implementation parameterizes $\sqrt{\J} := F'$ and $\Prior$ via the logit transformation. To account for subject-specific effects, we added these to the logit parameters:
\begin{align}\label{eq:mixed-effects-resources}
	F'(\theta|Subject_i) \propto \exp(\alpha(\theta) + \alpha^{(i)}(\theta)) \\
\label{eq:mixed-effects-prior}
	\Prior(\theta|Subject_i) \propto \exp(\beta(\theta) + \beta^{(i)}(\theta)) 
\end{align}
Indeed, the literature on mixed-effects models contains models where the by-group adjustments are nonparametrically fitted functions \citep{Karcher2001GeneralizedNM}.
The simplest adaptation of this approach to our setting is to assume an independent Gaussian prior for the adjustment to encoding or prior at each discretization point $\theta_i$.
However, this approach does not encourage adjustments to be smooth, leading to substantial overfitting and difficult-to-interpret discontinuous fits.
We instead considered a prior that encourages smoothness: % Conceptually, we aim to consider
\begin{equation}\label{eq:smoothness-prior-conceptual}
	\Probcap(\alpha^{(i)}|\mu=0; \sigma^2) = \frac{1}{Z_{\sigma}} \exp(-\frac{\|\deriv{\theta} \alpha^{(i)}\|_2^2}{2\sigma^2})
\end{equation}
where $\frac{1}{Z_{\sigma}}$ is the normalization constant.
A penalty on the norm of $\alpha^{(i)}$ itself is not necessary, as adding any constant to $\alpha^{(i)}$ does not alter the resulting resource allocation (or prior).
For interval data, a discretized version of (\ref{eq:smoothness-prior-conceptual}) can be achieved by placing a Gaussian prior on the differences between values at successive points (analogously to the smootheness-based regularization term~\ref{eq:regularization}).
However, for circular data, a naive application does not lead to a correctly-normalized probability distribution.
To solve this, we parameterized the adjustment using its discrete Fourier transform\footnote{The zeroth-order coefficient is not relevant, as adding any constant does not alter the resulting resource allocation (or prior).}, $\alpha^{(i)}(\theta) = \sum_{k=1}^{K} \left(w_k \sin(k\theta) + v_k \cos(k\theta)\right)$; and utilized the fact that $\|\deriv{\theta} \alpha^{(i)}\|_2^2 \propto   \sum_{k=1}^K k^2\left( |w_k|^2 + |v_k|^2\right)$ to compute (\ref{eq:smoothness-prior-conceptual}) including the correct normalization:
\begin{equation}
	\Probcap(\alpha^{(i)}|\mu=0; \sigma^2) = \frac{\pi^K K!}{(2\sigma^2)^{K}} \exp(-\frac{ \sum_{k=1}^K k^2\left( |w_k|^2 + |v_k|^2\right)}{2\sigma^2})
\end{equation}
or equivalently
\begin{equation}
	\log	\Probcap(\alpha^{(i)}|\mu=0; \sigma^2) = \log (\pi^K K!) - K \log (2\sigma^2) -\frac{ \sum_{k=1}^K k^2\left( |w_k|^2 + |v_k|^2\right)}{2\sigma^2}
\end{equation}
as the precision matrix has determinant $\frac{\pi^{2K} \left(K!\right)^2}{\sigma^{4K}}$.
We chose $K=50$, providing ample space for fine-grained fits at the level allowed by the discretization. 
%
The likelihood then is
\begin{equation}\label{eq:subject-likelihood}
	\int_{\mathbb{R}^{2K}}\int_{\mathbb{R}^{2K}} \dots \int_{\mathbb{R}^{2K}}\int_{\mathbb{R}^{2K}}	\exp(\ref{eq:trial-lik-sum})  	\prod_{i=1}^{\# Subjects}  \Probcap(\alpha^{(i)}|\mu=0, \sigma_\alpha^2) 	\prod_{i=1}^{\# Subjects} \Probcap(\beta^{(i)}|\mu=0, \sigma_\beta^2) d\alpha^{(1)} d\beta^{(1)} \dots  d\alpha^{(\# Subjects)} d\beta^{(\# Subjects)} 
\end{equation}
As this integral is intractable, we fitted parameters using ELBO variational inference \citep{Blei2016VariationalIA,DBLP:journals/corr/KingmaW13} and report negative log-likelihood of the fitted model at $\beta^{(i)}, \alpha^{(i)} \equiv 0$, i.e., evaluating only the fixed-effects structure.


\subsection{Computing Resources and Variability}\label{sec:computing-resources}

%The reported variability is $\sqrt{\operatorname{SD}(\widehat{\theta})^2 + \rho^2}$, where again $\operatorname{SD}(\widehat{\theta})$ is computed using circular statistics when the stimulus space is circular\footnote{The circular SD is given as $\sqrt{-2\ln(\overline{R})}$, where $\overline{R}$ is the Eucliden distance between the origin and the vector mean of the angles.}.
%For the human data, average responses and response SD are smoothed using kernel density estimates.
We report biases using the mean of the estimate $\widehat{\theta}$ across the encodings $m$ on the discretized grid, taking the ordinary mean for interval stimuli and the circular mean for circular stimuli.
As discussed in Materials and Methods, guessing ($\xi>0$) reduces the bias by a factor of $1-\xi$. Nonnegligible guessing rates are fitted on the dataset collected by \citet{gekas2013complexityas} (in line with observations made by the original authors); we account for this factor in plotting the bias.

We compute model predictions for variability as
\begin{equation}
	\sqrt{\operatorname{SD}(\widehat{\theta})^2 + \rho^2}
\end{equation}
where $\rho^2$ is the motor variance and where again $\operatorname{SD}(\widehat{\theta})$, the standard deviation of the estimate $\widehat{\theta}$ conditional on the stimulus $\theta$, is computed using circular statistics when the stimulus space is circular\footnote{The circular SD is given as $\sqrt{-2\ln(\overline{R})}$, where $\overline{R}$ is the Eucliden distance between the origin and the vector mean of the angles.}.

We compute the resource allocation $\sqrt{\FI}$ as follows.
%We write $\operatorname{Vol}(\mathcal{X})$  for the size of $\mathcal{X}$; for an interval, it is $max(\mathcal{X}) - min(\mathcal{X})$; for circular stimuli it is $\operatorname{Vol}(\mathcal{X}) = 180$ (orientation) or $\operatorname{Vol}(\mathcal{X}) = 360$ (direction, color).
At $i=1,...,N-1$, write
\begin{equation}
	V_i = F(\theta_{i+1}) - F(\theta_i)
\end{equation}
so
\begin{equation}
	F'(\theta_i) \approx \frac{F(\theta_{i+1}) - F(\theta_i)}{\theta_{i+1} - \theta_i} = \frac{V_i N}{\operatorname{Vol}(\mathcal{X})}
\end{equation}
As $F$ is represented in the implementation as the cumulative sum (\ref{eq:cum-sum}) of the discretization of $\sqrt{\J} := F'$, $F$ is internally represented in terms of $V_i$.
Based on this, for an interval variable, we represent $\sqrt{\FI(\theta_i)} = \frac{F'(\theta_i)}{\operatorname{SD}(m|\theta_i)}$ as
%\footnote{In the more general case where $\mathcal{Y}$ has length different from $1$ and the Gaussian may be implemented with an extra factor for numerical stability, we have 
%\begin{equation}
%	\sqrt{\FI(\theta_i)} = \frac{F'(\theta_i)}{\operatorname{SD}(m|\theta_i)} =  \frac{V_i}{\sigma} \frac{N}{\operatorname{Vol}(\mathcal{X})} \frac{1}{\alpha}
%\end{equation}
%where
%\begin{equation}
%	\Probmin(m|\theta_i) \propto \exp(-\frac{1}{2\sigma^2}  \left|\frac{m-F(\theta_i)}{\alpha}\right|^2)
%\end{equation}
%and thus
%\begin{equation}
%	Var(m|\theta_i) \approx \sigma^2 \alpha^2
%\end{equation}
%}
\begin{equation}
 \frac{V_iN}{\sigma\operatorname{Vol}(\mathcal{X})}
\end{equation}
For a circular variable, where the encoding follows a von Mises distribution with parameter $\kappa$, $\sqrt{\FI(\theta_i)}$ can be represented analogously as
%\footnote{In the more general case where $\mathcal{Y}$ may be indexed differently -- e.g., as $[0,\pi]$ instead of $[0,2\pi]$ -- this becones
%\begin{equation}\label{eq:fisher-circular-computed-generalized}
%	\sqrt{\FI(\theta_i)} = \frac{F'(\theta_i)}{\operatorname{SD}(m|\theta_i)} = V_i \sqrt{\kappa} \frac{ N}{\operatorname{Vol}(\mathcal{X})} \frac{2\pi}{\operatorname{Vol}(\mathcal{Y})}
%\end{equation}
%because
%\begin{equation}
%	\Probmin(m|\theta_i) \propto \exp(\kappa \cdot \cos((m-F(\theta_i)) \frac{2\pi}{\operatorname{Vol}(\mathcal{Y})}))
%\end{equation}
%and hence
%\begin{equation}
%	Var(m|\theta_i) \approx \frac{1}{\kappa} \left(\frac{\operatorname{Vol}(\mathcal{Y})}{2\pi}\right)^2
%\end{equation}
%}
\begin{equation}\label{eq:fisher-circular-computed}
 \frac{ V_i \sqrt{\kappa}N}{\operatorname{Vol}(\mathcal{X})}
\end{equation}
when noise is small.
In the presence of stimulus noise with variance $\tau^2$, this is extended to:
\begin{equation}
	\sqrt{\FI(\theta_i)} \approx \sqrt{\frac{1}{\tau^2 + \frac{1}{(\text{Equation } \ref{eq:fisher-circular-computed})^2}}}
\end{equation}


