


\paragraph*{Details}
In order to avoid boundary effects, we considered the stimulus set $\{0, 1, \dots, 150\}$, corresponding to the continuous space $\mathcal{X} = [0,151]$ (or $[0,151)$; Figure~\ref{fig:spaces-volumes}); hence, we used one grid point per integer in this range.
We removed responses outside of the stimulus space, affecting 24 out of 71,408 observations (0.03\% of data).




Implementing Weber's law, we parameterized the resource allocation as
\begin{equation}
	\sqrt{\FI(\theta)} \propto \frac{1}{\theta+\varphi}
\end{equation}
where $\varphi>0$ is needed to avoid numerical problems close to lower end to the stimulus space (as in \citet{Petzschner2011IterativeBE}).
We set $\varphi=1$ for simplicity, a small fraction of the stimulus space.
The transfer map has the simple form (as in \citet{Petzschner2011IterativeBE}):
\begin{equation}
	F(\theta) = \log(\theta+\varphi)
\end{equation}
The normal-in-sensory space priors are correspondingly parameterized as a log-normal distribution:
\begin{equation}
	p(\theta) \propto \frac{1}{\theta+\varphi} \exp(-\frac{(\log(\theta+\varphi)-\mu)^2}{2\sigma^2})
\end{equation}
These normal-in-sensory-space priors varied only in their means $\mu$ but not their variances $\sigma$, following \citet{Petzschner2011IterativeBE}.
On the other hand, the normal-in-stimulus-space priors were allowed to vary in both parameters, following \citet{cicchini2014compressive}.

\paragraph*{Model Fitting}
The discrete nature of the stimulus space brings a set of challenges for gradient-based model fitting, not encountered in the other datasets.
First, we had to constrain the motor variability to exceed a minimum value chosen so that that per-trial likelihood was never numerically equal to one, preventing a situation where the gradients of the likelihood w.r.t. $\widehat{\theta}$ become numerically zero.
Second, for high exponents ($p=8,10$), gradient-based fitting was numerically unstable, and we instead used Nelder-Mead (implemented in \texttt{scipy.optim}) initialized at the corresponding fit at the next lower exponent ($p-2$). %, as the gradient-based method was numerically unstable.
As the model with the flat prior has very few parameters, we used Nelder-Mead throughout (initialized based on a fit from the gradient-based method).
We verified that results agreed with our gradient-based method, which is always initialized from scratch, on exponents $<8$ on one fold.
While Nelder-Mead avoided numerical problems affecting the gradient-based method, it was substantially slower to converge than the gradient-based method (in particular when not initialized at a point found with the gradient-based method), in particular for the normal (in sensory space of stimulus space) priors, which have $\approx 30$ or $\approx 50$ parameters.


\paragraph*{Results}

Model fit statistics are shown in Figure~\ref{fig:nll-Xiang}.
Fitted priors for the 21 intervals are shown in Figure~\ref{fig:priors-Xiang-0}--\ref{fig:priors-Xiang} (at $p=0,2$).
Fits for unimodal priors are qualitatively quite similar, in particular when visualized in sensory space (Figure~\ref{fig:priors-Xiang}).
Unlike suggested by \citet{cicchini2014compressive}, the normal-in-stimulus-space prior has its mean parameter at small numbers, below the relevant range.




\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont


	\includegraphics[width=.4\textwidth]{{figures/evaluateCrossValidationResults_Xiang_AVE.py}_RelativeLF.pdf}

		\definecolor{pltPurple}{rgb}{0.5019607843137255, 0.0, 0.5019607843137255}
	\definecolor{pltRed}{rgb}{1.0, 0.0, 0.0}
	\definecolor{pltGreen}{rgb}{0.0, 0.5019607843137255, 0.0}
	\definecolor{pltOrange}{rgb}{1.0, 0.6470588235294118, 0.0}
	\definecolor{pltBlue}{rgb}{0.0, 0.0, 1.0}
	\definecolor{pltGray}{rgb}{0.5, 0.5, 0.5}



	{\huge	\textcolor{pltOrange}{---}} Flat prior (Figure~\ref{fig:xiang-flat})

	{\huge	\textcolor{pltPurple}{---}} Normal in sensory space (Figure~\ref{fig:xiang-lognormal})

	{\huge	\textcolor{pltGray}{---}} Normal in stimulus space (Figure~\ref{fig:xiang-normal})


	\end{center}

\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Reproduced with:
python3 evaluateCrossValidationResults_Xiang_AVE.py
\end{Verbatim}
\fi



	\caption{Magnitude estimation (analyzing data collected by \citet{Xiang2021ConfidenceAC}).
	Heldout negative Log-Likelihood as a function of the loss function exponent (lower is better), compared to the model reported in Main Text, Figure 4a.
Data are presented as mean values with standard errors across $n=10$ folds. Note that error bars are imperceptible in some model variants.
	The model with a normal (in stimulus space or sensory space) prior consistently improves over models with a flat prior exactly reflecting the experimental stimulus distribution.
	}\label{fig:nll-Xiang}
\end{figure}


\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont
	\begin{tikzpicture}
		\node (stimulus) at (1,0)[draw=none, anchor=north west]{\includegraphics[width=.47\textwidth]{{figures/RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ.py_0_0_10.0_151}_StimulusSpace.pdf}};
		\node (label) at (1,0)[draw=none, anchor=north west]{\includegraphics[width=.47\textwidth]{{figures/RunXiang_ZeroExp_Normal_VarSig_SignGD_AVE_VIZ.py_0_0_10.0_151}_StimulusSpace.pdf}};

		\node (sensory) at (9,0)[draw=none, anchor=north west]{\includegraphics[width=.47\textwidth]{{figures/RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ.py_0_0_10.0_151}_SensorySpace.pdf}};
		\node (label) at (9,0)[draw=none, anchor=north west]{\includegraphics[width=.47\textwidth]{{figures/RunXiang_ZeroExp_Normal_VarSig_SignGD_AVE_VIZ.py_0_0_10.0_151}_SensorySpace.pdf}};

		\node at (stimulus.north)[anchor=south]{In Stimulus Space $\mathcal{X}$};
		\node at (sensory.north)[anchor=south]{In Sensory Space $\mathcal{Y}$};


\end{tikzpicture}

        \definecolor{pltPurple}{rgb}{0.5019607843137255, 0.0, 0.5019607843137255}
        \definecolor{pltRed}{rgb}{1.0, 0.0, 0.0}
        \definecolor{pltGreen}{rgb}{0.0, 0.5019607843137255, 0.0}
        \definecolor{pltOrange}{rgb}{1.0, 0.6470588235294118, 0.0}
        \definecolor{pltBlue}{rgb}{0.0, 0.0, 1.0}



	{\textcolor{pltBlue}{\huge{---} }} Normal in stimulus space


	{\textcolor{pltOrange}{\huge{---} }} Normal in sensory space

	\end{center}

\ifshowcode
 \begin{Verbatim}[fontsize=\tiny]
 Reproduced with:
 python3 RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ.py 2 2 10.0 151
 python3 RunXiang_ZeroExp_Normal_VarSig_SignGD_AVE_VIZ.py 2 2 10.0 151
 python3 RunXiang_ZeroExp_SignGD_AVE4_VIZ.py 2 2 10.0 151
 \end{Verbatim}
\fi

	\caption{Fitted priors in numerosity estimation dataset of \citet{Xiang2021ConfidenceAC}, at $p=0$, for each of the 21 intervals used in the experiment, for two unimodal parametric priors.
	In sensory space, priors peak within or slightly below each interval.
	This is compatible with combination of an overall prior favoring lower numbers being combined with interval-specific priors.
	}\label{fig:priors-Xiang-0}
\end{figure}




\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont
	\begin{tikzpicture}
		\node (stimulus) at (1,0)[draw=none, anchor=north west]{\includegraphics[width=.47\textwidth]{{figures/RunXiang_Lognormal_SignGD_AVE_VIZ.py_2_0_10.0_151}_StimulusSpace.pdf}};
		\node (label) at (1,0)[draw=none, anchor=north west]{\includegraphics[width=.47\textwidth]{{figures/RunXiang_Normal_VarSig_AVE_VIZ.py_2_0_10.0_151}_StimulusSpace.pdf}};

		\node (sensory) at (9,0)[draw=none, anchor=north west]{\includegraphics[width=.47\textwidth]{{figures/RunXiang_Lognormal_SignGD_AVE_VIZ.py_2_0_10.0_151}_SensorySpace.pdf}};
		\node (label) at (9,0)[draw=none, anchor=north west]{\includegraphics[width=.47\textwidth]{{figures/RunXiang_Normal_VarSig_AVE_VIZ.py_2_0_10.0_151}_SensorySpace.pdf}};

		\node at (stimulus.north)[anchor=south]{In Stimulus Space $\mathcal{X}$};
		\node at (sensory.north)[anchor=south]{In Sensory Space $\mathcal{Y}$};


\end{tikzpicture}

        \definecolor{pltPurple}{rgb}{0.5019607843137255, 0.0, 0.5019607843137255}
        \definecolor{pltRed}{rgb}{1.0, 0.0, 0.0}
        \definecolor{pltGreen}{rgb}{0.0, 0.5019607843137255, 0.0}
        \definecolor{pltOrange}{rgb}{1.0, 0.6470588235294118, 0.0}
        \definecolor{pltBlue}{rgb}{0.0, 0.0, 1.0}



	{\textcolor{pltBlue}{\huge{---} }} Normal in stimulus space


	{\textcolor{pltOrange}{\huge{---} }} Normal in sensory space

	\end{center}

\ifshowcode
 \begin{Verbatim}[fontsize=\tiny]
 Reproduced with:
 python3 RunXiang_Lognormal_SignGD_AVE_VIZ.py 2 2 10.0 151
 python3 RunXiang_Normal_VarSig_AVE_VIZ.py 2 2 10.0 151
 python3 RunXiang_FreePrior_SignGD_HigherP_AVE_VIZ.py 2 2 10.0 151
 \end{Verbatim}
\fi

	\caption{Fitted priors in numerosity estimation dataset of \citet{Xiang2021ConfidenceAC}, at $p=2$, for each of the 21 intervals used in the experiment, for two unimodal parametric priors.
	In sensory space, priors peak within or slightly below each interval.
	This is compatible with combination of an overall prior favoring lower numbers being combined with interval-specific priors.
	}\label{fig:priors-Xiang}
\end{figure}











\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont

		\includegraphics[width=.89\textwidth]{{figures/RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_AllPriors_OnlyHuman.py_0_0_10.0_151}.pdf}


	p=0 ($\Delta$ NLL $ = 0$)



	\includegraphics[width=.89\textwidth]{{figures/RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ.py_0_0_10.0_151}.pdf} % at p=2


	p=2 ($\Delta$ NLL $ = 1$)

	\includegraphics[page=1,width=.89\textwidth]{figures/RunXiang_Lognormal_NelderMead_AVE_VIZForPaper_ACREXP.py_ALL_0_10.0_151.pdf}

	p=4 ($\Delta$ NLL $ = 3$)

	\includegraphics[page=2,width=.89\textwidth]{figures/RunXiang_Lognormal_NelderMead_AVE_VIZForPaper_ACREXP.py_ALL_0_10.0_151.pdf}

	p=6 ($\Delta$ NLL $ = 4$)

	\includegraphics[page=3,width=.89\textwidth]{figures/RunXiang_Lognormal_NelderMead_AVE_VIZForPaper_ACREXP.py_ALL_0_10.0_151.pdf}

	p=8 ($\Delta$ NLL $ = 1$)

	\includegraphics[page=4,width=.89\textwidth]{figures/RunXiang_Lognormal_NelderMead_AVE_VIZForPaper_ACREXP.py_ALL_0_10.0_151.pdf}
	\end{center}

	\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Model fitting:
python3 RunXiang_ZeroExp_Lognormal_SignGD_AVE.py 0 2 10.0 151
python3 RunXiang_Lognormal_SignGD_AVE.py {2,4,6} 2 10.0 151

Reproduced with:
python3 RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ.py 0 2 10.0 151
python3 RunXiang_Lognormal_SignGD_AVE_VIZForPaper.py {2,4,6} 2 10.0 151
\end{Verbatim}
\fi




	\caption{Numerosity estimation \cite[data collected by][]{Xiang2021ConfidenceAC}: normal (in sensory space) prior. For the human data, we plot the mean with error bands indicating bootstrapped 95\% confidence intervals.
}\label{fig:xiang-lognormal}
\end{figure}



\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont



		\includegraphics[width=.89\textwidth]{{figures/RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_AllPriors_OnlyHuman.py_0_0_10.0_151}.pdf}



	p=0 ($\Delta$ NLL $ = 24$)

	\includegraphics[width=.89\textwidth]{{figures/RunXiang_ZeroExp_Normal_VarSig_SignGD_AVE_VIZ.py_0_0_10.0_151}.pdf}

p=2 ($\Delta$ NLL $ = 13$)


	\includegraphics[page=1,width=.89\textwidth]{{figures/RunXiang_Normal_NelderMead_AVE_VIZForPaper_ACREXP.py_ALL_0_10.0_151}.pdf}

p=4 ($\Delta$ NLL $ = -4$)

	\includegraphics[page=2,width=.89\textwidth]{{figures/RunXiang_Normal_NelderMead_AVE_VIZForPaper_ACREXP.py_ALL_0_10.0_151}.pdf}

p=6 ($\Delta$ NLL $ = 12$)

	\includegraphics[page=3,width=.89\textwidth]{{figures/RunXiang_Normal_NelderMead_AVE_VIZForPaper_ACREXP.py_ALL_0_10.0_151}.pdf}

p=8 ($\Delta$ NLL $ = -12$)

	\includegraphics[page=4,width=.89\textwidth]{{figures/RunXiang_Normal_NelderMead_AVE_VIZForPaper_ACREXP.py_ALL_0_10.0_151}.pdf}

	\end{center}

	\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Model fitting:
python3 RunXiang_ZeroExp_Normal_VarSig_SignGD_AVE.py 0 2 10.0 151
python3 RunXiang_Normal_VarSig_AVE.py {2,4,6,8} 10.0 151


Reproduced with:
python3 RunXiang_ZeroExp_Normal_VarSig_SignGD_AVE_VIZ.py 0 2 10.0 151
python3 RunXiang_Normal_VarSig_AVE_VIZForPaper.py {2,4,6,8} 10.0 151
\end{Verbatim}
\fi

	\caption{Numerosity estimation \cite[data collected by][]{Xiang2021ConfidenceAC}: normal (in stimulus space) prior.
Unlike suggested by \citet{cicchini2014compressive}, fitting numerosity estimation data with a normal-in-stimulus-space prior requires a mean parameter at small numbers, below the relevant range.
For the human data, we plot the mean with error bands indicating bootstrapped 95\% confidence intervals.
}\label{fig:xiang-normal}
\end{figure}



\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont


		\includegraphics[width=.89\textwidth]{{figures/RunXiang_ZeroExp_Lognormal_SignGD_AVE_VIZ_AllPriors_OnlyHuman.py_0_0_10.0_151}.pdf}



	p=0 ($\Delta$ NLL $ = 744$)


	\includegraphics[width=.89\textwidth]{{figures/RunXiang_ZeroExp_Flat_SignGD_AVE_VIZ.py_0_0_10.0_151}.pdf}


	p=2 ($\Delta$ NLL $ = 811$)

	\includegraphics[page=1,width=.89\textwidth]{{figures/RunXiang_Flat_NelderMead_AVE_VIZForPaper_ACREXP.py_ALL_0_10.0_151}.pdf}

	p=4 ($\Delta$ NLL $ = 881$)

	\includegraphics[page=2,width=.89\textwidth]{{figures/RunXiang_Flat_NelderMead_AVE_VIZForPaper_ACREXP.py_ALL_0_10.0_151}.pdf}

	p=6 ($\Delta$ NLL $ = 957$)

	\includegraphics[page=3,width=.89\textwidth]{{figures/RunXiang_Flat_NelderMead_AVE_VIZForPaper_ACREXP.py_ALL_0_10.0_151}.pdf}

	p=8 ($\Delta$ NLL $ = 1017$)

	\includegraphics[page=4,width=.89\textwidth]{{figures/RunXiang_Flat_NelderMead_AVE_VIZForPaper_ACREXP.py_ALL_0_10.0_151}.pdf}

	\end{center}

	\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Model fitting:
python3 RunXiang_ZeroExp_Flat_SignGD_AVE.py 0 2 10.0 151
python3 RunXiang_Flat_SignGD_AVE.py {2,4,6} 2 10.0 151
python3 RunXiang_Flat_NelderMead_AVE.py {2,4,6,8} 2 10.0 151

Reproduced with:
python3 RunXiang_ZeroExp_Flat_SignGD_AVE_VIZ.py 0 2 10.0 151
python3 RunXiang_Flat_SignGD_AVE_VIZForPaper.py {2,4,6} 2 10.0 151
python3 RunXiang_Flat_NelderMead_AVE_VIZForPaper.py {2,4,6,8} 2 10.0 151
\end{Verbatim}
\fi

	\caption{Numerosity estimation \cite[data collected by][]{Xiang2021ConfidenceAC}: flat prior.
For the human data, we plot the mean with error bands indicating bootstrapped 95\% confidence intervals.
}\label{fig:xiang-flat}
\end{figure}



