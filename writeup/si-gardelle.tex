Figure~\ref{fig:nll-Gardelle} shows model fit statistics, with references to further results.
As described in the main text (Materials and Methods), we follow \citet{Wei2015ABO} in excluding data associated with the shortest exposure duration (20 ms) for visualization purposes as responses are too variable to be reliably estimated with Kernel smoothing; however, these data are always included in model fitting, and the model identifies the largest sensory noise magnitude for these.

\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont


\includegraphics[width=.9\textwidth]{{figures/evaluateCrossValidationResults_Gardelle_180.py}_RelativeLF.pdf}



\definecolor{pltPurple}{rgb}{0.5019607843137255, 0.0, 0.5019607843137255}
\definecolor{pltRed}{rgb}{1.0, 0.0, 0.0}
\definecolor{pltGreen}{rgb}{0.0, 0.5019607843137255, 0.0}
\definecolor{pltOrange}{rgb}{1.0, 0.6470588235294118, 0.0}
\definecolor{pltBlue}{rgb}{0.0, 0.0, 1.0}


{\huge\textcolor{pltGreen}{---}} Freely fitted prior (Figure~\ref{fig:gardelle-free} and \ref{fig:gardelle-free-centered})

{\huge\textcolor{pltPurple}{---}} Naturalistic prior (Figure~\ref{fig:gardelle-naturalistic})

{\huge\textcolor{pltRed}{---}} Uniform prior (Figure~\ref{fig:gardelle-uniprior})

{\huge\textcolor{pltBlue}{---}} Uniform encoding (Figure~\ref{fig:gardelle-uniencoding})
	\end{center}


	\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Reproduced with:
python3 evaluateCrossValidationResults_Gardelle_180.py
\end{Verbatim}
\fi



\caption{Orientation perception \citep[data collected by][]{deGardelle2010AnOI}: Crossvalidated negative Log-Likelihood compared to best-fitting model as a function of the loss function exponent (lower is better).
Dotted line indicates the best achieved fit. 
Left: Ordinary $L^p$ distance centered at $F^{-1}(m)$. 
Right: cosine-based distance.
Data are presented as mean values with standard errors across $n=10$ folds. Note that error bars are imperceptible in some model variants.
The cosine-based implementation of the loss function achieves lower Negative Log-Likelihood, in particular, a model with uniform (red) or freely fitted (green) prior. Note that $p=2$ with freely fitted prior or uniform encoding can achieve relatively good Negative Log-Likelihood (though not optimal) in the left panel, but at the price of fitting an implausible prior peaking at oblique directions, at odds with both natural scene statistics or experiment-internal stimulus statistics (cf. Figure~\ref{fig:gardelle-free-centered}, $p=2$). \emph{Note: At $p=0$, a uniform prior outperforms the freely fitted prior, suggesting slight overfitting in the latter. We did not observe such a phenomenon at other exponents.}}\label{fig:nll-Gardelle}
\end{figure}


\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont

p=0 ($\Delta$ NLL $ = 161$)

\includegraphics[width=.7\textwidth]{figures/RunGardelle_FreePrior_Zero_VIZ.py_0_0_10.0_180.pdf} 

p=2 ($\Delta$ NLL $ = 35$)

\includegraphics[page=1,width=.7\textwidth]{{figures/RunGardelle_FreePrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180}.pdf} 

p=4 ($\Delta$ NLL $ = 7$)

\includegraphics[page=2,width=.7\textwidth]{{figures/RunGardelle_FreePrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180}.pdf}  

p=6 ($\Delta$ NLL $ = 2$)

\includegraphics[page=3,width=.7\textwidth]{{figures/RunGardelle_FreePrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180}.pdf}  

p=8 ($\Delta$ NLL $ = 0$)

\includegraphics[page=4,width=.7\textwidth]{{figures/RunGardelle_FreePrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180}.pdf}  

p=10 ($\Delta$ NLL $ = 0$)

\includegraphics[page=5,width=.7\textwidth]{{figures/RunGardelle_FreePrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180}.pdf}  
	\end{center}

	\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Model fitting:
python3 RunGardelle_FreePrior_Zero.py 0 {0-10} 10.0 180
python3 RunGardelle_FreePrior_CosineLoss.py {2,4,6,8,10} {0-10} 10.0 180


Reproduced with:
python3 RunGardelle_FreePrior_Zero_VIZ.py 0 0 10.0 180
python3 RunGardelle_FreePrior_CosineLoss_VIZ.py {2,4,6,8,10} 0 10.0 180
\end{Verbatim}
\fi



\caption{
Orientation perception \citep[data collected by][]{deGardelle2010AnOI}: Freely fitted prior (see Figure~\ref{fig:gardelle-free-centered} for corresponding result with centered implementation of loss function).
For each model, we show the increase in negative log-likelihood (NLL) compared to the best-fitting model reported in the main text; higher $\Delta$ NLL  corresponds to poorer fit.
For small exponents as assumed in prior work on orientation perception \citep{Girshick2011CardinalRV, Wei2015ABO}, an implausible prior peaking in oblique directions results.
For higher exponents, such as p=8, which achieve better quantitative model fit (see Figure~\ref{fig:nll-Gardelle}), an approximately uniform prior is identified.}\label{fig:gardelle-free}
\end{figure}


\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont

p=0  ($\Delta$ NLL $ = 161$)

\includegraphics[width=.7\textwidth]{figures/RunGardelle_FreePrior_Zero_VIZ.py_0_0_10.0_180.pdf} 

p=2 ($\Delta$ NLL $ = 18$)

\includegraphics[page=1,width=.7\textwidth]{{figures/RunGardelle_FreePrior_CenteredLoss_VIZ_ACREXP.py_ALL_0_10.0_180}.pdf} 

p=4 ($\Delta$ NLL $ = 47$)

\includegraphics[page=2,width=.7\textwidth]{{figures/RunGardelle_FreePrior_CenteredLoss_VIZ_ACREXP.py_ALL_0_10.0_180}.pdf}  

p=6 ($\Delta$ NLL $ = 33$)

\includegraphics[page=3,width=.7\textwidth]{{figures/RunGardelle_FreePrior_CenteredLoss_VIZ_ACREXP.py_ALL_0_10.0_180}.pdf}  

p=8 ($\Delta$ NLL $ = 30$)

\includegraphics[page=4,width=.7\textwidth]{{figures/RunGardelle_FreePrior_CenteredLoss_VIZ_ACREXP.py_ALL_0_10.0_180}.pdf}  

p=10 ($\Delta$ NLL $ = 30$)

\includegraphics[page=5,width=.7\textwidth]{{figures/RunGardelle_FreePrior_CenteredLoss_VIZ_ACREXP.py_ALL_0_10.0_180}.pdf}  
	\end{center}

	\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Model fitting:
python3 RunGardelle_FreePrior_Zero.py 0 {0-10} 10.0 180
python3 RunGardelle_FreePrior_CenteredLoss.py {2,4,6,8,10} {0-10} 10.0 180

Reproduced with:
python3 RunGardelle_FreePrior_Zero_VIZ.py 0 0 10.0 180
python3 RunGardelle_FreePrior_CenteredLoss_VIZ.py {2,4,6,8,10} 0 10.0 180
\end{Verbatim}
\fi

\caption{
Orientation perception \citep[data collected by][]{deGardelle2010AnOI}: Freely fitted prior, with centered implementation of loss function (see Figure~\ref{fig:gardelle-free} for corresponding result with cosine-based implementation).
For small exponents as assumed in prior work on orientation perception \citep{Girshick2011CardinalRV, Wei2015ABO}, an implausible prior peaking in oblique directions results.
For higher exponents, the fitted prior peaks at cardinal directions in accordance with natural image statistics, though the best-fitting models show a uniform prior and use the cosine-based implementation (see Figure~\ref{fig:gardelle-free}).
}
\label{fig:gardelle-free-centered}
\end{figure}



\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont



p=0 ($\Delta$ NLL $ = 133$)

\includegraphics[width=.7\textwidth]{figures/RunGardelle_UniformPrior_Zero_VIZ.py_0_0_10.0_180.pdf}

p=2 ($\Delta$ NLL $ = 53$)

\includegraphics[page=1,width=.7\textwidth]{figures/RunGardelle_UniformPrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf}

p=4 ($\Delta$ NLL $ = 16$)

\includegraphics[page=2,width=.7\textwidth]{figures/RunGardelle_UniformPrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf}

p=6 ($\Delta$ NLL $ = 4$)

\includegraphics[page=3,width=.7\textwidth]{figures/RunGardelle_UniformPrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf}

p=8 ($\Delta$ NLL $ = 0$)

\includegraphics[page=4,width=.7\textwidth]{figures/RunGardelle_UniformPrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf}

p=10 ($\Delta$ NLL $ = 1$)

\includegraphics[page=5,width=.7\textwidth]{figures/RunGardelle_UniformPrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf}
	\end{center}

\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Model fitting:
python3 RunGardelle_UniformPrior_Zero.py 0 {0-10} 10.0 180
python3 RunGardelle_UniformPrior_CosineLoss.py {2,4,6,8,10} {0-10} 10.0 180

Reproduced with:
python3 RunGardelle_UniformPrior_Zero_VIZ.py 0 0 10.0 180
python3 RunGardelle_UniformPrior_CosineLoss_VIZ.py {2,4,6,8,10} 0 10.0 180
\end{Verbatim}
\fi

\caption{Orientation perception \citep[data collected by][]{deGardelle2010AnOI}: Uniform prior, cosine-based implementation of $L^p$ loss.}\label{fig:gardelle-uniprior}
\end{figure}


\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont


p=0 ($\Delta$ NLL $ = 243$)

\includegraphics[width=.7\textwidth]{figures/RunGardelle_NaturalPrior_Zero_VIZ.py_0_0_10.0_180.pdf}

p=2 ($\Delta$ NLL $ = 86$)

\includegraphics[page=1,width=.7\textwidth]{figures/RunGardelle_NaturalPrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf}

p=4 ($\Delta$ NLL $ = 47$)

\includegraphics[page=2,width=.7\textwidth]{figures/RunGardelle_NaturalPrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf}

p=6 ($\Delta$ NLL $ = 42$)

\includegraphics[page=3,width=.7\textwidth]{figures/RunGardelle_NaturalPrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf}


p=8 ($\Delta$ NLL $ = 42$)

\includegraphics[page=4,width=.7\textwidth]{figures/RunGardelle_NaturalPrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf}

p=10 ($\Delta$ NLL $ = 42$)

\includegraphics[page=5,width=.7\textwidth]{figures/RunGardelle_NaturalPrior_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf}
	\end{center}

\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Model fitting:
python3 RunGardelle_NaturalPrior_Zero.py 0 {0-10} 10.0 180
python3 RunGardelle_NaturalPrior_CosineLoss.py {2,4,6,8,10} {0-10} 10.0 180

Reproduced with:
python3 RunGardelle_NaturalPrior_Zero_VIZ.py 0 0 10.0 180
python3 RunGardelle_NaturalPrior_CosineLoss_VIZ.py {2,4,6,8,10} 0 10.0 180
\end{Verbatim}
\fi

\caption{Orientation perception \citep[data collected by][]{deGardelle2010AnOI}:  prior based on natural image statistics, cosine-based implementation of $L^p$ loss. \emph{Note: Fit at p=0 may look counterintuitive; we verified that a qualitatively equivalent fit was found even when initializing at the p=2 fit.}}\label{fig:gardelle-naturalistic}
\end{figure}

\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont



p=0 ($\Delta$ NLL $ = 180$)

\includegraphics[width=.7\textwidth]{figures/RunGardelle_UniformEncoding_Zero_VIZ.py_0_0_10.0_180.pdf}

p=2 ($\Delta$ NLL $ = 75$)

\includegraphics[page=1,width=.7\textwidth]{figures/RunGardelle_UniformEncoding_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf}

p=4 ($\Delta$ NLL $ = 86$)

\includegraphics[page=2,width=.7\textwidth]{figures/RunGardelle_UniformEncoding_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf}

p=6 ($\Delta$ NLL $ = 150$)



\includegraphics[page=3,width=.7\textwidth]{figures/RunGardelle_UniformEncoding_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf}

p=8 ($\Delta$ NLL $ = 147$)

\includegraphics[page=4,width=.7\textwidth]{figures/RunGardelle_UniformEncoding_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf}

p=10 ($\Delta$ NLL $ = 145$)

\includegraphics[page=5,width=.7\textwidth]{figures/RunGardelle_UniformEncoding_CosineLoss_VIZ_ACREXP.py_ALL_0_10.0_180.pdf}
	\end{center}



\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Model fitting:
python3 RunGardelle_UniformEncoding_Zero.py 0 {0-10} 10.0 180
python3 RunGardelle_UniformEncoding_CosineLoss.py {2,4,6,8} {0-10} 10.0 180

Reproduced with:
python3 RunGardelle_UniformEncoding_Zero_VIZ.py 0 0 10.0 180
python3 RunGardelle_UniformEncoding_CosineLoss_VIZ.py {2,4,6,8} 0 10.0 180
\end{Verbatim}
\fi

\caption{Orientation perception \citep[data collected by][]{deGardelle2010AnOI}: uniform encoding, cosine-based implementation of $L^p$ loss. 
\textit{Note: It may be surprising that almost zero bias is fitted at high exponents. We verified that the fitted flat prior is not due to the presence of regularization: The model simply has no way of accounting for both bias and variability if encoding is uniform.}
}\label{fig:gardelle-uniencoding}
\end{figure}




\begin{figure}
\centering


\includegraphics[width=.8\textwidth]{../mainPaper_figures/figure2c_gardelle_720.pdf}


\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Model fitting:
python3 RunGardelle_NaturalPrior_CosineLoss.py 8 0 20.0 1800
python3 RunGardelle_UniformPrior_CosineLoss.py 8 0 20.0 1800


Reproduced with:
python3 RunGardelle_NaturalPrior_CosineLoss_VIZ_OnlyModel.py 8 0 20.0 1800
python3 RunGardelle_UniformPrior_CosineLoss_VIZ_OnlyModel.py 8 0 20.0 1800
python3 RunGardelle_NaturalPrior_CosineLoss_VIZ2_OnlyVar.py {2,4,6,8} 0 20.0 1800
cd mainPaper_figures
pdflatex figure2c_gardelle_720.tex
\end{Verbatim}
\fi


	\caption{Orientation perception \citep[data collected by][]{deGardelle2010AnOI}: Effect of the size of the discretization grid, at the example of orientation perception: Results corresponding to Main Text, Figure 3 A, with a discretized grid of size 1800 instead of 180. The regularization weight $\lambda$ (Equation~\ref{eq:regularization}) was adapted to the grid size, every other aspect of the fitting produce was unchanged.
Results closely match those reported in the Main Text.
For the human data, means and bootstrapped 95\% confidence bands were plotted. 
}\label{fig:Gardelle-720}
\end{figure}





\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont

\includegraphics[width=.5\textwidth]{{figures/evaluateCrossValidationResults_Gardelle_180_Extra.py}_RelativeLF.pdf}

\definecolor{pltPurple}{rgb}{0.5019607843137255, 0.0, 0.5019607843137255}
\definecolor{pltRed}{rgb}{1.0, 0.0, 0.0}
\definecolor{pltGreen}{rgb}{0.0, 0.5019607843137255, 0.0}
\definecolor{pltOrange}{rgb}{1.0, 0.6470588235294118, 0.0}
\definecolor{pltBlue}{rgb}{0.0, 0.0, 1.0}


{\huge\textcolor{pltGreen}{---}} Freely fitted prior

{\huge\textcolor{pltPurple}{---}} Naturalistic prior

{\huge\textcolor{pltRed}{---}} Uniform prior

{\huge\textcolor{pltBlue}{---}} Uniform encoding

{\huge\textcolor{yellow}{---}} Power-law link between encoding and prior ($p_{prior} \propto \FI^q$)

{\huge\textcolor{pink}{---}} Matched encoding and prior ($p_{prior} \propto \sqrt{\FI}$)
	\end{center}

\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Reproduced with:
python3 evaluateCrossValidationResults_Gardelle_180_Extra.py
\end{Verbatim}
\fi


\caption{Orientation perception \citep[data collected by][]{deGardelle2010AnOI}: Follow-up analyses for orientation perception (compare Figure~\ref{fig:nll-Gardelle}), with cosine-based loss and restricting to $p>0$.
Data are presented as mean values with standard errors across $n=10$ folds. Note that error bars are imperceptible in some model variants.
When all model components are freely fitted, the fitting procedure identifies encoding resource allocation peaking at the cardinals in agreement with prior work, and an approximately uniform prior, unlike assumed in previous models \citep{Girshick2011CardinalRV,Wei2015ABO}.
However, this does not rule out that priors more in line with prior work, i.e., with a shape analogous to the encoding, could fit the data similarly well.
To test this, we fitted two additional models:
First, we considered a model where prior and encoding are directly matched (pink), which subsumes the prior and encoding assumed by \citep{Girshick2011CardinalRV,Wei2015ABO}; this one achieves poor quantitative fit.
Second, we fitted a model where prior and encoding are linked by a power law \citep{DBLP:conf/nips/MoraisP18}. This model achieves near-optimal fit (yellow); however, the resulting fit closely agrees with that obtained when both prior and encoding are fitted independently shown in Figure~\ref{fig:gardelle-free}: $q$ is fitted to be \emph{negative} at $p=2,4$, and approximately zero for higher exponents.
In conclusion, the data are better explained by a model with a uniform prior than by models where both prior and encoding have similar shapes, peaking at the cardinals, assumed in prior work.
}\label{fig:nll-Gardelle-followup}
\end{figure}


