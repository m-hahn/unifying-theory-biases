
\paragraph*{Details}
Within the relevant trials, the dataset has two contrast levels, for which we fitted different sensory noise parameters.
In visualizations, we followed the original paper in pooling the two contrast levels.
The resulting fit assigns higher sensory variance to the lower contrast level.


The exact experimental prior consists of $\approx$ 5\% uniform distribution and $\approx$ 95\% a discrete distribution supported at discrete steps in intervals of 16 $^\circ$  around the central direction, with densities of 0\% at $\pm$ 80 $^\circ$, $\approx 2.5\%$ at $\pm$ 64 $^\circ$, $\approx 5.8\%$ at $\pm$ 48 $^\circ$, $\approx 34.5\%$ at $\pm$ 32 $^\circ$, $\approx 5.8\%$ at $\pm$ 16 $^\circ$, and $\approx 2.5\%$ at $\pm$ 0 $^\circ$, where $0^\circ$ is the central direction.
We interpolated this distribution linearly for representing the stimulus distribution in Main Text Figure 4b--d and in Figure~\ref{fig:stylized-exact-prior}.



\paragraph*{Results}
Table~\ref{fig:nll-Gekas} shows model performance as a function of loss function.




\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont

	\includegraphics[width=.45\textwidth]{{figures/evaluateCrossValidationResults_Gekas_Fixed.py}_RelativeLF.pdf}

	\definecolor{pltPurple}{rgb}{0.5019607843137255, 0.0, 0.5019607843137255}
	\definecolor{pltRed}{rgb}{1.0, 0.0, 0.0}
	\definecolor{pltGreen}{rgb}{0.0, 0.5019607843137255, 0.0}
	\definecolor{pltOrange}{rgb}{1.0, 0.6470588235294118, 0.0}
	\definecolor{pltBlue}{rgb}{0.0, 0.0, 1.0}


	{\huge	\textcolor{pltGreen}{---}} Freely fitted prior (Figure~\ref{fit:gekas-free})


	{\huge	\textcolor{pltRed}{---}} Uniform prior (Figure~\ref{fit:gekas-uniprior})

	{\huge	\textcolor{pltBlue}{---}} Uniform encoding (Figure~\ref{fit:gekas-uniencoding})
	\end{center}

\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Reproduced with:
python3 evaluateCrossValidationResults_Gekas_Fixed.py
\end{Verbatim}
\fi

	\caption{Perception of motion direction  \citep[data collected by][]{gekas2013complexityas}: Heldout negative Log-Likelihood compared to the model reported in Main Text, Figure 4, as a function of the loss function exponent (lower is better).
Due to computational cost of fitting per-subject parameters, the model was only run on one fold.
While the MAP estimator cannot fit the data, larger exponents all provide similar fit.
Assuming a uniform prior only slightly decreases the quality of model fit, whereas assuming a uniform encoding (as in the modeling of \citet{Chalk2010RapidlyLS,gekas2013complexityas}) decreases it strongly. 
}\label{fig:nll-Gekas}
\end{figure}



\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont



        \begin{minipage}[b]{0.48\linewidth}
		\centering
$p=0$ ($\Delta NLL = 42$)

	\includegraphics[width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_MAP_ELBO_VIZ.py_0_0_1.0_180.pdf}

$p=4$ ($\Delta NLL = 0$)

\includegraphics[page=2,width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_ACREXP.py_ALL_0_1.0_180_50.pdf}

$p=8$ ($\Delta NLL = 6$)

\includegraphics[page=4,width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_ACREXP.py_ALL_0_1.0_180_50.pdf}

	\end{minipage}
        \begin{minipage}[b]{0.48\linewidth}
		\centering
	$p=2$ ($\Delta NLL = 6$)

\includegraphics[width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_ACREXP.py_ALL_0_1.0_180_50.pdf}


$p=6$ ($\Delta NLL = 2$)

\includegraphics[page=3,width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_ACREXP.py_ALL_0_1.0_180_50.pdf}


$p=10$ ($\Delta NLL = 13$)

\includegraphics[page=5,width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_ACREXP.py_ALL_0_1.0_180_50.pdf}
	\end{minipage}

	\end{center}

\ifshowcode
        \begin{Verbatim}[fontsize=\tiny]
Model fitted using:
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_MAP_ELBO.py 0 0 1.0 180
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO.py {2,4,6,8,10} 0 1.0 180 50

Figure produced using:
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_MAP_ELBO_VIZ.py 0 0 1.0 180
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2.py {2,4,6,8,10} 0 1.0 180 50
        \end{Verbatim}
\fi

	\caption{Perception of motion direction  \citep[data collected by][]{gekas2013complexityas}: Results corresponding to Main Text, Figure 4 A--C, across loss function exponents. A very similar qualitative pattern is predicted for all nonzero exponents.
	As described in the main text, the central direction was transformed into $[90^\circ, 135^\circ]$ for each subject; see Figure~\ref{fit:gekas-notransfo} for identified priors per subject without this transformation.
For the measured biases, we plot means (dashed) with error bands indicating standard errors over subjects.
	}
	\label{fit:gekas-free}
\end{figure}









\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont

        \begin{minipage}[b]{0.48\linewidth}
		\centering
$p=0$ ($\Delta NLL = 62$)

\includegraphics[width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_MAP_ELBO_VIZ.py_0_0_1.0_180.pdf}

$p=4$ ($\Delta NLL = 5$)

\includegraphics[page=2,width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_ELBO_VIZ_ACREXP.py_ALL_0_1.0_180_50.pdf}

$p=8$ ($\Delta NLL = 6$)

\includegraphics[page=4,width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_ELBO_VIZ_ACREXP.py_ALL_0_1.0_180_50.pdf}

	\end{minipage}
        \begin{minipage}[b]{0.48\linewidth}
		\centering
	$p=2$ ($\Delta NLL = 11$)

\includegraphics[page=1,width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_ELBO_VIZ_ACREXP.py_ALL_0_1.0_180_50.pdf}


$p=6$ ($\Delta NLL = 6$)

\includegraphics[page=3,width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_ELBO_VIZ_ACREXP.py_ALL_0_1.0_180_50.pdf}


$p=10$ ($\Delta NLL = 14$)

\includegraphics[page=5,width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_ELBO_VIZ_ACREXP.py_ALL_0_1.0_180_50.pdf}
	\end{minipage}
	\end{center}


	\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Model fitted using:
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_MAP_ELBO.py 0 0 1.0 180 50
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_ELBO.py $p 0 1.0 180 50

Reproduced with:
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_MAP_ELBO_VIZ.py 0 0 1.0 180 50
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformPrior_ELBO_VIZ.py $p 0 1.0 180 50
\end{Verbatim}
\fi

	\caption{Perception of motion direction  \citep[data collected by][]{gekas2013complexityas}: Results corresponding to Figure~\ref{fit:gekas-free}, with the fixed-effects component of the prior ($\beta(\theta)$ in Equation \ref{eq:mixed-effects-prior}) constrained to be zero. That is, nonuniformity in the prior is entirely relegated to per-participant adjustments.
	Results are similar to the full model (Figure~\ref{fit:gekas-free}), as biases are dominated by likelihood repulsion.
For the measured biases, we plot means (dashed) with error bands indicating standard errors over subjects.
%The model can fit the data almost as well as the full model; forcing the fixed-effects component of the prior to be zero forces the model to assume a somwehat flatter resource allocation in the third row, in order to account for the difference in bias magnitude between the first and third rows.
	}
	\label{fit:gekas-uniprior}
\end{figure}





\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont

        \begin{minipage}[b]{0.48\linewidth}
		\centering
$p=0$ ($\Delta NLL = 61$)

\includegraphics[width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_MAP_ELBO_VIZ.py_0_0_1.0_180.pdf}

$p=4$ ($\Delta NLL = 70$)

\includegraphics[page=2,width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_ELBO_VIZ_ACREXP.py_ALL_0_1.0_180_50.pdf}

$p=8$ ($\Delta NLL = 80$)

\includegraphics[page=4,width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_ELBO_VIZ_ACREXP.py_ALL_0_1.0_180_50.pdf}

	\end{minipage}
        \begin{minipage}[b]{0.48\linewidth}
		\centering
	$p=2$ ($\Delta NLL = 67$)

\includegraphics[page=1,width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_ELBO_VIZ_ACREXP.py_ALL_0_1.0_180_50.pdf}


$p=6$ ($\Delta NLL = 75$)

\includegraphics[page=3,width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_ELBO_VIZ_ACREXP.py_ALL_0_1.0_180_50.pdf}


$p=10$ ($\Delta NLL = 78$)

\includegraphics[page=5,width=0.95\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_ELBO_VIZ_ACREXP.py_ALL_0_1.0_180_50.pdf}
	\end{minipage}
	\end{center}

\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Model fitted using:
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_MAP_ELBO.py 0 0 1.0 180 50
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_ELBO.py $p 0 1.0 180 50

Reproduced with:
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_MAP_ELBO_VIZ.py 0 0 1.0 180 50
python3 RunGekas_CosinePLoss_ShiftedPrior_Fourier_UniformEncoding_ELBO_VIZ.py $p 0 1.0 180 50
\end{Verbatim}
\fi

	\caption{Perception of motion direction  \citep[data collected by][]{gekas2013complexityas}: Results corresponding to Figure~\ref{fit:gekas-free}, with the fixed-effects component of the encoding ($\alpha(\theta)$ in Equation \ref{eq:mixed-effects-resources}) constrained to be zero. That is, nonuniformity in the resource allocation is entirely relegated to per-participant adjustments.
	Per-participants adjustments broadly recover the oblique effect; the prior is also fitted consistently. This model can be viewed as an instantiation of the modeling considered in \citet{gekas2013complexityas}  but with participant-specific adjustments. Those adjustments highlight the importance of the nonuniform resource allocation, and explain why the modeling of \citet{gekas2013complexityas} did not match the qualitative pattern in the data well.
For the measured biases, we plot means (dashed) with error bands indicating standard errors over subjects.
}
	\label{fit:gekas-uniencoding}
\end{figure}



\begin{figure}
	\begin{center}
\fontfamily{phv}\selectfont

A.	Resources

\includegraphics[width=0.75\textwidth]{figures/RunGekas_CosinePLoss_NoShiftedPrior_Fourier_ELBO_VIZ_Encoding_MainPaper.py_4_0_1.0_180.pdf}

B. Priors by Participant

\includegraphics[width=0.9\textwidth]{figures/RunGekas_CosinePLoss_NoShiftedPrior_Fourier_ELBO_VIZ_Priors.py_4_0_1.0_180.pdf}
	\end{center}

	\ifshowcode
\begin{Verbatim}[fontsize=\tiny]
Model fitted using:
python3 RunGekas_CosinePLoss_NoShiftedPrior_Fourier_ELBO.py 4 0 1.0 180 50

Reproduced with:
python3 RunGekas_CosinePLoss_NoShiftedPrior_Fourier_ELBO_VIZ_Encoding_MainPaper.py 4 0 1.0 180 50
python3 RunGekas_CosinePLoss_NoShiftedPrior_Fourier_ELBO_VIZ_Priors.py 4 0 1.0 180 50
\end{Verbatim}
\fi


	\label{fit:gekas-notransfo}
	\caption{Perception of motion direction  \citep[data collected by][]{gekas2013complexityas}: Fitted model for perception of motion direction, but without transforming central directions into $[90^\circ, 135^\circ]$. Participants are grouped based on distance to cardinal and oblique directions as in Main text, Figure 4. Top: By pooling across participants, the model identifies peaks in encoding resource allocation at the cardinal directions (same as Main text, Figure 4a; in accordance with documented decreased discriminability around the oblique directions~\citep{Gros1998,Dakin2005}). Bottom: Fitted prior for each of the 36 subjects. Arrows indicate the central direction for each participant. A bimodal prior is fitted for most participants.}


\end{figure}







\begin{figure}
\centering
\includegraphics[width=0.85\textwidth]{figures/RunGekas_CosinePLoss_ShiftedPrior_Fourier_ELBO_VIZ2_Stylized_ExactPrior.py_4_0_1.0_180.pdf}

\caption{Perception of motion direction  \citep[data collected by][]{gekas2013complexityas}: Stylized predictions (compare Main Text, Figure 4e) for priors exactly matching the stimulus distributions. If the behavioral prior were to reflect the exact stimulus distribution, the repulsive bias away from the cardinals would be modulated more strongly by prior attaction, though the overall qualitative pattern would remain the same.}\label{fig:stylized-exact-prior}
\end{figure}


