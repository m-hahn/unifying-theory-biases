
%\chapter{Further Details and Results for Analyzed Datasets}
\clearpage
\subsection{Details and Results for Analyzed Datasets}\label{sec:details-datasets}

%\begin{table}
%\centering
%
%\begin{tabular}{c|cccccccccccccccccc}
%         & Sensory Noise & Stimulus Noise & Prior Conditions\\ \hline
%Gardelle & 5 duration conditions$^\dagger$ \\
%Tomassini & 3 duration conditions$^+$ & 2 conditions \\
%Gekas & 2 contrast conditions$^+$ & & 36 (per subject)\\
%Remington & 1 condition \\
%Xiang & 2 duration conditions  & & XXX (per block)\\
%Bae & 2$^+$ (delayed or undelayed) \\
%\end{tabular}
%
%$^\dagger$ Shortest duration not plotted due to difficulty obtaining kernel estimates
%
%$^*$ not plotted due to data sparsity
%
%$^+$ see Figures XXX for fits by condition
%
%\caption{Summary of condition manipulations in the five datasets. All information is also provided in Materials\&Methods.}
%\end{table}


%%%%%%%%%%%%%%%%%% GARDELLE
%\clearpage


\subsubsection{Orientation Perception}
\paragraph{Orientation Perception (\citet{deGardelle2010AnOI})}
\input{si-gardelle} % 432KB

%%%%%%%%%%%%%%%%%%  TOMASSSINI
\clearpage\paragraph{Stimulus Noise in Orientation Perception (\citet{Tomassini2010OrientationUR})}
\input{si-tomassini} % 320KB

%%%%%%%%%%%%%%%%%%  GEKAS
\clearpage
\subsubsection{Motion Direction (\citet{gekas2013complexityas})}
\input{si-gekas} % 804KB


%%%%%%%%%%%%%%%%%% Xiang
\clearpage
\subsubsection{Magnitude Estimation}


\paragraph{General Notes}
\paragraph*{Priors}
We compared the following priors based on prior work.
A uniform prior on an interval is assumed in the Bayesian models of \citet{Jazayeri2010TemporalCC,Remington2018LateBI}.
The model of \citet{Petzschner2011IterativeBE} represents a Kalman filter in the \emph{sensory} space; the history from previous trials is effectively summarized into a normal prior in sensory space.
Similarly, the model of \citet{cicchini2014compressive} resembles a Kalman filter in \emph{stimulus} space; it effectively summarizes the history from previous trials into a normal prior in stimulus space, whose mean is a weighted average of the preceding stimuli.

\paragraph*{Role of Repulsion}
It is perhaps surprising that our modeling indicates a repulsive bias component towards higher magnitudes, even though magnitude perception often shows underestimation biases, and overestimation biases are rarely directly observed \citep[e.g. perception of temporal frequency in Figure 2B in][]{Vintch2014CorticalCO}.
Indeed, such a repulsive bias is a necessary consequence of Bayesian estimation models when Weber's law holds and $p>0$, and is correspondingly implicit in prior Bayesian models, though it may be hard to directly detect due to the presence of a large central tendency effect and/or a prior favoring smaller magnitudes, which can cancel or outweigh the repulsive component.
For instance, \citet{Petzschner2011IterativeBE} fitted a ``shift term'' in the estimate in their Bayesian model; this shift term plays a role corresponding to the loss-dependent likelihood repulsion.
Fitted values of the shift term indicate a nonzero (positive) repulsive bias (see below).
Relatedly, the posterior mean estimator used, for instance, by \citet{Jazayeri2010TemporalCC} necessarily involves some nonzero amount of likelihood repulsion.

We note that there are models that do not involve likelihood repulsion because they are not strictly Bayesian models. The model of \citet{cicchini2014compressive}, which is motivated as a Bayesian integration model akin to the Kalman filter, does not involve a repulsive bias component; it only shows attraction to a weighted average of previous stimuli. Indeed, the assumptions of the standard Kalman filter are violated by the stimulus-dependence of noise in magnitude estimation (their Equation 5). Exact Bayesian inference for such a model may perhaps be carried out with a suitable nonlinear Kalman filter \citep{Spinello2010NonlinearEW} and would give rise to a repulsive bias component in addition to the central tendency effect, similar to Equation~\ref{eq:petzschner-bias}. Investigating such a fully Bayesian model and comparing it to the model described by \citet{cicchini2014compressive} is an interesting problem for further research on numerosity perception. Another model, involving only an underestimation bias, is presented by \citet{Cheyette2020AUA}; this model differs from other proposals in that it optimizes the psychophysical mapping from $\theta$ to $\widehat{\theta}$ ``end-to-end'' to minimize the average $L^2$ loss subject to constraints on KL divergence from the prior, and does not involve Bayesian inference over uncertain sensory input \citep{Cheyette2021ThePO}.
Bayesian variants of this model would need to include likelihood repulsion; comparing such variants with the model of \cite{Cheyette2020AUA} is likewise an interesting problem for future research on numerosity perception.


\paragraph*{Role of Repulsion in the model of \citet{Petzschner2011IterativeBE}}
Here, we elaborate the role of likelihood repulsion implicit in the Bayesian model of magnitude perception by \citet{Petzschner2011IterativeBE}.
In particular, we show that the ``shift term'' they fitted corresponds to loss-dependent likelihood repulsion.
The model of \citet{Petzschner2011IterativeBE} implements Weber's law by assuming $F(\theta) = \log(\theta)$. The likelihood and the posterior are both lognormal. Given parameters $\mu$, $\sigma_{post}$ of the posterior, \cite{Petzschner2011IterativeBE} assume the following functional form for the estimate $\widehat{\theta}$: 
\begin{equation}
	\widehat{\theta} = \exp(\mu+\Delta x) \cdot d_0
\end{equation}
($d_0$ is a proportionality coefficient with units of distance or angles), where $\Delta x=0$ at $p=1$, $\Delta x=\sigma_{post}^2/2$ at $p=2$, and $\Delta x = -\sigma_{post}^2$ at $p=0$, by standard facts about lognormal distributions.
Further, by Gaussianity of prior and likelihood in sensory space, $\mu = w \cdot (F(\theta) + \delta ) + (1-w) \cdot \mu_{prior}$, where the weight $w$ only depends on the relative widths of prior and likelihood.
With this in mind, we can derive the bias of the estimate in the model of \cite{Petzschner2011IterativeBE} as follows (omitting the proportionality unit $d_0$)
\begin{align*}
	\mathbb{E}_{\delta \sim \mathcal{N}(0, \sigma_{Lik}^2)}\left[\exp(w (F(\theta) + \delta ) + (1-w) \mu_{prior}+\Delta x)\right] - \theta = &
	  \mathbb{E}_{\delta \sim \mathcal{N}(0, w^2\sigma_{Lik}^2)}[\exp(\delta )] \cdot \exp(w\log\theta+ (1-w) \mu_{prior}+\Delta x) - \theta \\
	=&	 \exp(w \log \theta + \frac{w^2\sigma_{Lik}^2}{2} + (1-w) \mu_{prior}+\Delta x) - \theta
\end{align*}
To lowest order in $t := \sigma_{Lik}^2$, this is ($w = \frac{1/\sigma^2}{1/\sigma^2+1/\sigma_{prior}^2} = 1 - \frac{t}{\sigma_{prior}^2} + O(t^2)$; $\sigma_{post} = \frac{t \sigma_{prior}^2}{t+\sigma_{prior}^2} = t + O(t^2)$) by differentiating w.r.t. $t$ at $t=0$:
\begin{align}\label{eq:petzschner-bias}
	Bias = 	 \underbrace{\sigma_{Lik}^2	\cdot \theta\cdot \left[\frac{(\mu_{prior} - \log\theta)}{\sigma_{prior}^2} - 1\right] }_{\text{Prior Attraction}}+ \underbrace{\sigma_{Lik}^2    \cdot \theta\cdot \left(\frac{3}{2} +  \frac{\Delta x}{\sigma_{post}^2}\right)}_{\text{Likelihood Repulsion}} + O(\sigma_{Lik}^4)
\end{align}
This result agrees with the result obtained by directly applying our Theorem 1 to the model of \cite{Petzschner2011IterativeBE}, plugging in the lognormal prior and the encoding assumed by \cite{Petzschner2011IterativeBE} ($\FI = \frac{1}{\theta^2\sigma_{lik}^2}$), here $p>0$ (similarly for $p=0$): 
\begin{align}
	\frac{1}{\FI} (\log p_{prior})' + \frac{p+2}{4} \left(\frac{1}{\FI}\right)'	=&	\frac{1}{\FI} \deriv{\theta}\left[\log \frac{1}{\theta} \exp(-\frac{(\log\theta-\mu_{prior})^2}{2\sigma_{lik}^2})\right] + \frac{p+2}{4} \left(\frac{1}{\FI}\right)'\\
	= &	\sigma_{Lik}^2	\cdot \theta\cdot \left[\frac{(\mu_{prior} - \log\theta)}{\sigma_{prior}^2} - 1\right] + \sigma_{lik}^2 \cdot \theta\cdot \frac{p+2}{2} 
\end{align}
which matches~(\ref{eq:petzschner-bias}) under the identification $\Delta x = \sigma_{post}^2 \cdot \left(\frac{p-1}{2}\right)$ (for $p>0$, similarly for $p=0$).
The likelihood repulsion term in (\ref{eq:petzschner-bias}) is always positive, even when $p=0$ ($\Delta x = -\sigma_{post}^2$). 
\citet{Petzschner2011IterativeBE} made $\Delta x$ a free parameter fitted together with the rest of the model; the fitted value was positive on average across subjects, indicating a loss function exponent $> 1$, and a corresponding repulsive bias.
Note, however, that just as in our model fits (Figure 5 in the main text), the overall bias may still be predominantly underestimating because of the shape of the prior; in (\ref{eq:petzschner-bias}), prior attraction results in underestimation even when $\log \theta = \mu_{prior}$.




\clearpage\paragraph{Numerosity Estimation (\citet{Xiang2021ConfidenceAC})}\label{sec:si-xiang}
\input{si-xiang} % 424KB

%%%%%%%%%%%%%%%%%% REMINGTON
\clearpage\paragraph{Time Interval Estimation (\citet{Remington2018LateBI})}\label{sec:si-remington}
\input{si-remington} % 116KB


%%%%%%%%%%%%%%%%%% BAE
\clearpage
\subsubsection{Color Perception (\citet{Bae2015WhySC})}\label{sec:bae}
\input{si-bae} % 1368KB --> can be compressed


