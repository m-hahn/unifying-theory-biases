# A unifying theory explains seemingly contradicting biases in perceptual estimation

This repository contains code and intermediate results for the manuscript.

* [code](code/)
* [SI Appendix](writeup/)
* [Main text figures](code/mainPaper_figures)

All plots are derived from PDFs in [code/figures/](code/figures/). See README files for details. You can also refer to the source code of the SI.
